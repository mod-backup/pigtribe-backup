GLOBAL.setmetatable(env, { __index = function(t, k) return GLOBAL.rawget(GLOBAL, k) end })

PrefabFiles = {
    "ptribe_pigelitefighter",
    "ptribe_fxs",
    "ptribe_fused_shadeling_bomb",
    "ptribe_carnival_prizebooth",
    "ptribe_upgrade",
    "ptribe_pigtorch",
    "ptribe_tribeblueprints",
    "ptribe_wildpig",

    -- 哈姆雷特移植
    "oinc",
    "deco_roomglow",
    "deco_lightglow",
    "shelf_slot",       --展柜槽
    "shelf",            --展柜
    "deco_chair",       --板凳
    "rug",              --地毯
    "deco_lamp",        --灯
    "deco_plantholder", --盆栽
    "deco",             --一堆装饰
    "deco_beam",
    "deco_windows",
    "city_lamp",           --灯
    "lawnornaments",
    "deco_table",          --桌子
    "deco_wall_ornament",
    "deco_swinging_light", --吊灯
    "hedge",               --树篱
    "topiary",             --草雕像
    "deco_academy",
    "deco_antiquities",
    "deco_florist",
    "interior_texture_packages",
    "deco_ruins_fountain",       --许愿井
    "wall_tigerpond",
    "hamlet_door",               --门
    "playerhouse_city_interior", --房屋内部
    "playerhouse_city",          --房屋
    "deed",                      --房产
    "exterior_texture_packages", --房屋套件
    "construction_permit",
    "deco_placers",
    "waterdrop",
    "floweroflife",
    "pugalisk_fountain",
    "ptribe_hats",
    "pigbanditexit",
    "pigbandit",
    "silvernecklace",
    "pigman_city",
    "pig_guard_tower",
    "halberd",
    "ptribe_wilbadaywalker",

    -- 海难移植
    -- "ptribe_snakeskin",  --蛇皮，先不做
    "ptribe_snake", --蛇
    "ptribe_planted_tree",
    "ptribe_jungletreeseed",
    "ptribe_jungletrees", --雨林树
    "ptribe_wildbore",    --野猪
    "ptribe_telescope",   -- 望远镜
    "ptribe_pigfactorys", --工厂
}

Assets = {
    Asset("ATLAS", "images/ptribe_pigtaskbg.xml"),                        --任务面板背景
    Asset("ATLAS", "images/inventoryimages/hamletinventoryimages.xml"),   --物品图标
    Asset("ATLAS", "images/inventoryimages/hamletinventoryimages_2.xml"), --物品图标
    Asset("ATLAS", "images/minimap/hamletminimap_data.xml"),              --小地图图标
    Asset("ATLAS", "images/hamlethud.xml"),                               --ui
    Asset("ATLAS", "images/inventoryimages/python_fountain.xml"),
    Asset("ATLAS", "images/inventoryimages/ptribe_pigtorch.xml"),
    Asset("ATLAS", "images/ptribe_pigweaponfactory_slot.xml"),

    Asset("ANIM", "anim/pigman_tribe.zip"),

    Asset("SOUNDPACKAGE", "sound/dontstarve_DLC003.fev"),
    Asset("SOUND", "sound/DLC003_sfx.fsb"),

    -- 猪镇房屋，将其和猪人房皮肤合一起
    Asset("ANIM", "anim/pig_townhouse1.zip"),
    Asset("ANIM", "anim/pig_townhouse5.zip"),
    Asset("ANIM", "anim/pig_townhouse6.zip"),
    Asset("ANIM", "anim/pig_townhouse1_pink_build.zip"),
    Asset("ANIM", "anim/pig_townhouse1_green_build.zip"),
    Asset("ANIM", "anim/pig_townhouse1_brown_build.zip"),
    Asset("ANIM", "anim/pig_townhouse1_white_build.zip"),
    Asset("ANIM", "anim/pig_townhouse5_beige_build.zip"),
    Asset("ANIM", "anim/pig_townhouse6_red_build.zip"),
    Asset("ANIM", "anim/pig_farmhouse_build.zip"),

    Asset("ANIM", "anim/pig_house_tropical.zip"), --野猪房
    Asset("ANIM", "anim/pig_cityhall.zip"),       --猪伯利市镇厅
}

AddMinimapAtlas("images/minimap/hamletminimap_data.xml")

----------------------------------------------------------------------------------------------------
modimport "modmain/language" -- 语言
modimport "modmain/tuning"
modimport "modmain/actions"
modimport "modmain/containers"
modimport "modmain/componentactions"
modimport "modmain/character"    -- 新角色
modimport "modmain/recipes"
modimport "modmain/pigbrainpost" --猪人brain，我本来是覆盖pigbrain文件的，但是没想到其他mod一个AddClassPostConstruct我的文件就失效了预制体相关的修改，内容多的单独一个文件
modimport "modmain/rpc"
modimport "modmain/playernet"    -- 玩家网络通讯
modimport "modmain/prefabpost"
modimport "modmain/ui"
modimport "modmain/sg" -- sg
modimport "modmain/modwiki"
