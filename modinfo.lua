local L = locale ~= "zh" and locale ~= "zhr" -- true 英文  false 中文

name = L and "Pigman Tribe" or "猪人部落"
description = L and [[
The game's guidebook has a built-in mod introduction, which is available for everyone to view in the game's guidebook.

1.6.19 Changes:
Fixed several bugs

there are problems welcome creative workshop feedback Blue gallery can only be edited in the code, there are questions or suggestions welcome creative workshop feedback

]] or [[

游戏图鉴内置了mod介绍，欢迎大家在游戏内图鉴查看mod介绍

1.6.19改动：
修复了几个bug

有问题或建议欢迎创意工坊留言反馈
]]

-- Blueprint Compatibility with Base Projection:
-- https://steamcommunity.com/sharedfiles/filedetails/?id=2928652892
-- For methods on editing blueprints and detailed mod descriptions, see:
-- https://n77a3mjegs.feishu.cn/docx/G8wvdfPskoCmI5xOzPHc9drwnHh?from=from_copylink
-- All Pigmen Tasks:
-- https://n77a3mjegs.feishu.cn/docx/GFPSdNDlcoC58NxKraDcsFgznLf?from=from_copylink

-- 蓝图兼容Base Projection (基地投影)：
-- https://steamcommunity.com/sharedfiles/filedetails/?id=2928652892
-- 编辑蓝图方法和mod详细介绍见：
-- https://n77a3mjegs.feishu.cn/docx/G8wvdfPskoCmI5xOzPHc9drwnHh?from=from_copylink
-- 猪人所有任务：
-- https://n77a3mjegs.feishu.cn/docx/GFPSdNDlcoC58NxKraDcsFgznLf?from=from_copylink


author = "绯世行、many、欧阳玉霞"
version = "1.6.19"

forumthread = ""

api_version = 10

priority = -2029316030 - 209631439

dst_compatible = true
dont_starve_compatible = false
all_clients_require_mod = true

icon_atlas = "modicon.xml"
icon = "modicon.tex"

server_filter_tags = {
}

local ON = { description = L and "On" or "开", data = true }
local OFF = { description = L and "Off" or "关", data = false }
function SWITCH()
    return { ON, OFF }
end

local ALPHA = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
    "U", "V", "W", "X", "Y", "Z" }
---生成快捷键选项表
---@param default any 默认值
function KEYSLIST(default, closable)
    local list = { { description = default .. "-default", data = default } }
    if closable then
        list[#list + 1] = OFF
    end

    for i = 1, #ALPHA do
        list[#list + 1] = { description = ALPHA[i], data = ALPHA[i] }
    end
    return list
end

---生成数字选择表
---@param tab any 可选数字
---@param default any 默认值
---@param closable any 是否含有关闭选项
function NUMLIST(tab, default, closable)
    local list = { description = default .. "-default", data = default }
    if closable then
        list[#list + 1] = OFF
    end
    for i = 1, #tab do
        list[#list + 1] = { description = tab[i] .. "", data = tab[i] }
    end
    return list
end

configuration_options = {
    {
        name = "language",
        label = L and "Language" or "语言",
        hover = L and "Set language of this mod." or "设置语言。",
        options = {
            { description = L and "Auto" or "自动", data = "AUTO" },
            { description = "English", data = "ENG" },
            { description = "中文", data = "CHI" },
        },
        default = "AUTO",
    },

    ------------------------------------------------------------------------------------------------
    {
        name = "LuckyGoldFromPigman",
        label = L and "Grant Coin from Village Chiefs and Pigmen" or "村长房和猪人是否会给予呼噜币",
        options = SWITCH(),
        default = false,
    },
    {
        name = "StealPigmanPopertyPunish",
        label = L and "Stealing Piglin Resources Penalty" or "偷窃猪人资源是否有惩罚",
        options = SWITCH(),
        default = true,
    },
    {
        name = "PigmanUsePlayerSkin",
        label = L and "Pigman Architecture uses player skins" or "猪人建筑使用玩家皮肤",
        hover = L and "After opening, the buildings built by the Pigman will randomly use the player's skin"
            or "开启后猪人建造的建筑会随机使用玩家的皮肤",
        options = SWITCH(),
        default = true,
    },
    {
        name = "PigmanUsePlayerSkinProb",
        label = L and "Pigmen Building Skin Chance" or "猪人建筑使用皮肤概率",
        options = NUMLIST({ 0, 0.3, 0.5, 0.7, 1 }, 0.7),
        default = 0.7,
    },
    {
        name = "TribeMinPighouseCount",
        label = L and "Minimum Pig Houses Near Chief's Hut" or "村长房附近的最少猪人房数量",
        hover = L and "Nearby Pigman houses that are less than this value will not become village chief houses"
            or "附近的猪人房小于这个值的猪人房不会成为村长房",
        options = NUMLIST({ 0, 1, 2, 3, 4, 5 }, 0),
        default = 0,
    },
    {
        name = "EnableTribeConflict",
        label = L and "Enable Tribal Conflict Event" or "是否开启部落冲突事件",
        options = SWITCH(),
        default = true,
    },
    ------------------------------------------------------------------------------------------------
    {
        name = "SpecialPigmanGenerationMult",
        label = L and "Enable Special Pigman Generation" or "特殊猪人生成倍率",
        options = NUMLIST({ 0, 0.5, 1, 2, 4, 5, 10 }, 1),
        hover = L and "The higher the value, the more likely it is to generate a special pigman"
            or "值越大越有概率生成特殊猪人",
        default = 1,
    },
    {
        name = "KungFuEmployEffic",
        label = L and "Kungfu pig employment efficiency" or "功夫猪人雇佣效率",
        options = NUMLIST({ 0.5, 1, 2, 3, 5 }, 1),
        default = 1,
    },
    -- {
    --     name = "TerrariaLaserDamageMult",
    --     label = L and "Terraria Pig laser damage ratio" or "泰拉猪人激光攻击倍率",
    --     options = NUMLIST({ 0.5, 1, 2, 3, 5 }, 1),
    --     default = 1,
    -- },
    {
        name = "ArmorDamageLimit",
        label = L and "Armor pig take maximum damage in 5 seconds" or "铁甲猪人5秒受到伤害上限",
        options = NUMLIST({ 150, 300, 500 }, 300),
        default = 300,
    },
    {
        name = "MiteSkillCooldown",
        label = L and "Mite pig skill cooldown" or "爆螨猪人技能冷却时长",
        options = NUMLIST({ 4, 8, 12, 20 }, 8),
        default = 8,
    },
    {
        name = "BlowdartInitAttackPeriod",
        label = L and "Blowdart pig initial attack period" or "吹箭猪人初始攻击间隔",
        options = NUMLIST({ 1, 2, 3, 5, 7, 9 }, 5),
        default = 5,
    },
    {
        name = "PigmanCityUpgradeMult",
        label = L and "Pigman Guardian Upgrade Enhancement Rate" or "猪人守卫升级强化倍率",
        options = NUMLIST({ 0.5, 1, 2, 3 }, 1),
        default = 1,
    },

    ------------------------------------------------------------------------------------------------
    {
        name = "AllowTaskGenerateTile",
        label = L and "Allows the task to generate tile" or "允许任务生成地皮",
        hover = L and "Whether to allow the quest to generate tile, because the quest generated land will not disappear"
            or "是否允许任务生成地皮，因为任务生成的地皮是不会消失的",
        options = SWITCH(),
        default = true,
    },
    {
        name = "TaskDifficultyLevel",
        label = L and "Task difficulty level" or "任务难度级别",
        options = {
            { description = L and "Easy" or "简单", data = 1 },
            { description = L and "Medium" or "中等", data = 2 },
            { description = L and "Hard" or "困难", data = 3 },
        },
        default = 2,
    },
}
