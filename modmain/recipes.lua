local Utils = require("ptribe_utils/utils")
local ResourceConvert = require("ptribe_resourceconvert")
local TechTree = require("techtree")
local ImageUtils = require("ptribe_imageutils")
local InitWork = require("ptribe_utils/initwork")
local BP = require("ptribe_blueprints")
local BluePrintsUtils = require("ptribe_blueprintsutils")
----------------------------------------------------------------------------------------------------

table.insert(TechTree.AVAILABLE_TECH, "PTRIBE_PIG_SHOP")
table.insert(TechTree.AVAILABLE_TECH, "PTRIBE_BLUEPRINT")
table.insert(TechTree.AVAILABLE_TECH, "PTRIBE_HOME")
-- table.insert(TechTree.AVAILABLE_TECH, "CITY")

TECH.PTRIBE_PIG_SHOP = { PTRIBE_PIG_SHOP = 1 }
TECH.PTRIBE_BLUEPRINT = { PTRIBE_BLUEPRINT = 1 }
TECH.PTRIBE_HOME = { PTRIBE_HOME = 1 }
-- TECH.CITY = { CITY = 1 }

TUNING.PROTOTYPER_TREES.PTRIBE_PIG_SHOP = TechTree.Create({
    PTRIBE_PIG_SHOP = 1,
})
TUNING.PROTOTYPER_TREES.PTRIBE_BLUEPRINT = TechTree.Create({
    PTRIBE_BLUEPRINT = 1,
})
TUNING.PROTOTYPER_TREES.PTRIBE_HOME = TechTree.Create({
    PTRIBE_HOME = 1,
})
-- TUNING.PROTOTYPER_TREES.CITY = TechTree.Create({
--     CITY = 1,
-- })

for _, v in pairs(AllRecipes) do
    v.level.PTRIBE_PIG_SHOP = v.level.PTRIBE_PIG_SHOP or 0
    v.level.PTRIBE_BLUEPRINT = v.level.PTRIBE_BLUEPRINT or 0
    v.level.PTRIBE_HOME = v.level.PTRIBE_HOME or 0
    -- v.level.CITY = v.level.CITY or 0
end

RECIPETABS["PTRIBE_PIG_SHOP"] = {
    str = "PTRIBE_PIG_SHOP",
    sort = 100,
    icon = "tab_prizebooth.tex",
    crafting_station = true,
    shop = true,
    icon_atlas = "images/hud2.xml"
}
RECIPETABS["PTRIBE_BLUEPRINT"] = {
    str = "PTRIBE_BLUEPRINT",
    sort = 100,
    icon = "tab_prizebooth.tex",
    crafting_station = true,
    icon_atlas = "images/hud2.xml"
}
RECIPETABS["PTRIBE_HOME"] = {
    str = "PTRIBE_HOME",
    sort = 100,
    icon = "tab_home_decor.tex",
    icon_atlas = "images/hamlethud.xml",
    crafting_station = true,
}
-- RECIPETABS["CITY"] = {
--     str = "CITY",
--     sort = 12,
--     icon = "tab_city.tex",
--     icon_atlas = "images/hamlethud.xml",
--     crafting_station = true
-- }

AddPrototyperDef("ptribe_carnival_prizebooth", {
    icon_atlas = CRAFTING_ICONS_ATLAS,
    icon_image = "station_prizebooth.tex",
    is_crafting_station = true,
    action_str = "TRADE",
    filter_text = STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_PIG_SHOP
})
AddPrototyperDef("pigking", {
    icon_atlas = "images/hamlethud.xml",
    icon_image = "tab_home_decor.tex",
    is_crafting_station = true,
    action_str = "TRADE",
    filter_text = STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_BLUEPRINT
})
AddPrototyperDef("wallrenovation", {
    icon_atlas = "images/hamlethud.xml",
    icon_image = "tab_home_decor.tex",
    is_crafting_station = true,
    filter_text = STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_HOME
})
-- AddPrototyperDef("playerhouse_city", {
--     icon_atlas = "images/hamlethud.xml",
--     icon_image = "tab_city.tex",
--     is_crafting_station = true,
--     filter_text = STRINGS.UI.CRAFTING_STATION_FILTERS.CITY
-- })

----------------------------------------------------------------------------------------------------
-- 猪人商店
local prefix = "pigshop_"
AddClassPostConstruct("components/builder_replica", function(self)
    Utils.FnDecorator(self, "CanLearn", function(self, recipename)
        if self.classified then
            local p = self.classified.current_prototyper:value()
            -- print("判断", recipename, p and p.prefab, p and p.goods)
            local start_pos = string.find(recipename, prefix)
            if p and p.prefab == "ptribe_carnival_prizebooth" and start_pos then
                local name = string.sub(recipename, start_pos + string.len(prefix))
                if ResourceConvert.IsGoods(name) and p.goods then
                    return { p.goods[name] }, true
                else
                    -- 非出售的商品隐藏
                    return { false }, true
                end
            end
        end
    end)
end)

local OINC_RECIPES = {} --每个配方的呼噜币消耗
local function OincIngredient(recipename, val, id)
    OINC_RECIPES[recipename] = val
    return Ingredient("oinc", val, "images/inventoryimages2.xml", nil, "quagmire_coin" .. (id or 1) .. ".tex")
end

for name, val in pairs(ResourceConvert.GetAllGoods()) do
    -- 判断是否是蓝图
    if name:sub(-10) == "_blueprint" then
        local oriName = string.upper(name:sub(1, -10 - 1))
        STRINGS.NAMES[string.upper(name)] = (STRINGS.NAMES[oriName] or "") .. (L and " Blueprint" or "蓝图")
        local desc = STRINGS.CHARACTERS.GENERIC.DESCRIBE[string.upper(oriName)]
        if type(desc) == "table" then
            local _, v = next(desc) -- 可能是键值表，随便拿一个
            desc = v
        end
        STRINGS.CHARACTERS.GENERIC.DESCRIBE[string.upper(name)] = desc
    end

    local recName = "pigshop_" .. name --配方名不能和原版一样，会冲突
    STRINGS.NAMES[string.upper(recName)] = STRINGS.NAMES[string.upper(name)]
    local desc = STRINGS.CHARACTERS.GENERIC.DESCRIBE[string.upper(name)]
    STRINGS.RECIPE_DESC[string.upper(recName)] = type(desc) == "table" and next(desc) or desc

    local image = ImageUtils.GetItemImage(name)

    AddRecipe2(recName, { OincIngredient(recName, val) }, TECH.PTRIBE_PIG_SHOP, {
        nounlock = true,
        no_deconstruction = true,
        actionstr = "CARNIVAL_PRIZESHOP", --购买
        sg_state = "give",
        description = recName,            --对于配方的描述
        numtogive = 1,
        product = name,
        atlas = image and image.atlas,
        image = image and image.image or (name .. ".tex")
    })
    OINC_RECIPES[recName] = val
end

----------------------------------------------------------------------------------------------------
-- 翻新
local RECNAMES = {}
local function AddHamletRecipe(name, tech, cost, placer, image, data)
    data = data or {}

    if not STRINGS.RECIPE_DESC[string.upper(name)] then
        local desc = STRINGS.CHARACTERS.GENERIC.DESCRIBE[string.upper(name)]
        STRINGS.RECIPE_DESC[string.upper(name)] = type(desc) == "table" and next(desc) or desc
    end

    local ingredients = {}
    if cost and cost > 0 then
        table.insert(ingredients, OincIngredient(ingredients, cost))
        OINC_RECIPES[name] = cost
    end
    if data.ingredients then
        ConcatArrays(ingredients, data.ingredients)
    end

    AddRecipe2(name, ingredients, tech, {
        nounlock = data.nounlock,
        no_deconstruction = true,
        description = name,
        placer = placer,
        builder_tag = data.builder_tag,
        min_spacing = data.min_spacing, --最小建造间距
        atlas = data.atlas or "images/inventoryimages/hamletinventoryimages.xml",
        image = image and (image .. ".tex") or nil
    }, data.filters)
    RECNAMES[name] = true
end

local function AddHomeRecipe(name, cost, placer, image, data)
    data = data or {}
    data.nounlock = true
    data.builder_tag = "ptribe_inhouse" --这个科技栏不支持placer的存在，如果不限制的话其他的科技栏也会显示这些装饰
    AddHamletRecipe(name, TECH.PTRIBE_HOME, cost, placer, image, data)
end

--房屋套件
AddHomeRecipe("player_house_cottage_craft", 10)
AddHomeRecipe("player_house_tudor_craft", 10)
AddHomeRecipe("player_house_gothic_craft", 10)
AddHomeRecipe("player_house_brick_craft", 10)
AddHomeRecipe("player_house_turret_craft", 10)
AddHomeRecipe("player_house_villa_craft", 30)
AddHomeRecipe("player_house_manor_craft", 30)

--椅子
AddHomeRecipe("deco_chair_classic", 2, "chair_classic_placer", "reno_chair_classic")
AddHomeRecipe("deco_chair_corner", 2, "chair_corner_placer", "reno_chair_corner")
AddHomeRecipe("deco_chair_bench", 2, "chair_bench_placer", "reno_chair_bench")
AddHomeRecipe("deco_chair_horned", 2, "chair_horned_placer", "reno_chair_horned")
AddHomeRecipe("deco_chair_footrest", 2, "chair_footrest_placer", "reno_chair_footrest")
AddHomeRecipe("deco_chair_lounge", 2, "chair_lounge_placer", "reno_chair_lounge")
AddHomeRecipe("deco_chair_massager", 2, "chair_massager_placer", "reno_chair_massager")
AddHomeRecipe("deco_chair_stuffed", 2, "chair_stuffed_placer", "reno_chair_stuffed")
AddHomeRecipe("deco_chair_rocking", 2, "chair_rocking_placer", "reno_chair_rocking")
AddHomeRecipe("deco_chair_ottoman", 2, "chair_ottoman_placer", "reno_chair_ottoman")
AddHomeRecipe("deco_chaise", 15, "deco_chaise_placer", "reno_chair_chaise")

-- 展示柜
AddHomeRecipe("shelves_wood", 2, "shelves_wood_placer", "reno_shelves_wood", { min_spacing = 2 })
AddHomeRecipe("shelves_basic", 2, "shelves_basic_placer", "reno_shelves_basic", { min_spacing = 2 })
AddHomeRecipe("shelves_cinderblocks", 1, "shelves_cinderblocks_placer", "reno_shelves_cinderblocks", { min_spacing = 2 })
AddHomeRecipe("shelves_marble", 8, "shelves_marble_placer", "reno_shelves_marble", { min_spacing = 2 })
AddHomeRecipe("shelves_glass", 8, "shelves_glass_placer", "reno_shelves_glass", { min_spacing = 2 })
AddHomeRecipe("shelves_ladder", 8, "shelves_ladder_placer", "reno_shelves_ladder", { min_spacing = 2 })
AddHomeRecipe("shelves_hutch", 8, "shelves_hutch_placer", "reno_shelves_hutch", { min_spacing = 2 })
AddHomeRecipe("shelves_industrial", 8, "shelves_industrial_placer", "reno_shelves_industrial", { min_spacing = 2 })
AddHomeRecipe("shelves_adjustable", 8, "shelves_adjustable_placer", "reno_shelves_adjustable", { min_spacing = 2 })
AddHomeRecipe("shelves_midcentury", 6, "shelves_midcentury_placer", "reno_shelves_midcentury", { min_spacing = 2 })
AddHomeRecipe("shelves_wallmount", 6, "shelves_wallmount_placer", "reno_shelves_wallmount", { min_spacing = 2 })
AddHomeRecipe("shelves_aframe", 6, "shelves_aframe_placer", "reno_shelves_aframe", { min_spacing = 2 })
AddHomeRecipe("shelves_crates", 6, "shelves_crates_placer", "reno_shelves_crates", { min_spacing = 2 })
AddHomeRecipe("shelves_fridge", 6, "shelves_fridge_placer", "reno_shelves_fridge", { min_spacing = 2 })
AddHomeRecipe("shelves_floating", 6, "shelves_floating_placer", "reno_shelves_floating", { min_spacing = 2 })
AddHomeRecipe("shelves_pipe", 6, "shelves_pipe_placer", "reno_shelves_pipe", { min_spacing = 2 })
AddHomeRecipe("shelves_hattree", 6, "shelves_hattree_placer", "reno_shelves_hattree", { min_spacing = 2 })
AddHomeRecipe("shelves_pallet", 6, "shelves_pallet_placer", "reno_shelves_pallet", { min_spacing = 2 })

-- 地毯
AddHomeRecipe("rug_round", 2, "rug_round_placer", "reno_rug_round", { min_spacing = 0 })
AddHomeRecipe("rug_square", 2, "rug_square_placer", "reno_rug_square", { min_spacing = 0 })
AddHomeRecipe("rug_oval", 2, "rug_oval_placer", "reno_rug_oval", { min_spacing = 0 })
AddHomeRecipe("rug_rectangle", 3, "rug_rectangle_placer", "reno_rug_rectangle", { min_spacing = 0 })
AddHomeRecipe("rug_fur", 5, "rug_fur_placer", "reno_rug_fur", { min_spacing = 0 })
AddHomeRecipe("rug_hedgehog", 5, "rug_hedgehog_placer", "reno_rug_hedgehog", { min_spacing = 0 })
AddHomeRecipe("rug_porcupuss", 10, "rug_porcupuss_placer", "reno_rug_porcupuss", { min_spacing = 0 })
AddHomeRecipe("rug_hoofprint", 5, "rug_hoofprint_placer", "reno_rug_hoofprint", { min_spacing = 0 })
AddHomeRecipe("rug_octagon", 5, "rug_octagon_placer", "reno_rug_octagon", { min_spacing = 0 })
AddHomeRecipe("rug_swirl", 5, "rug_swirl_placer", "reno_rug_swirl", { min_spacing = 0 })
AddHomeRecipe("rug_catcoon", 5, "rug_catcoon_placer", "reno_rug_catcoon", { min_spacing = 0 })
AddHomeRecipe("rug_rubbermat", 5, "rug_rubbermat_placer", "reno_rug_rubbermat", { min_spacing = 0 })
AddHomeRecipe("rug_web", 5, "rug_web_placer", "reno_rug_web", { min_spacing = 0 })
AddHomeRecipe("rug_metal", 5, "rug_metal_placer", "reno_rug_metal", { min_spacing = 0 })
AddHomeRecipe("rug_wormhole", 5, "rug_wormhole_placer", "reno_rug_wormhole", { min_spacing = 0 })
AddHomeRecipe("rug_braid", 5, "rug_braid_placer", "reno_rug_braid", { min_spacing = 0 })
AddHomeRecipe("rug_beard", 5, "rug_beard_placer", "reno_rug_beard", { min_spacing = 0 })
AddHomeRecipe("rug_nailbed", 5, "rug_nailbed_placer", "reno_rug_nailbed", { min_spacing = 0 })
AddHomeRecipe("rug_crime", 5, "rug_crime_placer", "reno_rug_crime", { min_spacing = 0 })
AddHomeRecipe("rug_tiles", 5, "rug_tiles_placer", "reno_rug_tiles", { min_spacing = 0 })

-- 灯
AddHomeRecipe("deco_lamp_fringe", 8, "deco_lamp_fringe_placer", "reno_lamp_fringe")
AddHomeRecipe("deco_lamp_stainglass", 8, "deco_lamp_stainglass_placer", "reno_lamp_stainglass")
AddHomeRecipe("deco_lamp_downbridge", 8, "deco_lamp_downbridge_placer", "reno_lamp_downbridge")
AddHomeRecipe("deco_lamp_2embroidered", 8, "deco_lamp_2embroidered_placer", "reno_lamp_2embroidered")
AddHomeRecipe("deco_lamp_ceramic", 8, "deco_lamp_ceramic_placer", "reno_lamp_ceramic")
AddHomeRecipe("deco_lamp_glass", 8, "deco_lamp_glass_placer", "reno_lamp_glass")
AddHomeRecipe("deco_lamp_2fringes", 8, "deco_lamp_2fringes_placer", "reno_lamp_2fringes")
AddHomeRecipe("deco_lamp_candelabra", 8, "deco_lamp_candelabra_placer", "reno_lamp_candelabra")
AddHomeRecipe("deco_lamp_elizabethan", 8, "deco_lamp_elizabethan_placer", "reno_lamp_elizabethan")
AddHomeRecipe("deco_lamp_gothic", 8, "deco_lamp_gothic_placer", "reno_lamp_gothic")
AddHomeRecipe("deco_lamp_orb", 8, "deco_lamp_orb_placer", "reno_lamp_orb")
AddHomeRecipe("deco_lamp_bellshade", 8, "deco_lamp_bellshade_placer", "reno_lamp_bellshade")
AddHomeRecipe("deco_lamp_crystals", 8, "deco_lamp_crystals_placer", "reno_lamp_crystals")
AddHomeRecipe("deco_lamp_upturn", 8, "deco_lamp_upturn_placer", "reno_lamp_upturn")
AddHomeRecipe("deco_lamp_2upturns", 8, "deco_lamp_2upturns_placer", "reno_lamp_2upturns")
AddHomeRecipe("deco_lamp_spool", 8, "deco_lamp_spool_placer", "reno_lamp_spool")
AddHomeRecipe("deco_lamp_edison", 8, "deco_lamp_edison_placer", "reno_lamp_edison")
AddHomeRecipe("deco_lamp_adjustable", 8, "deco_lamp_adjustable_placer", "reno_lamp_adjustable")
AddHomeRecipe("deco_lamp_rightangles", 8, "deco_lamp_rightangles_placer", "reno_lamp_rightangles")
AddHomeRecipe("deco_lamp_hoofspa", 8, "deco_lamp_hoofspa_placer", "reno_lamp_hoofspa")

-- 盆栽
AddHomeRecipe("deco_plantholder_basic", 6, "deco_plantholder_basic_placer", "reno_plantholder_basic")
AddHomeRecipe("deco_plantholder_wip", 6, "deco_plantholder_wip_placer", "reno_plantholder_wip")
AddHomeRecipe("deco_plantholder_marble", 6, "deco_plantholder_marble_placer", "reno_plantholder_marble")
AddHomeRecipe("deco_plantholder_bonsai", 6, "deco_plantholder_bonsai_placer", "reno_plantholder_bonsai")
AddHomeRecipe("deco_plantholder_dishgarden", 6, "deco_plantholder_dishgarden_placer", "reno_plantholder_dishgarden")
AddHomeRecipe("deco_plantholder_philodendron", 6, "deco_plantholder_philodendron_placer", "reno_plantholder_philodendron")
AddHomeRecipe("deco_plantholder_orchid", 6, "deco_plantholder_orchid_placer", "reno_plantholder_orchid")
AddHomeRecipe("deco_plantholder_draceana", 6, "deco_plantholder_draceana_placer", "reno_plantholder_draceana")
AddHomeRecipe("deco_plantholder_xerographica", 6, "deco_plantholder_xerographica_placer", "reno_plantholder_xerographica")
AddHomeRecipe("deco_plantholder_birdcage", 6, "deco_plantholder_birdcage_placer", "reno_plantholder_birdcage")
AddHomeRecipe("deco_plantholder_palm", 6, "deco_plantholder_palm_placer", "reno_plantholder_palm")
AddHomeRecipe("deco_plantholder_zz", 6, "deco_plantholder_zz_placer", "reno_plantholder_zz")
AddHomeRecipe("deco_plantholder_fernstand", 6, "deco_plantholder_fernstand_placer", "reno_plantholder_fernstand")
AddHomeRecipe("deco_plantholder_fern", 6, "deco_plantholder_fern_placer", "reno_plantholder_fern")
AddHomeRecipe("deco_plantholder_terrarium", 6, "deco_plantholder_terrarium_placer", "reno_plantholder_terrarium")
AddHomeRecipe("deco_plantholder_plantpet", 6, "deco_plantholder_plantpet_placer", "reno_plantholder_plantpet")
AddHomeRecipe("deco_plantholder_traps", 6, "deco_plantholder_traps_placer", "reno_plantholder_traps")
AddHomeRecipe("deco_plantholder_pitchers", 6, "deco_plantholder_pitchers_placer", "reno_plantholder_pitchers")
AddHomeRecipe("deco_plantholder_winterfeasttreeofsadness", 2, "deco_plantholder_winterfeasttreeofsadness_placer",
    "reno_plantholder_winterfeasttreeofsadness", { ingredients = { Ingredient("twigs", 1) } })
AddHomeRecipe("deco_plantholder_winterfeasttree", 50, "deco_plantholder_winterfeasttree_placer", "reno_lamp_festivetree")

-- 桌子
AddHomeRecipe("deco_table_round", 2, "deco_table_round_placer", "reno_table_round")
AddHomeRecipe("deco_table_banker", 4, "deco_table_banker_placer", "reno_table_banker")
AddHomeRecipe("deco_table_diy", 3, "deco_table_diy_placer", "reno_table_diy")
AddHomeRecipe("deco_table_raw", 1, "deco_table_raw_placer", "reno_table_raw")
AddHomeRecipe("deco_table_crate", 1, "deco_table_crate_placer", "reno_table_crate")
AddHomeRecipe("deco_table_chess", 1, "deco_table_chess_placer", "reno_table_chess")

-- 挂墙上的装饰品
AddHomeRecipe("deco_wallornament_photo", 2, "deco_wallornament_photo_placer", "reno_wallornament_photo")
AddHomeRecipe("deco_wallornament_fulllength_mirror", 2, "deco_wallornament_fulllength_mirror_placer",
    "reno_wallornament_fulllength_mirror")
AddHomeRecipe("deco_wallornament_embroidery_hoop", 3, "deco_wallornament_embroidery_hoop_placer",
    "reno_wallornament_embroidery_hoop")
AddHomeRecipe("deco_wallornament_mosaic", 4, "deco_wallornament_mosaic_placer", "reno_wallornament_mosaic")
AddHomeRecipe("deco_wallornament_wreath", 4, "deco_wallornament_wreath_placer", "reno_wallornament_wreath")
AddHomeRecipe("deco_wallornament_axe", 5, "deco_wallornament_axe_placer", "reno_wallornament_axe",
    { ingredients = { Ingredient("axe", 1) } })
AddHomeRecipe("deco_wallornament_hunt", 5, "deco_wallornament_hunt_placer", "reno_wallornament_hunt",
    { ingredients = { Ingredient("spear", 1) } })
AddHomeRecipe("deco_wallornament_periodic_table", 5, "deco_wallornament_periodic_table_placer",
    "reno_wallornament_periodic_table")
AddHomeRecipe("deco_wallornament_gears_art", 8, "deco_wallornament_gears_art_placer", "reno_wallornament_gears_art")
AddHomeRecipe("deco_wallornament_cape", 5, "deco_wallornament_cape_placer", "reno_wallornament_cape")
AddHomeRecipe("deco_wallornament_no_smoking", 3, "deco_wallornament_no_smoking_placer", "reno_wallornament_no_smoking")
AddHomeRecipe("deco_wallornament_black_cat", 5, "deco_wallornament_black_cat_placer", "reno_wallornament_black_cat")
AddHomeRecipe("deco_antiquities_wallfish", 2, "deco_antiquities_wallfish_placer", "reno_antiquities_wallfish",
    { ingredients = { Ingredient("fish", 1) } })
AddHomeRecipe("deco_antiquities_beefalo", 10, "deco_antiquities_beefalo_placer", "reno_antiquities_beefalo",
    { ingredients = { Ingredient("horn", 1) } })

-- 窗帘，把单机版没加的窗帘全加上，不过图标可能没有，用其他reno_window_round_burlap的图标替代
AddHomeRecipe("window_round_backwall", 3, "window_round_backwall_placer", "reno_window_round_burlap")
AddHomeRecipe("window_round_burlap_backwall", 3, "window_round_burlap_backwall_placer", "reno_window_round_burlap")
AddHomeRecipe("window_small_peaked_backwall", 3, "window_small_peaked_backwall_placer", "reno_window_small_peaked")
AddHomeRecipe("window_large_square_backwall", 4, "window_large_square_backwall_placer", "reno_window_large_square")
AddHomeRecipe("window_tall_backwall", 4, "window_tall_backwall_placer", "reno_window_tall")
AddHomeRecipe("window_round_arcane_backwall", 4, "window_round_arcane_backwall_placer", "reno_window_round_burlap")
AddHomeRecipe("window_small_peaked_curtain_backwall", 3, "window_small_peaked_curtain_backwall_placer",
    "reno_window_small_peaked_curtain")
AddHomeRecipe("window_large_square_curtain_backwall", 5, "window_large_square_curtain_backwall_placer",
    "reno_window_large_square_curtain")
AddHomeRecipe("window_tall_curtain_backwall", 5, "window_tall_curtain_backwall_placer", "reno_window_round_burlap")
AddHomeRecipe("window_square_weapons_backwall", 4, "window_square_weapons_backwall_placer", "reno_window_tall_curtain")
AddHomeRecipe("window_greenhouse_backwall", 8, "window_greenhouse_backwall_placer", "reno_window_greenhouse")


-- 柱子
AddHomeRecipe("deco_wood_cornerbeam", 1, "deco_wood_cornerbeam_placer", "reno_cornerbeam_wood")
AddHomeRecipe("deco_millinery_cornerbeam", 1, "deco_millinery_cornerbeam_placer", "reno_cornerbeam_millinery")
AddHomeRecipe("deco_round_cornerbeam", 1, "deco_round_cornerbeam_placer", "reno_cornerbeam_round")
AddHomeRecipe("deco_marble_cornerbeam", 5, "deco_marble_cornerbeam_placer", "reno_cornerbeam_marble")

-- 地板
AddHomeRecipe("interior_floor_wood", 5)
AddHomeRecipe("interior_floor_marble", 15)
AddHomeRecipe("interior_floor_check", 7)
AddHomeRecipe("interior_floor_plaid_tile", 10)
AddHomeRecipe("interior_floor_sheet_metal", 6)
AddHomeRecipe("interior_floor_gardenstone", 10)
AddHomeRecipe("interior_floor_geometrictiles", 12)
AddHomeRecipe("interior_floor_shag_carpet", 6)
AddHomeRecipe("interior_floor_transitional", 6)
AddHomeRecipe("interior_floor_woodpanels", 10)
AddHomeRecipe("interior_floor_herringbone", 12)
AddHomeRecipe("interior_floor_hexagon", 12)
AddHomeRecipe("interior_floor_hoof_curvy", 12)
AddHomeRecipe("interior_floor_octagon", 12)

-- 墙纸
AddHomeRecipe("interior_wall_wood", 1)
AddHomeRecipe("interior_wall_checkered", 6)
AddHomeRecipe("interior_wall_floral", 6)
AddHomeRecipe("interior_wall_sunflower", 6)
AddHomeRecipe("interior_wall_harlequin", 10)
AddHomeRecipe("interior_wall_peagawk", 6)
AddHomeRecipe("interior_wall_plain_ds", 4)
AddHomeRecipe("interior_wall_plain_rog", 4)
AddHomeRecipe("interior_wall_rope", 6)
AddHomeRecipe("interior_wall_circles", 10)
AddHomeRecipe("interior_wall_marble", 15)
AddHomeRecipe("interior_wall_mayorsoffice", 15)
AddHomeRecipe("interior_wall_fullwall_moulding", 15)
AddHomeRecipe("interior_wall_upholstered", 8)

-- 吊灯
AddHomeRecipe("swinging_light_basic_bulb", 5, "swinging_light_basic_bulb_placer", "reno_light_basic_bulb",
    { min_spacing = 0 })
AddHomeRecipe("swinging_light_basic_metal", 6, "swinging_light_basic_metal_placer", "reno_light_basic_metal",
    { min_spacing = 0 })
AddHomeRecipe("swinging_light_chandalier_candles", 8, "swinging_light_chandalier_candles_placer",
    "reno_light_chandalier_candles", { min_spacing = 0 })
AddHomeRecipe("swinging_light_rope_1", 1, "swinging_light_rope_1_placer", "reno_light_rope_1", { min_spacing = 0 })
AddHomeRecipe("swinging_light_rope_2", 1, "swinging_light_rope_2_placer", "reno_light_rope_2", { min_spacing = 0 })
AddHomeRecipe("swinging_light_floral_bulb", 10, "swinging_light_floral_bulb_placer", "reno_light_floral_bulb",
    { min_spacing = 0 })
AddHomeRecipe("swinging_light_pendant_cherries", 12, "swinging_light_pendant_cherries_placer",
    "reno_light_pendant_cherries", { min_spacing = 0 })
AddHomeRecipe("swinging_light_floral_scallop", 12, "swinging_light_floral_scallop_placer", "reno_light_floral_scallop",
    { min_spacing = 0 })
AddHomeRecipe("swinging_light_floral_bloomer", 12, "swinging_light_floral_bloomer_placer", "reno_light_floral_bloomer",
    { min_spacing = 0 })
AddHomeRecipe("swinging_light_tophat", 12, "swinging_light_tophat_placer", "reno_light_tophat", { min_spacing = 0 })
AddHomeRecipe("swinging_light_derby", 12, "swinging_light_derby_placer", "reno_light_derby", { min_spacing = 0 })

-- 门
AddHomeRecipe("wood_door", 10, "wood_door_placer", "wood_door",
    { atlas = "images/inventoryimages/hamletinventoryimages_2.xml" })
AddHomeRecipe("stone_door", 10, "stone_door_placer", "stone_door")
AddHomeRecipe("organic_door", 15, "organic_door_placer", "organic_door")
AddHomeRecipe("iron_door", 15, "iron_door_placer", "iron_door")
AddHomeRecipe("curtain_door", 15, "curtain_door_placer", "curtain_door")
AddHomeRecipe("plate_door", 15, "plate_door_placer", "plate_door")
AddHomeRecipe("round_door", 20, "round_door_placer", "round_door")
AddHomeRecipe("pillar_door", 20, "pillar_door_placer", "pillar_door")

-- --房屋扩建许可证
AddHomeRecipe("construction_permit", 50)


----------------------------------------------------------------------------------------------------
-- 城镇规划
-- 不需要城镇规划科技，这里把城镇科技放到装饰类别里，玩家使用蓝图解锁

local function AddCityRecipe(name, cost, placer, image, data)
    AddHamletRecipe(name, TECH.LOST, cost, placer, image, data)
end

AddCityRecipe("playerhouse_city", 30, "playerhouse_city_placer", nil, {
    ingredients = { Ingredient("boards", 4), Ingredient("cutstone", 3) },
    filters = { "STRUCTURES" }
})
AddCityRecipe("city_lamp", nil, "city_lamp_placer", nil, {
    ingredients = { Ingredient("cutstone", 3), Ingredient("transistor", 1), Ingredient("lantern", 1) },
    filters = { "STRUCTURES" }
})

AddCityRecipe("hedge_block_item", nil, nil, nil, {
    ingredients = { Ingredient("cutgrass", 9), Ingredient("nitre", 1) },
    filters = { "DECOR" }
})
AddCityRecipe("hedge_cone_item", nil, nil, nil, {
    ingredients = { Ingredient("cutgrass", 9), Ingredient("nitre", 1) },
    filters = { "DECOR" }
})
AddCityRecipe("hedge_layered_item", nil, nil, nil, {
    ingredients = { Ingredient("cutgrass", 9), Ingredient("nitre", 1) },
    filters = { "DECOR" }
})
AddCityRecipe("hedge_layered_item", nil, nil, nil, {
    ingredients = { Ingredient("cutgrass", 9), Ingredient("nitre", 1) },
    filters = { "DECOR" }
})
AddCityRecipe("ptribe_pigweaponfactory", nil, "ptribe_pigweaponfactory_placer",
    ImageUtils.GetItemImage("ptribe_pigweaponfactory", true).image, {
        ingredients = { Ingredient("boards", 5), Ingredient("cutstone", 3), Ingredient("spear", 2) },
        filters = { "STRUCTURES" }
    })

AddCityRecipe("lawnornament_1", 10, "lawnornament_1_placer", nil, { filters = { "DECOR" } })
AddCityRecipe("lawnornament_2", 10, "lawnornament_2_placer", nil, { filters = { "DECOR" } })
AddCityRecipe("lawnornament_3", 10, "lawnornament_3_placer", nil, { filters = { "DECOR" } })
AddCityRecipe("lawnornament_4", 10, "lawnornament_4_placer", nil, { filters = { "DECOR" } })
AddCityRecipe("lawnornament_5", 10, "lawnornament_5_placer", nil, { filters = { "DECOR" } })
AddCityRecipe("lawnornament_6", 10, "lawnornament_6_placer", nil, { filters = { "DECOR" } })
AddCityRecipe("lawnornament_7", 10, "lawnornament_7_placer", nil, { filters = { "DECOR" } })
AddCityRecipe("pugalisk_fountain", 100, "pugalisk_fountain_placer", "python_fountain", {
    atlas = "images/inventoryimages/python_fountain.xml",
    ingredients = { Ingredient("moonrocknugget", 10), Ingredient("flint", 10), Ingredient("goldnugget", 10) },
    filters = { "STRUCTURES" }
})

----------------------------------------------------------------------------------------------------
local function HamletIngredient(ingredienttype, amount, atlas, imageoverride)
    return Ingredient(ingredienttype, amount,
        atlas or "images/inventoryimages/hamletinventoryimages.xml", nil,
        imageoverride or (ingredienttype .. ".tex"))
end

-- 默认添加
AddRecipe2("disguisehat", { Ingredient("twigs", 2), Ingredient("pigskin", 1), Ingredient("beardhair", 1) },
    TECH.NONE, {
        atlas = "images/inventoryimages/hamletinventoryimages.xml",
        image = "disguisehat.tex"
    }, { "CLOTHING" })


AddRecipe2("halberd", { Ingredient("goldnugget", 2), Ingredient("twigs", 2) },
    TECH.SCIENCE_TWO, {
        atlas = "images/inventoryimages/hamletinventoryimages.xml",
        image = "halberd.tex",
    }, { "WEAPONS" })

AddRecipe2("pig_guard_tower", { Ingredient("cutstone", 3), HamletIngredient("halberd", 1), Ingredient("pigskin", 4) },
    TECH.SCIENCE_TWO, {
        atlas = "images/inventoryimages/hamletinventoryimages.xml",
        image = "pig_guard_tower.tex",
        placer = "pig_guard_tower_placer"
    }, { "STRUCTURES" })

AddRecipe2("oinc10", { OincIngredient("oinc10", 10) },
    TECH.NONE, {
        atlas = "images/inventoryimages2.xml",
        image = "quagmire_coin2.tex"
    }, { "REFINE" })
AddRecipe2("oinc100", { OincIngredient("oinc100", 100) },
    TECH.NONE, {
        atlas = "images/inventoryimages2.xml",
        image = "quagmire_coin3.tex"
    }, { "REFINE" })
AddRecipe2("oinc1000", { OincIngredient("oinc1000", 1000) },
    TECH.NONE, {
        atlas = "images/inventoryimages2.xml",
        image = "quagmire_coin4.tex"
    }, { "REFINE" })
AddRecipe2("ptribe_upgrade", { OincIngredient("ptribe_upgrade", 10) },
    TECH.NONE, {
        atlas = "images/inventoryimages/ptribe_upgrade.xml",
        image = "ptribe_upgrade.tex"
    }, { "REFINE" })
----------------------------------------------------------------------------------------------------
-- 薇尔芭专属科技
AddRecipe2("pig_guard_tower_palace",
    { Ingredient("cutstone", 3), HamletIngredient("halberd", 2), Ingredient("pigskin", 4) },
    TECH.SCIENCE_TWO, {
        builder_tag = "wilba",
        atlas = "images/inventoryimages/hamletinventoryimages.xml",
        image = "pig_guard_tower.tex",
        placer = "pig_guard_tower_palace_placer"
    }, { "CHARACTER" })
AddRecipe2("ptribe_pigtorch",
    { Ingredient("boards", 4), Ingredient("cutstone", 3), Ingredient("pigskin", 4), Ingredient("charcoal", 4) },
    TECH.SCIENCE_TWO, {
        builder_tag = "wilba",
        atlas = "images/inventoryimages/ptribe_pigtorch.xml",
        image = "ptribe_pigtorch.tex",
        placer = "ptribe_pigtorch_placer",
    }, { "CHARACTER" })
AddRecipe2("silvernecklace",
    { Ingredient("beardhair", 2), Ingredient("silk", 2), Ingredient("petals", 1) },
    TECH.SCIENCE_ONE, {
        builder_tag = "wilba",
        atlas = "images/inventoryimages/hamletinventoryimages.xml",
        image = "silvernecklace.tex",
    }, { "CHARACTER" })

----------------------------------------------------------------------------------------------------

local COINS = {
    oinc = 1,
    oinc10 = 10,
    oinc100 = 100,
    oinc1000 = 1000
}
local COINS_INDEX = { oinc = 1, oinc10 = 2, oinc100 = 3, oinc1000 = 4 }
local COINS_ORDER = { "oinc", "oinc10", "oinc100", "oinc1000" }

--- 扣除指定数量的呼噜币
local function RemoveCoins(self, cost)
    local coins = {}
    local coinNum = {}

    -- local items = self:GetItemsWithTag("oinc") -- 不行，拿不到箱子里的
    for _, name in ipairs(COINS_ORDER) do
        coins[name] = coins[name] or {}
        local items = self:GetItemByName(name, math.huge, true)

        for item, num in pairs(items) do
            coinNum[COINS_INDEX[name]] = (coinNum[COINS_INDEX[name]] or 0) + num
            table.insert(coins[name], item)
        end
    end

    -- print("需要", cost)
    -- for k, v in pairs(coins) do
    --     for _, vv in ipairs(v) do
    --         print(vv, GetStackSize(vv))
    --     end
    -- end

    -- 从零钱开始，将呼噜币从玩家物品栏移除
    for _, coinType in ipairs(COINS_ORDER) do
        local val = COINS[coinType]
        for _, coin in ipairs(coins[coinType] or {}) do
            local num = GetStackSize(coin)
            for _ = 1, num do
                self:RemoveItem(coin, false, true):Remove()
                cost = cost - val
                if cost <= 0 then
                    if cost < 0 then
                        -- 找零
                        cost = math.abs(cost)
                        for i = #COINS_ORDER, 1, -1 do
                            coinType = COINS_ORDER[i]
                            val = COINS[coinType]

                            if cost > val then
                                coin = SpawnPrefab(coinType)
                                num = math.floor(cost / val)
                                if num > 1 then
                                    coin.components.stackable.stacksize = num
                                end
                                self:GiveItem(coin)

                                cost = cost - (val * num)

                                if cost <= 0 then return end
                            end
                        end
                    end
                    return
                end
            end
        end
    end
end

InitWork.RepairInventoryGetItemsWithTag()
AddComponentPostInit("inventory", function(self)
    self.RemoveCoins = RemoveCoins

    local oldHas = self.Has
    -- 物品栏是否有蜘蛛栏物品，使用这个判断，我在这里拦截，对呼噜币进行换算，也许修改HasIngredients也行，不过应该没这个简单
    -- 需要注意，无论是否有足够的呼噜币，最后返回的数量都是一元呼噜币的个数
    function self:Has(item, amount, ...)
        if not COINS[item] then
            return oldHas(self, item, amount, ...)
        end

        local all = 0
        for key, val in pairs(COINS) do
            local _, count = oldHas(self, key, 1, ...)
            all = all + count * val
        end

        local need = COINS[item] * amount
        if all >= need then
            return true, math.floor(all / COINS[item])
        else
            return false, all
        end
    end
end)

AddClassPostConstruct("components/inventory_replica", function(self)
    local oldHas = self.Has
    function self:Has(item, amount, ...)
        if not COINS[item] or self.inst.components.inventory or not self.classified then --主机交给主机组件判断
            return oldHas(self, item, amount, ...)
        end

        local all = 0
        for key, val in pairs(COINS) do
            local _, count = oldHas(self, key, 1, ...)
            all = all + count * val
        end

        local need = COINS[item] * amount
        if all >= need then
            return true, math.floor(all / COINS[item])
        else
            return false, all
        end
    end
end)

AddComponentPostInit("builder", function(self)
    -- 对于配方的nounlock，如果我加上就会被分类到”模组物品“下，建筑可以正常建造，如果不加，被分类到“翻新”，但是无法正常建造，不过对于物品加不加都可以正常获得
    -- 这里在建造前添加到配方中，使KnowsRecipe方法返回true，可以正常建造，其实我这里不知道该怎么写，只好覆盖了原方法，都是因为这个不支持placer
    Utils.FnDecorator(self, "MakeRecipeAtPoint", function(self, recipe)
        if not self:KnowsRecipe(recipe) and RECNAMES[recipe.name] then
            self:AddRecipe(recipe.name)
        end
    end)

    --- 对消耗呼噜币的扣除
    Utils.FnDecorator(self, "RemoveIngredients", function(self, ingredients, recname, ...)
        if self.freebuildmode or not OINC_RECIPES[recname] then return end

        -- 自己扣除呼噜币
        self.inst.components.inventory:RemoveCoins(OINC_RECIPES[recname])

        --移除呼噜币的扣除
        local newIngredients = {}
        for item, ents in pairs(ingredients) do
            if not COINS[item] then
                newIngredients[item] = ents
            end
        end
        return nil, false, { self, newIngredients, recname, ... }
    end)
end)

----------------------------------------------------------------------------------------------------
for p, name in pairs(BP.BLUEPRINT_LIST) do
    STRINGS.RECIPE_DESC[string.upper(p)] = name

    AddRecipe2(p, { Ingredient("goldnugget", 1) }, TECH.PTRIBE_BLUEPRINT, {
        nounlock = true,
        no_deconstruction = true,
        actionstr = "CARNIVAL_PRIZESHOP",
        sg_state = "give",
        image = "blueprint_rare.tex",
    })
end

-- AddClassPostConstruct("widgets/redux/craftingmenu_details", function(self)
--     Utils.FnDecorator(self, "UpdateBuildButton", nil, function(retTab, self)
--         if self.data == nil then return end

--         local button = self.build_button_root.button
--         local recipe = self.data.recipe

--         local blueprintName = TRIBE_BLUEPRINTS[recipe.name]
--         if button and blueprintName and not TheInput:ControllerAttached() then
--             if not BluePrintsUtils.IsEnable(blueprintName) then
--                 button:SetText(STRINGS.UI.CRAFTING.RECIPEACTION.PTRIBE_DISABLE)
--             end
--         end
--     end)
-- end)

-- BluePrintsUtils.SetEnable(name, enable)
