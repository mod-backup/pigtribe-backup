local GetPrefab = require("ptribe_utils/getprefab")

AddStategraphEvent("wilson", EventHandler("transform_to_werewilba", function(inst, data)
    if inst:HasTag("ptribe_silvernecklace") then return end

    if inst.components.rider:IsRiding() then
        inst.sg:GoToState("werewilba_mounted")
    else
        inst.sg:GoToState("werewilba")
    end
end))

AddStategraphEvent("wilson", EventHandler("transform_to_wilba", function(inst, data)
    inst.sg:GoToState("wilba_back")
end))

AddStategraphState("wilson", State {
    name = "werewilba_mounted",
    tags = { "busy" },
    onenter = function(inst)
        inst.AnimState:PlayAnimation("fall_off")
        inst.SoundEmitter:PlaySound("dontstarve/beefalo/saddle/dismount")
    end,

    events =
    {
        EventHandler("animover", function(inst)
            -- TheCamera:SetDistance(14)
            inst.components.rider:ActualDismount()
            inst.sg:GoToState("werewilba")
        end),
    }
})

--- 变疯猪
AddStategraphState("wilson", State {
    name = "werewilba",
    tags = { "busy" },
    onenter = function(inst)
        inst.components.ptribe_wilbatransform:TransformToWere()
        inst.Physics:Stop()
        inst.components.playercontroller:Enable(false)
        inst.AnimState:PlayAnimation("transform_pre")
        inst.components.health:SetInvincible(true)
        inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/transform_1")
    end,

    timeline =
    {
        TimeEvent(105 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/bark")
        end),
        TimeEvent(139 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/breath_out")
        end),
        TimeEvent(151 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/transform_2")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/music/werepig_of_london", "werepigmusic")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            inst.components.ptribe_wilbatransform:TransformToWerePost()
            inst.sg:GoToState("werewilba_pst")
        end),
    },

    ontimeout = function(inst)
        if not inst.were then
            inst.components.ptribe_wilbatransform:TransformToWerePost()
        end
        inst.components.health:SetInvincible(false)
        inst.components.playercontroller:Enable(true)
    end,
})

AddStategraphState("wilson", State {
    name = "werewilba_pst",
    tags = { "busy" },
    onenter = function(inst)
        inst.components.playercontroller:Enable(false)
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("transform_pst")
        inst.components.health:SetInvincible(true)
    end,

    events =
    {
        EventHandler("animover", function(inst)
            inst.sg:GoToState("idle")
        end),
    },

    onexit = function(inst)
        inst.components.health:SetInvincible(false)
        inst.components.playercontroller:Enable(true)
    end,
})

local function dropbeard(inst)
    local bit = SpawnPrefab("beardhair")
    local x, y, z = inst.Transform:GetWorldPosition()
    y = y + 2
    bit.Transform:SetPosition(x, y, z)
    local speed = 1 + math.random()
    local angle = math.random() * 360
    bit.Physics:SetVel(speed * math.cos(angle), 2 + math.random() * 3, speed * math.sin(angle))
end

-- 变回来
AddStategraphState("wilson", State {
    name = "wilba_back",
    tags = { "busy" },
    onenter = function(inst)
        inst.components.ptribe_wilbatransform:TransformToWilba()
        inst.Physics:Stop()
        inst.components.playercontroller:Enable(false)
        inst.AnimState:PlayAnimation("reform")
        inst.components.health:SetInvincible(true)
        inst.SoundEmitter:PlaySound("dontstarve/creatures/werepig/transformToPig")
    end,

    timeline =
    {
        TimeEvent(59 * FRAMES, function(inst)
            dropbeard(inst)
        end),
        TimeEvent(63 * FRAMES, function(inst)
            dropbeard(inst)
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            inst.sg:GoToState("idle")
        end),
    },

    onexit = function(inst)
        inst.components.health:SetInvincible(false)
        inst.components.playercontroller:Enable(true)
    end,
})

local function StopTalkSound(inst, instant)
    if not instant and inst.endtalksound ~= nil and inst.SoundEmitter:PlayingSound("talk") then
        inst.SoundEmitter:PlaySound(inst.endtalksound)
    end
    inst.SoundEmitter:KillSound("talk")
end

local function CancelTalk_Override(inst, instant)
    if inst.sg.statemem.talktask ~= nil then
        inst.sg.statemem.talktask:Cancel()
        inst.sg.statemem.talktask = nil
        StopTalkSound(inst, instant)
    end
end

AddStategraphState("wilson", State {
    name = "ptribe_wilba_home",
    tags = { "hiding", "notalking", "nomorph", "busy", "nopredict", "nodangle" },
    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("hide")
        inst.AnimState:PushAnimation("hide_idle", false)
        inst.SoundEmitter:PlaySound("dontstarve/movement/foley/hidebush")
    end,
    timeline =
    {
        TimeEvent(12 * FRAMES, function(inst)
            GetPrefab.SetPosition(inst, inst.ptribe_homePos:Get())
            inst:SnapCamera()
            inst:ScreenFade(true, 1)
        end),
        TimeEvent(24 * FRAMES, function(inst)
            -- inst:PerformBufferedAction()

            inst.sg:RemoveStateTag("busy")
            inst.sg:RemoveStateTag("nopredict")
            inst.sg:AddStateTag("idle")
        end),
    },
    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("bucked_post")
            end
        end),
    },
    ontimeout = function(inst)
        inst.sg:GoToState("idle")
    end,

    onexit = CancelTalk_Override,
})


--- 怪物形态制作道具
AddStategraphState("wilson", State {
    name = "ptribe_wilba_monster_doaction",
    tags = { "doing", "busy", "keepchannelcasting" },

    onenter = function(inst, silent)
        inst.components.locomotor:Stop()
        inst.AnimState:PushAnimation("idle", true)
        inst.sg.statemem.action = inst.bufferedaction
        inst.sg.statemem.silent = silent
        inst.sg:SetTimeout(10 * FRAMES)
    end,

    timeline =
    {
        TimeEvent(6 * FRAMES, function(inst)
            inst.sg:RemoveStateTag("busy")
            if inst.sg.statemem.silent then
                inst.components.talker:IgnoreAll("silentpickup")
                inst:PerformBufferedAction()
                inst.components.talker:StopIgnoringAll("silentpickup")
            else
                inst:PerformBufferedAction()
            end
        end),
    },

    ontimeout = function(inst)
        inst.sg:GoToState("idle", true)
    end,

    onexit = function(inst)
        if inst.bufferedaction == inst.sg.statemem.action and
            (inst.components.playercontroller == nil or inst.components.playercontroller.lastheldaction ~= inst.bufferedaction) then
            inst:ClearBufferedAction()
        end
    end,
})

----------------------------------------------------------------------------------------------------
local function WilbaMonsterWork(inst, data)
    local ba = inst.sg.statemem.action
    local action = ba.action
    if not action then return end

    if action == ACTIONS.CHOP then
        if data.onenter then
            inst.sg:AddStateTag("prechop")
            inst.sg:AddStateTag("chopping")
            inst:AddTag("prechop")
        elseif data.timeline then
            if data.timeline == 13 then
                inst.sg:RemoveStateTag("prechop")
                inst:RemoveTag("prechop")
            end
        elseif data.onexit then
            inst:RemoveTag("prechop")
        end
    elseif action == ACTIONS.MINE then
        if data.onenter then
            inst.sg:AddStateTag("premine")
            inst.sg:AddStateTag("mining")
            inst:AddTag("premine")
        elseif data.timeline then
            if data.timeline == 12 then
                PlayMiningFX(inst, ba.target) --不加的话挖矿没声音
                inst.sg.statemem.recoilstate = "mine_recoil"
            elseif data.timeline == 13 then
                inst.sg:RemoveStateTag("premine")
                inst:RemoveTag("premine")
            end
        elseif data.onexit then
            inst:RemoveTag("premine")
        end
    elseif action == ACTIONS.HAMMER then
        if data.onenter then
            inst.sg:AddStateTag("prehammer")
            inst.sg:AddStateTag("hammering")
            inst:AddTag("prehammer")
        elseif data.timeline then
            if data.timeline == 13 then
                inst.sg:RemoveStateTag("prehammer")
                inst:RemoveTag("prehammer")
            end
        elseif data.onexit then
            inst:RemoveTag("prehammer")
        end
    elseif action == ACTIONS.DIG then
        if data.onenter then
            inst.sg:AddStateTag("predig")
            inst.sg:AddStateTag("digging")
            inst:AddTag("predig")
        elseif data.timeline then
            if data.timeline == 13 then
                inst.sg:RemoveStateTag("predig")
                inst:RemoveTag("predig")
            end
        elseif data.onexit then
            inst:RemoveTag("predig")
        end
    end
end

local function WereDefaultAnim(inst, volume)
    volume = volume or 0.5
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/bark")
    if inst.atk_2 == true then
        inst.AnimState:PlayAnimation("atk_2_werewilba")
        inst.atk_2 = nil
    else
        inst.AnimState:PlayAnimation("atk_werewilba")
        inst.atk_2 = true
    end
    inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_whoosh", nil, volume)
end

AddStategraphState("wilson", State {
    name = "ptribe_wilbamonster_work",
    tags = { "working" },

    onenter = function(inst)
        inst.sg.statemem.action = inst:GetBufferedAction()

        WilbaMonsterWork(inst, { onenter = true })

        -- inst.sg.statemem.action.action
        WereDefaultAnim(inst, 0.8)
    end,

    timeline =
    {
        TimeEvent(12 * FRAMES, function(inst)
            WilbaMonsterWork(inst, { timeline = 12 })
            inst:PerformBufferedAction()
        end),
        TimeEvent(13 * FRAMES, function(inst)
            WilbaMonsterWork(inst, { timeline = 13 })
        end),

        TimeEvent(14 * FRAMES, function(inst)
            if inst.components.playercontroller ~= nil and
                inst.components.playercontroller:IsAnyOfControlsPressed(
                    CONTROL_PRIMARY,
                    CONTROL_ACTION,
                    CONTROL_CONTROLLER_ACTION) and
                inst.sg.statemem.action ~= nil and
                inst.sg.statemem.action:IsValid() and
                inst.sg.statemem.action.target ~= nil and
                inst.sg.statemem.action.target.components.workable ~= nil and
                inst.sg.statemem.action.target.components.workable:CanBeWorked() and
                inst.sg.statemem.action.target:IsActionValid(inst.sg.statemem.action.action) and
                CanEntitySeeTarget(inst, inst.sg.statemem.action.target) then
                --No fast-forward when repeat initiated on server
                inst.sg.statemem.action.options.no_predict_fastforward = true
                inst:ClearBufferedAction()
                inst:PushBufferedAction(inst.sg.statemem.action)
            end
        end),
    },

    events =
    {
        EventHandler("unequip", function(inst) inst.sg:GoToState("idle") end),
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },

    onexit = function(inst)
        WilbaMonsterWork(inst, { onexit = true })
    end,
})

--- 变身噩梦猪人
AddStategraphState("wilson", State {
    name = "transform_to_werewilba2",
    tags = { "busy", "pausepredict", "nomorph", "nodangle" },

    onenter = function(inst, type)
        inst.sg.statemem.type = type

        inst.AnimState:OverrideSymbol("shadow_hands", "shadow_skinchangefx", "shadow_hands")
        inst.AnimState:OverrideSymbol("shadow_ball", "shadow_skinchangefx", "shadow_ball")
        inst.AnimState:OverrideSymbol("splode", "shadow_skinchangefx", "splode")
        inst.AnimState:PlayAnimation("skin_change", false)

        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:RemotePausePrediction()
        end

        inst.sg:SetTimeout(40 * FRAMES)
    end,

    timeline =
    {
        -- gift_pst plays first and it is 20 frames long
        TimeEvent(20 * FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/transform_1")
        end),
    },
    ontimeout = function(inst)
        if inst.sg.statemem.type == "daywalker" then
            inst.components.ptribe_wilbatransform:TransformToDaywalker()
        end
        inst.sg:GoToState("idle")
    end,

    onexit = function(inst)
        inst.AnimState:OverrideSymbol("shadow_hands", "shadow_hands", "shadow_hands")
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller:EnableMapControls(true)
            inst.components.playercontroller:Enable(true)
        end
        inst.components.inventory:Show()
        inst:ShowActions(true)
    end,
})
