local Utils = require("ptribe_utils/utils")
local Shapes = require("ptribe_utils/shapes")
local GetPrefab = require("ptribe_utils/getprefab")

AddStategraphActionHandler("pig", ActionHandler(ACTIONS.MINE, "chop"))
AddStategraphActionHandler("pig", ActionHandler(ACTIONS.DIG, "chop"))
AddStategraphActionHandler("pig", ActionHandler(ACTIONS.HAMMER, "chop"))
AddStategraphActionHandler("pig", ActionHandler(ACTIONS.GIVE, "dropitem"))
AddStategraphActionHandler("pig", ActionHandler(ACTIONS.PICK, "ptribe_pick"))
AddStategraphActionHandler("pig", ActionHandler(ACTIONS.HARVEST, "ptribe_pick"))

local function go_to_idle(inst)
    inst.sg:GoToState("idle")
end

-- 采集
AddStategraphState("pig", State {
    name = "ptribe_pick",
    tags = { "chopping" },

    onenter = function(inst)
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("atk")
    end,

    timeline =
    {
        TimeEvent(13 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
    },

    events =
    {
        EventHandler("animover", go_to_idle),
    },
})

------------------------------------------------------------------------------------------------

AddStategraphState("pig", State {
    name = "charge_antic_pre",
    tags = { "moving", "charging", "busy", "atk_pre", "canrotate" },

    onenter = function(inst)
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("paw_pre")
    end,

    events =
    {
        EventHandler("animover", function(inst) inst.sg:GoToState("charge_antic_loop") end),
    },
})
AddStategraphState("pig", State {
    name = "charge_antic_loop",
    tags = { "moving", "charging", "busy", "atk_pre", "canrotate" },

    onenter = function(inst)
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("paw_loop", true)
        inst.sg:SetTimeout(1.5)
    end,

    ontimeout = function(inst)
        inst.sg:GoToState("charge_pre")
        inst:PushEvent("attackstart")
    end,
})
AddStategraphState("pig", State {
    name = "charge_pre",
    tags = { "busy", "charging", "moving", "running" },

    onenter = function(inst)
        inst.components.locomotor:RunForward()
        inst.AnimState:PlayAnimation("charge_pre")
    end,

    events =
    {
        EventHandler("animover", function(inst)
            inst.sg:GoToState("charge_loop")
        end),
    },
})
AddStategraphState("pig", State {
    name = "charge_loop",
    tags = { "charging", "moving", "running" },

    onenter = function(inst)
        inst.components.locomotor:RunForward()
        if not inst.AnimState:IsCurrentAnimation("charge_loop") then
            inst.AnimState:PlayAnimation("charge_loop", true)
            inst.components.locomotor:SetExternalSpeedMultiplier(inst, "ptribe_charge_loop", 2)
        end
    end,

    events =
    {
        EventHandler("animover", function(inst) inst.sg:GoToState("charge_loop") end),
    },
})

AddStategraphState("pig", State {
    name = "charge_pst",
    tags = { "canrotate", "idle" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.AnimState:PlayAnimation("charge_pst")
        -- 给冲撞加点效果怎么样？
        -- SpawnPrefab("explode_reskin").Transform:SetPosition(inst.Transform:GetWorldPosition())
    end,

    events =
    {
        EventHandler("animover", function(inst)
            inst.sg:GoToState("idle")
        end),
    },
})

AddStategraphState("pig", State {
    name = "charge_attack",
    tags = { "chargingattack" },

    onenter = function(inst)
        inst.components.combat:StartAttack()
        inst.components.locomotor:StopMoving()
        inst.AnimState:PlayAnimation("charge_atk")
        inst.SoundEmitter:PlaySound("dontstarve/pig/attack")
    end,

    timeline =
    {
        TimeEvent(12 * FRAMES, function(inst)
            inst.components.combat:DoAttack()
            inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_whoosh")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst) inst.sg:GoToState("attack") end),
    },
})

------------------------------------------------------------------------------------------------
--- 包含野猪冲刺处理
local function LocomoteEventFn(inst)
    local is_attacking = inst.sg:HasStateTag("attack") or inst.sg:HasStateTag("chargingattack")
    local is_busy = inst.sg:HasStateTag("busy")
    local is_moving = inst.sg:HasStateTag("moving")
    local is_running = inst.sg:HasStateTag("running") or inst.sg:HasStateTag("chargingattack")
    local should_charge = inst:HasTag("ChaseAndRam")
    local is_charging = inst.sg:HasStateTag("charging")

    if is_attacking or is_busy then return end

    local should_move = inst.components.locomotor:WantsToMoveForward()
    local should_run = inst.components.locomotor:WantsToRun()

    if is_moving and not should_move then
        --虽然在这里移除移速不优雅，但是charge_loop一直在快速循环，没法设置速度
        inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "ptribe_charge_loop")

        if is_charging then
            inst.sg:GoToState("charge_pst")
        elseif is_running then
            inst.sg:GoToState("run_stop")
        else
            inst.sg:GoToState("walk_stop")
        end
    elseif (not is_moving and should_move) or (is_moving and should_move and is_running ~= should_run) then
        if should_run then
            if should_charge then
                inst.sg:GoToState("charge_antic_pre")
            else
                inst.sg:GoToState("run_start")
            end
        else
            inst.sg:GoToState("walk_start")
        end
    end
end

AddStategraphPostInit("pig", function(sg)
    Utils.FnDecorator(sg.events["locomote"], "fn", function(inst)
        if inst.prefab == "ptribe_wildbore" then
            LocomoteEventFn(inst) --- 懒得合并了
            return nil, true
        end
    end)

    Utils.FnDecorator(sg.events["doattack"], "fn", function(inst)
        if inst.sg:HasStateTag("charging")
            and inst.components.health and not inst.components.health:IsDead()
            and not inst.sg:HasStateTag("busy") then
            inst.sg:GoToState("charge_attack")
            return nil, true
        end
    end)

    ------------------------------------------------------------------------------------------------

    -- Utils.FnDecorator(sg.states["attack"], "onenter", nil, function(retTab, inst)
    --     -- 激光
    --     local home = inst.components.homeseeker and inst.components.homeseeker.home
    --     if home and home.components.ptribe_pigability and home.components.ptribe_pigability.type == "terraria" then
    --         SpawnAt("ptribe_terrarium_fx", inst:GetPosition()).Transform:SetRotation(inst:GetRotation() + 270)
    --     end
    -- end)

    Utils.FnDecorator(sg.states["attack"].events["animover"], "fn", function(inst)
        local home = inst.components.homeseeker and inst.components.homeseeker.home
        if home and home.components.ptribe_pigability and home.components.ptribe_pigability.type == "space" then
            --空间猪人攻击完后闪现到远处
            local pos = inst:GetPosition()

            for i = 1, 5 do --最多只试5次
                local newPos = Shapes.GetRandomLocation(pos, 8, 12)
                if TheWorld.Map:IsAboveGroundAtPoint(newPos.x, newPos.y, newPos.z) then
                    SpawnAt("sand_puff_large_front", pos)
                    SpawnAt("sand_puff_large_back", newPos)

                    GetPrefab.SetPosition(inst, newPos.x, newPos.y, newPos.z)
                    inst.components.combat:ResetCooldown()
                    break
                end
            end
        end
    end)
end)
