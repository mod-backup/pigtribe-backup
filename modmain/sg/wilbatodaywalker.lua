local Utils = require("ptribe_utils/utils")
local Shapes = require("ptribe_utils/shapes")
local GetPrefab = require("ptribe_utils/getprefab")
local Timer = require("ptribe_utils/timer")

AddStategraphActionHandler("wilson", ActionHandler(ACTIONS.PTRIBE_WILBA_MONSTER_WORK,
    function(inst)
        return "attack_pounce_pre"
    end))

local function DoAOEAttack(inst, dist, radius, heavymult, mult, forcelanded, targets)
    GetPrefab.DoArcAttack(inst, {
        dist = dist,
        radius = radius,
        heavymult = heavymult,
        mult = mult,
        forcelanded = forcelanded,
        targets = targets,
        validfn = Utils.TrueFn,
    })
end

local function DoAOEWork(inst, dist, radius, targets)
    GetPrefab.DoAOEWork(inst, false, dist, radius, nil, targets)
end

--------------------------------------------------------------------------

local function DoRoarShake(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 1.4, .02, .2, inst, 30)
end

local function DoPounceShake(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .4, .02, .15, inst, 20)
end

local function DoDefeatShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .6, .025, .2, inst, 20)
end

--------------------------------------------------------------------------
--- 普通攻击阶段飞扑，在工作时也会进入
AddStategraphState("wilson", State {
    name = "attack_pounce_pre",
    tags = { "attack", "busy", "jumping" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.components.locomotor:EnableGroundSpeedMultiplier(false)
        inst.AnimState:PlayAnimation("run_pre")
        inst.SoundEmitter:PlaySound("daywalker/action/step")

        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target) --不用判断，不会报错
        inst.components.combat:StartAttack()

        if target ~= nil and target:IsValid() then
            inst:ForceFacePoint(target.Transform:GetWorldPosition())
        end

        inst.Physics:SetMotorVelOverride(11, 0, 0)
    end,

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg.statemem.pouncing = true
                inst.sg:GoToState("attack_pounce", inst.sg.statemem.speedmult)
            end
        end),
    },

    onexit = function(inst)
        if not inst.sg.statemem.pouncing then
            inst.Physics:ClearMotorVelOverride()
            inst.Physics:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
        end
    end,
})

AddStategraphState("wilson", State {
    name = "attack_pounce",
    tags = { "attack", "busy", "jumping", "nointerrupt", "notalksound" },

    onenter = function(inst, speedmult)
        inst.components.locomotor:EnableGroundSpeedMultiplier(false)
        -- StartAttackCooldown(inst)
        inst.AnimState:PlayAnimation("atk3")
        inst.SoundEmitter:PlaySound("daywalker/voice/speak_short")
        inst.SoundEmitter:PlaySound("daywalker/action/attack3")
        inst.sg.statemem.speedmult = speedmult or 1
        inst.Physics:SetMotorVelOverride(11 * inst.sg.statemem.speedmult, 0, 0)
    end,

    onupdate = function(inst)
        if inst.sg.statemem.speed ~= nil then
            inst.sg.statemem.speed = inst.sg.statemem.speed * 0.75
            inst.Physics:SetMotorVelOverride(inst.sg.statemem.speed * inst.sg.statemem.speedmult, 0, 0)
        end
        if inst.sg.statemem.collides ~= nil then
            DoAOEAttack(inst, 0.2, 1.4, nil, nil, nil, inst.sg.statemem.collides)
        end
    end,

    timeline =
    {
        FrameEvent(0, function(inst)
            inst.components.combat:SetDefaultDamage(TUNING.DAYWALKER_POUNCE_DAMAGE)
            inst.sg.statemem.collides = {}
        end),
        FrameEvent(5, function(inst)
            inst.sg.statemem.collides = nil
        end),
        FrameEvent(10, function(inst) inst.SoundEmitter:PlaySound("daywalker/action/step") end),
        FrameEvent(11, DoPounceShake),
        FrameEvent(12, function(inst)
            inst.sg:AddStateTag("pounce_recovery")
            inst.components.combat:SetDefaultDamage(TUNING.DAYWALKER_XCLAW_DAMAGE)
            local targets = {}
            inst.sg.statemem.targets = targets
            DoAOEWork(inst, 3, 3, targets)
            DoAOEWork(inst, 0.4, 1.6, targets)
            DoAOEAttack(inst, 3, 3, 1 + 0.1 * inst.sg.statemem.speedmult, 1 + 0.2 * inst.sg.statemem.speedmult, false,
                targets)
            DoAOEAttack(inst, 0.4, 1.6, 1, 1, false, targets)
            --local targets table; this code is valid even if we left state
            for k in pairs(targets) do
                if k:IsValid() and k:HasTag("smallcreature") then
                    targets[k] = nil
                end
            end
        end),
        FrameEvent(14, function(inst)
            inst.sg:RemoveStateTag("nointerrupt")
        end),
        FrameEvent(15, function(inst)
            inst.sg.statemem.speed = 8
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("attack_pounce_pst")
            end
        end),
    },

    onexit = function(inst)
        if not inst.sg.statemem.slam then
            inst.Physics:ClearMotorVelOverride()
            inst.Physics:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.components.combat:SetDefaultDamage(TUNING.DAYWALKER_DAMAGE)
        end
    end,
})

AddStategraphState("wilson", State {
    name = "attack_pounce_pst",
    tags = { "busy", "caninterrupt", "pounce_recovery" },

    onenter = function(inst)
        inst.AnimState:PlayAnimation("atk3_pst")
    end,

    timeline =
    {
        FrameEvent(1, function(inst)
            inst.sg:RemoveStateTag("busy")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },
})

----------------------------------------------------------------------------------------------------
AddStategraphState("wilson", State {
    name = "attack_slam_pre",
    tags = { "attack", "busy", "jumping" },

    onenter = function(inst)
        inst.components.locomotor:Stop()
        inst.components.locomotor:EnableGroundSpeedMultiplier(false)
        inst.AnimState:PlayAnimation("atk_slam_pre")

        inst.sg.statemem.speed = 3.5
        inst.sg.statemem.decel = -0.25
    end,

    onupdate = function(inst)
        if inst.sg.statemem.decel ~= 0 then
            if inst.sg.statemem.speed <= 0 then
                inst.Physics:ClearMotorVelOverride()
                inst.Physics:Stop()
                inst.sg.statemem.decel = 0
            else
                inst.Physics:SetMotorVelOverride(inst.sg.statemem.speed, 0, 0)
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.decel
            end
        end
    end,

    timeline =
    {
        FrameEvent(0, function(inst)
            inst.SoundEmitter:PlaySound("daywalker/voice/hurt")
            inst.SoundEmitter:PlaySound("daywalker/action/step", nil, 0.3)
        end),
        FrameEvent(18, function(inst)
            inst.sg.statemem.decel = 0
            inst.Physics:SetMotorVelOverride(1, 0, 0)
        end),
        FrameEvent(19, function(inst) inst.Physics:SetMotorVelOverride(2, 0, 0) end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg.statemem.slam = true
                inst.sg:GoToState("attack_slam", inst.sg.statemem.target)
            end
        end),
    },

    onexit = function(inst)
        if not inst.sg.statemem.slam then
            inst.Physics:ClearMotorVelOverride()
            inst.Physics:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
        end
    end,
})
AddStategraphState("wilson", State {
    name = "attack_slam",
    tags = { "attack", "busy", "jumping", "notalksound" },

    onenter = function(inst)
        inst.components.locomotor:EnableGroundSpeedMultiplier(false)
        -- StartAttackCooldown(inst)
        inst.AnimState:PlayAnimation("atk_slam")

        inst.sg.statemem.speedmult = 1
        inst.sg.statemem.speed = 2
        inst.sg.statemem.decel = 0
        inst.Physics:SetMotorVelOverride(2, 0, 0)
    end,

    onupdate = function(inst)
        if inst.sg.statemem.decel ~= 0 then
            if inst.sg.statemem.speed <= 0 then
                inst.Physics:ClearMotorVelOverride()
                inst.Physics:Stop()
                inst.sg.statemem.decel = 0
            else
                inst.Physics:SetMotorVelOverride(inst.sg.statemem.speed * inst.sg.statemem.speedmult, 0, 0)
                inst.sg.statemem.speed = inst.sg.statemem.speed + inst.sg.statemem.decel
            end
        end
    end,

    timeline =
    {
        FrameEvent(1, function(inst) inst.SoundEmitter:PlaySound("daywalker/action/attack_slam_whoosh") end),
        FrameEvent(2, function(inst)
            inst.SoundEmitter:PlaySound("daywalker/voice/attack_big")
            inst.SoundEmitter:PlaySound("daywalker/action/step")
        end),
        FrameEvent(25, function(inst) inst.SoundEmitter:PlaySound("daywalker/action/attack_slam_down") end),

        FrameEvent(1, function(inst) inst.Physics:SetMotorVelOverride(8, 0, 0) end),
        FrameEvent(6, function(inst) inst.Physics:SetMotorVelOverride(2, 0, 0) end),
        FrameEvent(8, function(inst) inst.Physics:SetMotorVelOverride(1.5, 0, 0) end),
        FrameEvent(20, function(inst)
            inst.sg.statemem.tracking = false
            inst.Physics:SetMotorVelOverride(3, 0, 0)
        end),
        FrameEvent(21, function(inst)
            if inst.sg.statemem.targetpos ~= nil then
                local x, y, z = inst.Transform:GetWorldPosition()
                local dx = inst.sg.statemem.targetpos.x - x
                local dz = inst.sg.statemem.targetpos.z - z
                if dx ~= 0 or dz ~= 0 then
                    local rot1 = math.atan2(-dz, dx)
                    local diff = DiffAngleRad(inst.Transform:GetRotation() * DEGREES, rot1)
                    if diff * RADIANS < 60 then
                        local dist = math.sqrt(dx * dx + dz * dz) * math.cos(diff)
                        local maxdist = (12 + 20 + 3 * 36) * FRAMES
                        inst.sg.statemem.speedmult = math.clamp(math.abs(dist / maxdist), 0.25, 1.2)
                    else
                        inst.sg.statemem.speedmult = 1
                    end
                else
                    inst.sg.statemem.speedmult = 0.25
                end
            end
            inst.Physics:SetMotorVelOverride(12 * inst.sg.statemem.speedmult, 0, 0)
        end),
        FrameEvent(22, function(inst) inst.Physics:SetMotorVelOverride(20 * inst.sg.statemem.speedmult, 0, 0) end),
        FrameEvent(23, function(inst) inst.Physics:SetMotorVelOverride(36 * inst.sg.statemem.speedmult, 0, 0) end),
        FrameEvent(24, function(inst)
            inst.sg:AddStateTag("nointerrupt")
        end),
        FrameEvent(26, function(inst)
            inst.sg.statemem.speed = 4
            inst.sg.statemem.decel = -1

            local x, y, z = inst.Transform:GetWorldPosition()
            local sinkhole = SpawnPrefab("daywalker_sinkhole")
            sinkhole.Transform:SetPosition(x, y, z)
            sinkhole:PushEvent("docollapse")
            inst.components.combat:SetDefaultDamage(TUNING.DAYWALKER_SLAM_DAMAGE)
            DoAOEAttack(inst, 0, TUNING.DAYWALKER_SLAM_SINKHOLERADIUS, 0.7, 0.7, false)

            Timer.PeriodicExecutor(inst, function(index)
                if index >= 2 then return end
                return 0.5 / (index + 1)
            end, function(index)
                local r = TUNING.DAYWALKER_SLAM_SINKHOLERADIUS
                local bigRadius = index * (2 * r + 0.5) -- 0.5为gap
                for _, p in ipairs(Shapes.SmallCircleCentersWithSpacing(bigRadius, r)) do
                    local sinkhole = SpawnPrefab("daywalker_sinkhole")
                    sinkhole.Transform:SetPosition(x + p[1], y, z + p[2])
                    sinkhole:PushEvent("docollapse")
                    -- inst.components.combat:SetDefaultDamage(TUNING.DAYWALKER_SLAM_DAMAGE) --我这里就不设置伤害了，我怕影响其他state的伤害计算
                    GetPrefab.DoAOEAttack(inst, Vector3(x + p[1], y, z + p[2]), {
                        range = r,
                        validfn = GetPrefab.ForceTargetTest2
                    })
                end
            end)
        end),
        FrameEvent(61, function(inst)
            inst.sg:RemoveStateTag("nointerrupt")
            inst.sg:AddStateTag("caninterrupt")
        end),
    },

    events =
    {
        EventHandler("animover", function(inst)
            if inst.AnimState:AnimDone() then
                inst.sg:GoToState("idle")
            end
        end),
    },

    onexit = function(inst)
        inst.Physics:ClearMotorVelOverride()
        inst.Physics:Stop()
        inst.components.locomotor:EnableGroundSpeedMultiplier(true)
        inst.components.combat:SetDefaultDamage(TUNING.DAYWALKER_DAMAGE)
    end,
})
