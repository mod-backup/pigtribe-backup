local Utils = require("ptribe_utils/utils")
local Shapes = require("ptribe_utils/shapes")
local GetPrefab = require("ptribe_utils/getprefab")

--懒得修改房子进出动作了，要修改跳入前置、跳入、跳出动画，还有声音
-- AddStategraphPostInit("wilson", function(sg)
--     Utils.FnDecorator(sg.states["jumpin"], "onenter", nil,function(retTab,inst, data)
--         -- 修改进入室内的动画
--         if data.teleporter and data.teleporter:HasTag("hamlet_teleport") then
--             inst.AnimState:PlayAnimation("give")
--         end
--     end)
-- end)

----------------------------------------------------------------------------------------------------

-- 使用消耗品道具
AddStategraphActionHandler("wilson", ActionHandler(ACTIONS.PTRIBE_USE_ITEM,
    function(inst, action)
        return action.invobject.components.ptribe_consumable.state
    end))
AddStategraphActionHandler("wilson_client", ActionHandler(ACTIONS.PTRIBE_USE_ITEM,
    function(inst, action)
        return action.invobject.components.ptribe_consumable.state
    end))

----------------------------------------------------------------------------------------------------
local function IsWere(inst)
    return inst.prefab == "wilba" and inst.components.ptribe_wilbatransform.were
end
local function GetWilbaType(inst)
    return inst.components.ptribe_wilbatransform.type
end

local function WereDefaultAnim(inst, volume)
    volume = volume or 0.5
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/bark")
    if inst.atk_2 == true then
        inst.AnimState:PlayAnimation("atk_2_werewilba")
        inst.atk_2 = nil
    else
        inst.AnimState:PlayAnimation("atk_werewilba")
        inst.atk_2 = true
    end
    inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_whoosh", nil, volume)
end

AddPrefabPostInit("wilba", function(inst)
    if not TheWorld.ismastersim then return end

    Utils.FnDecorator(inst.sg, "GoToState", function(self, statename, params)
        -- print("跳转", statename, IsWere(inst), IsWere(inst) and GetWilbaType(inst))
        if not IsWere(inst) then return end

        local cur = self.currentstate

        -- 排除法
        if GetWilbaType(inst) == "daywalker" then
            if statename == "doshortaction" or statename == "domediumaction" or statename == "dolongaction" or statename == "doaction" then
                return nil, false, { self, "ptribe_wilba_monster_doaction", params }
            elseif statename ~= "attack" and statename ~= "hit" and statename ~= "death"
                and statename ~= "run_start" and statename ~= "run" and statename ~= "run_stop"
                and statename ~= "attack_pounce_pre" and statename ~= "attack_pounce" and statename ~= "attack_pounce_pst"
                and statename ~= "attack_slam_pre" and statename ~= "attack_slam" then
                return nil, false, { self, "idle", params }
            end
        end
    end)

    -- 疯猪状态下不受地震特效减速影响
    Utils.FnDecorator(inst, "IsCarefulWalking", function(inst)
        return { false }, GetWilbaType(inst) == "daywalker"
    end)
    Utils.FnDecorator(inst.components.carefulwalker, "TrackTarget", function()
        return nil, GetWilbaType(inst) == "daywalker"
    end)
end)

AddStategraphPostInit("wilson", function(sg)
    Utils.FnDecorator(sg.actionhandlers[ACTIONS.CHOP], "deststate", function(inst)
        return { "ptribe_wilbamonster_work" }, IsWere(inst)
    end)
    Utils.FnDecorator(sg.actionhandlers[ACTIONS.MINE], "deststate", function(inst)
        return { "ptribe_wilbamonster_work" }, IsWere(inst)
    end)
    Utils.FnDecorator(sg.actionhandlers[ACTIONS.HAMMER], "deststate", function(inst)
        return { "ptribe_wilbamonster_work" }, IsWere(inst)
    end)
    Utils.FnDecorator(sg.actionhandlers[ACTIONS.DIG], "deststate", function(inst)
        return { "ptribe_wilbamonster_work" }, IsWere(inst)
    end)

    Utils.FnDecorator(sg.actionhandlers[ACTIONS.ATTACK], "deststate", function(inst)
        if not IsWere(inst) then return end

        if GetWilbaType(inst) == "daywalker" then
            return { "attack_pounce_pre" }, true --飞扑
        else
            return { "attack" }, true
        end
    end)

    ------------------------------------------------------------------------------------------------
    Utils.FnDecorator(sg.events["onhop"], "fn", function(inst)
        if not IsWere(inst) then return end
        if GetWilbaType(inst) == "daywalker" then --疯猪不能上下船
            return nil, true
        end
    end)





    ------------------------------------------------------------------------------------------------
    -- 变身时播放idle_sanity_loop动画
    Utils.FnDecorator(sg.states["idle"], "onenter", mil, function(retTab, inst, pushanim)
        if not IsWere(inst) then return end
        local anims = {}

        if GetWilbaType(inst) == "daywalker" then
            pushanim = false
            table.insert(anims, "idle")
        else
            table.insert(anims, "idle_sanity_pre")
            table.insert(anims, "idle_sanity_loop")
        end

        if #anims > 0 then
            if pushanim then
                for k, v in pairs(anims) do
                    inst.AnimState:PushAnimation(v, k == #anims)
                end
            else
                inst.AnimState:PlayAnimation(anims[1], #anims == 1)
                for k, v in pairs(anims) do
                    if k > 1 then
                        inst.AnimState:PushAnimation(v, k == #anims)
                    end
                end
            end
        end
    end)

    Utils.FnDecorator(sg.states["attack"], "onenter", nil, function(retTab, inst)
        if IsWere(inst) then
            WereDefaultAnim(inst)
        end
    end)

    Utils.FnDecorator(sg.states["run"], "onenter", nil, function(retTab, inst)
        if not IsWere(inst) then return end
        if GetWilbaType(inst) == "daywalker" then
        elseif inst.AnimState:IsCurrentAnimation("run_loop") then
            inst.AnimState:PlayAnimation("run_werewilba_loop", true)
        end
    end)

    Utils.FnDecorator(sg.states["changeoutsidewardrobe"], "onenter", nil, function(retTab, inst)
        if inst.components.ptribe_wilbatransform and inst.components.ptribe_wilbatransform.ready_to_transform2 then
            inst.AnimState:PlayAnimation("skin_change", false)                                        --直接播放第二个动画，去掉放回礼物的前置动画
            inst.AnimState:SetDeltaTimeMultiplier(inst.AnimState:GetCurrentAnimationNumFrames() / 67) --必须把62 * FRAMES的逻辑执行了
        end
    end)
end)
