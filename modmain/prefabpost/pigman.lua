local Utils = require("ptribe_utils/utils")
local GetPrefab = require("ptribe_utils/getprefab")
local ResourceConvert = require("ptribe_resourceconvert")

local NormalRetargetFn

-- 非雇佣状态下执行的工作不会有掉落物，直接计入资源
local function OnPigManWorking(inst, data)
    local target = data.target
    if target and target.components.workable.workleft <= 0 and not inst.components.follower:GetLeader() and target.components.lootdropper then
        local home = inst.components.homeseeker and inst.components.homeseeker:GetHome()
        if home and home.components.ptribe_pigmanager then
            local prefabs = target.components.lootdropper:GenerateLoot()
            local resource = ResourceConvert.GetResource(prefabs)
            home.components.ptribe_pigmanager:ManagerResourceDoDelta(resource)
        end
        GetPrefab.SetNoLoot(target)
    end
end

-- 捡的、收获的
local function OnPickUpItem(inst, data)
    if data.item
        and ResourceConvert.IsPickUpGarbage(data.item)
        and (not inst.ptribe_givePlayer or data.item.prefab ~= inst._pig_token_prefab) -- 猪人的礼物不能删了
    then
        inst:DoTaskInTime(0, function()
            if data.item:IsValid() and data.item.prefab then
                local manager = inst:GetTribeManager()
                if manager then
                    manager.components.ptribe_pigmanager:ResourceDoDelta(ResourceConvert.GetResource(data.item.prefab))
                end
                data.item:Remove()
            end
        end)
    end
end

-- 猪人吃的也转为资源
local function OnEat(inst, data)
    if TUNING.TRIBE_LUCK_GOLD_FROM_PIGMAN and math.random() < 0.3 then
        inst.ptribe_pig_lucky_gold = true
    end

    local manager = inst:GetTribeManager()
    if manager then
        local resource = ResourceConvert.GetResource(data.food.prefab)
        manager.components.ptribe_pigmanager:ResourceDoDelta(resource) --food应该不可能为nil
    end
end

local function StartFollowing(inst)
    local home = inst.components.homeseeker and inst.components.homeseeker:GetHome()
    if home and home.components.ptribe_pigmanager then
        home.components.ptribe_pigmanager:CancelTask() -- 被雇佣时取消任务
    end
end

-- 月圆之夜有管理者的猪人即便没回家不会变成疯猪
local function OnIsFullmoon(self, isfullmoon)
    if isfullmoon and self._task ~= nil then
        if self.inst.components.follower.leader then return end -- 有leader就变

        local manager = self.inst:GetTribeManager()
        if manager then
            self._task:Cancel()
            self._task = nil
        end
    end
end

-- 当猪人死亡时，不掉落部落生成的帽子
local function OnDeathDropEverything(self, ondeath, keepequip)
    if ondeath then
        for _, v in pairs(self.equipslots) do
            if v:HasTag("ptribe_pigmanbuild") then
                local dropped = self:DropItem(v, true, true)
                if dropped then
                    dropped:RemoveAllEventCallbacks() --移除所有监听回调，防止特殊帽子生成破损后的帽子，但是如果掉落时耐久就已经为0，则还是会生成的
                    dropped:Remove()
                end
            end
        end
    end
end

local function AddLoyaltyTimeBefore(self, time)
    if time > 0 and self.leader and self.leader:HasTag("player") then
        local manager = self.inst:GetTribeManager()
        if manager then
            return nil, false, { self, time + math.max(0, manager.components.ptribe_pigmanager.favorability * 2) }
        end
    end
end

-- 当猪人脱离加载范围并且离家很远时直接将其传送回家
local HOME_MAX_DIS_SQ = 30 * 30
local function OnPigEntitySleep(inst)
    local home = inst.components.homeseeker and inst.components.homeseeker:GetHome()
    if inst:HasTag("ptribe_tribeinvader")                                               --不传送部落入侵的侵略者
        or not home                                                                     --必须有家
        or not home.components.spawner or home.components.spawner:IsOccupied()
        or home.components.ptribe_pigmanager and home.components.ptribe_pigmanager.task --有任务的猪人不会回家
        or (inst.components.follower and inst.components.follower.leader)               --必须没有leader
        or inst:GetDistanceSqToInst(home) <= HOME_MAX_DIS_SQ                            --必须在允许范围外
    then
        return
    end

    home.components.spawner:GoHome(inst)
    home.components.spawner:ReleaseChild()
end

local PIG_MUST_TAGS = { "pig", "character", "_combat" }
local PIG_CANT_TAGS = { "werepig", "hostile", "INLIMBO" }
local function OnKilledOther(inst, data)
    local victim = data.victim
    -- 猪人击杀亮茄尖端后攻击本体
    if victim and victim.prefab == "lunarthrall_plant_vine_end" and victim.parentplant then
        -- inst:PushEvent("attacked", { attacker = victim.parentplant, damage = 0 })
        local x, y, z = inst.Transform:GetWorldPosition()
        for _, v in ipairs(TheSim:FindEntities(x, y, z, 20, PIG_MUST_TAGS, PIG_CANT_TAGS)) do
            local target = v.components.combat.target
            if not target or IsEntityDeadOrGhost(target) then
                v.components.combat:SetTarget(victim.parentplant)
            end
        end
    end
end

local function OnAttacked(inst, data)
    local attacker = data.attacker
    if attacker and attacker:HasTag("player")
        and not inst:HasTag("werepig") and not inst:HasTag("hostile") and not inst:HasTag("monster") then
        local manager = inst:GetTribeManager()
        if manager then
            manager.components.ptribe_pigmanager:FavorabilityDelta(-2)
        end
    end
end

local function StopFollowing(inst, data)
    local leader = data and data.leader
    if leader and leader:HasTag("player") and (not inst:IsValid() or IsEntityDead(inst)) then
        local manager = inst:GetTribeManager()
        if manager and manager.components.ptribe_pigmanager.favorability > -20 then
            manager.components.ptribe_pigmanager:FavorabilityDelta(-2) --减轻一点儿，猪人打boss很可能死伤惨重
        end
    end
end

local function GetAttackedBefore(self, ...)
    -- 其他猪人伤害转移
    local armorPig = self.inst.ptribe_absorbDamagePig
    if not self.inst.damageThreshold and armorPig and armorPig:IsValid()
        and not GetPrefab.TargetTestForAll(self.inst, armorPig) then
        armorPig.components.combat:GetAttacked(...)
        return nil, true
    end
end

--- 打猪人不会还手
local function SetTargetBefore(self, target)
    if not target or not target:IsValid()
        or not self.inst:HasTag("pig")
        or self.inst:HasTag("werepig")
        or self.inst:HasTag("hostile")
    then
        return
    end
    if target.prefab == "wilba" and not target:HasTag("monster") then
        return nil, true
        -- elseif target.prefab == "lunarthrall_plant" and not target.resttask then --亮茄恢复后的第一下攻击不会吸引仇恨，感觉有点儿违和先不加了
        --     return nil, true
    end
end

local attackPlayerDisSq = TUNING.PIG_TARGET_DIST * TUNING.PIG_TARGET_DIST
local function TargetFnAfter(retTab, inst)
    if next(retTab)
        or inst:HasTag("NPC_contestant")
        or inst:IsInLimbo()
    then
        return retTab
    end

    -- 如果好感度为负数，检测附近是否有玩家
    local manager = inst:GetTribeManager()
    if manager and manager.components.ptribe_pigmanager.favorability < TUNING.PTRIBE_PIG_FAVORABILITY_ATTACK then
        local x, _, z = inst.Transform:GetWorldPosition()
        for _, v in ipairs(AllPlayers) do
            local vx, _, vz = v.Transform:GetWorldPosition()
            if VecUtil_DistSq(x, z, vx, vz) < attackPlayerDisSq then
                return { v }
            end
        end
    end
end

local function KeepTargetFnBefore(inst, target)
    -- 当亮茄从虚弱状态恢复时不再攻击
    if target and target.prefab == "lunarthrall_plant" and not target.resttask and inst.components.combat.attackrange < 6 then
        return { false }, true
    end
end

local function DoDeltaBefore(self, amount, ...)
    -- 铁甲猪人伤害上限（还没计算30%的减免）
    if amount >= 0 or not self.inst.damageThreshold then return end
    if self.inst.damageThreshold >= TUNING.TRIBE_ARMOR_DAMAGE_THRESHOLD then
        return nil, true
    end

    local newAmount = math.max(amount, self.inst.damageThreshold - TUNING.TRIBE_ARMOR_DAMAGE_THRESHOLD)
    self.inst.damageThreshold = math.min(self.inst.damageThreshold - amount, TUNING.TRIBE_ARMOR_DAMAGE_THRESHOLD)
    return nil, false, { self, newAmount, ... }
end

--参考鱼人，根据优先级选择
local FOLLOW_PIG_MUST_TAGS = { "character", "pig" }
local FOLLOW_PIG_CANT_TAGS = { "werepig", "hostile" }
local function OnTrade(inst, data)
    local giver = data.giver
    if not giver or not data.item then return end

    -- 提高部落好感度
    if giver:HasTag("player") then
        local manager = inst:GetTribeManager()
        if manager then
            local resource = ResourceConvert.GetResource(data.item.prefab)
            manager.components.ptribe_pigmanager:FavorabilityDelta(resource / 4)
        end
    end

    if giver.prefab == "wilba"
        and data.item.components.edible
        and (data.item.components.edible.foodtype == FOODTYPE.MEAT
            or data.item.components.edible.foodtype == FOODTYPE.HORRIBLE)
    then
        local otherpigs = GetPrefab.GetOtherEnts(inst, {
            radius = 16,
            maxcount = TUNING.PTRIBE_WILBA_PIG_COUNT,
            MUST_TAGS = FOLLOW_PIG_MUST_TAGS,
            CANT_TAGS = FOLLOW_PIG_CANT_TAGS
        })
        for _, pig in ipairs(otherpigs) do
            if giver.components.minigame_participator == nil then
                giver:PushEvent("makefriend")
                giver.components.leader:AddFollower(pig)
            end
            pig.components.follower:AddLoyaltyTime(data.item.components.edible:GetHunger() *
                TUNING.PIG_LOYALTY_PER_HUNGER)
            pig.components.follower.maxfollowtime = TUNING.PIG_LOYALTY_MAXTIME +
                TUNING.PIG_LOYALTY_POLITENESS_MAXTIME_BONUS
        end
    end
end

--- 获取部落管理者
local function GetTribeManager(inst)
    if inst._ptribe_manager and inst._ptribe_manager:IsValid() then
        return inst._ptribe_manager
    end

    local home = inst.components.homeseeker and inst.components.homeseeker:GetHome()
    local manager = home and home.components.ptribe_pigmanager and home.components.ptribe_pigmanager:GetManager()
    inst._ptribe_manager = manager and manager.components.ptribe_pigmanager:IsValidManager() and manager or nil
    return inst._ptribe_manager
end



AddPrefabPostInit("pigman", function(inst)
    inst:AddComponent("ptribe_pigtask")
    inst.components.ptribe_pigtask:SetHeight(220)

    if not TheWorld.ismastersim then return end

    inst.ptribe_task = nil           --猪人任务，对于没有猪人房的猪人执行的任务，优先级高于部落的任务
    inst.ptribe_givePlayer = nil     --要给予礼物的玩家
    inst.prtibe_giveTime = GetTime() --给予道具的时间
    inst._ptribe_manager = nil
    inst.GetTribeManager = GetTribeManager

    --存储任务创建对象需要，虽然我很想把这个组件添加到玩家身上，但是加到玩家身上不会调用LoadPostPass，也就读取不了数据了
    if not inst.components.entitytracker then
        inst:AddComponent("entitytracker")
    end

    -- Utils.FnDecorator(inst.components.werebeast, "onsetwerefn", function()
    -- end)
    -- 虽然我很不想通过监听的方式修改同监听回调的内容，但是只能这里获取了，普通猪人不受影响，但是猪人守卫不管
    inst.components.werebeast:WatchWorldState("isfullmoon", OnIsFullmoon)

    if not NormalRetargetFn then
        NormalRetargetFn = inst.components.combat.targetfn
    end
    Utils.FnDecorator(inst.components.combat, "GetAttacked", GetAttackedBefore)
    Utils.FnDecorator(inst.components.combat, "SetTarget", SetTargetBefore)
    Utils.FnDecorator(inst.components.combat, "targetfn", nil, TargetFnAfter)
    Utils.FnDecorator(inst.components.combat, "keeptargetfn", KeepTargetFnBefore)

    Utils.FnDecorator(inst.components.health, "DoDelta", DoDeltaBefore)

    Utils.FnDecorator(inst.components.inventory, "DropEverything", OnDeathDropEverything)

    Utils.FnDecorator(inst.components.follower, "AddLoyaltyTime", AddLoyaltyTimeBefore)

    inst:ListenForEvent("working", OnPigManWorking)
    inst:ListenForEvent("gotnewitem", OnPickUpItem)
    inst:ListenForEvent("oneat", OnEat)
    inst:ListenForEvent("startfollowing", StartFollowing)
    inst:ListenForEvent("stopfollowing", StopFollowing)
    inst:ListenForEvent("trade", OnTrade)
    inst:ListenForEvent("entitysleep", OnPigEntitySleep)
    inst:ListenForEvent("killed", OnKilledOther)
    inst:ListenForEvent("attacked", OnAttacked)
end)


----------------------------------------------------------------------------------------------------
-- 公主的猪人守卫不攻击玩家
local function GuardTargetFnBefore(inst)
    local home = inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
    if not home or not home:HasTag("wilbastructure") or not NormalRetargetFn then return end

    return { NormalRetargetFn(inst) }, true
end

local pigguardDirty = true
-- 猪人守卫不主动攻击猪人
AddPrefabPostInit("pigguard", function(inst)
    if not TheWorld.ismastersim then return end

    if pigguardDirty then
        pigguardDirty = nil --我不知道怎么用require获取unpack的预制体，只能使用这种方式修改一次了
        local RETARGET_GUARD_CANT_TAGS = Utils.ChainFindUpvalue(inst.components.combat.targetfn,
            "RETARGET_GUARD_CANT_TAGS")
        if RETARGET_GUARD_CANT_TAGS ~= nil then
            table.insert(RETARGET_GUARD_CANT_TAGS, "pig")
            table.insert(RETARGET_GUARD_CANT_TAGS, "pigroyalty") --不攻击皇家成员
        end
    end

    Utils.FnDecorator(inst.components.combat, "targetfn", GuardTargetFnBefore)
end)
