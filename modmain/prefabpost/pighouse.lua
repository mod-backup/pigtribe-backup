local Utils = require("ptribe_utils/utils")
local ResourceConvert = require("ptribe_resourceconvert")
local BluePrintsUtils = require("ptribe_blueprintsutils")

local LABEL_FORMAT = PTRIBE_LAGUAGE_LOC("Favorability : %d\nResource : %d", "好感度：%d\n资源量：%d")
local function UpdateLabel(inst)
    if not inst.Label then return end
    if inst:HasTag("burnt") then
        inst.Label:SetText("") --不知道怎么移除
        return
    end

    inst.Label:SetText(string.format(LABEL_FORMAT,
        inst.replica.ptribe_pigmanager:GetFavorability(),
        inst.replica.ptribe_pigmanager:GetResource()
    ))
end

-- 客户端初始化个管理者，每个管理者只执行一次
local function InitManager(inst)
    if inst.replica.ptribe_pigmanager:GetBlueprint() == "" then return end

    -- CreateFlooring(inst)
    -- inst.name = STRINGS.NAMES.PIGHOUSE .. "(" .. inst.ptribe_blueprint:value() .. ")"
    -- 显示资源
    if not inst.Label then
        inst.entity:AddLabel()
    end
    inst.Label:SetFontSize(24)
    inst.Label:SetFont(BODYTEXTFONT)
    inst.Label:SetWorldOffset(0, 0, 0)
    inst.Label:SetColour(1, 1, 1)
    inst.Label:Enable(true)
    UpdateLabel(inst)
end

----------------------------------------------------------------------------------------------------

--- 显示蓝图范围
local function OnEnableHelper(inst, enabled)
    local disSq = BluePrintsUtils.GetMaxDisSq(inst.replica.ptribe_pigmanager:GetBlueprint())
    if enabled and disSq > 0 then
        if inst.helper == nil then
            inst.helper = CreateEntity()

            --[[Non-networked entity]]
            inst.helper.entity:SetCanSleep(false)
            inst.helper.persists = false

            inst.helper.entity:AddTransform()
            inst.helper.entity:AddAnimState()

            inst.helper:AddTag("CLASSIFIED")
            inst.helper:AddTag("NOCLICK")
            inst.helper:AddTag("placer")

            -- 默认半径是6
            local scale = math.sqrt(disSq) / 6
            -- inst.helper.Transform:SetScale(scale, scale, scale) --放大的倍率有点儿大了
            inst.helper.AnimState:SetScale(scale, scale)

            inst.helper.AnimState:SetBank("firefighter_placement")
            inst.helper.AnimState:SetBuild("firefighter_placement")
            inst.helper.AnimState:PlayAnimation("idle")
            inst.helper.AnimState:SetLightOverride(1)
            inst.helper.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
            inst.helper.AnimState:SetLayer(LAYER_BACKGROUND)
            inst.helper.AnimState:SetSortOrder(1)
            inst.helper.AnimState:SetAddColour(0, .2, .5, 0)

            inst.helper.entity:SetParent(inst.entity)
        end
    elseif inst.helper ~= nil then
        inst.helper:Remove()
        inst.helper = nil
    end
end

----------------------------------------------------------------------------------------------------

local TYPES = {
    -- "terraria", --泰拉猪人，有人反馈太卡、特效太显眼
    -- "nurse", --护士猪人，回血配合铁甲太离谱了
    "kungfu",
    "blowdart",
    "armor",
    "blastMite",
    "space",
    "wildbore",
    "poop"
}

local function ChildFn(inst)
    local self = inst.components.ptribe_pigability
    self.type = "none" --初始化标记，表面已经初始化过了
    if TUNING.TRIBE_SPECIAL_PIGMAN_GENERATION_MULT <= 0 then return end

    -- 根据房子皮肤决定猪人类型
    local bank = inst.components.ptribe_saveskin and inst.components.ptribe_saveskin.enable
        and inst.components.ptribe_saveskin.data and inst.components.ptribe_saveskin.data.bank
    if bank == "pig_house_tropical" then
        self.type = "wildbore"
        return "ptribe_wildbore"
    end

    -- 特殊能力，每次生成都重置
    -- 特殊猪人的强度太高了，我这里只是降低了特殊猪人的生成概率，也行应该找画师画几个道具，道具来让猪人房生成指定猪人
    local completeRate = inst.components.ptribe_pigmanager:GetCompleteRate()
    if math.random() < completeRate * 0.6 * TUNING.TRIBE_SPECIAL_PIGMAN_GENERATION_MULT then
        self.type = TYPES[math.random(1, #TYPES)]
    end
    if self.type == "kungfu" then
        return "ptribe_pigelitefighter" .. math.random(1, 4)
    end

    -- self.type = "poop"
end

-- hats下的帽子，排除了一些有副作用的，还有特别超模的
local HATS = { "strawhat", "tophat", "beefalohat", "featherhat", "beehat", "minerhat", "footballhat",
    "earmuffshat", "winterhat", "bushhat", "flowerhat", "walrushat", "molehat", "wathgrithrhat",
    "wathgrithr_improvedhat", "walterhat", "icehat", "rainhat", "catcoonhat", "watermelonhat", "eyebrellahat",
    "red_mushroomhat", "green_mushroomhat", "blue_mushroomhat", "dragonheadhat", "dragonbodyhat",
    "dragontailhat", "deserthat", "goggleshat", "moonstorm_goggleshat", "kelphat",
    "cookiecutterhat", "batnosehat", "plantregistryhat", "balloonhat",
    "monkey_mediumhat", "monkey_smallhat", "polly_rogershat", "nightcaphat", "woodcarvedhat", }

local function OnSpawnedFn(inst, child)
    local resource = inst.components.ptribe_pigmanager:GetManagerResource()
    if resource > 20 and math.random() < (resource * resource / 10000) then
        local hat = SpawnPrefab(HATS[math.random(1, #HATS)]) --随机帽子，可能有nb的帽子
        hat:AddTag("ptribe_pigmanbuild")                     --加上特殊标签
        child.components.inventory:Equip(hat)
        child.AnimState:Show("hat")
        inst.components.ptribe_pigmanager:ManagerResourceDoDelta(-(math.max(15, ResourceConvert.GetResource(hat.prefab)))) --最少需要15
    end
end

local function onignite(inst)
    inst.components.sleepingbag:DoWakeUp()
end
local function onsleep(inst, sleeper)
    -- 只有薇尔芭能在猪人房睡觉
    if sleeper.prefab ~= "wilba" then
        sleeper:DoTaskInTime(0, function()
            sleeper:PushEvent("performaction", { action = sleeper.bufferedaction })
            sleeper:ClearBufferedAction()
            sleeper.AnimState:PlayAnimation("pickup_pst")
            sleeper.sg:GoToState("idle", true)
        end)
        return
    end

    sleeper:ListenForEvent("onignite", onignite, inst)
end
local function onwake(inst, sleeper)
    sleeper:RemoveEventCallback("onignite", onignite, inst)
end
local function temperaturetick(inst, sleeper)
    if sleeper.components.temperature ~= nil then
        if sleeper.components.temperature:GetCurrent() > 40 then
            sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() -
                TUNING.SLEEP_TEMP_PER_TICK)
        elseif sleeper.components.temperature:GetCurrent() < 10 then
            sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() +
                TUNING.SLEEP_TEMP_PER_TICK)
        end
    end
end

local function onbuilt(inst)
    inst.components.ptribe_pigmanager:Init() --我需要提前查找附近管理器，因为第一只猪人生成的时候还没有查找管理者
    inst.ptribe_built = true                 --玩家建造的猪人房不换皮，这个数据不用保存
end

local function TakeOwnershipAfter(retTab, self, child)
    if not self.inst.ptribe_pigBbilityInitTask then
        self.inst.ptribe_pigBbilityInitTask = self.inst:DoTaskInTime(0, function() --我需要一个延迟才能改名、获取部落完成率
            self.inst.components.ptribe_pigability:InitChild(child)
            self.inst.ptribe_pigBbilityInitTask = nil
        end)
    end

    return retTab
end

AddPrefabPostInit("pighouse", function(inst)
    inst:AddTag("tent")

    if not TheNet:IsDedicated() then
        inst:AddComponent("deployhelper")
        inst.components.deployhelper.onenablehelper = OnEnableHelper

        inst:ListenForEvent("ptribe_pigmanager.blueprint", InitManager)
        inst:ListenForEvent("ptribe_pigmanager.resource", UpdateLabel)
        inst:ListenForEvent("ptribe_pigmanager.favorability", UpdateLabel)
    end

    if not TheWorld.ismastersim then return end

    -- inst:AddComponent("named")
    inst:AddComponent("timer")
    inst:AddComponent("entitytracker")
    inst:AddComponent("ptribe_pigmanager") --部落管理
    inst:AddComponent("ptribe_pigability") --猪人强化
    inst:AddComponent("ptribe_saveskin")

    -- 薇尔芭可以睡觉
    inst:AddComponent("sleepingbag")
    inst.components.sleepingbag.onsleep = onsleep
    inst.components.sleepingbag.onwake = onwake
    inst.components.sleepingbag.health_tick = TUNING.SLEEP_HEALTH_PER_TICK * 4
    --convert wetness delta to drying rate
    inst.components.sleepingbag.dryingrate = math.max(0, -TUNING.SLEEP_WETNESS_PER_TICK / TUNING.SLEEP_TICK_PERIOD)
    inst.components.sleepingbag:SetTemperatureTickFn(temperaturetick)

    inst.components.spawner.childfn = ChildFn
    Utils.FnDecorator(inst.components.spawner, "onspawnedfn", OnSpawnedFn)
    --生成时初始化、加载时初始化、还有第一只猪人的初始化
    Utils.FnDecorator(inst.components.spawner, "TakeOwnership", nil, TakeOwnershipAfter)

    inst:ListenForEvent("onbuilt", onbuilt)
end)
