local Utils = require("ptribe_utils/utils")
local BluePrintsUtils = require("ptribe_blueprintsutils")
local Timer = require("ptribe_utils/timer")

local function FixCamera(inst)
    local target = GetClosestInstWithTag("hamlet_housefloor", inst, 30)
    if not target then return false end

    TheCamera.controllable = false
    TheCamera.cutscene = true
    TheCamera.headingtarget = 0
    TheCamera.distancetarget = 21.5
    TheCamera:SetTarget(target)
    TheCamera.targetoffset = Vector3(2, 1.5, 0)
    return true
end

-- 写得很狼狈，但是我真的不知道怎么解决这个问题，也许不该使用playerprox这个组件，好像有的玩家没有离开房间也会调用
local function RecoverCamera(inst, time)
    local target = GetClosestInstWithTag("hamlet_housefloor", inst, 30)
    if target then return false end

    if not TheFocalPoint then return false end

    local x, y, z = TheFocalPoint.Transform:GetWorldPosition()
    if not x then
        if time >= 2 then
            -- 最后一次
            x, y, z = ThePlayer.Transform:GetWorldPosition()
        else
            return false
        end
    end

    TheCamera:SetDefault()
    TheCamera:SetTarget(TheFocalPoint)
    return true
end

-- 客户端调整摄像机
local function OnDirtyEventCameraStuff(inst)
    local val = inst.ptribe_playerCameraMode:value()

    if val == 1 then
        TheCamera.controllable = false
        TheCamera.cutscene = true
        TheCamera.headingtarget = 0
        TheCamera.distancetarget = 20
        TheCamera.targetoffset = Vector3(-2.3, 1.7, 0)
    elseif val == 3 then
        -- 离开房间
        Timer.TryDoTask(inst, "RecoverCamera", RecoverCamera, nil, { 0.2, 0.5, 1, 2, 3 }, true)
    elseif val == 4 then
        -- 进入房间时
        --有时候进入者视角卡在地图某个点上，我在这里多次分时判断，希望能解决这个问题
        Timer.TryDoTask(inst, "FixCamera", FixCamera, nil, { 0.2, 0.5, 1, 2 }, true)
    end
end

--懒得找防寒隔热的组件了，直接覆盖onupdate更省事
local function OnTemperatureUpdateBefore(self)
    if self.inst:HasTag("ptribe_inhouse") then
        local cur = self:GetCurrent()
        if cur > 30 then
            self:SetTemperature(self.current - 0.1)
        elseif cur < 20 then
            self:SetTemperature(self.current + 0.1)
        end

        return nil, true
    end
end

local function OnMoistureUpdateBefore(self)
    if self.inst:HasTag("ptribe_inhouse") then
        if self.moisture > 0 then
            self:DoDelta(-0.1)
        end
        return nil, true
    end
end

-- PlayFootstep函数使用，我需要玩家在室内走路时有声音，但是因为没有地皮，只能覆盖函数，这里使用木板地皮的声音
local function GetCurrentTileTypeBefore(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    if z > 950 then
        return { WORLD_TILES.WOODFLOOR, GetTileInfo(WORLD_TILES.WOODFLOOR) }, true
    end
end

local SHARE_TARGET_DIST = 30
local MAX_TARGET_SHARES = 5
local function IsNonWerePig(dude)
    return dude:HasTag("pig") and not dude:HasTag("werepig")
end
local function OnAttacked(inst, data)
    local attacker = data.attacker
    local manager = inst.components.ptribe_entnearby:FindEntityByPrefab("ptribe_nearby_manager", "pighouse")
    if not attacker or not attacker.components.health or attacker.components.health:IsDead()
        or not data.damage or data.damage <= 0 --没伤害就不召唤
        or not manager or manager.components.ptribe_pigmanager.favorability < 80
    then
        return
    end

    -- 猪人帮忙
    if not attacker:HasTag("pig") or attacker:HasTag("hostile") then
        inst.components.combat:ShareTarget(attacker, SHARE_TARGET_DIST, IsNonWerePig, MAX_TARGET_SHARES)
    end
end

local function DropItem(inst, data)
    local item = data and data.item
    -- 丢弃大便
    if item and (item.prefab == "poop" or item.prefab == "guano") then
        local manager = inst.components.ptribe_entnearby:FindEntityByPrefab("ptribe_nearby_manager", "pighouse")
        if manager then
            manager.components.ptribe_pigmanager:FavorabilityDelta(-5)
        end
    end
end

local function GotNewItem(inst, data)
    local item = data and data.item
    -- 拾取大便
    if item and (item.prefab == "poop" or item.prefab == "guano") then
        local manager = inst.components.ptribe_entnearby:FindEntityByPrefab("ptribe_nearby_manager", "pighouse")
        if manager then
            manager.components.ptribe_pigmanager:FavorabilityDelta(2)
        end
    end
end

local function ManagerTestFn(inst, ent)
    local manager = ent.components.ptribe_pigmanager and ent.components.ptribe_pigmanager:IsValidManager() and ent or nil
    return manager and inst:GetDistanceSqToInst(manager) <= manager.components.ptribe_pigmanager:GetMaxDisSq()
end

local function GetMaxDisSq(inst, ent)
    if ent.components.ptribe_pigmanager then
        return ent.components.ptribe_pigmanager:GetMaxDisSq()
    end

    return 16
end

local function DelayInit(inst)
    -- 初始主机同步一下蓝图启用数据
    if inst.userid and inst.userid ~= "" then
        local enables = {}
        for name, d in pairs(BluePrintsUtils.GetBlueprintBaseInfo()) do
            enables[name] = d.enable or false
        end
        SendModRPCToClient(GetClientModRPC("PigmanTribe", "SetBlueprintEnable"), inst.userid, json.encode(enables))
    end
end

AddPlayerPostInit(function(inst)
    inst.ptribe_playerCameraMode = net_tinybyte(inst.GUID, "BakuStuffNetStuff", "DirtyEventCameraStuff")

    if not TheNet:IsDedicated() then
        inst:ListenForEvent("DirtyEventCameraStuff", OnDirtyEventCameraStuff)
    end

    if not TheWorld.ismastersim then return end

    inst:DoTaskInTime(0, DelayInit)

    inst:AddComponent("ptribe_playertask")

    inst:AddComponent("ptribe_entnearby")
    -- 附近部落
    inst.components.ptribe_entnearby:AddMark("ptribe_nearby_manager", {
        period = 5,
        targetPrefabs = { pighouse = true },
        maxDisSq = GetMaxDisSq,
        radius = TUNING.TRIBE_MAX_DIS,
        testFn = ManagerTestFn
    })

    Utils.FnDecorator(inst.components.temperature, "OnUpdate", OnTemperatureUpdateBefore)
    Utils.FnDecorator(inst.components.moisture, "OnUpdate", OnMoistureUpdateBefore)
    Utils.FnDecorator(inst, "GetCurrentTileType", GetCurrentTileTypeBefore)

    inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("dropitem", DropItem)
    inst:ListenForEvent("gotnewitem", GotNewItem)
end)
