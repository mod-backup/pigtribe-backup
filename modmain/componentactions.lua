local Constructor = require("ptribe_utils/constructor")
Constructor.SetEnv(env)

AddComponentAction("SCENE", "trader", function(inst, doer, actions, right)
    if not right then return end
    if inst.prefab == "pigman" then
        if inst.replica.combat:GetTarget() == nil --非战斗下
            and inst.components.ptribe_pigtask and inst.components.ptribe_pigtask.taskId:value() ~= 0 then
            --查看任务
            table.insert(actions, ACTIONS.PTRIBE_VIEW_TASK)
        end
    elseif inst:HasTag("pig") and inst:HasTag("pigelite") then
        --跟功夫猪回家
        table.insert(actions, ACTIONS.PTRIBE_FOLLOW_PIG_HOME)
    end
end)
AddComponentAction("SCENE", "shelfer", function(inst, doer, actions, right)
    if inst:HasTag("cost_one_oinc") and inst:HasTag("playercrafted")
        and inst:HasTag("ptribe_slot_one") then
        table.insert(actions, ACTIONS.PTRIBE_TAKE)
    end
end)

-- 薇尔芭
local WILBA_TOOLS = {
    CHOP = true,
    DIG = true,
    HAMMER = true,
    MINE = true,
}
AddComponentAction("SCENE", "workable", function(inst, doer, actions, right)
    if doer.prefab == "wilba" and doer:HasTag("monster")
        and not inst:HasTag("INLIMBO") then
        for k, _ in pairs(WILBA_TOOLS) do
            if inst:HasTag(k .. "_workable") then
                --薇尔芭变疯猪后可以执行的动作
                table.insert(actions, ACTIONS[k])
            end
        end
    end
end)

AddComponentAction("USEITEM", "equippable", function(inst, doer, target, actions, right)
    if target:HasTag("trader") then
        if target:HasTag("guard")
            and target.replica.follower ~= nil and target.replica.follower:GetLeader() == doer
            and inst.replica.equippable:EquipSlot() then
            -- 替换猪人守卫身上的装备
            table.insert(actions, ACTIONS.PTRIBE_REPLACE_TRADE)
        end
    end
end)

AddComponentAction("USEITEM", "inventoryitem", function(inst, doer, target, actions, right)
    if inst:HasTag("ptribe_upgrade") and target:HasTag("ptribe_upgradable") then
        -- 升级
        table.insert(actions, ACTIONS.PTRIBE_UPGRADE)
    elseif target:HasTag("cost_one_oinc") then
        if target:HasTag("playercrafted")
            and not target:HasTag("ptribe_slot_one") then
            -- 物品放入柜中
            table.insert(actions, ACTIONS.PTRIBE_PLACE)
        end
    elseif target:HasTag("OnFloor") then
        if inst.prefab == "beefalowool" then
            if right then
                -- 旋转地毯
                table.insert(actions, ACTIONS.PTRIBE_ROTATE)
            else
                -- 合并地毯
                table.insert(actions, ACTIONS.PTRIBE_MERGE)
            end
        end
    end
end)

AddComponentAction("USEITEM", "renovator", function(inst, doer, target, actions, right)
    if target:HasTag("renovatable") then
        -- 翻新房屋
        table.insert(actions, ACTIONS.RENOVATE)
    end
end)
AddComponentAction("USEITEM", "roombuilder", function(inst, doer, target, actions, right)
    if target:HasTag("predoor") then
        -- 建造房屋
        table.insert(actions, ACTIONS.BUILD_ROOM)
    end
end)

----------------------------------------------------------------------------------------------------
local function CheckConsumable(inst, doer, target, actions)
    local com = inst.components.ptribe_consumable
    if com
        and inst:HasTag("ptribe_consumable")
        and (not com.userCheckFn or com.userCheckFn(inst, doer, target))
        and (not com.targetCheckFn or com.targetCheckFn(inst, doer, target))
    then
        table.insert(actions, ACTIONS.PTRIBE_USE_ITEM)
    end
end

AddComponentAction("INVENTORY", "ptribe_consumable", function(inst, doer, actions, right)
    CheckConsumable(inst, doer, doer, actions)
end)
AddComponentAction("USEITEM", "ptribe_consumable", function(inst, doer, target, actions, right)
    CheckConsumable(inst, doer, target, actions)
end)
