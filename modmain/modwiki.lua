local Constructor = require("ptribe_utils/constructor")
Constructor.SetEnv(env)


local function GetDefaultItem()
    return {
        build = "pigman_tribe",
        bank = "pigman_tribe",
        anim = "idle",
        animoffsetbgx = 80,
        animoffsetbgy = 60,
    }
end

local hamlet = "images/inventoryimages/hamletinventoryimages.xml"
Constructor.AddScrapbookWiki("pigmanTribe", {
    ptribe_wiki_pigmantribe = GetDefaultItem(),
    ptribe_wiki_blueprint = GetDefaultItem(),
    ptribe_wiki_tribecompleterate = GetDefaultItem(),
    ptribe_wiki_resource = GetDefaultItem(),
    ptribe_wiki_tribefavorability = GetDefaultItem(),
    ptribe_wiki_tribemanager = GetDefaultItem(),
    ptribe_wiki_pigproperty = GetDefaultItem(),
    ptribe_wiki_pigability = GetDefaultItem(),
    ptribe_wiki_piginteract = GetDefaultItem(),
    ptribe_wiki_pigtask = GetDefaultItem(),
    ptribe_wiki_events = GetDefaultItem(),
    wilba = {
        atlas = "images/avatars/avatar_wilba.xml",
        tex = "avatar_wilba.tex",
        build = "wilba",
        bank = "wilson",
        anim = "idle",
        deps = { "silvernecklace", "pig_guard_tower_palace" },
    },
    silvernecklace = {
        atlas = hamlet,
        tex = "silvernecklace.tex",
        build = "silvernecklace",
        bank = "silvernecklace",
        anim = "silvernecklace",
        dapperness = TUNING.DAPPERNESS_SMALL,
        fueledtype1 = FUELTYPE.USAGE,
        fueledmax = TUNING.FEATHERHAT_PERISHTIME
    },
    construction_permit = {
        atlas = hamlet,
        prefab = "construction_permit",
        tex = "construction_permit.tex",
        build = "permit_reno",
        bank = "permit_reno",
        anim = "idle",
        burnable = true,
    },
    deed = {
        atlas = hamlet,
        prefab = "deed",
        tex = "deed.tex",
        build = "deed",
        bank = "deed",
        anim = "idle",
        burnable = true, --是否可燃
    },
    waterdrop = {
        atlas = "images/inventoryimages/hamletinventoryimages_2.xml",
        tex = "waterdrop.tex",
        prefab = "waterdrop",
        build = "waterdrop",
        bank = "waterdrop",
        anim = "idle",
        deps = { "lifeplant" },
        foodtype = "VEGGIE",
        hungervalue = TUNING.CALORIES_SUPERHUGE * 3,
        healthvalue = TUNING.HEALING_SUPERHUGE * 3,
        sanityvalue = TUNING.SANITY_HUGE * 3,
    },
    pugalisk_fountain = {
        atlas = "images/inventoryimages/python_fountain.xml",
        tex = "python_fountain.tex",
        prefab = "pugalisk_fountain",
        build = "python_fountain",
        bank = "fountain",
        anim = "flow_loop",
    },
    lifeplant = {
        atlas = hamlet,
        tex = "lifeplant.tex",
        prefab = "lifeplant",
        build = "lifeplant",
        bank = "lifeplant",
        anim = "idle_loop",
        finiteuses = 2000
    },
    ptribe_carnival_prizebooth_kit = {
        atlas = "images/inventoryimages1.xml",
        tex = "carnival_prizebooth_kit.tex",
        prefab = "ptribe_carnival_prizebooth_kit",
        build = "carnival_prizebooth",
        bank = "carnival_prizebooth",
        anim = "kit_item",
        deps = { "deed", "pugalisk_fountain" },
    },
    rug_round = {
        atlas = hamlet,
        tex = "reno_rug_round.tex",
        build = "rugs",
        bank = "rugs",
        anim = "rug_round",
    },
    disguisehat = {
        atlas = hamlet,
        tex = "disguisehat.tex",
        build = "hat_disguise",
        bank = "disguisehat",
        anim = "anim",
    },
    bandithat = {
        atlas = hamlet,
        tex = "bandithat.tex",
        build = "hat_bandit",
        bank = "bandithat",
        anim = "anim",
    },
    pigbandit = {
        build = "pig_bandit",
        bank = "townspig",
        anim = "idle_loop",
        deps = { "bandithat" },
    },
    pigman_royalguard = {
        build = "pig_royalguard",
        bank = "townspig",
        anim = "idle_loop",
    },
    pigman_royalguard_2 = {
        build = "pig_royalguard_2",
        bank = "townspig",
        anim = "idle_loop",
        deps = { "silvernecklace" }
    },
    pig_guard_tower = {
        atlas = "images/inventoryimages/hamletinventoryimages.xml",
        tex = "pig_guard_tower.tex",
        build = "pig_tower_build",
        bank = "pig_shop",
        anim = "idle",
        deps = { "pigman_royalguard", "ptribe_upgrade" },
    },
    pig_guard_tower_palace = {
        atlas = "images/inventoryimages/hamletinventoryimages.xml",
        tex = "pig_guard_tower.tex",
        build = "pig_tower_royal_build",
        bank = "pig_shop",
        anim = "idle",
        deps = { "pigman_royalguard_2", "ptribe_upgrade" },
    },
    ptribe_upgrade = {
        atlas = "images/inventoryimages/ptribe_upgrade.xml",
        tex = "ptribe_upgrade.tex",
        build = "ptribe_upgrade",
        bank = "ptribe_upgrade",
        anim = "idle",
    }
})
