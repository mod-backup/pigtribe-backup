if GetModConfigData("language") ~= "AUTO" then
    LANGUAGE_SETTING = GetModConfigData("language") == "ENG"
else
    -- system language
    local lan = require "languages/loc".GetLanguage()
    if lan == LANGUAGE.CHINESE_S or lan == LANGUAGE.CHINESE_S_RAIL then
        LANGUAGE_SETTING = false
    else
        LANGUAGE_SETTING = true
    end
end

L = LANGUAGE_SETTING -- For env

GLOBAL.PTRIBE_LAGUAGE_LOC = function(e, c)
    return L and e or c
end

--本地化
if L then
    modimport("scripts/languages/ptribe_en")
    modimport("scripts/languages/ptribe_modwiki_en")
    modimport("scripts/languages/shipwrecked_en")
else
    modimport("scripts/languages/ptribe_zh")
    modimport("scripts/languages/ptribe_modwiki_zh")
    modimport("scripts/languages/shipwrecked_zh")
end

function RecursiveMerge(dest, src)
    for k, v in pairs(src) do
        if type(v) == "table" then
            if type(dest[k] or false) == "table" then
                RecursiveMerge(dest[k], v) -- 递归合并子表
            elseif dest[k] == nil then
                dest[k] = v                -- 如果dest中没有这个表，直接赋值
            end
        else
            if dest[k] == nil then
                dest[k] = v -- 只有当dest中没有这个键时才赋值
            end
        end
    end
end

-- 还需要处理哈姆雷特的文本
RecursiveMerge(STRINGS, L and require("languages/hamlet_en") or require("languages/hamlet_zh"))
