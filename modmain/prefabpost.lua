local Utils = require("ptribe_utils/utils")
local GetPrefab = require("ptribe_utils/getprefab")
local InitWork = require("ptribe_utils/initwork")
local Skins = require("ptribe_skins")
local ResourceConvert = require("ptribe_resourceconvert")
local BluePrintsUtils = require("ptribe_blueprintsutils")
local BaseProjectionUtils = require("ptribe_baseprojectionutils")


for _, name in ipairs({
    "pigman",
    "pighouse",
    "playerpost"
}) do
    modimport("modmain/prefabpost/" .. name)
end

----------------------------------------------------------------------------------------------------

AddPrefabPostInitAny(function(inst)
    if not BluePrintsUtils.GetPhysicsRadius(inst.prefab) then
        BluePrintsUtils.RegisterPhysicsRadius(inst)
    end

    if not TheWorld.ismastersim then return end

    inst:AddComponent("ptribe_pigmanbuild")
end)

----------------------------------------------------------------------------------------------------

AddPrefabPostInit("world", function(inst)
    if not inst.ismastersim then return end

    TheWorld:DoTaskInTime(3, function()
        local msgs = BaseProjectionUtils.PopErrorMessage()
        if #msgs > 0 then
            TheNet:SystemMessage("解析本地蓝图文件时出现错误：")
            for _, msg in ipairs(msgs) do
                TheNet:SystemMessage(msg)
            end
        end
    end)

    inst:AddComponent("ptribe_tribemanager") --部落管理器

    inst:AddComponent("banditmanager")       --强盗猪人

    if TUNING.TRIBE_ENABLE_TRIBE_CONFLICT then
        inst:AddComponent("prtibe_conflictmanager") --部落冲突
    end

    inst:AddComponent("ptribe_pigskinmanager") --猪人建筑皮肤管理

    inst:AddComponent("ptribe_wildpigmanager") --野外猪人
end)

-- 猪王附近生成猪人摊位
local function SpawnCarnivalPrizebooth(inst)
    if not FindEntity(inst, 32, function(ent) return ent.prefab == "ptribe_carnival_prizebooth" end) then
        local pos = GetPrefab.FindPlaceablePos(inst:GetPosition(), 12, 20, 1, 10)
        if pos then
            SpawnAt("ptribe_carnival_prizebooth", pos)
        end
    end
end

local function OnAcceptBefore(inst, giver, item)
    if giver.prefab == "wilba" then
        local tradable = item.components.tradable

        tradable.goldvalue = math.ceil(tradable.goldvalue * 1.5)
        tradable.halloweencandyvalue = tradable.halloweencandyvalue and math.ceil(tradable.halloweencandyvalue * 1.5) or
            tradable.halloweencandyvalue
    end
end

AddPrefabPostInit("pigking", function(inst)
    inst:AddTag("prototyper")

    if not TheWorld.ismastersim then return end

    -- 猪王旁边必刷一个猪人摊位，这里简单一点儿每次启动游戏就检查是否有
    inst:DoTaskInTime(0, SpawnCarnivalPrizebooth)

    inst:AddComponent("prototyper")
    inst.components.prototyper.trees = TUNING.PROTOTYPER_TREES.PTRIBE_BLUEPRINT

    Utils.FnDecorator(inst.components.trader, "onaccept", OnAcceptBefore)
end)
----------------------------------------------------------------------------------------------------
-- 雪球机不会扑灭永不妥协猪王附近的猪火炬
AddPrefabPostInit("pigking_pigtorch", function(inst)
    inst:AddTag("uncompromising_pigtorch")
end)
AddClassPostConstruct("components/firedetector", function(self)
    local NOTAGS = Utils.ChainFindUpvalue(self.Activate, "LookForFiresAndFirestarters", "NOTAGS")
    if NOTAGS then
        table.insert(NOTAGS, "uncompromising_pigtorch")
    end
end)



----------------------------------------------------------------------------------------------------
-- 哈姆雷特兼容

AddPrefabPostInit("forest", function(inst)
    inst:AddComponent("interiorspawner")
end)
AddPrefabPostInit("cave", function(inst)
    inst:AddComponent("interiorspawner")
end)


require "components/map"
local home = {}     --室内的中心坐标，由于地皮一定在中心
local DIS = 28      --室内的最大半径
local DIS_SQ = 28 * 28
local lastHome = {} --缓存，短时间内在一个房间附近求值的可能性较大
-- 室内可放置建筑，物品不会掉入“水”中

local function CheckPointBefore(self, x, y, z)
    if z >= 950 then --判断的基础，也许光判断z就行了，只要移除房子的时候删除附近的物品？
        -- 缓存
        if lastHome.home then
            if lastHome.home:IsValid() then
                if VecUtil_DistSq(lastHome.pos[1], lastHome.pos[2], x, z) < DIS_SQ then
                    return { true }, true
                end
            else
                lastHome.home = nil
                lastHome.pos = nil
            end
        end

        -- 缓存表
        for ent, pos in pairs(home) do
            if ent:IsValid() then
                -- print(x, z, pos[1], pos[2], VecUtil_DistSq(pos[1], pos[2], x, z))
                if VecUtil_DistSq(pos[1], pos[2], x, z) < DIS_SQ then
                    lastHome.home = ent
                    lastHome.pos = pos
                    return { true }, true
                end
            else
                home[ent] = nil
            end
        end

        -- 查找
        local ents = TheSim:FindEntities(x, 0, z, DIS, { "hamlet_housefloor" }) --查找地板
        if #ents > 0 then
            for _, ent in ipairs(ents) do
                local ex, _, ez = ent.Transform:GetWorldPosition()
                home[ent] = { ex, ez }
                lastHome.home = ent
                lastHome.pos = { ex, ez }
            end
            return { true }, true
        end
    end
end

local function GetTileCenterPointBefore(self, x, y, z)
    if z and z >= 950 then
        return { math.floor(x / 4) * 4 + 2, 0, math.floor(z / 4) * 4 + 2 }, true
    end
end

-- 根据components/deployable.lua判断需要覆盖的方法
Utils.FnDecorator(Map, "IsAboveGroundAtPoint", CheckPointBefore)
Utils.FnDecorator(Map, "IsPassableAtPoint", CheckPointBefore)
Utils.FnDecorator(Map, "IsVisualGroundAtPoint", CheckPointBefore)
-- Utils.FnDecorator(Map, "CanPlantAtPoint", CheckPointBefore) --允许房间里种植，不知道算不算超模，如果可以的话，需要处理耕地机等其他问题
Utils.FnDecorator(Map, "GetTileCenterPoint", GetTileCenterPointBefore)

AddComponentPostInit("birdspawner", function(self)
    -- 室内不会生成鸟
    Utils.FnDecorator(self, "GetSpawnPoint", function(self, pt)
        return nil, pt.z >= 950
    end)
end)


local StopAmbientRainSound, StopTreeRainSound, StopUmbrellaRainSound, StopBarrierSound
local _rainfx, _snowfx, _lunarhailfx
local function WeatherClientOnUpdateBefore()
    if not ThePlayer or TheWorld.ismastersim then return end

    -- 只在客机执行，这里只改本地玩家视觉效果，实际效果在其他地方修改
    local _, _, z = ThePlayer.Transform:GetWorldPosition()
    if z > 950 then
        if StopAmbientRainSound then
            StopAmbientRainSound()
        end
        if StopTreeRainSound then
            StopTreeRainSound()
        end
        if StopUmbrellaRainSound then
            StopUmbrellaRainSound()
        end
        if StopBarrierSound then
            StopBarrierSound()
        end

        if _rainfx then
            _rainfx.particles_per_tick = 0
            _rainfx.splashes_per_tick = 0
        end
        if _lunarhailfx then
            _lunarhailfx.particles_per_tick = 0
            _lunarhailfx.splashes_per_tick = 0
        end
        if _snowfx then
            _snowfx.particles_per_tick = 0
        end

        return nil, true
    end
end

AddClassPostConstruct("components/weather", function(self)
    if not TheWorld.ismastersim then
        StopAmbientRainSound = Utils.ChainFindUpvalue(self.OnUpdate, "StopAmbientRainSound")
        StopTreeRainSound = Utils.ChainFindUpvalue(self.OnUpdate, "StopTreeRainSound")
        StopUmbrellaRainSound = Utils.ChainFindUpvalue(self.OnUpdate, "StopUmbrellaRainSound")
        StopBarrierSound = Utils.ChainFindUpvalue(self.OnUpdate, "StopBarrierSound")

        _rainfx = Utils.ChainFindUpvalue(self.OnPostInit, "_rainfx")
        _snowfx = Utils.ChainFindUpvalue(self.OnPostInit, "_snowfx")
        _lunarhailfx = Utils.ChainFindUpvalue(self.OnPostInit, "_lunarhailfx")
        -- _pollenfx = Utils.ChainFindUpvalue(self.OnPostInit, "_pollenfx") --应该不用管这个特效

        Utils.FnDecorator(self, "OnUpdate", WeatherClientOnUpdateBefore)
        self.LongUpdate = self.OnUpdate
    end
end)

----------------------------------------------------------------------------------------------------
--读书失败台词替换
local oldGetActionFailString = GLOBAL.GetActionFailString
GLOBAL.GetActionFailString = function(inst, ...)
    local msg = oldGetActionFailString(inst, ...)
    if inst.prefab == "wilba"
        and msg == "only_used_by_waxwell_and_wicker" --读书失败
    then
        inst.prefab = "wickerbottom"
        msg = oldGetActionFailString(inst, ...)
        inst.prefab = "wilba"
    end

    return msg
end

----------------------------------------------------------------------------------------------------
-- 猪王游戏中护法不能攻击薇尔芭
local function CanTargetBefore(self, target)
    if target and target.prefab == "wilba" and not target:HasTag("monster") then
        return { false }, true
    end
end
for i = 1, 4 do
    AddPrefabPostInit("pigelite" .. i, function(inst)
        if not TheWorld.ismastersim then return end
        Utils.FnDecorator(inst.components.combat, "CanTarget", CanTargetBefore)
    end)
end

----------------------------------------------------------------------------------------------------
-- 清洁扫把
local function SpawnToolFx(tool, target)
    local fx_prefab = "explode_reskin"
    local skin_fx = SKIN_FX_PREFAB[tool:GetSkinName()]
    if skin_fx ~= nil and skin_fx[1] ~= nil then
        fx_prefab = skin_fx[1]
    end
    --也不缩放和偏移了，将就一下
    SpawnPrefab(fx_prefab).Transform:SetPosition(target.Transform:GetWorldPosition())
end

local function ToolSpellBefore(tool, target)
    if not target or not target:IsValid() then
        return
    end

    if target.prefab == "pighouse" then
        local saveSkin = target.components.ptribe_saveskin

        if target.AnimState:GetBuild() == "pig_cityhall" then
            --村长房用的不一样的bank和build，如果扫了就不再切回来了
            target.AnimState:SetBank("pig_house")
            target.AnimState:SetBuild("pig_house")
            target.AnimState:PlayAnimation("idle")
            SpawnToolFx(tool, target) --特效还是得手动生成
            return nil, true
        elseif not target.skinname
            and saveSkin and (not saveSkin.data or saveSkin.data.index <= #Skins.PIGHOUSE_SKINS) then
            --扫帚可以给猪人房换皮肤
            if saveSkin.data and saveSkin.data.index == #Skins.PIGHOUSE_SKINS then
                -- 正常换皮肤，我需要还原bank和build
                saveSkin.data.index = 0
                saveSkin:Enable(false)
            else
                SpawnToolFx(tool, target) --特效还是得手动生成

                local i = saveSkin.data and (saveSkin.data.index + 1) or 1
                saveSkin:Set({
                    bank = Skins.PIGHOUSE_SKINS[i].bank,
                    build = Skins.PIGHOUSE_SKINS[i].build,
                    index = i, --额外数据
                }, saveSkin.oldData)
                saveSkin:Enable(true)

                return nil, true
            end
        end
    end
end
AddPrefabPostInit("reskin_tool", function(inst)
    if not TheWorld.ismastersim then return end
    Utils.FnDecorator(inst.components.spellcaster, "spell", ToolSpellBefore)
end)
----------------------------------------------------------------------------------------------------
-- 工厂生产的武器非猪人生物使用为4倍耐久消耗。
local function GetOwner(inst)
    return inst.components.inventoryitem and inst.components.inventoryitem.owner
end

local function UseBefore(self, num)
    num = num or 1
    if num > 0 and self.inst:HasTag("ptribe_pigmanbuild") then
        local owner = GetOwner(self.inst)
        if owner and not owner:HasTag("pig") then
            return nil, false, { self, num * TUNING.PTRIBE_PIG_WEAPON_OTHER_COST_MULT }
        end
    end
end
AddComponentPostInit("finiteuses", function(self)
    Utils.FnDecorator(self, "Use", UseBefore)
end)

local function SetLocalMultiplierBefore(self, newMult)
    return nil, false, { self, newMult * self.ptribe_localPerishMultiplyer }
end

AddComponentPostInit("perishable", function(self)
    self.ptribe_localPerishMultiplyer = 1                                   --临时存储的倍率

    Utils.FnDecorator(self, "SetLocalMultiplier", SetLocalMultiplierBefore) --这里是通过覆盖方法来修改腐烂倍率的，源码没问题，但是如果其他mod直接修改变量就没效果的
end)

local function OnWeaponSwitch(inst, data, isEquip)
    local owner = data and data.owner

    if not inst:HasTag("ptribe_pigmanbuild")
        or not owner
        or owner:HasTag("pig")
    then
        return
    end

    if inst.components.perishable then
        local oldMult = inst.components.perishable.ptribe_localPerishMultiplyer
        if isEquip then
            inst.components.perishable.ptribe_localPerishMultiplyer = TUNING.PTRIBE_PIG_WEAPON_OTHER_COST_MULT
        else
            inst.components.perishable.ptribe_localPerishMultiplyer = 1
        end
        inst.components.perishable:SetLocalMultiplier(inst.components.perishable:GetLocalMultiplier() / oldMult)
    end
end

local function OnWeaponEquipped(inst, data)
    OnWeaponSwitch(inst, data, true)
end
local function OnWeaponUnEquipped(inst, data)
    OnWeaponSwitch(inst, data, false)
end

AddComponentPostInit("equippable", function(self, inst)
    inst:ListenForEvent("equipped", OnWeaponEquipped)
    inst:ListenForEvent("unequipped", OnWeaponUnEquipped)
end)

-- 生产的武器用分解法杖分解只会掉落呼噜币
local COINS_ORDER = { "oinc1000", "oinc100", "oinc10", "oinc" }
local function OnSpellBefore(staff, target)
    if target:HasTag("weapon") and target:HasTag("ptribe_pigmanbuild") then
        -- 生成呼噜币
        local resource = ResourceConvert.GetResource(target.prefab)
        local cur = 1000
        for _, prefab in ipairs(COINS_ORDER) do
            local count = math.floor(resource / cur)
            resource = resource % cur
            cur = cur / 10

            for i = 1, count do
                GetPrefab.SpawnLootPrefab(target, prefab)
            end
        end

        -- 施法者
        local caster = staff.components.inventoryitem.owner
        if caster ~= nil then
            caster.SoundEmitter:PlaySound("dontstarve/common/staff_dissassemble")

            if caster.components.staffsanity then
                caster.components.staffsanity:DoCastingDelta(-TUNING.SANITY_MEDLARGE)
            elseif caster.components.sanity ~= nil then
                caster.components.sanity:DoDelta(-TUNING.SANITY_MEDLARGE)
            end
        end

        staff.components.finiteuses:Use(1)

        target:PushEvent("ondeconstructstructure", caster)
        GetPrefab.DischargePrefab(target)
        GetPrefab.GetOne(target):Remove()

        return nil, true
    end
end

AddPrefabPostInit("greenstaff", function(inst)
    if not TheWorld.ismastersim then return end
    Utils.FnDecorator(inst.components.spellcaster, "spell", OnSpellBefore)
end)

----------------------------------------------------------------------------------------------------
-- AddComponentPostInit("edible", function(self, inst)
--     if not inst.components.ptribe_foodbuff then
--         inst:AddComponent("ptribe_foodbuff")
--     end
-- end)

InitWork.AddTempTagMethod()
InitWork.RegisterDrawable()

local BuffManagerUtils = require("ptribe_buffmanagerutils")
BuffManagerUtils.RegisterMethods()

----------------------------------------------------------------------------------------------------
local function OnBuffDone(inst)
    inst.components.talker:Say(STRINGS.PTRIBE_FOOD_BUFFS_STOP)
end

AddPlayerPostInit(function(inst)
    if not TheWorld.ismastersim then return end

    inst:ListenForEvent("ptribe_buffdone", OnBuffDone)
end)

local ExcludeTagsInc = { "pig" } --对猪人无害
--- 便便会生成毒雾，猪人免疫
local function HitBefore(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    TheWorld:DoTaskInTime(0, function()
        local poops = {}
        local count = 0
        local sumX = 0
        local sumZ = 0

        for _, v in ipairs(TheSim:FindEntities(x, y, z, 2.5)) do
            if v.prefab == "poop" then
                table.insert(poops, v)
                count = count + GetStackSize(v)
                local vx, vy, vz = v.Transform:GetWorldPosition()
                sumX = sumX + vx
                sumZ = sumZ + vz
            end
        end

        if #poops < 5 then
            for _, v in ipairs(poops) do
                if not v.ptribe_removeTask then
                    v.ptribe_removeTask = v:DoTaskInTime(5, ErodeAway) --不能在地图上堆积太多了
                end
            end
            return
        end

        local cloud = GetPrefab.GetSporeCloud(ExcludeTagsInc, {
            damage = count * 4, --因为不会自动堆叠，这里伤害一般5*4 = 20
            time = #poops * 10
        })
        cloud.persists = false --不保存，因为再上线就会对猪人造成伤害了
        cloud.Transform:SetPosition(sumX / #poops, 0, sumZ / #poops)

        for _, v in ipairs(poops) do
            v:Remove()
        end
    end)
end

AddPrefabPostInit("monkeyprojectile", function(inst)
    if not TheWorld.ismastersim then return end

    Utils.FnDecorator(inst.components.projectile, "onhit", HitBefore)
end)
