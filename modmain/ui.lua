local Utils = require("ptribe_utils/utils")
local ImageButton = require "widgets/imagebutton"
local BP = require("ptribe_blueprints")
local BluePrintsUtils = require("ptribe_blueprintsutils")

AddClassPostConstruct("widgets/hoverer", function(self)
    --self.text是和名字一样的蓝色字体，self.secondarytext是白字，但是只有按Alt查看时才显示
    Utils.FnDecorator(self.text, "SetString", function(text, str)
        local target = TheInput:GetWorldEntityUnderMouse()
        if not target then return end

        local blueprint = target.replica.ptribe_pigmanager and target.replica.ptribe_pigmanager:GetBlueprint()
        local isManager = blueprint and blueprint ~= ""

        local builder = Utils.GetBuilder(str)
        if target:HasTag("ptribe_pigmanbuild") then
            builder:Append("\n猪人的！")
        end

        if target.replica.ptribe_upgrade then
            -- 等级
            builder:Append("\n等级："):Append(target.replica.ptribe_upgrade:GetLevel())
        end

        if target.components.ptribe_pigtask and target.components.ptribe_pigtask.endTime then
            -- 任务剩余时间
            builder:Append("\n任务剩余：")
                :Append(Utils.FormatTime(math.floor(target.components.ptribe_pigtask.endTime - GetTime())))
        end


        if isManager then
            -- 蓝图名
            builder:Append("\n蓝图："):Append(blueprint)
        end

        if target.replica.ptribe_pigfactory then
            local fac = target.replica.ptribe_pigfactory
            local product = fac:GetProduct()
            if product ~= "" then
                local remain = math.max(fac:GetCooldown() + target.startTime - GetTime(), 0)
                if remain > 0 then
                    builder:Append("\n生产"):Append((STRINGS.NAMES[string.upper(product)] or product))
                        :Append(" : "):Append(Utils.FormatTime(remain))
                end
            end
        end
        return nil, false, { text, builder:ToString() }
    end)
end)

----------------------------------------------------------------------------------------------------

local function _MakeBuildButtonAfter(retTab, self)
    local root = unpack(retTab)

    local button = root:AddChild(ImageButton())
    button:SetOnClick(function()
        local blueprintName = BP.BLUEPRINT_LIST[self.data.recipe.name]
        SendModRPCToServer(MOD_RPC["PigmanTribe"]["SetBlueprintEnable"],
            json.encode({ [blueprintName] = not BluePrintsUtils.IsEnable(blueprintName) }))
    end)

    button:SetScale(.7, .7, .7)
    button:SetPosition(-100, 0)
    button:Hide()

    root.ptribe_button = button
    return { root }
end

local function UpdateBuildButtonAfter(retTab, self)
    local button = self.build_button_root and self.build_button_root.ptribe_button

    if not button or not self.data then
        return
    end

    local recipe = self.data.recipe

    local blueprintName = BP.BLUEPRINT_LIST[recipe.name]
    if not self.build_button_root.button:IsVisible()
        or not blueprintName then
        button:Hide()
        return
    end

    button:SetText(BluePrintsUtils.IsEnable(blueprintName) and STRINGS.UI.CRAFTING.RECIPEACTION.PTRIBE_ENABLE
        or STRINGS.UI.CRAFTING.RECIPEACTION.PTRIBE_DISABLE) --是否启用
    local w, h = button.text:GetRegionSize()
    button.image:ScaleToSize(Clamp(w + 50, 145, 300), 65)
    button:Show()
end

AddClassPostConstruct("widgets/redux/craftingmenu_details", function(self)
    Utils.FnDecorator(self, "_MakeBuildButton", nil, _MakeBuildButtonAfter)
    Utils.FnDecorator(self, "UpdateBuildButton", nil, UpdateBuildButtonAfter)
end)
