local day_time = TUNING.DAY_TIME_DEFAULT

local MAX_SIZE = require("ptribe_blueprints").MAX_SIZE
--部落最大距离，与蓝图的最大距离保持一致，后面还会根据mod和本地读取的蓝图数据更新这个值
TUNING.TRIBE_MAX_DIS = 4 * math.sqrt(MAX_SIZE.width * MAX_SIZE.width + MAX_SIZE.height * MAX_SIZE.height)

-- 部落
TUNING.TRIBE_LUCK_GOLD_FROM_PIGMAN = GetModConfigData("LuckyGoldFromPigman")             --如果为false则不能再从猪人房和猪人身上获取幸运黄金
TUNING.TRIBE_STEAL_PIGMAN_PROPERTY_PUHISH = GetModConfigData("StealPigmanPopertyPunish") --偷窃猪人资源是否有惩罚
TUNING.TRIBE_PLAYER_SKIN_ENABLE = GetModConfigData("PigmanUsePlayerSkin")                --允许扫描进入游戏的玩家皮肤，猪人建造随机玩家皮肤
TUNING.TRIBE_CHANGE_SKIN_PROB = GetModConfigData("PigmanUsePlayerSkinProb")              --换皮肤的概率
TUNING.TRIBE_TRIBE_MIN_PIGHOUSE_COUNT = GetModConfigData("TribeMinPighouseCount")        --村长房附近的最少猪人房数量
TUNING.TRIBE_ENABLE_TRIBE_CONFLICT = GetModConfigData("EnableTribeConflict")             --是否开启部落冲突
TUNING.TRIBE_ALLOW_MISMATCH = false                                                      --当地皮不可用或地皮可用但是有建筑挡位时，是否考虑相邻一格地皮来完成对应模块的建设，仅当相邻模块在蓝图留空时执行
TUNING.TRIBE_RESOURCE_MAX = 5000                                                         --部落资源储量最大值
TUNING.TRIBE_TASK_MAX_COUNT = 10                                                         --允许的最大超时次数
TUNING.TRIBE_SHOP_REFRESH_TIME = TUNING.TOTAL_DAY_TIME * 2                               --商店刷新时间，两次一次
TUNING.TRIBE_CONFLICT_FIND_RAIDUS = TUNING.TRIBE_MAX_DIS * 5                             --部落入侵时入侵部落的查找范围
TUNING.TRIBE_CONFLICT_ATTACK_PERIOD = TUNING.TOTAL_DAY_TIME * 5                          --部落入侵的入侵周期
TUNING.PTRIBE_PIG_FAVORABILITY_ATTACK = -40                                              --部落猪人主动攻击玩家的好感度下限
TUNING.PTRIBE_SPAWN_WILD_PIG_PERIOD = TUNING.TOTAL_DAY_TIME * 2                          --随机猪人事件的间隔

-- 猪人
TUNING.TRIBE_SPECIAL_PIGMAN_GENERATION_MULT = GetModConfigData("SpecialPigmanGenerationMult") --特殊猪人生成倍率
TUNING.TRIBE_KUNGFU_EMPLOY_EFFIC = GetModConfigData("KungFuEmployEffic")                      --功夫猪人雇佣效率
-- TUNING.TRIBE_TERRARIA_DAMAGE_MULT = GetModConfigData("TerrariaLaserDamageMult")          -- 泰拉猪人激光伤害倍率
TUNING.TRIBE_ARMOR_DAMAGE_THRESHOLD = GetModConfigData("ArmorDamageLimit")                    --铁甲猪人5秒伤害上限
TUNING.TRIBE_MITE_COOLDOWN = GetModConfigData("MiteSkillCooldown")                            --爆螨猪人技能冷却
TUNING.PIG_BLOWDART_ATTACK_PERIOD = GetModConfigData("BlowdartInitAttackPeriod")              --吹箭猪人初始攻击间隔
TUNING.TRIBE_PIGMAN_CITY_MULT = GetModConfigData("PigmanCityUpgradeMult")                     --猪人守卫升级加强倍率

-- 任务
TUNING.TRIBE_ALLOW_TASK_GENERATE_TILE = GetModConfigData("AllowTaskGenerateTile") --是否允许任务生成地皮
TUNING.TRIBE_TASK_DIFFICULTY_LEVEL = GetModConfigData("TaskDifficultyLevel")      --任务难度等级
TUNING.TRIBE_TASKS_LENGTH = 10                                                    --任务数量上限
TUNING.TRIBE_DO_TASK_TIMEOUT = TUNING.TOTAL_DAY_TIME                              --任务超时时间，考虑到晚上，这里设置为1天

-- 猪人
TUNING.PTRIBE_PIG_GIVE_WILBA_PERIOD = TUNING.TOTAL_DAY_TIME * 2  --猪人给予薇尔芭道具的冷却时间，在这个时间上下浮动
TUNING.PTRIBE_PIG_GIVE_PLAYER_PERIOD = TUNING.TOTAL_DAY_TIME * 5 --高好感度下猪人给予玩家礼物的冷却时间

-- 薇尔芭
TUNING.PTRIBE_WILBA_PIG_COUNT = 4 --薇尔芭每块肉雇佣的猪人数量
TUNING.PTRIBE_MAX_AVTIVE_PIG_COUNT = 50

TUNING.PTRIBE_SPAWN_WEAPON_MIN_RESOURCE = 100 --生产武器后剩余的最小资源，即生产武器后最少也要保留的值
TUNING.PTRIBE_PIG_WEAPON_OTHER_COST_MULT = 4  --非猪人使用武器消耗倍率
TUNING.PTRIBE_GUARD_TOWER_MAX_LEVEL = 10      --瞭望塔最大等级

TUNING.PTRIBE_WILD_PIG_POT_COUNT_MAX = 3      --野外猪人厨子锅的数量上限
TUNING.PTRIBE_WILD_PIG_SEED_COUNT_MAX = 6     --野外猪人农夫作物的数量上限

----------------------------------------------------------------------------------------------------
-- 单机版常量

TUNING.DECO_RUINS_BEAM_WORK = 6

TUNING.WILBA_HEALTH = 150
TUNING.WILBA_HUNGER = 200
TUNING.WILBA_SANITY = 100
TUNING.WILSON_RUN_SPEED = 6
TUNING.WEREWILBA_HEALTH = 350
TUNING.WEREWILBA_DAMAGE = TUNING.SPEAR_DAMAGE * 1.75 --其实是威尔逊的1.75
TUNING.HALBERD_DAMAGE = TUNING.SPEAR_DAMAGE * 1.3
TUNING.HALBERD_USES = 100
TUNING.SNAKE_SPEED = 3
TUNING.SNAKE_HEALTH = 100
TUNING.SNAKE_DAMAGE = 10
TUNING.SNAKE_ATTACK_PERIOD = 3
TUNING.SNAKE_TARGET_DIST = 8
TUNING.SNAKE_KEEP_TARGET_DIST = 15
TUNING.DRAGONFLY_VOMIT_TARGETS_FOR_SATISFIED = 40
TUNING.JUNGLETREE_GROW_TIME = {
    { base = 4.5 * day_time, random = 0.5 * day_time }, --tall to short
    { base = 8 * day_time,   random = 5 * day_time },   --short to normal
    { base = 8 * day_time,   random = 5 * day_time },   --normal to tall
}
TUNING.JUNGLETREE_CHOPS_SMALL = 5
TUNING.SNAKE_POISON_START_DAY = 3
TUNING.SNAKE_JUNGLETREE_POISON_CHANCE = 0.25
TUNING.SNAKE_JUNGLETREE_CHANCE = 0.5
TUNING.SNAKE_JUNGLETREE_AMOUNT_SMALL = 1
TUNING.JUNGLETREE_CHOPS_NORMAL = 10
TUNING.SNAKE_JUNGLETREE_AMOUNT_MED = 1
TUNING.JUNGLETREE_CHOPS_TALL = 15
TUNING.SNAKE_JUNGLETREE_AMOUNT_TALL = 2
TUNING.WINDBLOWN_SCALE_MIN = {
    LIGHT = 0.1,
    MEDIUM = 0.1,
    HEAVY = 0.01
}
TUNING.WINDBLOWN_SCALE_MAX = {
    LIGHT = 1.0,
    MEDIUM = 0.25,
    HEAVY = 0.05
}
TUNING.WRATH_SMALL = -8
TUNING.JUNGLETREESEED_GROWTIME = { base = 4.5 * day_time, random = 0.75 * day_time }
TUNING.TELESCOPE_USES = 5
TUNING.TELESCOPE_ARC = 15
TUNING.TELESCOPE_RANGE = 200
TUNING.SUPERTELESCOPE_RANGE = 400
----------------------------------------------------------------------------------------------------
