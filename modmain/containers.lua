local PigFactoryUtils = require("ptribe_pigfactoryutils")
local Containers = require("containers")
local params = Containers.params

params.pigman_royalguard = params.wobybig
params.pigman_royalguard_2 = params.wobybig

----------------------------------------------------------------------------------------------------
-- 武器工厂3x4
params.ptribe_pigweaponfactory = {
    widget =
    {
        slotpos = {},
        slotbg = {},
        animbank = "ui_chester_shadow_3x4",
        animbuild = "ui_chester_shadow_3x4",
        pos = Vector3(0, 200, 0),
        side_align_tip = 160,
        buttoninfo =
        {
            text = STRINGS.ACTIONS.PTRIBE_DISTRIBUTE,
            position = Vector3(0, -180, 0),
        }
    },
    type = "chest",
}

for y = 2.5, -0.5, -1 do
    for x = 0, 2 do
        table.insert(params.ptribe_pigweaponfactory.widget.slotpos,
            Vector3(75 * x - 75 * 2 + 75, 75 * y - 75 * 2 + 75, 0))
        table.insert(params.ptribe_pigweaponfactory.widget.slotbg,
            { image = "ptribe_pigweaponfactory_slot.tex", atlas = "images/ptribe_pigweaponfactory_slot.xml" })
    end
end

function params.ptribe_pigweaponfactory.itemtestfn(container, item, slot)
    return item:HasTag("weapon") and container.inst.replica.ptribe_upgrade and
        PigFactoryUtils.IsAllowSpawn(item.prefab, container.inst.replica.ptribe_upgrade:GetLevel())
end

function params.ptribe_pigweaponfactory.widget.buttoninfo.fn(inst, doer)
    if inst.components.container ~= nil then
        BufferedAction(doer, inst, ACTIONS.PTRIBE_DISTRIBUTE):Do()
    elseif inst.replica.container ~= nil and not inst.replica.container:IsBusy() then
        SendRPCToServer(RPC.DoWidgetButtonAction, ACTIONS.PTRIBE_DISTRIBUTE.code, inst,
            ACTIONS.PTRIBE_DISTRIBUTE.mod_name)
    end
end

function params.ptribe_pigweaponfactory.widget.buttoninfo.validfn(inst)
    return inst.replica.container ~= nil and not inst.replica.container:IsEmpty()
end
