local BluePrintsUtils = require("ptribe_blueprintsutils")
local PigTasksUtils = require("ptribe_pigtasksutils")
local WoodenSignPopup = require "screens/redux/ptribe_pigtaskpopup"
local BP = require("ptribe_blueprints")
----------------------------------------------------------------------------------------------------
AddModRPCHandler("PigmanTribe", "SetPlayerTaskId", function(player, taskId)
    if not player.components.ptribe_playertask then return end
    if taskId == 0 then
        player.components.ptribe_playertask:SetPlayerTaskId(taskId)
    else
        if player.components.ptribe_playertask.cachePig == player.components.ptribe_playertask.pig then
            --提交
            player.components.ptribe_playertask:SetPlayerTaskId(taskId)
        else
            --切换任务
            player.components.ptribe_playertask:SetPlayerTaskId(0)
            player.components.ptribe_playertask:SetPlayerTaskId(taskId)
        end
    end
end)

AddModRPCHandler("PigmanTribe", "SetBlueprintEnable", function(player, data)
    for name, enable in pairs(data and json.decode(data) or {}) do
        BluePrintsUtils.SetEnable(name, enable)
    end
    SendModRPCToClient(GetClientModRPC("PigmanTribe", "SetBlueprintEnable"), nil, data) --客机
end)

AddClientModRPCHandler("PigmanTribe", "PopupPlayerTaskDialog", function(taskId, oldTaskId, isSamePig)
    if not ThePlayer then return end

    local task = PigTasksUtils.GetTaskById(taskId)
    if not task then return end --task一定存在，但是有时候出bug好像不存在，这里排除一下

    if ThePlayer.ptribe_taskDialog then
        TheFrontEnd:PopScreen(ThePlayer.ptribe_taskDialog)
        ThePlayer.ptribe_taskDialog = nil
    end

    local buttons = {}
    table.insert(buttons, {
        text = oldTaskId == taskId and isSamePig == 1 and STRINGS.UI.PTRIBE_PIG_TASK.SUBMIT or
            STRINGS.UI.PTRIBE_PIG_TASK.ACCEPT,
        cb = function()
            SendModRPCToServer(MOD_RPC["PigmanTribe"]["SetPlayerTaskId"], taskId)

            -- if oldTaskId == taskId then
            TheFrontEnd:PopScreen()
            ThePlayer.ptribe_taskDialog = nil
            -- else
            --     --接受
            --     oldTaskId = taskId
            --     ThePlayer.ptribe_taskDialog.dialog.actions.items[1]:SetText("提交")
            -- end
        end
    })
    if oldTaskId == taskId and isSamePig == 1 then
        table.insert(buttons, {
            text = STRINGS.UI.PTRIBE_PIG_TASK.GIVE_UP,
            cb = function()
                SendModRPCToServer(MOD_RPC["PigmanTribe"]["SetPlayerTaskId"], 0)
                TheFrontEnd:PopScreen()
                ThePlayer.ptribe_taskDialog = nil
            end
        })
    end

    table.insert(buttons, {
        text = STRINGS.UI.PTRIBE_PIG_TASK.CLOSE,
        cb = function()
            TheFrontEnd:PopScreen()
            ThePlayer.ptribe_taskDialog = nil
        end
    })

    ThePlayer.ptribe_taskDialog = WoodenSignPopup(
        task.name,
        task.desc,
        task.condition,
        task.reward, buttons)
    TheFrontEnd:PushScreen(ThePlayer.ptribe_taskDialog)
end)

AddClientModRPCHandler("PigmanTribe", "SetBlueprintEnable", function(data)
    if not ThePlayer then return end
    for name, enable in pairs(data and json.decode(data) or {}) do
        BluePrintsUtils.SetEnable(name, enable)
    end

    ThePlayer:PushEvent("refreshcrafting") -- 刷新制作栏
end)
