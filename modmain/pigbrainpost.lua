require "behaviours/wander"
require "behaviours/follow"
require "behaviours/faceentity"
require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/doaction"
--require "behaviours/choptree"
require "behaviours/findlight"
require "behaviours/panic"
require "behaviours/chattynode"
require "behaviours/leash"

local BrainCommon = require "brains/braincommon"
local ResourceConvert = require("ptribe_resourceconvert")
local Utils = require("ptribe_utils/utils")

local MAX_CHARGE_TIME = 5
local MAX_CHARGE_DIST = 15
local CHASE_GIVEUP_DIST = 10

local MIN_FOLLOW_DIST = 2
local TARGET_FOLLOW_DIST = 5
local MAX_FOLLOW_DIST = 9
local MAX_WANDER_DIST = 20

local LEASH_RETURN_DIST = 10
local LEASH_MAX_DIST = 30

local START_RUN_DIST = 3
local STOP_RUN_DIST = 5
local MAX_CHASE_TIME = 10
local MAX_CHASE_DIST = 30
local SEE_LIGHT_DIST = 20
local TRADE_DIST = 20
local SEE_TREE_DIST = 15
local SEE_TARGET_DIST = 20
local SEE_FOOD_DIST = 10

local SEE_BURNING_HOME_DIST_SQ = 20 * 20

local COMFORT_LIGHT_LEVEL = 0.3

local KEEP_CHOPPING_DIST = 10

local RUN_AWAY_DIST = 5
local STOP_RUN_AWAY_DIST = 8

local function ShouldRunAway(inst, target)
    return not inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GetTraderFn(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local players = FindPlayersInRange(x, y, z, TRADE_DIST, true)
    for i, v in ipairs(players) do
        if inst.components.trader:IsTryingToTradeWithMe(v) then
            return v
        end
    end
end

local function KeepTraderFn(inst, target)
    return inst.components.trader:IsTryingToTradeWithMe(target)
end

local FINDFOOD_CANT_TAGS = { "outofreach" }
local function FindFoodAction(inst)
    if inst.sg:HasStateTag("busy") then
        return
    end

    -- 物品栏
    if inst.components.inventory ~= nil and inst.components.eater ~= nil then
        local target = inst.components.inventory:FindItem(function(item)
            return
                item.prefab ~= inst._pig_token_prefab
                and inst.components.eater:CanEat(item)
        end)
        if target ~= nil then
            return BufferedAction(inst, target, ACTIONS.EAT)
        end
    end

    if inst.components.minigame_spectator ~= nil then
        return
    end

    local time_since_eat = inst.components.eater:TimeSinceLastEating()
    if time_since_eat ~= nil and time_since_eat <= TUNING.PIG_MIN_POOP_PERIOD * 2 then
        return
    end

    local noveggie = time_since_eat ~= nil and time_since_eat < TUNING.PIG_MIN_POOP_PERIOD * 4

    local target = FindEntity(inst,
        SEE_FOOD_DIST,
        function(item)
            return item:GetTimeAlive() >= 8
                and item.prefab ~= "mandrake"
                and item.components.edible ~= nil
                and (not noveggie or item.components.edible.foodtype == FOODTYPE.MEAT)
                and item:IsOnPassablePoint()
                and inst.components.eater:CanEat(item)
                and not item.ptribe_pigForbidEat --禁止猪人吃掉它
        end,
        nil,
        FINDFOOD_CANT_TAGS
    )
    if target ~= nil then
        return BufferedAction(inst, target, ACTIONS.EAT)
    end

    target = FindEntity(inst,
        SEE_FOOD_DIST,
        function(item)
            return item.components.shelf ~= nil
                and item.components.shelf.itemonshelf ~= nil
                and item.components.shelf.cantakeitem
                and item.components.shelf.itemonshelf.components.edible ~= nil
                and (not noveggie or item.components.shelf.itemonshelf.components.edible.foodtype == FOODTYPE.MEAT)
                and item:IsOnPassablePoint()
                and inst.components.eater:CanEat(item.components.shelf.itemonshelf)
        end,
        nil,
        FINDFOOD_CANT_TAGS
    )
    if target ~= nil then
        return BufferedAction(inst, target, ACTIONS.TAKEITEM)
    end
end

local function IsDeciduousTreeMonster(guy)
    return guy.monster and guy.prefab == "deciduoustree"
end

local CHOP_MUST_TAGS = { "CHOP_workable" }
local function FindDeciduousTreeMonster(inst)
    return FindEntity(inst, SEE_TREE_DIST / 3, IsDeciduousTreeMonster, CHOP_MUST_TAGS)
end

local function KeepChoppingAction(inst)
    return inst.tree_target ~= nil
        or (inst.components.follower.leader ~= nil and
            inst:IsNear(inst.components.follower.leader, KEEP_CHOPPING_DIST))
        or FindDeciduousTreeMonster(inst) ~= nil
end

local function StartChoppingCondition(inst)
    return inst.tree_target ~= nil
        or (inst.components.follower.leader ~= nil and
            inst.components.follower.leader.sg ~= nil and
            inst.components.follower.leader.sg:HasStateTag("chopping"))
        or FindDeciduousTreeMonster(inst) ~= nil
end

local function FindTreeToChopAction(inst)
    local target = FindEntity(inst, SEE_TREE_DIST, nil, CHOP_MUST_TAGS)
    if target ~= nil then
        if inst.tree_target ~= nil then
            target = inst.tree_target
            inst.tree_target = nil
        else
            target = FindDeciduousTreeMonster(inst) or target
        end
        return BufferedAction(inst, target, ACTIONS.CHOP)
    end
end

local function HasValidHome(inst)
    local home = inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
    return home ~= nil
        and home:IsValid()
        and not (home.components.burnable ~= nil and home.components.burnable:IsBurning())
        and not home:HasTag("burnt")
end

local function GoHomeAction(inst)
    if not inst.components.follower.leader and
        HasValidHome(inst) and
        not inst.components.combat.target then
        return BufferedAction(inst, inst.components.homeseeker.home, ACTIONS.GOHOME)
    end
end

local function GetLeader(inst)
    return inst.components.follower.leader
end

local function GetHomePos(inst)
    return HasValidHome(inst) and inst.components.homeseeker:GetHomePos()
end

local function GetNoLeaderHomePos(inst)
    if GetLeader(inst) then
        return nil
    end
    return GetHomePos(inst)
end

local LIGHTSOURCE_TAGS = { "lightsource" }
local function GetNearestLightPos(inst)
    local light = GetClosestInstWithTag(LIGHTSOURCE_TAGS, inst, SEE_LIGHT_DIST)
    if light then
        return Vector3(light.Transform:GetWorldPosition())
    end
    return nil
end

local function GetNearestLightRadius(inst)
    local light = GetClosestInstWithTag(LIGHTSOURCE_TAGS, inst, SEE_LIGHT_DIST)
    if light then
        return light.Light:GetCalculatedRadius()
    end
    return 1
end

local function RescueLeaderAction(inst)
    return BufferedAction(inst, GetLeader(inst), ACTIONS.UNPIN)
end

local function WantsToGivePlayerPigTokenAction(inst)
    if not inst._pig_token_prefab
        or inst:HasTag("ptribe_tribeinvader")
        or inst:HasTag("ptribe_tribevictim")
    then
        return false
    end

    local has = inst.components.inventory:Has(inst._pig_token_prefab, 1)
    if (GetLeader(inst) ~= nil and inst.components.follower:GetLoyaltyPercent() >= TUNING.PIG_FULL_LOYALTY_PERCENT)
        and has then
        return true
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    for _, player in ipairs(AllPlayers) do
        local x2, _, z2 = player.Transform:GetWorldPosition()
        if not IsEntityDeadOrGhost(player) and distsq(x, z, x2, z2) < 64 then
            local selectedGift
            inst.ptribe_givePlayer = { player = player }

            -- 检查是否正在饥饿
            if player.components.hunger.current < 10 then
                if has then
                    inst.ptribe_givePlayer.give = true
                    return true
                elseif not player.components.timer:TimerExists("ptribe_pigGiveFood")
                    and inst.prtibe_giveTime and GetTime() > inst.prtibe_giveTime then
                    selectedGift = ResourceConvert.GetRandomGiveFood()
                    player.components.timer:StartTimer("ptribe_pigGiveFood", TUNING.TOTAL_DAY_TIME * 2) --只有一个猪人会给玩家
                    inst.ptribe_givePlayer.give = true
                end
            elseif (player.prefab == "wilba" and not player:HasTag("monster"))                                                                  -- 检查是否是公主
                or (inst.GetTribeManager and inst:GetTribeManager() and inst:GetTribeManager().components.ptribe_pigmanager.favorability >= 80) --是否高好感度
            then
                if has then
                    return true
                elseif GetTime() > inst.prtibe_giveTime then
                    selectedGift = ResourceConvert.GetRandomGiveMaterials()
                end
            end

            if selectedGift then
                inst._pig_token_prefab = selectedGift
                local note = SpawnPrefab(inst._pig_token_prefab)
                ResourceConvert.InitPigmanGiveMaterials(inst, note, player)

                inst.components.inventory:GiveItem(note)
                inst.prtibe_giveTime = GetTime() + TUNING.PTRIBE_PIG_GIVE_WILBA_PERIOD * (0.6 + math.random())
                return true
            end
        end
    end

    inst.ptribe_givePlayer = nil
    return false
end

local function GivePlayerPigTokenAction(inst)
    local player = GetLeader(inst)
    if not player and inst.ptribe_givePlayer then
        if IsEntityDeadOrGhost(inst.ptribe_givePlayer.player) then
            inst.ptribe_givePlayer = nil
        else
            player = inst.ptribe_givePlayer.player
        end
    end

    if player ~= nil then
        local note = next(inst.components.inventory:GetItemByName(inst._pig_token_prefab, 1))
        if note ~= nil then
            local action = inst.ptribe_givePlayer and inst.ptribe_givePlayer.give and ACTIONS.GIVE or ACTIONS.DROP
            return BufferedAction(inst, player, action, note)
        end
    end
end


local function GetFaceTargetFn(inst)
    return inst.components.follower.leader
end

local function KeepFaceTargetFn(inst, target)
    return inst.components.follower.leader == target
end

---### 我希望面对薇尔芭时能换个台词
local function PigTalkToPlayer(inst)
    local chatlines = inst.ptribe_faceWilba and "PIG_TALK_LOOKATWILBA" or "PIG_TALK_LOOKATWILSON"
    local strtbl = STRINGS[chatlines]
    return strtbl and strtbl[math.random(#strtbl)] or nil
end

local function GetFaceTargetNearestPlayerFn(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local player = FindClosestPlayerInRange(x, y, z, START_RUN_DIST + 1, true)
    if player and player.prefab == "wilba" then
        inst.ptribe_faceWilba = true --其实这里用ptribe_wilba_pig变量也行，但是有可能薇尔芭只是在附近，猪人面对的不是薇尔芭
    else
        inst.ptribe_faceWilba = nil
    end

    return player
end

local function KeepFaceTargetNearestPlayerFn(inst, target)
    return GetFaceTargetNearestPlayerFn(inst) == target
end

local function SafeLightDist(inst, target)
    return (target:HasTag("player") or target:HasTag("playerlight")
            or (target.inventoryitem and target.inventoryitem:GetGrandOwner() and target.inventoryitem:GetGrandOwner():HasTag("player")))
        and 4
        or target.Light:GetCalculatedRadius() / 3
end

local function IsHomeOnFire(inst)
    return inst.components.homeseeker
        and inst.components.homeseeker.home
        and inst.components.homeseeker.home.components.burnable
        and inst.components.homeseeker.home.components.burnable:IsBurning()
        and inst:GetDistanceSqToInst(inst.components.homeseeker.home) < SEE_BURNING_HOME_DIST_SQ
end

local function WatchingMinigame(inst)
    return inst.components.minigame_spectator ~= nil and inst.components.minigame_spectator:GetMinigame()
end

local function WatchingMinigame_MinDist(inst)
    return inst.components.minigame_spectator:GetMinigame().components.minigame.watchdist_min
end
local function WatchingMinigame_TargetDist(inst)
    return inst.components.minigame_spectator:GetMinigame().components.minigame.watchdist_target
end
local function WatchingMinigame_MaxDist(inst)
    return inst.components.minigame_spectator:GetMinigame().components.minigame.watchdist_max
end

local function WatchingCheaters(inst)
    local minigame = WatchingMinigame(inst) or nil
    if minigame ~= nil and minigame._minigame_elites ~= nil then
        for k, v in pairs(minigame._minigame_elites) do
            if k:WasCheated() then
                return minigame
            end
        end
    end
end

local function CurrentContestTarget(inst)
    local stage = inst.npc_stage
    if stage.current_contest_target then
        return stage.current_contest_target
    else
        return stage
    end
end

local function MarkPost(inst)
    if inst.yotb_post_to_mark ~= nil then
        return BufferedAction(inst, inst.yotb_post_to_mark, ACTIONS.MARK)
    end
end

local function CollctPrize(inst)
    if inst.yotb_prize_to_collect ~= nil then
        local x, y, z = inst.yotb_prize_to_collect.Transform:GetWorldPosition()
        if y < 0.1 and y > -0.1 and not inst.yotb_prize_to_collect:HasTag("INLIMBO") then
            return BufferedAction(inst, inst.yotb_prize_to_collect, ACTIONS.PICKUP)
        end
    end
end

local function IsWatchingMinigameIntro(inst)
    local minigame = inst.components.minigame_spectator ~= nil and inst.components.minigame_spectator:GetMinigame() or
        nil
    return minigame ~= nil and minigame.sg ~= nil and minigame.sg:HasStateTag("intro")
end

local function GetGameLocation(inst)
    return inst.components.knownlocations:GetLocation("pigking")
end

-- ###
local function GetTaskAction(inst, task, home)
    local prefab = task.prefab
    if task.type == "spawn" then
        -- 也行执行前应该再检查一下附近是否有中途添加的建筑
        -- 生成
        return BufferedAction(inst, nil, ACTIONS.PTRIBE_DOACTION, nil, task.pos)
    elseif prefab and prefab:IsValid() then
        if task.type == "extinguish" then
            if prefab.components.burnable and (prefab.components.burnable:IsSmoldering() or prefab.components.burnable:IsBurning()) then
                -- 灭火
                return BufferedAction(inst, prefab, ACTIONS.PTRIBE_DOACTION)
            end
        elseif task.type == "item" then
            if prefab.components.inventoryitem and not prefab.components.inventoryitem:IsHeld()
                and prefab.prefab ~= inst._pig_token_prefab --猪人不能把自己送出去的礼物再捡回来
            then
                -- 捡垃圾
                return BufferedAction(inst, prefab, ACTIONS.PICKUP)
            end
        elseif task.type == "interact" then
            if ResourceConvert.PrefabIsInteractive(prefab) then
                return BufferedAction(inst, prefab, ACTIONS.PTRIBE_DOACTION)
            end
        elseif task.type == "work" then
            local workAction = prefab.components.workable and prefab.components.workable:GetWorkAction() or nil
            if workAction then
                -- 工作
                return BufferedAction(inst, prefab, workAction)
            end
        end
    end
    home.components.ptribe_pigmanager:FinishTask() -- 任务完成处理
end

local function GetTask(inst)
    if inst.components.follower.leader then return end
    local home = inst.components.homeseeker and inst.components.homeseeker:GetHome()
    local task = home and home.components.ptribe_pigmanager and
        home.components.ptribe_pigmanager.task or nil
    return task, home
end

local function FindTask1Action(inst)
    local task, home = GetTask(inst)
    if task and task.priority == 1 then
        return GetTaskAction(inst, task, home)
    end
end

local function FindTask3Action(inst)
    local task, home = GetTask(inst)
    return task and GetTaskAction(inst, task, home) or nil
end

--默认跑着过去，isWalk为true时走过去
local function TaskShouldRunAway(inst)
    local task = GetTask(inst)
    return task and task.priority == 1 and not task.isWalk
end

local function FollowMinDis(inst)
    return inst.components.container and inst.components.container.canbeopened and 0 or MIN_FOLLOW_DIST
end

local function OnStart(self)
    -- 猪王摔跤比赛
    local in_contest = WhileNode(function()
            return self.inst:HasTag("NPC_contestant")
        end, "In contest",
        PriorityNode({
            --            IfNode(function() return self.inst.yotb_post_to_mark end, "mark post",
            DoAction(self.inst, CollctPrize, "collect prize", true),
            DoAction(self.inst, MarkPost, "mark post", true), --)
            WhileNode(function()
                    return self.inst.components.timer and self.inst.components.timer:TimerExists("contest_panic")
                end, "Panic Contest",
                ChattyNode(self.inst, "PIG_TALK_CONTEST_PANIC",
                    Panic(self.inst))),
            ChattyNode(self.inst, "PIG_TALK_CONTEST_OOOH",
                FaceEntity(self.inst, CurrentContestTarget, CurrentContestTarget), 5, 15),
        }, 0.1))

    -- 猪王摔跤比赛
    local watch_game = WhileNode(function()
            return WatchingMinigame(self.inst)
        end, "Watching Game",
        PriorityNode({
            IfNode(function()
                    return WatchingMinigame(self.inst).components.minigame.gametype == "pigking_wrestling"
                end, "Is Pig King Wrestling",
                PriorityNode({
                    ChattyNode(self.inst, "PIG_TALK_GAME_GOTO",
                        Follow(self.inst, WatchingMinigame, WatchingMinigame_MinDist, WatchingMinigame_TargetDist,
                            WatchingMinigame_MaxDist)),
                    WhileNode(function()
                            return IsWatchingMinigameIntro(self.inst)
                        end, "Is Intro",
                        PriorityNode({
                            RunAway(self.inst, "minigame_participator", 5, 7),
                            ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
                                DoAction(self.inst, FindFoodAction)),
                            FaceEntity(self.inst, WatchingMinigame, WatchingMinigame),
                        }, 0.1)),
                    ChattyNode(self.inst, "PIG_TALK_GAME_CHEER",
                        RunAway(self.inst, "minigame_participator", 5, 7)),
                    ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
                        DoAction(self.inst, FindFoodAction)),
                    ChattyNode(self.inst, "PIG_ELITE_SALTY",
                        FaceEntity(self.inst, WatchingCheaters, WatchingCheaters), 5, 15),
                    ChattyNode(self.inst, "PIG_TALK_GAME_CHEER",
                        FaceEntity(self.inst, WatchingMinigame, WatchingMinigame), 5, 15),
                }, 0.1)
            ),
            PriorityNode({
                ChattyNode(self.inst, "PIG_TALK_MISC_GAME_GOTO",
                    Follow(self.inst, WatchingMinigame, WatchingMinigame_MinDist, WatchingMinigame_TargetDist,
                        WatchingMinigame_MaxDist)),
                ChattyNode(self.inst, "PIG_TALK_MISC_GAME_CHEER",
                    RunAway(self.inst, "minigame_participator", 5, 7)),
                ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
                    DoAction(self.inst, FindFoodAction)),
                ChattyNode(self.inst, "PIG_TALK_MISC_GAME_CHEER",
                    FaceEntity(self.inst, WatchingMinigame, WatchingMinigame), 5, 15),
            }, 0.1),
        }, 0.1)
    )

    local day = WhileNode(function()
            return TheWorld.state.isday
                or self.inst:HasTag("ptribe_tribeinvader") -- 侵略者晚上也跟白天一样
                or self.inst:HasTag("guard")               -- 这里只是针对皇家守卫，原版的猪人守卫用的不是这个brain
        end, "IsDay",
        PriorityNode {
            --猪找吃的
            ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
                DoAction(self.inst, FindFoodAction)),
            --砍树
            IfThenDoWhileNode(function()
                    return StartChoppingCondition(self.inst)
                end, function()
                    return KeepChoppingAction(self.inst)
                end, "chop",
                LoopNode {
                    ChattyNode(self.inst, "PIG_TALK_HELP_CHOP_WOOD",
                        DoAction(self.inst, FindTreeToChopAction)) }),
            --跟着领导者
            ChattyNode(self.inst, "PIG_TALK_FOLLOWWILSON",
                Follow(self.inst, GetLeader, FollowMinDis, TARGET_FOLLOW_DIST, MAX_FOLLOW_DIST)),
            --对着领导者，时不时讲两句
            IfNode(function()
                    return GetLeader(self.inst)
                end, "has leader",
                ChattyNode(self.inst, "PIG_TALK_FOLLOWWILSON", FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn))),


            --### 三级，建造，不要问我为什么没有二级任务，暂时用不到
            ChattyNode(self.inst, "PIG_PTRIBE_DO_ACTION",
                DoAction(self.inst, FindTask3Action)),

            --生物追逐玩家跑出的领地的最大距离后，会先停在原地，然后等到下一次检测时，返回原本位置
            Leash(self.inst, GetNoLeaderHomePos, LEASH_MAX_DIST, LEASH_RETURN_DIST),
            --猪人不会离玩家太近
            ChattyNode(self.inst, "PIG_TALK_RUNAWAY_WILSON",
                RunAway(self.inst, "player", START_RUN_DIST, STOP_RUN_DIST, function(target)
                    return not self.inst.components.ptribe_pigtask
                        or self.inst.components.ptribe_pigtask.taskId:value() == 0 --###有任务的猪人不应该远离玩家
                end)),

            --面朝玩家，时不时讲两句
            ChattyNode(self.inst, PigTalkToPlayer,
                FaceEntity(self.inst, GetFaceTargetNearestPlayerFn, KeepFaceTargetNearestPlayerFn)),
            --漫步
            Wander(self.inst, GetNoLeaderHomePos, MAX_WANDER_DIST)
        }, .5)

    local night = WhileNode(function()
            return not TheWorld.state.isday
                and not self.inst:HasTag("ptribe_tribeinvader") --### 猪人入侵期间，攻击者不会回家
                and not self.inst:HasTag("ptribe_tribevictim")  -- 猪人入侵期间，受害者者不会回家
                and not self.inst:HasTag("guard")               -- 这里只是针对皇家守卫，原版的猪人守卫用的不是这个brain
        end, "IsNight",
        PriorityNode {
            --远离蜘蛛
            ChattyNode(self.inst, "PIG_TALK_RUN_FROM_SPIDER",
                RunAway(self.inst, "spider", 4, 8)),
            --猪找吃的
            ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
                DoAction(self.inst, FindFoodAction)),

            --玩家靠近就远离，除非是交易的
            RunAway(self.inst, "player", START_RUN_DIST, STOP_RUN_DIST, function(target)
                return ShouldRunAway(self.inst, target)
            end),

            --夜晚回家
            ChattyNode(self.inst, "PIG_TALK_GO_HOME",
                WhileNode(function()
                        return not TheWorld.state.iscaveday or not self.inst:IsInLight()
                    end, "Cave nightness",
                    DoAction(self.inst, GoHomeAction, "go home", true))),

            --喜欢待在有光的地方
            WhileNode(function()
                    return TheWorld.state.isnight and self.inst:IsLightGreaterThan(COMFORT_LIGHT_LEVEL)
                end, "IsInLight", -- wants slightly brighter light for this
                Wander(self.inst, GetNearestLightPos, GetNearestLightRadius, {
                    minwalktime = 0.6,
                    randwalktime = 0.2,
                    minwaittime = 5,
                    randwaittime = 5
                })
            ),
            --寻找光源
            ChattyNode(self.inst, "PIG_TALK_FIND_LIGHT",
                FindLight(self.inst, SEE_LIGHT_DIST, SafeLightDist)),
            --没有光源，四处逃窜
            ChattyNode(self.inst, "PIG_TALK_PANIC",
                Panic(self.inst)),
        }, 1)

    local root = PriorityNode({
        --### 一级，在恐惧之上
        ChattyNode(self.inst, "PIG_PTRIBE_DO_ACTION",
            DoAction(self.inst, FindTask1Action, nil, Utils.FnParameterExtend(TaskShouldRunAway, self.inst))),

        --恐惧
        BrainCommon.PanicWhenScared(self.inst, .25, "PIG_TALK_PANICBOSS"),
        --被作祟后执行Panic
        WhileNode(function()
                return self.inst.components.hauntable and self.inst.components.hauntable.panic
            end, "PanicHaunted",
            ChattyNode(self.inst, "PIG_TALK_PANICHAUNT",
                Panic(self.inst))),
        -- 条件修饰节点，受到火焰伤害后执行Panic
        WhileNode(function()
                return self.inst.components.health.takingfiredamage
            end, "OnFire",
            ChattyNode(self.inst, "PIG_TALK_PANICFIRE",
                Panic(self.inst))),

        BrainCommon.IpecacsyrupPanicTrigger(self.inst),
        -- 追逐并攻击
        ChattyNode(self.inst, "PIG_TALK_FIGHT",
            WhileNode(function()
                    return self.inst.components.combat.target == nil or not self.inst.components.combat:InCooldown()
                end, "AttackMomentarily",
                PriorityNode({
                    WhileNode(
                        function()
                            return self.inst.prefab == "ptribe_wildbore" and self.inst.components.combat.target and
                                (distsq(self.inst.components.combat.target:GetPosition(), self.inst:GetPosition()) > 6 * 6 or self.inst.sg:HasStateTag("charging"))
                        end,
                        --If you're far away or already doing a charge, charge.
                        "RamAttack", ChaseAndRam(self.inst, MAX_CHARGE_TIME, CHASE_GIVEUP_DIST, MAX_CHARGE_DIST)),
                    -- 常规攻击
                    ChaseAndAttack(self.inst, MAX_CHASE_TIME, MAX_CHASE_DIST),
                }, 0.1)
            )),
        --救援，帮玩家去除粘液
        ChattyNode(self.inst, "PIG_TALK_RESCUE",
            WhileNode(function()
                    return GetLeader(self.inst) and GetLeader(self.inst).components.pinnable and
                        GetLeader(self.inst).components.pinnable:IsStuck()
                end, "Leader Phlegmed",
                DoAction(self.inst, RescueLeaderAction, "Rescue Leader", true))),
        --猪人攻击后在冷却期间远离目标
        ChattyNode(self.inst, "PIG_TALK_FIGHT",
            WhileNode(function()
                    return self.inst.components.combat.target and self.inst.components.combat:InCooldown()
                end, "Dodge",
                RunAway(self.inst, function()
                    return self.inst.components.combat.target
                end, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST))),

        --房子着火
        WhileNode(function()
                return IsHomeOnFire(self.inst)
            end, "OnFire",
            ChattyNode(self.inst, "PIG_TALK_PANICHOUSEFIRE",
                Panic(self.inst))),
        --有猪人想打自己就跑
        RunAway(self.inst, function(guy)
            return guy:HasTag("pig") and guy.components.combat and guy.components.combat.target == self.inst
        end, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST),
        --有人要跟自己交易
        ChattyNode(self.inst, "PIG_TALK_ATTEMPT_TRADE",
            FaceEntity(self.inst, GetTraderFn, KeepTraderFn)),
        --猪人送礼
        ChattyNode(self.inst, "PIG_TALK_GIVE_GIFT",
            WhileNode(function()
                    return WantsToGivePlayerPigTokenAction(self.inst)
                end, "Wants To Give Token",
                DoAction(self.inst, GivePlayerPigTokenAction, "Giving Token", true))),

        in_contest,
        watch_game,
        day,
        night,
    }, .5)

    self.bt = BT(self.inst, root)
end

AddClassPostConstruct("brains/pigbrain", function(self)
    self.OnStart = OnStart
end)
