local Utils = require("ptribe_utils/utils")

for _, name in ipairs({
    "pig",
    "wilba",
    "wilbatodaywalker",
    "wilson",
}) do
    modimport("modmain/sg/" .. name)
end
----------------------------------------------------------------------------------------------------

AddStategraphActionHandler("ptribe_pick", ActionHandler(ACTIONS.PTRIBE_DOACTION, "ptribe_pick"))

AddStategraphEvent("pigelite", EventHandler("stopfollowing", function(inst)
    if inst:HasTag("ptribe_pig") and (inst.components.health == nil or not inst.components.health:IsDead()) then
        inst.sg:GoToState("despawn")
    end
end))

AddStategraphPostInit("pigelite", function(sg)
    if sg.states["despawn"] then          --有两个同名的sg，不判断的话猪年活动召唤猪人会崩溃
        Utils.FnDecorator(sg.states["despawn"].events["animqueueover"], "fn", function(inst)
            inst:PushEvent("detachchild") --猪人房重新生成
        end)
    end
end)

----------------------------------------------------------------------------------------------------
