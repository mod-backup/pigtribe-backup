local Utils = require("ptribe_utils/utils")
local Constructor = require("ptribe_utils/constructor")
Constructor.SetEnv(env)
local ResourceConvert = require("ptribe_resourceconvert")
local BluePrintsUtils = require("ptribe_blueprintsutils")
local GetPrefab = require("ptribe_utils/getprefab")
local Shapes = require("ptribe_utils/shapes")

-- 猪人守卫同时拥有inventory和container组件，如果玩家右键容器里的食物吃掉时，当吃掉最后一个食物时物品不会移除，这可能是科雷的bug，这里在每次吃掉吃前移出容器
Utils.FnDecorator(ACTIONS.EAT, "fn", function(act)
    local obj = act.target or act.invobject
    if obj and obj.components.edible ~= nil and act.doer.components.eater ~= nil then
        local owner = obj.components.inventoryitem and obj.components.inventoryitem.owner
        if owner and owner:HasTag("guard")
            and owner.components.container and owner.components.container:IsHolding(obj)
        then
            obj = owner.components.container:RemoveItem(obj)
            return { act.doer.components.eater:Eat(obj, act.doer) }, true
        end
    end
end)

local DEFAULT_ACTION_DIS = 2.5
local function extra_arrive_dist(doer)
    local task = doer.ptribe_task
    if not task then
        local home = doer.components.homeseeker and doer.components.homeseeker:GetHome()
        task = home and home.components.ptribe_pigmanager and home.components.ptribe_pigmanager.task or nil
    end

    local prefab = task and task.prefab
    if not prefab then return DEFAULT_ACTION_DIS end

    local radius
    if type(prefab) == "string" then
        radius = BluePrintsUtils.GetPhysicsRadius(prefab)
    else
        radius = BluePrintsUtils.GetPhysicsRadius(prefab.prefab)
    end

    return radius or DEFAULT_ACTION_DIS
end

--在PushBufferedAction里,要设置instant为true才会调用fn,否则就只是推送startaction事件，应该是先不处理交给sg来决定执行时间的
-- extra_arrive_dist、distance:执行action的最短距离
local PTRIBE_DOACTION = Constructor.AddAction({ mount_valid = true, extra_arrive_dist = extra_arrive_dist },
    "PTRIBE_DOACTION",
    STRINGS.ACTIONS.PTRIBE_DOACTION.GENERIC,
    function(act)
        local doer = act.doer
        local task = doer.ptribe_task --猪人自己的

        local home
        local manager
        local data
        if not task then
            --部落的
            home = doer.components.homeseeker and doer.components.homeseeker:GetHome()
            task = home and home.components.ptribe_pigmanager and home.components.ptribe_pigmanager.task or nil
            if not task then return end

            manager = home.components.ptribe_pigmanager:GetManager()
            if not manager then
                home.components.ptribe_pigmanager:CancelTask() --村长房没了
                return
            end
        end
        local type = task.type

        if type == "spawn" or type == "deploy" then
            if task.tile then
                -- 地皮
                local x, y = TheWorld.Map:GetTileCoordsAtPoint(task.pos:Get())
                TheWorld.Map:SetTile(x, y, task.tile)

                if manager then
                    home.components.ptribe_pigmanager:ManagerResourceDoDelta(-1)
                end
            else
                -- 建筑
                local inst = SpawnAt(task.prefab, task.pos)
                if inst then --有可能是不存在的预制体 ，这里要判断
                    -- 换皮
                    if math.random() < TUNING.TRIBE_CHANGE_SKIN_PROB then
                        TheWorld.components.ptribe_pigskinmanager:RandomSkin(inst)
                    end

                    if manager then
                        -- 初始化
                        inst.components.ptribe_pigmanbuild:Init()
                        ResourceConvert.PrefabInit(inst, doer, manager)
                    end
                    data = inst
                else
                    TheNet:SystemMessage("无法生成预制体 " .. task.prefab .. " ，请检查游戏是否存在该预制体，或者重新修改蓝图数据")
                end

                if type == "deploy" and inst.components.deployable and inst.components.deployable.ondeploy then
                    inst.components.deployable.ondeploy(inst, task.pos, doer, 0)
                end

                if manager then
                    home.components.ptribe_pigmanager:ManagerResourceDoDelta(-ResourceConvert.GetResource(task.prefab))
                end
            end
        elseif type == "extinguish" then
            -- 一级灭火
            if task.prefab.components.burnable then
                task.prefab.components.burnable:Extinguish(true, -4)
            end
            data = task.prefab
        elseif type == "interact" then
            -- 交互
            ResourceConvert.PrefabInteract(task.prefab, doer, manager) --交互前会再判断一次的，因此这里不用判断
            data = task.prefab
        elseif type == "wildpig" then
            -- 野外猪人交互
            if task.prefab.components.ptribe_wildpigbuild:CanInteract() then
                task.prefab.components.ptribe_wildpigbuild:Interact(doer)
            end
        end

        if task.onFinish then
            task.onFinish(doer, data)
        end

        if doer.ptribe_task then
            doer.ptribe_task = nil
        elseif manager then
            home.components.ptribe_pigmanager:FinishTask()
        end
        return true
    end
)

AddStategraphActionHandler("pig", ActionHandler(PTRIBE_DOACTION, "pickup"))

-- 从猪人接任务
Constructor.AddAction({ priority = 1 },
    "PTRIBE_VIEW_TASK",
    STRINGS.ACTIONS.PTRIBE_VIEW_TASK.GENERIC,
    function(act)
        if not act.doer.components.ptribe_playertask then return false end
        local taskId = act.target.components.ptribe_pigtask.taskId:value()
        if taskId == 0 then return false end

        -- act.target.ptribe_lookingTask = act.doer --查看任务时猪人保持静止，但是客机得推送窗口的关闭了，感觉挺麻烦的
        local oldTaskId = act.doer.components.ptribe_playertask.taskId or 0
        local isSamePig = act.doer.components.ptribe_playertask.pig == act.target and 1 or 0
        act.doer.components.ptribe_playertask.cachePig = act.target --先设置猪人

        SendModRPCToClient(GetClientModRPC("PigmanTribe", "PopupPlayerTaskDialog"),
            act.doer.userid, taskId, oldTaskId, isSamePig)
        return true
    end,
    "give", "give")

Constructor.AddAction({ priority = 1 },
    "PTRIBE_FOLLOW_PIG_HOME",
    STRINGS.ACTIONS.PTRIBE_FOLLOW_PIG_HOME.GENERIC,
    function(act)
        local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
        if not pigking then return false end

        act.target:PushEvent("despawn")
        act.doer.ptribe_homePos = Shapes.GetRandomLocation(pigking:GetPosition(), 4, 12)
        act.doer.sg:GoToState("ptribe_wilba_home")

        return true
    end,
    "give", "give")

----------------------------------------------------------------------------------------------------
--柜子
Constructor.AddAction({ priority = 5, distance = 1 },
    "PTRIBE_PLACE",
    STRINGS.ACTIONS.PTRIBE_PLACE.GENERIC,
    function(act)
        return act.target.components.shelfer
            and act.target.components.shelfer:AcceptGift(act.doer, act.invobject)
            or false
    end,
    "give", "give")

Constructor.AddAction({ priority = 5, distance = 1 },
    "PTRIBE_TAKE",
    STRINGS.ACTIONS.PTRIBE_TAKE.GENERIC,
    function(act)
        local item = act.target.components.shelfer and act.target.components.shelfer:GiveGift()
        if item and act.doer.components.inventory then
            act.doer.components.inventory:GiveItem(item)
            return true
        end
    end,
    "give", "give")

----------------------------------------------------------------------------------------------------
--合并地毯和旋转地毯
Constructor.AddAction({ priority = 5 },
    "PTRIBE_MERGE",
    STRINGS.ACTIONS.PTRIBE_MERGE.GENERIC,
    function(act)
        local status, msg = act.target:Merge()
        if status then
            act.invobject.components.stackable:Get():Remove()
            return true
        else
            return false, msg
        end
    end,
    "give", "give")
Constructor.AddAction({ priority = 5 },
    "PTRIBE_ROTATE",
    STRINGS.ACTIONS.PTRIBE_ROTATE.GENERIC,
    function(act)
        act.target:Rotate()
        act.invobject.components.stackable:Get():Remove()
        return true
    end,
    "give", "give")


--进出房子
Utils.FnDecorator(ACTIONS.JUMPIN, "strfn", nil, function(retTab, act)
    if not next(retTab) and act.target then
        if act.target:HasTag("hamlet_houseexit") then
            return { "PTRIBE_LEAVE_HOME" } --离开
        elseif act.target:HasTag("hamlet_door") then
            return { "PTRIBE_ENTER_HOME" } --进入
        end
    end
    return retTab
end)

-- 翻新
Constructor.AddAction({ priority = 5 },
    "RENOVATE",
    STRINGS.ACTIONS.RENOVATE.GENERIC,
    function(act)
        if act.target:HasTag("renovatable") then
            if act.invobject.components.renovator then
                act.invobject.components.renovator:Renovate(act.target)
            end

            act.invobject:Remove()

            return true
        end
    end,
    "give", "give")

-- 建造房屋
Constructor.AddAction({ priority = 5 },
    "BUILD_ROOM",
    STRINGS.ACTIONS.BUILD_ROOM.GENERIC,
    function(act)
        if act.invobject.components.roombuilder and act.target.components.hamletdoor then
            if act.target.components.hamletdoor.centerPos then
                return false, "ALREADY_EXISTED"
            end

            if act.invobject.components.roombuilder:CreateNewRoom(act.target) then
                act.invobject:Remove()
                return true
            end
        end
        return false
    end,
    "give", "give")

--- 给予装备并替换装备，因为我给猪人守卫添加了container，导致给予的动作被替换为存放，我需要一个高优先级的动作，玩家可以用装备替换猪人已经穿戴的装备
Constructor.AddAction({ priority = 5 },
    "PTRIBE_REPLACE_TRADE",
    STRINGS.ACTIONS.PTRIBE_REPLACE_TRADE.GENERIC,
    function(act)
        local item = act.invobject
        local slot = item.components.equippable ~= nil and item.components.equippable.equipslot
        if not slot or not act.target.components.container then return false end

        local current = act.target.components.inventory:GetEquippedItem(slot)
        if current ~= nil then
            local it = act.target.components.inventory:RemoveItem(current) --会触发监听事件的
            if not act.target.components.container:GiveItem(it) then
                act.target.components.inventory:DropItem(it)               --计算不在物品栏了照样可以用来丢出
            end
        end

        act.target.components.inventory:Equip(item)
        return true
    end,
    "give", "give")

--- 通用升级处理
Constructor.AddAction({ priority = 5 },
    "PTRIBE_UPGRADE",
    STRINGS.ACTIONS.PTRIBE_UPGRADE.GENERIC,
    function(act)
        if not act.target.components.ptribe_upgrade then return false end

        local need = act.target.components.ptribe_upgrade:GetNeed()
        need = math.min(need, GetStackSize(act.invobject))

        local res, msg = act.target.components.ptribe_upgrade:Upgrade(act.doer, need)
        if res then
            act.invobject.components.stackable:Get(need):Remove()
            return true
        else
            return false, msg
        end
    end,
    "give", "give")

--- 允许噩梦猪人把可工作对象作为攻击对象
Constructor.AddAction(
    { priority = 10, rmb = true, distance = math.huge },
    "PTRIBE_WILBA_MONSTER_WORK",
    STRINGS.ACTIONS.ATTACK.GENERIC,
    Utils.TrueFn,
    "attack_pounce_pre"
)
--- 噩梦猪人重砸技能
Constructor.AddAction(
    { priority = 10, rmb = true, distance = math.huge },
    "PTRIBE_USE_WILBA_SKILL",
    STRINGS.ACTIONS.PTRIBE_USE_WILBA_SKILL,
    Utils.TrueFn,
    "attack_slam_pre"
)

----------------------------------------------------------------------------------------------------
-- 分发武器
Constructor.AddAction(
    { instant = true },
    "PTRIBE_DISTRIBUTE",
    STRINGS.ACTIONS.PTRIBE_DISTRIBUTE,
    function(act)
        local player = act.doer
        local fac = act.target
        if not player or not player.components.leader
            or not fac or not fac.components.container or fac.components.container:IsEmpty()
        then
            return false
        end

        -- 所有猪人守卫
        local guards = {}
        for k, _ in pairs(player.components.leader.followers) do
            if k:HasTag("guard") and k.components.container then
                table.insert(guards, k)
            end
        end

        local weapons = fac.components.container:RemoveAllItems()
        if fac.components.container:IsOpen() then
            fac.components.container:Close(player)
        end

        for _, weapon in ipairs(weapons) do
            local isPig = false
            for _, pig in ipairs(guards) do
                local slot = weapon.components.equippable and weapon.components.equippable.equipslot
                if not pig.components.container:IsFull() and slot then
                    pig.components.container:GiveItem(weapon)
                    if not pig.components.inventory:GetEquippedItem(slot) then
                        pig.components.inventory:Equip(weapon)
                    end
                    isPig = true
                    break
                end
            end

            if not isPig then
                player.components.inventory:GiveItem(weapon)
            end
        end

        return true
    end,
    "give", "give"
)
----------------------------------------------------------------------------------------------------
-- 使用道具
Constructor.AddAction({ priority = 1 },
    "PTRIBE_USE_ITEM",
    function(act)
        local str = act.invobject.components.ptribe_consumable.str
        return type(str) == "function" and str(act.invobject, act.doer, act.target) or str
    end,
    function(act)
        return act.invobject.components.ptribe_consumable:Use(act.doer, act.target)
    end
)
