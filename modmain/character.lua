local name = "wilba"
table.insert(PrefabFiles, name)

STRINGS.CHARACTER_SURVIVABILITY[name] = PTRIBE_LAGUAGE_LOC("Severe", "严峻")

table.insert(Assets, Asset("ATLAS", "images/saveslot_portraits/" .. name .. ".xml"))
table.insert(Assets, Asset("ATLAS", "images/selectscreen_portraits/" .. name .. ".xml"))
table.insert(Assets, Asset("ATLAS", "images/map_icons/" .. name .. ".xml"))
table.insert(Assets, Asset("ATLAS", "images/avatars/avatar_" .. name .. ".xml"))
table.insert(Assets, Asset("ATLAS", "images/avatars/avatar_ghost_" .. name .. ".xml"))
table.insert(Assets, Asset("ATLAS", "images/avatars/self_inspect_" .. name .. ".xml"))
table.insert(Assets, Asset("ATLAS", "images/names_" .. name .. ".xml"))
table.insert(Assets, Asset("ATLAS", "bigportraits/" .. name .. "_none.xml"))
table.insert(Assets, Asset("ANIM", "anim/ghost_" .. name .. "_build.zip"))

AddMinimapAtlas("images/map_icons/" .. name .. ".xml")
AddModCharacter(name, "FEMALE")
PREFAB_SKINS[name] = { name .. "_none" }
STRINGS.SKIN_NAMES[name .. "_none"] = STRINGS.CHARACTER_NAMES[name]

-- 台词
-- local speech = deepcopy(require "speech_wilson") --原版台词表
-- local override = require("speech_" .. name .. "_" .. PTRIBE_LAGUAGE_LOC("en", "zh"))
-- local function replace(t1, t2)
--     for k, v in pairs(t2) do
--         local tv = type(v)
--         if t1[k] == nil then
--             t1[k] = v
--         elseif type(t1[k]) == tv then
--             if tv == "string" then
--                 t1[k] = v
--             elseif tv == "table" then
--                 replace(t1[k], v)
--             end
--         end
--     end
-- end
-- replace(speech, override)

-- STRINGS.CHARACTERS[name:upper()] = speech
