local GetPrefab = require("ptribe_utils/getprefab")
local Utils = require("ptribe_utils/utils")
local FoodBuffDefs = require("ptribe_foodbuffdefs")
local ResourceConvert = require("ptribe_resourceconvert")

local FN = {}

---野外猪人，brain部分的代码还是在brian文件中设置
---Spawn：生成野外猪人，返回一个表
---Init function|nil (pigs)，初始化野外猪人相关的东西
---Remove function|nil 可省，移除野外猪人相关的东西，猪人不用移除，交由组件移除，需要注意，调用函数的时候猪人已经死亡了
---AcceptTest function|nil (inst, item, giver) 默认不接受
---OnAccept function|nil (inst, giver, item)
---deleteitemonaccept boolean|nil 接受后删除物品，默认不删除
FN.PIGS = {}

----------------------------------------------------------------------------------------------------
local function GiveRandomBuff(harvester, isGood)
    local type = FoodBuffDefs[math.random(1, #FoodBuffDefs)]
    -- local data = shallowcopy(type.data[math.random(1, #type.data)])
    local index
    if isGood then
        index = type.data[1] and 1 or 2 --优先使用好的buff
    else
        index = type.data[2] and 2 or 1 --优先使用坏的buff
    end

    local data = shallowcopy(type.data[index])

    if harvester:ptribe_HasDebuff(type.type, "wildpig_food") then
        harvester:ptribe_RemoveDebuff(type.type, "wildpig_food")
    end

    data.isSave = true --重新游戏继续
    harvester:ptribe_AddDebuff(type.type, "wildpig_food", data)

    harvester:DoTaskInTime(0.5, function()
        local buff = string.upper(type.type)
        local str = STRINGS.PTRIBE_FOOD_BUFFS[buff] and STRINGS.PTRIBE_FOOD_BUFFS[buff][index]
        if harvester.components.talker and str then
            harvester.components.talker:Say(str)
        end
    end)

    SpawnPrefab("spider_heal_target_fx").Transform:SetPosition(harvester.Transform:GetWorldPosition())
    if harvester.SoundEmitter ~= nil then
        harvester.SoundEmitter:PlaySound("webber1/creatures/spider_cannonfodder/heal_fartcloud")
    end
end

-- 野外的猪人厨子：有两顶锅，时不时做饭和收获，玩家与其交易可获得烹饪后的料理
FN.PIGS.chef = {
    Init = function(inst)
        inst.ptribe_ragePoint = 0 --怒气值
        inst.ptribe_ragePointTask = nil
    end,
    AcceptTest = function(inst, item, giver)
        return item.components.edible ~= nil and item:HasTag("preparedfood")
    end,
    -- 给玩家添加buff
    OnAccept = function(inst, giver, item)
        if inst.components.combat:TargetIs(giver) then
            inst.components.combat:SetTarget(nil)
            return
        end

        if not inst.sg:HasStateTag("busy") then
            inst.sg:GoToState("cheer")
        end

        if giver then
            GiveRandomBuff(giver, true)
        end
    end,
    deleteitemonaccept = true,

    GiveRandomBuff = GiveRandomBuff
}

----------------------------------------------------------------------------------------------------
-- 随机蔬菜
local RANDOM_VEGGIES = {}
require("prefabs/veggies")
for name, _ in pairs(VEGGIES) do
    table.insert(RANDOM_VEGGIES, name)
end

FN.PIGS.farmer = {
    AcceptTest = function(inst, item, giver)
        return item.components.edible ~= nil and item:HasTag("preparedfood")
    end,
    OnAccept = function(inst, giver, item)
        if giver.components.inventory then
            local res = math.max(ResourceConvert.GetResource(item), 4)
            for i = 1, res, 4 do
                local veggie = SpawnPrefab(RANDOM_VEGGIES[math.random(1, #RANDOM_VEGGIES)])
                giver.components.inventory:GiveItem(veggie)
            end
        end
    end,
    deleteitemonaccept = true,
}

----------------------------------------------------------------------------------------------------
local function Disappear(inst)
    for k, v in pairs(inst.components.leader.followers) do
        SpawnPrefab("small_puff").Transform:SetPosition(k.Transform:GetWorldPosition())
        k:Remove()
    end

    SpawnPrefab("small_puff").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst:Remove()
end

local function CheckState(inst)
    if inst.components.inventory:IsFull() then
        -- 这里懒得考虑队员是否满了
        if not inst.ptribe_disappearTask and not inst.components.follower.leader then
            inst.components.talker:Say(STRINGS.PTRIBE_PIG_SCAVENGER_DISAPPEAR)
            inst.ptribe_disappearTask = inst:DoTaskInTime(2, Disappear)
        end
    end
end

local function OnAttacked(inst)
    if inst.ptribe_disappearTask then
        inst.ptribe_disappearTask:Cancel()
    end
end

-- 拾荒者
FN.PIGS.scavenger = {
    Init = function(inst)
        inst:AddComponent("leader")

        inst.components.locomotor:SetTriggersCreep(false)
        inst:DoPeriodicTask(2, CheckState)
        inst:ListenForEvent("attacked", OnAttacked)
    end
}

----------------------------------------------------------------------------------------------------

FN.PIGS_LIST = {}
for type, _ in pairs(FN.PIGS) do
    table.insert(FN.PIGS_LIST, type)
end

return FN
