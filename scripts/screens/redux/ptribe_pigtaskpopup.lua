local Utils = require("ptribe_utils/utils")
local Image = require "widgets/image"
local Menu = require "widgets/menu"
local Screen = require "widgets/screen"
local TEMPLATES = require "widgets/redux/templates"
local Text = require "widgets/text"
local Widget = require "widgets/widget"
local ItemTile = require "widgets/itemtile"
local ImageUtils = require("ptribe_imageutils")

local function FakePrefab(name, count)
    count = count or 1
    local img = ImageUtils.GetItemImage(name)
    local item = SpawnPrefab(name)
    --伪造组件
    item.replica.inventoryitem = Utils.FakeFn(require("components/inventoryitem_replica"))
    item.replica.inventoryitem.GetImage = Utils.ConstantFn(img and img.image or (name .. ".tex"))
    item.replica.inventoryitem.GetAtlas = Utils.ConstantFn(img and img.atlas or
    GetInventoryItemAtlas(name .. ".tex"))
    item.replica.inventoryitem.CanOnlyGoInPocket = Utils.FalseFn
    item.replica.inventoryitem.IsWet = Utils.FalseFn

    item.replica.stackable = {
        StackSize = Utils.ConstantFn(count),
        IsStack = Utils.TrueFn
    }
    item.IsAcidSizzling = Utils.EmptyFn

    return item
end


-- This is mostly copied from TEMPLATES.CurlyWindow
local function AddTextToDialog(w, sizeX, sizeY, name, bottom_buttons, button_spacing, desc, condition, reward)
    if name then
        w.title = w.top:AddChild(Text(TITLEFONT, 60, name, UICOLOURS.GOLD))
        w.title:SetPosition(0, -20)
        w.title:SetRegionSize(600, 50)
        w.title:SetHAlign(ANCHOR_MIDDLE)
        if JapaneseOnPS4() then
            w.title:SetSize(40)
        end
    end

    if desc then
        w.desc = w:AddChild(Text(BUTTONFONT, 40, desc, UICOLOURS.BLACK))
        w.desc:EnableWordWrap(true)
        w.desc:SetPosition(0, -35)
        w.desc:SetRegionSize(sizeX - 100, sizeY - 130)
        w.desc:SetVAlign(ANCHOR_TOP)  --垂直
        w.desc:SetHAlign(ANCHOR_LEFT) --水平
    end

    if condition then
        if type(condition) == "string" then
            w.condition = w:AddChild(Text(BUTTONFONT, 40, condition, UICOLOURS.BLACK))
            w.condition:EnableWordWrap(true)
            w.condition:SetPosition(0, -150)
            w.condition:SetRegionSize(sizeX - 100, sizeY - 130)
            w.condition:SetVAlign(ANCHOR_TOP)  --垂直
            w.condition:SetHAlign(ANCHOR_LEFT) --水平
        else
            --任务要求几个预制体
            w.condition = {}
            local baseX = -GetTableSize(condition) / 2 * 60
            for n, count in pairs(condition) do
                --伪造预制体
                local invitem = FakePrefab(n, count)

                local img = w:AddChild(ItemTile(invitem))

                img:SetScale(.6)
                img:SetPosition(baseX, -20)
                baseX = baseX + 60

                table.insert(w.condition, img)
            end
        end
    end

    if reward then
        -- ent.components.inventoryitem.imagename
        w.reward = {}
        local baseX = -GetTableSize(reward) / 2 * 60
        for n, count in pairs(reward) do
            local invitem = FakePrefab(n, count)
            local img = w:AddChild(ItemTile(invitem))

            img:SetScale(.6)
            img:SetPosition(baseX, -120)
            baseX = baseX + 60

            table.insert(w.reward, img)
        end
    end

    if bottom_buttons then
        -- If plain text widgets are passed in, then Menu will use this style.
        -- Otherwise, the style is ignored. Use appropriate style for the
        -- amount of space for buttons. Different styles require different
        -- spacing.
        local style = "carny_long"
        if button_spacing == nil then
            -- 1,2,3,4 buttons can be big at 170,340,510,680 widths.
            local space_per_button = sizeX / #bottom_buttons
            local has_space_for_big_buttons = space_per_button > 169
            if has_space_for_big_buttons then
                style = "carny_xlong"
                button_spacing = 320
            else
                button_spacing = 230
            end
        end
        local button_height = 50
        local button_area_width = button_spacing / 2 * #bottom_buttons
        local is_tight_bottom_fit = button_area_width > sizeX * 2 / 3
        if is_tight_bottom_fit then
            button_height = 60
        end

        -- Does text need to be smaller than 30 for JapaneseOnPS4()?
        w.actions = w.bottom:AddChild(Menu(bottom_buttons, button_spacing, true, style, nil, 30))
        w.actions:SetPosition(-(button_spacing * (#bottom_buttons - 1)) / 2, button_height - 30)

        --~ w.focus_forward = w.actions
    end
end

---
local PigTaskPopup = Class(Screen, function(self, name, desc, condition, reward, bottom_buttons)
    Screen._ctor(self, "PigTaskPopup")

    self.black = self:AddChild(TEMPLATES.BackgroundTint())
    self.proot = self:AddChild(TEMPLATES.ScreenRoot())

    self.bg = self.proot:AddChild(Image("images/ptribe_pigtaskbg.xml", "ptribe_pigtaskbg.tex"))

    local sizeX, sizeY = self.bg:GetSize()
    local button_spacing = nil

    self.dialog = self.proot:AddChild(Widget("dialog"))
    self.dialog.top = self.dialog:AddChild(Widget("top"))
    self.dialog.top:SetPosition(0, 200)
    self.dialog.bottom = self.dialog:AddChild(Widget("bottom"))
    self.dialog.bottom:SetPosition(0, -210)
    AddTextToDialog(self.dialog, sizeX, sizeY, name, bottom_buttons, button_spacing, desc, condition, reward)

    self.buttons = bottom_buttons or {}

    self.default_focus = self.dialog.actions
end)


function PigTaskPopup:OnControl(control, down)
    if PigTaskPopup._base.OnControl(self, control, down) then return true end

    if control == CONTROL_CANCEL and not down then
        if #self.buttons > 1 and self.buttons[#self.buttons] then
            self.buttons[#self.buttons].cb()
            TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/click_move")
            return true
        end
    end
end

function PigTaskPopup:OnDestroy()
    if self.dialog.reward then
        for _, itemTile in ipairs(self.dialog.reward) do
            itemTile.item:Remove()
        end
        self.dialog.reward = nil
    end
    if self.dialog.condition then
        for _, itemTile in ipairs(self.dialog.condition) do
            itemTile.item:Remove()
        end
        self.dialog.condition = nil
    end

    self._base.OnDestroy(self)
end

function PigTaskPopup:GetHelpText()
    local controller_id = TheInput:GetControllerID()
    local t = {}
    if #self.buttons > 1 and self.buttons[#self.buttons] then
        table.insert(t, TheInput:GetLocalizedControl(controller_id, CONTROL_CANCEL) .. " " .. STRINGS.UI.HELP.BACK)
    end
    return table.concat(t, "  ")
end

return PigTaskPopup
