-- local BluePrintsUtils = require("ptribe_blueprintsutils")
-- local Screen = require "widgets/screen"
-- local Widget = require "widgets/widget"
-- local Text = require "widgets/text"

-- local TEMPLATES = require "widgets/redux/templates"
-- local ScrollableList = require "widgets/scrollablelist"

-- local BlueprintScreen = Class(Screen, function(self, owner)
--     Screen._ctor(self, "BlueprintScreen")

--     self.owner = owner

--     self.isopen = false

--     self._scrnw, self._scrnh = TheSim:GetScreenSize()

--     self:SetScaleMode(SCALEMODE_PROPORTIONAL)
--     self:SetMaxPropUpscale(MAX_HUD_SCALE)
--     self:SetPosition(0, 0, 0)
--     self:SetVAnchor(ANCHOR_MIDDLE)
--     self:SetHAnchor(ANCHOR_MIDDLE)

--     self.scalingroot = self:AddChild(Widget("travelablewidgetscalingroot"))
--     self.scalingroot:SetScale(TheFrontEnd:GetHUDScale())

--     self.inst:ListenForEvent("continuefrompause", function()
--         if self.isopen then
--             self.scalingroot:SetScale(TheFrontEnd:GetHUDScale())
--         end
--     end, TheWorld)
--     self.inst:ListenForEvent("refreshhudsize", function(hud, scale)
--         if self.isopen then self.scalingroot:SetScale(scale) end
--     end, owner.HUD.inst)

--     self.root = self.scalingroot:AddChild(TEMPLATES.ScreenRoot("root"))

--     -- secretly this thing is a modal Screen, it just LOOKS like a widget
--     self.black = self.root:AddChild(Image("images/global.xml", "square.tex"))
--     self.black:SetVRegPoint(ANCHOR_MIDDLE)
--     self.black:SetHRegPoint(ANCHOR_MIDDLE)
--     self.black:SetVAnchor(ANCHOR_MIDDLE)
--     self.black:SetHAnchor(ANCHOR_MIDDLE)
--     self.black:SetScaleMode(SCALEMODE_FILLSCREEN)
--     self.black:SetTint(0, 0, 0, 0)
--     self.black.OnMouseButton = function() self:OnCancel() end

--     self.destspanel = self.root:AddChild(TEMPLATES.RectangleWindow(350, 550))
--     self.destspanel:SetPosition(0, 25)

--     self.current = self.destspanel:AddChild(Text(BODYTEXTFONT, 35))
--     self.current:SetPosition(0, 250, 0)
--     self.current:SetRegionSize(350, 50)
--     self.current:SetHAlign(ANCHOR_MIDDLE)

--     self.cancelbutton = self.destspanel:AddChild(
--         TEMPLATES.StandardButton(function() self:OnCancel() end, "Cancel", { 120, 40 }))
--     self.cancelbutton:SetPosition(0, -250)

--     self:LoadDests()
--     self:Show()
--     self.default_focus = self.dests_scroll_list
--     self.isopen = true
-- end)

-- function BlueprintScreen:LoadDests()
--     self:RefreshDests()
-- end

-- function BlueprintScreen:RefreshDests()
--     self.destwidgets = {}
--     local i = 1
--     for name, v in pairs(BluePrintsUtils.GetBlueprintBaseInfo()) do
--         table.insert(self.destwidgets, {
--             index = i,
--             info = { name = name, width = v.width, height = v.height, enable = v.enable },
--         })
--         i = i + 1
--     end

--     local function ScrollWidgetsCtor(context, index)
--         local widget = Widget("widget-" .. index)

--         widget:SetOnGainFocus(function()
--             self.dests_scroll_list:OnWidgetFocus(widget)
--         end)

--         widget.destitem = widget:AddChild(self:DestListItem())
--         local dest = widget.destitem

--         widget.focus_forward = dest

--         return widget
--     end

--     local function ApplyDataToWidget(context, widget, data, index)
--         widget.data = data
--         widget.destitem:Hide()
--         if not data then
--             widget.focus_forward = nil
--             return
--         end

--         widget.focus_forward = widget.destitem
--         widget.destitem:Show()

--         local dest = widget.destitem

--         dest:SetInfo(data.info)
--     end

--     if self.dests_scroll_list then
--         self.dests_scroll_list:Kill()
--     end

--     self.dests_scroll_list = self.destspanel:AddChild(
--         TEMPLATES.ScrollingGrid(self.destwidgets, {
--             context = {},
--             widget_width = 350,
--             widget_height = 90,
--             num_visible_rows = 5,
--             num_columns = 1,
--             item_ctor_fn = ScrollWidgetsCtor,
--             apply_fn = ApplyDataToWidget,
--             scrollbar_offset = 10,
--             scrollbar_height_offset = -60,
--             peek_percent = 0,             -- may init with few clientmods, but have many servermods.
--             allow_bottom_empty_row = true -- it's hidden anyway
--         }))

--     self.dests_scroll_list:SetPosition(0, 0)

--     self.dests_scroll_list:SetFocusChangeDir(MOVE_DOWN, self.cancelbutton)
--     self.cancelbutton:SetFocusChangeDir(MOVE_UP, self.dests_scroll_list)
-- end

-- function BlueprintScreen:DestListItem()
--     local dest = Widget("destination")

--     local item_width, item_height = 340, 90
--     dest.backing = dest:AddChild(TEMPLATES.ListItemBackground(item_width,
--         item_height,
--         function() end))
--     dest.backing.move_on_click = true

--     dest.name = dest:AddChild(Text(BODYTEXTFONT, 35))
--     dest.name:SetVAlign(ANCHOR_MIDDLE)
--     dest.name:SetHAlign(ANCHOR_LEFT)
--     dest.name:SetPosition(0, 10, 0)
--     dest.name:SetRegionSize(300, 40)

--     local cost_py = -20
--     local cost_font = UIFONT
--     local cost_fontsize = 20

--     dest.size = dest:AddChild(Text(cost_font, cost_fontsize))
--     dest.size:SetVAlign(ANCHOR_MIDDLE)
--     dest.size:SetHAlign(ANCHOR_LEFT)
--     dest.size:SetPosition(-100, cost_py, 0)
--     dest.size:SetRegionSize(100, 30)

--     dest.enable = dest:AddChild(Text(cost_font, cost_fontsize))
--     dest.enable:SetVAlign(ANCHOR_MIDDLE)
--     dest.enable:SetHAlign(ANCHOR_LEFT)
--     dest.enable:SetPosition(-30, cost_py, 0)
--     dest.enable:SetRegionSize(100, 30)

--     -- 每个蓝图基本信息
--     dest.SetInfo = function(_, info)
--         if info.name and info.name ~= "" then
--             dest.name:SetString(info.name)
--             dest.name:SetColour(1, 1, 1, 1)
--         else
--             dest.name:SetString("Unknow")
--             dest.name:SetColour(1, 1, 0, 0.6)
--         end

--         dest.size:Show()
--         dest.size:SetString("W: " .. info.width .. ", H: " .. info.height)
--         dest.size:SetColour(1, 1, 1, 0.8)

--         dest.enable:Show()
--         dest.enable:SetString("enable: " .. tostring(info.enable))
--         if info.enable then
--             dest.enable:SetColour(0, 1, 0, 0.8)
--         else
--             dest.enable:SetColour(1, 0, 0, 0.8)
--         end

--         dest.backing:SetOnClick(function()
--             SendModRPCToServer(MOD_RPC["PigmanTribe"]["SetBlueprintEnable"], info.name, info.enable and 0 or 1)
--         end)
--     end

--     dest.focus_forward = dest.backing
--     return dest
-- end

-- function BlueprintScreen:OnCancel()
--     if not self.isopen then return end
--     self.owner.HUD:ClosePigBlueprintScreen()
-- end

-- function BlueprintScreen:OnControl(control, down)
--     if BlueprintScreen._base.OnControl(self, control, down) then return true end

--     if not down then
--         if control == CONTROL_OPEN_DEBUG_CONSOLE then
--             return true
--         elseif control == CONTROL_CANCEL then
--             self:OnCancel()
--         end
--     end
-- end

-- function BlueprintScreen:Close()
--     if self.isopen then
--         self.attach = nil
--         self.black:Kill()
--         self.isopen = false

--         self.inst:DoTaskInTime(.2, function() TheFrontEnd:PopScreen(self) end)
--     end
-- end

-- return BlueprintScreen
