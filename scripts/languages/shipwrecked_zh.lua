STRINGS.NAMES.PTRIBE_SNAKE = "蛇"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_SNAKE = "我想知道它会不会卖点油给我？"
STRINGS.NAMES.PTRIBE_SNAKE_POISON = "毒蛇"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_SNAKE_POISON = "比普通的蛇更厉害！"
STRINGS.NAMES.PTRIBE_SNAKE_FIRE = "火蛇"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_SNAKE_FIRE = "呸！你走开！"
STRINGS.NAMES.PTRIBE_JUNGLETREE = "丛林树"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_JUNGLETREE = {
    BURNING = "真是浪费木材。",
    BURNT = "我觉得我本可以阻止这种事情发生的。",
    CHOPPED = "接招吧，大自然！",
    GENERIC = "那棵树需要理个发。"
}
STRINGS.NAMES.PTRIBE_JUNGLETREESEED = "丛林树种子"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_JUNGLETREESEED = "我可以听到小蛇发出的嘶嘶声！"
STRINGS.NAMES.PTRIBE_JUNGLETREESEED_SAPLING = "丛林树苗"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_JUNGLETREESEED_SAPLING = "它将成长为一棵不错的丛林树。"
STRINGS.NAMES.PTRIBE_WILDBORE = "野猪"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_WILDBORE = "看上去很凶的样子。"
STRINGS.NAMES.PTRIBE_TELESCOPE = "望远镜"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_TELESCOPE = "我用我的小眼睛来侦察..."
STRINGS.NAMES.PTRIBE_SUPERTELESCOPE = "超级望远镜"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_SUPERTELESCOPE = "我可以看到天地的尽头！"

----------------------------------------------------------------------------------------------------
