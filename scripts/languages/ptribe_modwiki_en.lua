STRINGS.SCRAPBOOK.CATS.PIGMANTRIBE = "Pigman Tribe"

STRINGS.NAMES.PTRIBE_WIKI_PIGMANTRIBE = "Pigman Tribe"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_PIGMANTRIBE = [[
Let the pig people build their own tribe, similar to Hamlet's pig town.
- Tribe Blueprint: The Pig People Room will select a suitable blueprint from the blueprint library as a tribe blueprint for the construction of the tribe.
- Pigmen interaction: Pigmen can build, cut trees, dig mines, collect resources, interact with buildings they have built, and interact with nearby players.
- Pigman Ability: As the tribe is completed, the pigman's attributes will gradually improve and they will possess some special abilities.
- Pigmen Quests: Pigmen will regularly release quests, and players can earn rewards and increase the tribe's favorability by completing them.
- Custom blueprint: compatible with Base Projection, can recognize blueprint files in local directories
]]
STRINGS.NAMES.PTRIBE_WIKI_BLUEPRINT = "Blueprint"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_BLUEPRINT = [[
- The final layout of all tribes is defined, with each blueprint featuring at least one Pig House (the manager's location). The Pig House will select a blueprint of appropriate size and rotation angle based on the surrounding environment and a scoring function.
- Players can enable or disable all blueprints in the blueprint library by right-clicking on the Pig King (specifically the Pig King).
- Changing blueprints will not alter the blueprints of current tribes but will affect new Pigmen tribes and all tribes when the game is loaded.
]]
STRINGS.NAMES.PTRIBE_WIKI_TRIBECOMPLETERATE = "Completion rate of tribes (blueprint)"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_TRIBECOMPLETERATE = [[
The completion rate of the tribe is the percentage of the buildings that the pig people have built out of the blueprint buildings.  The higher the completion rate, the closer the tribe is to completion.
- The higher the completion rate of the tribe, the higher the basic attributes of the pig people.
- The higher the completion rate of the tribe, the higher the probability of generating special pigmen, and some abilities of special pigmen will also be unlocked.
- The higher the completion rate of the tribe, the shorter the time it takes for the piggy weapon factory to make weapons.
- The higher the completion rate of the tribe, the more goods the pigman stall sells each time.
]]
STRINGS.NAMES.PTRIBE_WIKI_RESOURCE = "Tribe Resources"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_RESOURCE = [[
- All materials are converted into a number as the total resources of the tribe, and the amount of resources will be displayed at the bottom of the village head's room.
- A series of actions performed by the pig may increase or decrease resources.
- Resources may be negative.


Ways to increase resources:
- Pig people eat and pick up things
- Players give supplies to the village head's house
- Pig people cut trees, dig mines, and interact with buildings
- Win in tribal conflict events
- Pig people need to plant trees but cannot find suitable locations for planting trees
- Slowly increasing loading of resources outside the scope

Ways of consuming resources:
- Pig people carry out blueprint construction
- Pig and building interaction
- The hat given when the pigman room generates a pigman
- Failure in the tribal conflict event
- Pig weapons factory produces weapons
]]
STRINGS.NAMES.PTRIBE_WIKI_TRIBEFAVORABILITY = "Tribe Goodwill"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_TRIBEFAVORABILITY = [[
Each tribe has a positive feeling value for players, and various interactions between players and tribes will affect the positive feeling value.  A high positive feeling value also brings better experience for players.
- The goodwill values between tribes are independent and apply to all players.
- The range of positive feeling value is [-100,100], higher than 0 is positive feeling, lower than 0 is hatred
- The level of goodwill slowly changes every day, gradually stabilizing at [-20,20].  When it is greater than 20, it decreases by 5 every day, and when it is less than -20, it increases by 2 every day.  However, when the level of goodwill reaches 100 or -100, it will not change over time.

The influence of goodwill on players:
1. Pigmen will give players gifts at regular intervals with high affection
2. If the player is attacked with high positive feelings, nearby pig people will also be hostile towards the attacker
3. Food can hire pig people for a longer time under high level of goodwill
4. The tribal pigmen will actively attack players who approach them when their popularity is low

Increase goodwill:
1. Players give the village head housing resources
2. Players give food to the tribe pig
3. Players complete the task of the Horde Pig
4. Players pick up the feces of the tribe

Reduce the goodwill:
1. Players steal tribe resources (not under the stealth hat) and destroy tribe buildings
2. Players attack the tribe pig
3. When the player's favor level is higher than -20, the tribe pig people hired by the player will die
4. Players discard excrements in the tribe
]]
STRINGS.NAMES.PTRIBE_WIKI_TRIBEMANAGER = "Manager's House"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_TRIBEMANAGER = [[
One of the pigman houses in an area will be selected as the village leader house for the tribe, and there will only be one village leader house in nearby pigman houses.  The manager selects a suitable blueprint from the blueprint library as the tribe blueprint, and the village leader house assigns tasks to the pigmen based on the blueprint and current resource quantity.

The role of the village head's house:
- Assign the task of collecting resources to the tribe pig people
- Assign the task of distributing blueprints for construction to the tribal pig people
- Resource sources consumed by pig people and some construction activities
- as the center of judgement for players in the tribe

Conditions for the village head's housing:
1. No other tribes will be generated unless there are no other tribes within the maximum range of all blueprints.
2. Tribes cannot develop in small houses.
3. No tribes will develop near the meat statue.
4. If the blueprint score is too low, it will not generate a tribe (in other words, if the house is surrounded by sea, it will not generate).
]]
STRINGS.NAMES.PTRIBE_WIKI_PIGPROPERTY = "Pigmen Property"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_PIGPROPERTY = [[
The buildings and equipment built by pig people belong to the property of pig people.

- Players who collect buildings built by pigmen and eat dishes in the cooking pot will decrease the tribe's favorability, except for Velba and players wearing stealth hats.
- When the building of the pig people is on fire, one pig person will go to put out the fire.
- Non-player units will be directly attacked during harvesting.
- Pigmen generally increase the resources of the tribe when they gather.
- Any originally invincible building can be destroyed if it was built by pig people.
]]
STRINGS.NAMES.PTRIBE_WIKI_PIGABILITY = "Pigmen Ability"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_PIGABILITY = [[
The attributes of the pigmen will increase with the completion rate of the tribe (the completion rate of the blueprint), and different types of pigmen will also be unlocked.  Unlocking new pigmen requires the death of the current pigmen and the regeneration of the pigman room.
If there are enough resources, the newborn pigman will receive a random hat, which belongs to the pigman's property and will disappear directly when the pigman dies.

Special pig people:
- Kungfu Pig: Not afraid of the dark, not going home, and unquestionably strong.
- Arrow-blowing pig people: use endless arrows, and can use sleep arrows and lightning arrows when the tribe's completion rate is high.
- Iron Pigmen: With 30% damage reduction, they absorb damage for nearby pigmen, preventing the swarm from receiving high-damage hits for a short period of time (overcoming the boss group penalty mechanism, targeting bosses and other mods).
- Exploding Mite Pig: During combat, it constantly drops mites on its body, which explode upon impact with the enemy.
- Space Pigman: After attacking, it will flash to a nearby location and remove the attack cooldown.

The pig's handling of special situations:
1. When a building catches fire, the nearest pig people from the tribe will go to put out the fire.  This task has the highest priority, ahead of fighting and fear (not to burn down the house)
2. Pigmen guards will not actively attack you
3. Actively attack players when the tribe's favorability is low
4. When the manager is destroyed, the pig people will fall into a state of madness, with absolute hatred for a short period of time, and the pig people will receive enhanced movement speed and attack power (not handled at night), and will choose a new manager (village head's house) after a period of time.
5. On the night of the full moon, pig people who have managers will not become crazy pigs even if they do not go home
6. Deal with deadly bright eggplant more intelligently
]]
STRINGS.NAMES.PTRIBE_WIKI_PIGINTERACT = "Pigmen Interact"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_PIGINTERACT = [[
Besides building blueprints, cutting trees, and picking up supplies on the ground, pigmen can also interact with the buildings they build and players.

3.1 Interaction between pig people and blueprint architecture
- Wooden box: There is a chance to get a weapon, a coin, and some gold when it is built
- Fridge: Fill it with food when it is built
- Salt box: stuff some food inside when it is built
- Mushroom lamp: add a light bulb as fuel when it is built, and put a light bulb into it during interaction
- Pig torch: Pig guards no longer actively attack pigs
- Cooking pot: During interaction, the pigman will randomly cook some food
- Basic farms and improved farms: fertilizing, planting, and harvesting during interaction
- Mushroom farm: fertilize, grow mushrooms, and harvest during interaction
- Mad Scientist Laboratory: Do experiments during interaction and produce some potions
- Birdcage: Put in birds and feed them during interaction
- Brick oven: interactive collection and cooking
- Winter feast table: interactive placement and enjoyment of food
- Magma pool: It will not overheat and will not catch fire around it
- Meat drying rack: randomly drying items during interaction
- Beehive: Harvest honey during interactions, and the Pig People will not be attacked when harvesting honey (they will still be attacked in spring)
- Snowball Launcher: add fuel during interaction
- Feast tree pot: Randomly plant a tree during interaction and periodically add decorations
- Branches, grass, berry bushes, ....: Collect and fertilize during interaction

1. Pigmen can only interact with buildings they have built, which means they must be the property of the Pigmen (buildings in the blueprint)
2. Interaction may consume resources, but it may also increase resources.  Generally speaking, there is a trade-off between effort and reward.
3. Buildings constructed by pig people can randomly change players' skin

3.2 Interaction between pig people and nearby players
- If a hungry player approaches a pig, the pig will give them a food item, which will cool down the player for 2 days
- When the pig princess approaches the pigman, the pigman will give basic supplies and cool down for 1.2-3.2 days
- Under high tribal favor, the pigmen will approach and give players basic supplies, with a cooldown of 1.2-3.2 days for the pigmen
]]
STRINGS.NAMES.PTRIBE_WIKI_PIGTASK = "Pigman Task"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_PIGTASK = [[
-The Pig Man of the tribe regularly generates some tasks, and players can right-click on the Pig Man to view and complete the tasks. (Visit the creative workshop to view all tasks).
-Each task has a deadline, and after the deadline, the task will be cancelled directly.
-Each player can only take one task at a time.
]]
STRINGS.NAMES.PTRIBE_WIKI_EVENTS = "Tribe Events"
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_WIKI_EVENTS = [[
5.1 Robbery
When players are in the tribe, if they have more money on them, there is a chance that robber pigs will appear to steal the players' snoring coins.
Generation conditions:
- Players do not generate outside the tribe
- Currently, there are robber pigs in the world, but they do not produce
- Robber pigmen do not generate within the cooling period after disappearing or dying
- Players do not generate in the sea or on the ship
- The generation probability is related to the player's current snoring coin amount and whether it is dusk or night

5.2 Tribal conflicts
When a player is in a tribe, if the resources of the tribe are high, there is a chance that other tribes' pigs will invade. The winning party will receive resources, while the losing party will lose resources.
Generation conditions:
- It has been generated and is cooling down without triggering
- The full moon will not trigger it
- The highest probability of triggering is during the day, and the lowest probability is at night
- The player is in the tribe and not dead, and the resource amount of the tribe where the player is located is greater than or equal to 200
- There are suitable tribes nearby as attackers
]]
STRINGS.SCRAPBOOK.SPECIALINFO.WILBA = [[
1. Initial props:
-Silver necklace
-Housing permit

2. My favorite food is broth

3. Pig King's Daughter
-When trading with the Pig King, you can receive additional gold nuggets and candies
-Pig people occasionally give her basic supplies (such as branches, flint, grass, wood, or seeds)
-When a pig person is attacked by Vilba, they will not fight back, but every time they kill a pig person, their rationality will be reduced by 10 (Crazy Pig Form will not)
-A piece of meat can hire four nearby pig people or royal guards
-Purchasing goods at the Pig Man stall will result in a partial refund of lucky gold (20% off)
-The Pig King activity will not be beaten by Kung Fu pigs

4. A pig
-Pig people will not actively stay away from her
-The Pig Man Guard will not take the initiative to attack
-Fear of the dark, in the dark -1.5 rational decrease rate
-Collecting pig people's property and hammering pig people's buildings will not have any side effects
-You can use the pig house as a tent to sleep in
-Able to consume monster food (monster meat), including cooked and sun dried dishes, without any negative impact on rationality and life.
-When Vilba is attacked, if the attacker is not a pig, the attacker will be hated by nearby pig people

During the full moon period, or within half a day of eating 4 monster foods, Vilba will become violent. Vilba in the state of a berserker pig will gain the following abilities
-Night vision, so she is immune to Charlie.
-Immune to frostbite and overheating.
-Increase movement speed by 50%.
-I won't lose my sanity in the dark.
-Her attack power is 59.5.
-More health points (350 health points)
-Restores 0.5 health points per second when she is not hungry.
-Can excavate, cut, mine, and crush items.
-Do not slow down when moving heavy objects.
-Equipment other than backpacks will fall off and cannot be equipped.
-You cannot ride a cow.
-The decrease rate of hunger value is 400%.
-She is a monster, and pig people and other friendly creatures will attack her after seeing her.
-After the full moon beam, restore to an ordinary pig. If she eats monster food during this transformation, she will recover in 1 day. Each monster's food will extend its transformation time by half a day. When Verba transforms into her normal form, she will drop two whiskers.
-Wearing a silver necklace will prevent Vilba from turning into a berserk pig.

6. Personal Protection
As the Queen of the Pig, Vilba has four great protectors to protect her in the dark. When Vilba is attacked, the four guardians will appear and disappear for 15 seconds, with a cooldown of five minutes. (But if there are already four Kung Fu Pig Men among the followers, they won't be summoned again.)

7. Go home
Vilba can go home with the Pig Man and return to the vicinity of the Pig King by right clicking on the Kung Fu Pig

8. Can read books

9. The princess can build a pig human torch, and the pig humans generated by the pig torch will not actively attack the player

10. Nightmare Pig Form
-On the night of the full moon, using monster pancakes or monster Tatars can transform into Nightmare Pig Man again
-Within three minutes of taking the magic spring water and eating monster pancakes or monster tartars in monster form during non full moon periods, one can also transform into a Nightmare Pig Man
-After transformation, the hunger rate rapidly decreases, and when the hunger value reaches 0 or the monster form ends, it returns
-Nightmare Pig Man can right click to release the "Hammer" skill
-Nightmare Pigman state cannot be picked up, eaten, or equipped

11. Exclusive props:
-Silver necklace
-Royal Watchtower
]]
STRINGS.SCRAPBOOK.SPECIALINFO.SILVERNECKLACE = [[
- The exclusive item of Velba, which can be made in the player's column.
- Wearing a silver necklace will prevent Verba from turning into a raging pig.
- Other players can hire royal pig guards when wearing silver necklaces.
]]
STRINGS.SCRAPBOOK.SPECIALINFO.CONSTRUCTION_PERMIT = "Use it on a door, and thus a new room is created."
STRINGS.SCRAPBOOK.SPECIALINFO.DEED = [[
The Deed of Home Ownership from Hamlet allows you to right-click on the ground in your preferred location to place a house. The house has the following effects:

- Indoor rain, snow and glass rain proof
- Sanity restoration indoors
- Allows planting within the room
]]
STRINGS.SCRAPBOOK.SPECIALINFO.LIFEPLANT = [[
The magic spring water obtained from the Fountain of Youth has the following functions:
- Closer to restoring sanity, but will accelerate hunger.
- You can talk, water, and fertilize crops in nearby farmland.  Each action consumes durability, which disappears when the durability value reaches 0.
- Players can be resurrected after death, but will disappear after resurrection.
]]
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_CARNIVAL_PRIZEBOOTH_KIT =
"It can be unfolded into a Pigman's stall, which refreshes its goods periodically. The better the nearby tribe develops, the more items will be available at the same time."
STRINGS.SCRAPBOOK.SPECIALINFO.RUG_ROUND =
"It can be upgraded and rotated using beefalo wool, and the same applies to other carpets."
STRINGS.SCRAPBOOK.SPECIALINFO.PUGALISK_FOUNTAIN =
"There is a chance to purchase blueprints at the pigman stall.  After unlocking the blueprint, it can be produced in the construction column.  The Fountain of Youth produces a magic spring every 20 days."
STRINGS.SCRAPBOOK.SPECIALINFO.DISGUISEHAT = "The mask from Hamlet protects your character from the hatred of the pigman."
STRINGS.SCRAPBOOK.SPECIALINFO.BANDITHAT =
"Killing a robber pig will drop it. Wearing it will prevent you from being attacked when stealing from pig people."
STRINGS.SCRAPBOOK.SPECIALINFO.PIGBANDIT = "There is a chance to spawn near the tribe, to rob the money."
STRINGS.SCRAPBOOK.SPECIALINFO.PIGMAN_ROYALGUARD = [[
- Watchtower generation
- Won't be home tonight.
- Born with armor and a battle-axe
- Players can give weapons, helmets, armor, and other equipment
- After hiring, players can bring their own containers to hold weapons, helmets, armor, and food.  During combat, these items will be automatically equipped.  Food placed in the container will be eaten, while healing items placed in the container will be used when the player's health is low
- You can hire them with meat.  Verba hires 4 at a time, while other players hire 1 at a time
]]
STRINGS.SCRAPBOOK.SPECIALINFO.PIG_GUARD_TOWER = [[
- Can generate pigmen guards
- The watchtower has its own level, which can be upgraded with universal upgrade items.  The maximum level is 10
- The higher the level, the higher the attributes of the pigman guard:
- Maximum life
- Life recovery speed
- Flame and freeze resistance
- Attack rate
- Maximum armor
- Running speed
]]
STRINGS.SCRAPBOOK.SPECIALINFO.PIG_GUARD_TOWER_PLACE = [[
- Similar to the watchtower, but it's level 15 at full level
- You can hire them with meat.  Verba hires 4 of them at a time, and other players must wear silver necklaces to hire them
]]
STRINGS.SCRAPBOOK.SPECIALINFO.PTRIBE_UPGRADE =
"Like its name suggests, the upgrade item can be applied to many equipment or buildings and is created in the refining column"
