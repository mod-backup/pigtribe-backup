-- 台词
STRINGS.PIG_PTRIBE_DO_ACTION = {
    "猪人永不为奴！",
    "猪人不做帕鲁！",
    "猪人喜欢干活！",
    "猪人会越来越强大！",
    "为了部落！"
}

----------------------------------------------------------------------------------------------------
-- 物品与描述
STRINGS.NAMES.PTRIBE_FUSED_SHADELING_BOMB = "螨虫"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_FUSED_SHADELING_BOMB = "这只猪一定不爱干净"
STRINGS.NAMES.PTRIBE_CARNIVAL_PRIZEBOOTH = "猪人摊位"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_CARNIVAL_PRIZEBOOTH = "猪人开的！"
STRINGS.NAMES.PLAYERHOUSE_CITY_EXIT_DOOR = "温暖的家"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PLAYERHOUSE_CITY_EXIT_DOOR = "出口！"
STRINGS.NAMES.PTRIBE_CARNIVAL_PRIZEBOOTH_KIT = "猪人摊位套装"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_CARNIVAL_PRIZEBOOTH_KIT = "没有它我可能买不了东西。"
STRINGS.NAMES.PTRIBE_UPGRADE = "通用升级道具"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_UPGRADE = "几乎所有升级都需要它！"
STRINGS.RECIPE_DESC.PTRIBE_UPGRADE = "常用的升级道具"
STRINGS.NAMES.PIGMAN_ROYALGUARD = "猪人守卫"
STRINGS.NAMES.PIGMAN_ROYALGUARD_2 = "皇家守卫"
STRINGS.NAMES.PTRIBE_PIGTORCH = STRINGS.NAMES.PIGTORCH
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_PIGTORCH = STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH
STRINGS.RECIPE_DESC.PTRIBE_PIGTORCH = type(STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH) == "table" and
    next(STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH) or STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH
STRINGS.NAMES.PTRIBE_PIGWEAPONFACTORY = "猪猪武器工厂"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_PIGWEAPONFACTORY = "这是猪人们凝聚千百年来的智慧完成的！"
STRINGS.NAMES.PTRIBE_OWNERSHIP = "猪人所有权证书"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_OWNERSHIP = "它可以用来向附近的部落买入或卖入建筑。"
----------------------------------------------------------------------------------------------------
--ui
STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_HOME = "翻新"
STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_PIG_SHOP = "猪人商店"
STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_BLUEPRINT = "部落蓝图"
STRINGS.UI.CRAFTING_STATION_FILTERS.CITY = "城镇规划"
STRINGS.UI.PTRIBE_PIG_TASK = {
    SUBMIT = "提交",
    ACCEPT = "接受",
    GIVE_UP = "放弃",
    CLOSE = "关闭"
}
STRINGS.UI.CRAFTING.RECIPEACTION.PTRIBE_ENABLE = "已启用"
STRINGS.UI.CRAFTING.RECIPEACTION.PTRIBE_DISABLE = "已禁用"
----------------------------------------------------------------------------------------------------
-- actions
STRINGS.ACTIONS.JUMPIN.PTRIBE_ENTER_HOME = "进入"
STRINGS.ACTIONS.JUMPIN.PTRIBE_LEAVE_HOME = "离开"
STRINGS.ACTIONS.PTRIBE_DOACTION = { GENERIC = "盖房子" } --也许是我闲的蛋疼了，没必要整一个表
STRINGS.ACTIONS.PTRIBE_VIEW_TASK = { GENERIC = "查看任务" }
STRINGS.ACTIONS.PTRIBE_PLACE = { GENERIC = "放置" }
STRINGS.ACTIONS.PTRIBE_TAKE = { GENERIC = "拿取" }
STRINGS.ACTIONS.PTRIBE_MERGE = { GENERIC = "合并" }
STRINGS.ACTIONS.PTRIBE_ROTATE = { GENERIC = "旋转" }
STRINGS.ACTIONS.RENOVATE = { GENERIC = "翻新" }
STRINGS.ACTIONS.BUILD_ROOM = { GENERIC = "建造房屋" }
STRINGS.ACTIONS.PTRIBE_FOLLOW_PIG_HOME = { GENERIC = "回家" }
STRINGS.ACTIONS.PTRIBE_REPLACE_TRADE = { GENERIC = "给予" }
STRINGS.ACTIONS.PTRIBE_UPGRADE = { GENERIC = "升级" }
STRINGS.ACTIONS.PTRIBE_USE_WILBA_SKILL = {
    GENERIC = "使用技能",
    SMASH = "重砸",
}
STRINGS.ACTIONS.PTRIBE_GIVE_TRIBE_RESOURCE = "给予"
STRINGS.ACTIONS.PTRIBE_DISTRIBUTE = "分发"

STRINGS.ACTIONS.PTRIBE_USE_ITEM = {
    GENERIC = "使用",
    GIVE = "给予",
    BUY = "购入",
    SELL = "卖出"
}

-- ACTIONFAIL
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.PTRIBE_MERGE = {
    MAXN = "已经达到最大大小了"
}
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.BUILD_ROOM = {
    ALREADY_EXISTED = "这个门已经创建房间了"
}
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.PTRIBE_UPGRADE = {
    MAX_LEVEL = "已经满级了。"
}
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.PTRIBE_USE_ITEM = {
    ALREADY_MANAGER = "它已经是村长房了。"
}
---------------------------------------------------------------------------------------------------
-- 哈姆雷特补充和替换
STRINGS.NAMES.PIG_GUARD_TOWER_PALACE = "王室瞭望塔"
STRINGS.RECIPE_DESC.PIG_GUARD_TOWER_PALACE = "忠诚的皇家守卫来保护他的子民。"
STRINGS.NAMES.OINC1000 = "千元呼噜币"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.OINC1000 = "花不完的钱！"
----------------------------------------------------------------------------------------------------
--任务
STRINGS.PTRIBE_PIG_TASK = {
    GENERIC = {
        INSUFFICIENT_MATERIAL = "东西不够",
        NOT_POS = "没有找到合适的生成位置",
        TARGET_NOT_DEFEATED = "目标还未被击败"
    },

    -- 1
    EXPANSION_OF_PIG_TWON = {
        NAME = "猪镇扩建",
        DESC = "我需要一些木头和石板来建造猪镇，越多越好！你有这么多东西吗？猪人一定不会亏待你的！",
        COND = { boards = 10, cutstone = 10 }
    },
    ITS_GOLD = {
        NAME = "金子！是金子！",
        DESC = "猪人喜欢金子！你能给我一些儿金子吗？我会给你好东西，让你的心情像金子一样闪闪发亮！",
        COND = { goldnugget = 10 }
    },
    LOYAL_SUBJECT_OF_THE_KING = {
        NAME = "王的忠诚子民",
        DESC = "王孤独多时，渴望他的子民前来朝拜。你能护送我到猪王的宝座前吗？完成这一壮举，必将得到猪王的慷慨回报！",
        COND = "护送猪人到猪王位置",
        FAIL = {
            NO_PIG_KING = "没找到猪王",
            TOO_FAR_AWAY = "猪人没看见王"
        }
    },
    PIGMAN_IS_AFRAID_OF_PAIN = {
        NAME = "猪人怕疼",
        DESC = "我摔了一跤，现在到处都是疼。你能做些治疗药膏给我吗？我会很感激你的，也许还能分享一些秘密！",
        COND = { healingsalve = 2 },
    },
    PIGMAN_LIKES_TO_PICTURE = {
        NAME = "猪人也喜欢画",
        DESC = "猪人并非只会吃和睡，猪人也懂得艺术的美。我好想要一幅描绘肉类的画作啊",
        COND = "给予猪人一个画着肉的画框",
        FAIL = {
            NO_ITEM = "你没有我想要的艺术品"
        }
    },
    TASTE_OF_SPRING_FESTIVAL = {
        NAME = "春节的味道",
        DESC = "听说你们人类有一个节日叫春节，会在这一天吃一种叫做‘饺子’的食物，猪人也想尝尝",
        COND = { perogies = 5 },
    },
    THE_INSECURE_PIGMAN = {
        NAME = "缺少安全感的猪人",
        DESC = "我最近总觉得不安全，老是害怕怪物们来捣乱。你能在我家附近建造10个木栅栏吗？这样我晚上才能睡个好觉。",
        COND = "在猪人房附近建造10个木栅栏",
        FAIL = {
            LACK = "栅栏的数量好像不够",
            HOMELESS = "我已经无家可归了"
        }
    },
    THE_KINGS_MEAT_FEAST = {
        NAME = "王的肉肉盛宴",
        DESC = "我们的王对肉肉情有独钟，总是想要更多。你能带来十块美味的肉肉作为贡品吗？这样做了，你也许能获得王的宠爱和奖励！",
        COND = "给猪王10块肉",
        FAIL = {
            NO_PIG_KING = "没找到猪王",
            LACK_MEAT = "你还没有给王足够的肉"
        }
    },
    THE_LOST_PIGMAN = {
        NAME = "走丢的猪人",
        DESC = "我的弟弟威尔孙前两天和我走丢了，你能帮我把它找回来吗？猪人会给你好吃的，会很感谢你的！",
        COND = "找到走丢的猪人并带回",
        FAIL = {
            NO_BRING_BACK = "你要带回我的弟弟威尔孙！",
            PIG_DEATH = "你身上有我弟弟的味道，你这个坏家伙！"
        }
    },
    THE_TEST_OF_THE_MYSTIC_POTION = {
        NAME = "神秘药剂的试炼",
        DESC = "嘿嘿，我这有一瓶特制药剂，据说能让猪人变得更强壮——或者……嗯，有些不同。请你将这药剂送到我那位实验品，哦不，我的朋友那里，并确保他当着你的面喝下。",
        COND = "把得到的药剂喂给猪人",
        FAIL = {
            NO_PIG = "附近没有可以用于实验的猪人",
            PIG_DEATH = "猪人死了",
            NOT_DRINK = "你还没有让我朋友喝下呢",
            POTION_WASTE = "你把我辛苦研制的药剂糟蹋了！"
        }
    },
    TO_PROTECT_THE_ENVIRONMENT = {
        NAME = "保护环境，猪人有责",
        DESC = "猪人知道环境保护的重要性，你能帮我把这些树种种下吗，以绿化我们的家园吗？",
        COND = "将得到的10个树种种下",
        SUCCESS = {
            MANY_TREES = "你是真正的环境保护大使，猪人自愧不如！"
        },
        FAIL = {
            LACK_TREE = "你没有种够树苗"
        }
    },
    WANT_TO_EAT_BERRIES = {
        NAME = "猪人想吃浆果",
        DESC = "我肚子咕咕叫，好久没吃过新鲜浆果了，你能帮我找一些来吗？你帮猪人，猪人也帮你！",
        COND = { berries = 10 },
    },
    WILL_YOU_GIVE_ME_THE_BRANCHANDGRASS = {
        NAME = "树枝给我好吗？",
        DESC = "我想要做东西，但是缺少草和树枝，你能给猪人一些儿草和树枝吗？猪人会记住你的好的！",
        COND = { twigs = 10, cutgrass = 10 },
    },

    ----------------------------------------------------------------------------------------------------
    -- 2

    DELICIOUS_CACTUS_FLOWERS = {
        NAME = "好吃的仙人掌花",
        DESC = "猪人听说那个方向有一片龙蝇沙漠，沙漠里有一种植物叫仙人掌，它们会开一种好看的花儿，猪人想尝尝什么味道，你可以帮帮猪人吗？",
        COND = { cactus_flower = 5 },
    },
    -- "2ImAfraidOfTheDark",
    IM_AFRAID_OF_THE_DARK = {
        NAME = "我怕黑",
        DESC = "猪人害怕黑暗，猪人想要一顶发光的帽子！",
        COND = "给猪人戴上矿工帽",
        FAIL = {
            HAT_NOT_SHINE = "这帽子不会发光",
            NOT_MINER_HAT = "这不是矿工帽",
            NOT_HAT = "我想要矿工帽"
        }
    },
    -- "2RowdyHound",
    ROWDY_HOUND = {
        NAME = "吵闹的猎犬",
        DESC = "猎犬的嚎叫声让猪人夜夜难眠，我已经好几天没睡好觉了，它们在村头不停地吵闹，破坏了夜的平静。你能让它们安静一下吗？",
        COND = "击杀6只猎犬",
        FAIL = {
            HOUND_SURVIVAL = "你还没有摆平那些猎犬"
        }
    },
    -- "2ThoseWhoCanCookAreGoodPeople",
    THOSE_WHO_CAN_COOK_ARE_GOOD_PEOPLE = {
        NAME = "会做饭的都是好人",
        DESC = "嗷嗷，猪人想吃好吃的，你会做饭吗？给猪人做四份好吃的食物，证明你是好人！好人有好报，猪人有好东西给！",
        COND = { frogglebunwich = 1, honeyham = 1, leafymeatsouffle = 1, ratatouille = 1 },
    },

    ----------------------------------------------------------------------------------------------------
    -- 3

    -- "3PickerGuard",
    PICKER_GUARD = {
        NAME = "采药护卫",
        DESC = "猪人打算去采药，但是猪人怕怪物。你强壮，可以保护猪人吗？猪人会记得你的好，给你好东西！",
        COND = "护送猪人采集草药并保护它。",
        FAIL = {
            NOT_POS = "未找到合适的生成位置",
            NOT_HOME = "猪人的家都没了",
            LACK = "我还没有采集到足够的草药"
        }
    },
    -- "3PigmanAndMermFight",
    PIGMAN_AND_MERM_FIGHT = {
        NAME = "猪鱼之战",
        DESC = "猪人不喜欢鱼人，鱼人也不喜欢猪人。但猪人更强！你帮猪人，一起打败鱼人。证明猪人更强，你也是猪人的朋友。朋友帮猪人，猪人也帮朋友！",
        COND = "协助猪人战胜鱼人",
        FAIL = {
            NOT_POS = "未找到合适的生成位置",
            ENEMY_SURVIVAL = "战斗还没有结束",
            PIGS_DEATH = "猪人都死光了！猪人要你有什么用！"
        }
    },
    -- "3PigmanWhoWantsToTravel",
    PIGMAN_WHO_WANTS_TO_TRAVEL = {
        NAME = "想要旅行的猪人",
        DESC = "猪人厌倦了每天相同的风景，想要见见世面。我想要跟随你旅行，看看这个世界的奇观，我保证不会给你添麻烦！",
        COND = "两天内避免猪人死亡",
        SUCCESS = {
            THANK = "谢谢，这是回报"
        },
        FAIL = {
            LACK_OF_TIME = "还没到两天呢"
        }
    },
    -- "3RecoverStolenProperty",
    RECOVER_STOLEN_PROPERTY = {
        NAME = "找回失窃物",
        DESC = "猪人的浆果和木头不见了！坎普斯是小偷！",
        COND = "击杀坎普斯",
        FAIL = {
            NOT_POS = "未找到合适的生成位置",
            ENEMY_SURVIVAL = "你还没有杀死那个坎普斯",
        }
    },
    THE_FOREST_COUNTERATTACK = {
        NAME = "森林的反击",
        DESC = "猪人同胞们在砍树中遇到了树精！猪人讨厌它们！请你帮助猪人战胜树精！",
        COND = "1. 保护猪人，至少有一只存活\n2. 消灭树精",
        FAIL = {
            ENEMY_SURVIVAL = "战斗还没有结束！",
            PIG_DEATH = "同胞们都牺牲了..."
        }
    },
    TRUE_STRENGTH_LIES_WITHIN_ONESELF = {
        NAME = "自身的强大才是真正的强大",
        DESC = "猪人听说有强大的暗影装备，但猪人不敢它们。如果你带给猪人一把暗影剑和一件暗影甲，猪人会相信你的力量。猪人会给强大的朋友好东西！",
        COND = { nightsword = 1, armor_sanity = 1 },
    },

    ----------------------------------------------------------------------------------------------------
    -- 4

    -- "4Braveheart",
    BRAVEHEART = {
        NAME = "勇敢的心",
        DESC = "猪人不只是胖胖的，猪人也想勇敢！我听说有个东西叫蜂王冠，戴上它的人都能变得无比勇敢。你能帮我弄到一顶蜂王冠吗？这样我也能成为勇敢的猪人！",
        COND = { hivehat = 1 },
    },
    FALL_INTO = {
        NAME = "坠入",
        DESC = "猪人的朋友被蜘蛛包围了，请你帮帮猪人，救下它们！",
        COND = "1、保护猪人和鱼人\n2、消灭蜘蛛",
        FAIL = {
            IN_BATTLE = "它们现在很危险，快去救它们！",
            DEATH = "猪人不喜欢这样的结果！"
        }
    },
    -- "4NastyVillage",
    NASTY_VILLAGE = {
        NAME = "讨厌的村子",
        DESC = "那个猪人村子一直和我们作对，他们的部落发展得太让人眼红了！你能帮我们拆掉它们的村长房吗？这样我们村子就是最棒的了！",
        COND = "锤毁部落的村长房",
        FAIL = {
            NOT_TRIBE = "猪人没有自己的部落",
            NOT_TARGET = "没找到合适的目标",
            TARGET_EXIST = "那个村子的村长房还在呢！"
        }
    },
    -- "4PigmanJewelDream",
    PIGMAN_JEWEL_DREAM = {
        NAME = "猪人的宝石梦",
        DESC = "我听过闪亮亮的宝石，梦想能看一眼真正的宝石。你能帮我找到宝石吗？找到宝石，我就给你我最好的东西！",
        COND = { yellowgem = 1, orangegem = 1, greengem = 1, purplegem = 1, redgem = 1, bluegem = 1 },
    },
    -- "4PigmenHateSpider",
    PIGMAN_HATE_SPIDER = {
        NAME = "猪人讨厌蜘蛛",
        DESC = "蜘蛛让猪人的生活变得困难，猪人们害怕又讨厌它们。我想请你帮忙，击败那可怕的蜘蛛女王，让猪人们再次安心生活！",
        COND = "击杀蜘蛛女王",
        FAIL = {
            NOT_DEFEAT_QUEEN = "你还没有击败蜘蛛女王"
        }
    },

    ----------------------------------------------------------------------------------------------------
    -- 5

    -- "5BattleThrilling",
    BATTLE_THRILLING = {
        NAME = "战斗，爽！",
        DESC = "是时候展示真正的勇气了！我计划挑战那只可怕的龙蝇，证明猪人无与伦比的实力。战斗的荣耀将是我们的！你敢与我一同迎战这场史诗般的战斗吗？",
        COND = "1、猪人一起击杀龙蝇\n2、避免猪人死亡",
        FAIL = {
            NOT_DEFEAT_DRAGONFLY = "我们还没打龙蝇呢！"
        }
    },
    -- "5MedalOfBraveryFromThePigmen",
    MEDAL_OF_BRAVERY_FROM_THE_PIGMAN = {
        NAME = "猪人的勇者勋章",
        DESC = "传说中只有真正的勇者才能战胜巨鹿。如果你能击杀巨鹿，那你就是勇者！你会得到猪人的尊敬和奖赏。",
        COND = { deerclops_eyeball = 1 },
    },
    -- "5TerrifyingShadows",
    TERRIFYING_SHADERS = {
        NAME = "可怕的暗影",
        DESC = "最近有一只若隐若现的暗影生物一直在村头游荡，它的存在让所有猪人都感到恐惧。我们需要一个英雄来帮助我们，消灭这个邪恶的存在。请问，勇士，你愿意接受这个挑战吗？",
        COND = "击败暗影生物",
        FAIL = {
            NOT_DEFEAT_SHADOW = "那些暗影生物还在呢！"
        },
        GIVE_COIN = "希望这能帮上你的忙。"
    },
    -- "5TheShadowOfThePigVillage"
    THE_SHADOW_OF_THE_PIG_VILLAGE = {
        NAME = "猪村的阴影",
        DESC = "不安的阴影笼罩着我们的村落，一个邪恶的祭坛在不断地给猪人们带来噩梦。勇敢的旅行者，我们请求你摧毁这个祭坛，让我们的梦境再次安宁。",
        COND = "摧毁影响猪村的邪恶祭坛。",
        FAIL = {
            VILLAGE_EXIST = "那个邪恶祭坛还在呢！",
            TARGETS_EXIST = "邪恶的生物还没有消灭！",
            STATUE_EXIST = "要把那个不详的雕塑也摧毁！"
        }
    },
}

----------------------------------------------------------------------------------------------------
STRINGS.PIG_TALK_LOOKATWILBA = {
    "哦哦，高贵的薇尔芭公主！",
    "公主，你今天看起来很闪亮！",
    "薇尔芭公主，猪人可以为你做什么？",
    "公主，你的王冠比猪耳朵还要亮！",
    "哦，薇尔芭公主，你的笑容比肉还要甜！",
    "猪人们都爱薇尔芭公主！",
    "公主好！",
    "薇尔芭最美！"
}

----------------------------------------------------------------------------------------------------
STRINGS.PTRIBE_FOOD_BUFFS_STOP = "buff效果结束了。"
STRINGS.PTRIBE_FOOD_BUFFS = {
    HEALTH = {
        "获得60秒的持续回血效果。",
        "获得60秒的持续扣血效果。"
    },
    HUNGER_MULT = {
        "降低一天的饥饿速度。",
        "提高一天的饥饿速度。",
    },
    SPEED_MULT = {
        "提高两天的移速。",
        "降低两天的移速。",
    },
    ATTACK_MULT = {
        "提高一天的攻击倍率。",
        "降低一天的攻击倍率。",
    },
    ATTACK_TAKEN_MULT = {
        "提高一天的防御。",
        "降低一天的防御。",
    },
    -- SPAWN_ITEM = {},
    RANDOM_TEMPERATURE = {
        "",
        "一分钟内温度随机变化。",
    },
    AFFECT_ENTITY = {
        "一分钟内获得猪人的好感。",
        "一分钟内受到猪人的厌恶。",
    },
    WORMWOOD_VINED = {
        "一天内被攻击时将禁锢敌人。",
    },
    SHADOWTENTACLE = {
        "一天内攻击时有概率召唤触手攻击敌人。"
    },
    WORK_MULT = {
        "一天内提高工作效率。"
    },
    KILL_HEALTH_REGEN = {
        "一天内击杀敌人将回复血量。"
    },
    FOOD_EAT_REGEN = {
        "一天内提高食物的回复效率。",
        "一天内降低食物的回复效率。"
    },
    BURNABLE_RESIST = {
        "一天内提高火焰抗性。",
        "一天内降低火焰抗性。"
    },
    FREEZABLE_RESIST = {
        "一天内提高冰冻抗性。",
        "一天内降低冰冻抗性。"
    },
    BUILD_INGREDIENTMOD = {
        "一分钟内制作道具消耗的材料减半。",
    },
    FAST_PICK = {
        "一天内获得快速采集。"
    },
    ELECTRICATTACK = {
        "一天内攻击附带闪电效果。"
    },
    FAST_BUILD = {
        "一天内获得快速制作。"
    }
}

STRINGS.PTRIBE_PIG_CHEF_PLACE_POT = {
    "巧妇难为无米之炊。",
    "巧手莫为无面饼。",
    "要做饭，先架锅！",
}
STRINGS.PTRIBE_PIG_CHEF_DO_COOK = {
    "猪人喜欢做饭。",
    "自己做的饭最香！",
    "猪人是天生的大厨！",
    "没有动物能够抵御猪人做的美食。",
    "香，真香！"
}

STRINGS.PTRIBE_PIG_CHEF_WARN_EATER = {
    "再偷吃就不客气了。",
    "猪人要生气啦！",
    "那是猪人的食物。",
    "卑鄙的外来人。"
}

STRINGS.PTRIBE_PIG_FARMER_PLACE = {
    "猪人喜欢种地。",
    "倒萍越早，早稻越好。",
    "冬草肥田，春草肥禾。",
    "蚕豆不要粪，只要灰里困。",
    "好种出好苗，良种产量高。",
    "秋后不深耕，来年虫子生。",
    "锅底无柴难烧饭，田里无粪难增产。",
    "看田选种，看种种田。",
    "地里挑，场上选，忙半天，甜一年。",
    "旱锄地发暖，多锄地不板。",
    "种地无巧，粪水灌饱。",
    "谷子打苞，水淹齐腰。",
    "谷子黄，选种藏。",
    "年外不如年里，年里不如掩底。",
    "谷在犁上，菜在锄上。",
    "冬天比粪堆，来年比粮堆。",
    "春天粪堆密，秋后粮铺地。",
    "秧好一半禾，菜好半年粮。",
    "要想吃、好面，种麦泥里站。"
}

STRINGS.PTRIBE_PIG_FARMER_FERTILIZE = {
    "施肥了才能更好的生长。",
    "各肥混合用，不要胡乱壅。",
    "种地不上粪，好比瞎胡混。",
    "要想粮食打得多，就得水土不下坡。",
    "庄稼要好，施肥要巧。",
    "猪靠饭饱，田靠肥料。",
    "粪是土里虎，能增一石五。",
    "蚕豆一把灰，角角起堆堆。",
    "上粪上在劲头，锄地锄到地头。",
    "青草沤成粪，越长越有劲。",
    "要想庄稼旺，合理把粪上。",
    "灯无油不亮，稻无肥不长。",
    "草子三坐头，肥料就不愁。"
}
STRINGS.PTRIBE_PIG_FARMER_HARVEST = {
    "劳动最光荣！",
    "吃饭当节俭。",
    "粗茶淡饭，细水长流。",
    "粮食来之不易！",
}

STRINGS.PTRIBE_PIG_SCAVENGER_COLLECT = {
    "搜集物资。",
    "闻起来像宝贝！",
    "捡！捡！捡！猪人最喜欢捡东西！",
    "这个好闪亮！猪人喜欢！",
    "猪人不懂，但猪人喜欢！",
    "猪人找到秘密东西！",
    "猪人觉得这个重要！",
    "所有东西都是猪人的！"
}
STRINGS.PTRIBE_PIG_SCAVENGER_DISAPPEAR = "背包满了，猪人要回家了。"

STRINGS.PTRIBE_WILD_PIG_NAMES = {
    "洛天依",
    "锡徐",
    "猪猪侠",
    "菲菲",
    "波比",
    "超人强",
    "猪猪侠",
    "小呆呆"
}
