-- 台词
STRINGS.PIG_PTRIBE_DO_ACTION = {
    "Piggies never be slaves!",
    "Piggies no do pal!",
    "Me loves worky-work!",
    "Piggies get stronger and stronger!",
    "For the tribe!"
}

----------------------------------------------------------------------------------------------------
-- 物品与描述
STRINGS.NAMES.PTRIBE_FUSED_SHADELING_BOMB = "Mite"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_FUSED_SHADELING_BOMB = "This pig must not be clean"
STRINGS.NAMES.PTRIBE_CARNIVAL_PRIZEBOOTH = "Pig Shop"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_CARNIVAL_PRIZEBOOTH = "Operated by Pigs!"
STRINGS.NAMES.PLAYERHOUSE_CITY_EXIT_DOOR = "Home"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PLAYERHOUSE_CITY_EXIT_DOOR = "Exit!"
STRINGS.NAMES.PTRIBE_CARNIVAL_PRIZEBOOTH_KIT = "Pig Shop Kit"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_CARNIVAL_PRIZEBOOTH_KIT = "Without it, I might not be able to buy anything."
STRINGS.NAMES.PTRIBE_UPGRADE = "Generic Upgrade Item"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_UPGRADE = "It's needed for almost every upgrade!"
STRINGS.RECIPE_DESC.PTRIBE_UPGRADE = "Commonly used powerups."
STRINGS.NAMES.PIGMAN_ROYALGUARD = "Pigman Guard"
STRINGS.NAMES.PIGMAN_ROYALGUARD_2 = "Royal Guard"
STRINGS.NAMES.PTRIBE_PIGTORCH = STRINGS.NAMES.PIGTORCH
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_PIGTORCH = STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH
STRINGS.RECIPE_DESC.PTRIBE_PIGTORCH = type(STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH) == "table" and
    next(STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH) or STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH
STRINGS.NAMES.PTRIBE_PIGWEAPONFACTORY = "Pig Weapon Factory"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PTRIBE_PIGWEAPONFACTORY = "This is the wisdom of pigs over thousands of years!"
----------------------------------------------------------------------------------------------------
-- ui
STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_HOME = "Renovate"
STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_PIG_SHOP = "Pigman shop"
STRINGS.UI.CRAFTING_STATION_FILTERS.PTRIBE_BLUEPRINT = "Tribe Blueprint"
STRINGS.UI.CRAFTING_STATION_FILTERS.CITY = "City"
STRINGS.UI.PTRIBE_PIG_TASK = {
    SUBMIT = "Submit",
    ACCEPT = "Accept",
    GIVE_UP = "Give Up",
    CLOSE = "Close"
}
STRINGS.UI.CRAFTING.RECIPEACTION.PTRIBE_ENABLE = "Enabled"
STRINGS.UI.CRAFTING.RECIPEACTION.PTRIBE_DISABLE = "Disabled "
----------------------------------------------------------------------------------------------------
-- actions
STRINGS.ACTIONS.JUMPIN.PTRIBE_ENTER_HOME = "Enter"
STRINGS.ACTIONS.JUMPIN.PTRIBE_LEAVE_HOME = "Leave"
STRINGS.ACTIONS.PTRIBE_DOACTION = { GENERIC = "House" }
STRINGS.ACTIONS.PTRIBE_VIEW_TASK = { GENERIC = "View Task" }
STRINGS.ACTIONS.PTRIBE_PLACE = { GENERIC = "Place" }
STRINGS.ACTIONS.PTRIBE_TAKE = { GENERIC = "Take" }
STRINGS.ACTIONS.PTRIBE_MERGE = { GENERIC = "Merge" }
STRINGS.ACTIONS.PTRIBE_ROTATE = { GENERIC = "Rotate" }
STRINGS.ACTIONS.RENOVATE = { GENERIC = "Renovate" }
STRINGS.ACTIONS.BUILD_ROOM = { GENERIC = "Build Room" }
STRINGS.ACTIONS.PTRIBE_FOLLOW_PIG_HOME = { GENERIC = "Go Home" }
STRINGS.ACTIONS.PTRIBE_REPLACE_TRADE = { GENERIC = "Give" }
STRINGS.ACTIONS.PTRIBE_UPGRADE = { GENERIC = "Upgrade" }
STRINGS.ACTIONS.PTRIBE_USE_WILBA_SKILL = {
    GENERIC = "Use Skill",
    SMASH = "Smash",
}
STRINGS.ACTIONS.PTRIBE_GIVE_TRIBE_RESOURCE = "Give"
STRINGS.ACTIONS.PTRIBE_DISTRIBUTE = "Distribute"

STRINGS.ACTIONS.PTRIBE_USE_ITEM = {
    GENERIC = "Use",
    GIVE = "Give",
    BUY = "Buy",
    SELL = "Sell"
}

-- ACTIONFAIL
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.PTRIBE_MERGE = {
    MAXN = "It's reached its maximum size"
}
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.BUILD_ROOM = {
    ALREADY_EXISTED = "This door has created the room"
}
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.PTRIBE_UPGRADE = {
    MAX_LEVEL = "It's already full level."
}
STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.PTRIBE_USE_ITEM = {
    ALREADY_MANAGER = "It is already the house of the village head."
}
---------------------------------------------------------------------------------------------------
-- 哈姆雷特补充和替换
STRINGS.NAMES.PIG_GUARD_TOWER_PALACE = "Royal Lookout Tower"
STRINGS.RECIPE_DESC.PIG_GUARD_TOWER_PALACE = "Loyal royal guards come forth to protect his subjects."
STRINGS.NAMES.OINC1000 = "Tenpiece Oinc"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.OINC1000 = "Endless money!"
----------------------------------------------------------------------------------------------------
-- 任务
STRINGS.PTRIBE_PIG_TASK = {
    GENERIC = {
        INSUFFICIENT_MATERIAL = "Not enough materials",
        NOT_POS = "No suitable generation site found.",
        TARGET_NOT_DEFEATED = "The target has not been defeated yet."
    },

    -- 1
    EXPANSION_OF_PIG_TWON = {
        NAME = "Expansion of Pig Tribe",
        DESC =
        "I need some wooden boards and stone bricks to build Pig Tribe, the more the better! Do you have so many things? Pigman will definitely not treat you unfairly!",
        COND = { boards = 10, cutstone = 10 }
    },
    ITS_GOLD = {
        NAME = "Gold! It's gold!",
        DESC =
        "Pigmen love gold! Can you give me some gold? I'll give you good stuff to make your heart shine like gold!",
        COND = { goldnugget = 10 }
    },
    LOYAL_SUBJECT_OF_THE_KING = {
        NAME = "The king's loyal subjects.",
        DESC =
        "The king has been lonely for a long time, yearning for his subjects to come and pay homage. Can you escort me to the Pig King's throne? Accomplish this feat, and you shall receive the Pig King's generous reward!",
        COND = "Escort the pigman to the location of the Pig King.",
        FAIL = {
            NO_PIG_KING = "Can't find the Pig King.",
            TOO_FAR_AWAY = "The Pigman didn't see the King."
        }
    },
    PIGMAN_IS_AFRAID_OF_PAIN = {
        NAME = "Pigmen are afraid of pain",
        DESC =
        "I've taken a fall, and now it hurts everywhere. Can you make some healing salve for me? I would be very grateful, and I might even share some secrets!",
        COND = { healingsalve = 2 },
    },
    PIGMAN_LIKES_TO_PICTURE = {
        NAME = "Pigmen like art, too.",
        DESC =
        "Pigmen are not just about eating and sleeping; pigmen also appreciate the beauty of art. I really want a painting that depicts meat.",
        COND = "Give the pigman a picture frame with a painting of meat.",
        FAIL = {
            NO_ITEM = "You don't have the art pieces I desire."
        }
    },
    TASTE_OF_SPRING_FESTIVAL = {
        NAME = "The taste of Spring Festival.",
        DESC =
        "I heard you humans have a holiday called Spring Festival, and on this day you eat something called 'dumplings'. Piggy wants to try some too!",
        COND = { perogies = 5 },
    },
    THE_INSECURE_PIGMAN = {
        NAME = "Insecure Piggy",
        DESC =
        "I always feel unsafe and fear that monsters will come to make trouble. Can you build 10 wooden fences around my house? That way, I can sleep well at night.",
        COND = "Build 10 wooden fences around my piggy house, please!",
        FAIL = {
            LACK = "It seems like there aren't enough wooden fences.",
            HOMELESS = "I am homeless now."
        }
    },
    THE_KINGS_MEAT_FEAST = {
        NAME = "The king's meaty feast!",
        DESC =
        "Our king has a special fondness for meat and always craves more. Could you bring ten pieces of delicious meat as an offering? By doing so, you might earn the king's favor and a reward!",
        COND = "Give the Pig King 10 pieces of meat.",
        FAIL = {
            NO_PIG_KING = "Can't find the Pig King.",
            LACK_MEAT = "You haven't given the King enough meat."
        }
    },
    THE_LOST_PIGMAN = {
        NAME = "The Lost Pigman",
        DESC =
        "My brother Wilson got lost with me a couple of days ago; can you help me find him? The Pigman will give you something tasty and will be very grateful to you!",
        COND = "Find the lost Pigman and bring him back.",
        FAIL = {
            NO_BRING_BACK = "You must bring back my brother Wilson!",
            PIG_DEATH = "You smell like my brother, you bad guy!"
        }
    },
    THE_TEST_OF_THE_MYSTIC_POTION = {
        NAME = "The Trial of the Mysterious Potion.",
        DESC =
        "Hey, I have a special potion here that's said to make Pigmen stronger—or, well, a bit different. Please deliver this potion to my experimental subject, oh no, I mean my friend, and make sure he drinks it in front of you.",
        COND = "Administer the potion to the Pigman.",
        FAIL = {
            NO_PIG = "There are no Pigmen nearby to use for experiments.",
            PIG_DEATH = "The Pigman is dead.",
            NOT_DRINK = "You haven't made my friend drink it yet.",
            POTION_WASTE = "You've wasted the potion I worked so hard to create!"
        }
    },
    TO_PROTECT_THE_ENVIRONMENT = {
        NAME = "Protect the environment; Pigmen have a responsibility too.",
        DESC =
        "Pigman know the importance of environmental protection. Can you help me plant these trees to green our home?",
        COND = "Under the 10 trees that will be obtained",
        SUCCESS = {
            MANY_TREES = "You are a true environmental protection ambassador; the Pigmen pale in comparison!"
        },
        FAIL = {
            LACK_TREE = "You haven't planted enough saplings."
        }
    },
    WANT_TO_EAT_BERRIES = {
        NAME = "Pigman want to eat berries",
        DESC =
        "My stomach is growling; it's been so long since I've had fresh berries. Can you help me find some? You help Pigman, Pigman helps you!",
        COND = { berries = 10 },
    },
    WILL_YOU_GIVE_ME_THE_BRANCHANDGRASS = {
        NAME = "Could you give me the branches, please?",
        DESC =
        "I want to make something, but I'm short on grass and branches. Could you give Pigman some grass and branches? Pigman will remember your kindness!",
        COND = { twigs = 10, cutgrass = 10 },
    },

    ----------------------------------------------------------------------------------------------------
    -- 2

    DELICIOUS_CACTUS_FLOWERS = {
        NAME = "Delicious cactus flowers.",
        DESC =
        "Pigman has heard that in that direction there is a Dragonfly Desert, and in the desert, there's a plant called the cactus. They bloom pretty flowers, and Pigman wants to taste what they're like. Can you help Pigman out?",
        COND = { cactus_flower = 5 },
    },
    -- "2ImAfraidOfTheDark",
    IM_AFRAID_OF_THE_DARK = {
        NAME = "I'm afraid of the dark.",
        DESC = "Pigman is afraid of the dark; Pigman wants a glowing hat!",
        COND = "Put a Miner Hat on Pigman.",
        FAIL = {
            HAT_NOT_SHINE = "This hat doesn't glow.",
            NOT_MINER_HAT = "This is not a Miner Hat.",
            NOT_HAT = "I want a Miner Hat."
        }
    },
    -- "2RowdyHound",
    ROWDY_HOUND = {
        NAME = "Noisy hounds.",
        DESC =
        "The howling of the hounds keeps Pigman awake at night; I haven't had a good night's sleep in days. They keep making noise at the edge of the village, disturbing the peace of the night. Can you make them quiet down?",
        COND = "Kill six hounds.",
        FAIL = {
            HOUND_SURVIVAL = "You have not yet dealt with those hounds."
        }
    },
    -- "2ThoseWhoCanCookAreGoodPeople",
    THOSE_WHO_CAN_COOK_ARE_GOOD_PEOPLE = {
        NAME = "Those who can cook are good people.",
        DESC =
        "Oink oink, Pigman wants to eat something tasty, can you cook? Make four delicious meals for Pigman to prove you are a good person! Good people receive good things, Pigman has nice things to give!",
        COND = { frogglebunwich = 1, honeyham = 1, leafymeatsouffle = 1, ratatouille = 1 },
    },

    ----------------------------------------------------------------------------------------------------
    -- 3

    -- "3PickerGuard",
    PICKER_GUARD = {
        NAME = "Herbal Collection Guard",
        DESC =
        "The Pigman plans to gather herbs, but Pigman is afraid of monsters. You are strong; can you protect Pigman? Pigman will remember your kindness and give you good things!",
        COND = "Escort the Pigman to collect herbs and protect it.",
        FAIL = {
            NOT_POS = "No suitable generation site found.",
            NOT_HOME = "The Pigmen's homes are all gone.",
            LACK = "I haven't gathered enough herbs yet."
        }
    },
    -- "3PigmanAndMermFight",
    PIGMAN_AND_MERM_FIGHT = {
        NAME = "The Battle Between Pigmen and Fishmen",
        DESC =
        "Pigmen do not like Fishmen, and Fishmen do not like Pigmen either. But Pigmen are stronger! You help Pigmen, together we defeat the Fishmen. Prove Pigmen are stronger, and you are also a friend of the Pigmen. Friends help Pigmen, and Pigmen help friends!",
        COND = "Assist the Pigmen in defeating the Fishmen.",
        FAIL = {
            NOT_POS = "No suitable generation site found.",
            ENEMY_SURVIVAL = "The battle is not over yet.",
            PIGS_DEATH = "All the Pigmen are dead! What use are you to the Pigmen now!"
        }
    },
    -- "3PigmanWhoWantsToTravel",
    PIGMAN_WHO_WANTS_TO_TRAVEL = {
        NAME = "The Pigman Who Wants to Travel",
        DESC =
        "The Pigman has grown tired of the same daily scenery and wants to see the world. I would like to follow you on your travels and witness the wonders of this world, I promise I won't be any trouble!",
        COND = "Keep the Pigman alive for two days.",
        SUCCESS = {
            THANK = "Thank you, here is your reward."
        },
        FAIL = {
            LACK_OF_TIME = "It's not even two days yet."
        }
    },
    -- "3RecoverStolenProperty",
    RECOVER_STOLEN_PROPERTY = {
        NAME = "Retrieve the Stolen Goods.",
        DESC = "The Pigman's berries and wood are gone! Kampus is the thief!",
        COND = "Kill Kampus.",
        FAIL = {
            NOT_POS = "No suitable generation site found.",
            ENEMY_SURVIVAL = "You haven't killed that Krampus yet.",
        }
    },
    THE_FOREST_COUNTERATTACK = {
        NAME = "The forest's counterattack",
        DESC =
        "The fellow pigman encountered tree spirits while chopping trees! Pigman hate them! Please help the pigman defeat the tree spirit!",
        COND = "1. Protect pigman, at least one of them will survive. \n2 Eliminate tree spirits",
        FAIL = {
            ENEMY_SURVIVAL = "The battle is not over yet!",
            PIG_DEATH = "My fellow countrymen have all sacrificed."
        }
    },
    -- "3TrueStrengthLiesWithinOneself",
    TRUE_STRENGTH_LIES_WITHIN_ONESELF = {
        NAME = "True strength lies in one's own power.",
        DESC =
        "The Pigman has heard of powerful shadow equipment, but Pigman dares not approach them. If you bring Pigman a shadow sword and a piece of shadow armor, Pigman will believe in your strength. Pigman will give good things to powerful friends!",
        COND = { nightsword = 1, armor_sanity = 1 },
    },

    ----------------------------------------------------------------------------------------------------
    -- 4

    -- "4Braveheart",
    BRAVEHEART = {
        NAME = "Brave heart.",
        DESC =
        "Pigmen are not just chubby; pigmen also want to be brave! I heard there's something called the Bee Queen Crown, and anyone who wears it becomes incredibly brave. Can you help me get a Bee Queen Crown? That way, I can become a brave pigman too!",
        COND = { hivehat = 1 },
    },
    FALL_INTO = {
        NAME = "Fall into.",
        DESC = "The pigman's friends are surrounded by spiders, please help the pigmen and save them!",
        COND = "1. Protect the pigmen and the fishmen.\n2. Eliminate the spiders.",
        FAIL = {
            IN_BATTLE = "They are in danger now, go save them quickly!",
            DEATH = "Pigmen do not like this outcome!"
        }
    },
    -- "4NastyVillage",
    NASTY_VILLAGE = {
        NAME = "The detestable village.",
        DESC =
        "That pigman village has always been against us, their tribal development is too enviable! Can you help us tear down their chieftain's house? That way, our village will be the best!",
        COND = "Smash the tribal chieftain's house of the village.",
        FAIL = {
            NOT_TRIBE = "Pigmen have no tribe of their own.",
            NOT_TARGET = "No suitable target found.",
            TARGET_EXIST = "The chief's house of that village is still there!"
        }
    },
    -- "4PigmanJewelDream",
    PIGMAN_JEWEL_DREAM = {
        NAME = "The pigman's dream of gems.",
        DESC =
        "I've heard of glittering gems and dream of seeing a real gem. Can you help me find a gem? If you find one, I'll give you my best thing!",
        COND = { yellowgem = 1, orangegem = 1, greengem = 1, purplegem = 1, redgem = 1, bluegem = 1 },
    },
    -- "4PigmenHateSpider",
    PIGMAN_HATE_SPIDER = {
        NAME = "Pigmen hate spiders.",
        DESC =
        "Spiders make the lives of pigmen difficult; pigmen are both afraid of and detest them. I would like to ask for your help to defeat that dreadful Spider Queen, so that pigmen can live in peace once again!",
        COND = "Slay the Spider Queen.",
        FAIL = {
            NOT_DEFEAT_QUEEN = "You have not yet defeated the Spider Queen."
        }
    },

    ----------------------------------------------------------------------------------------------------
    -- 5

    -- "5BattleThrilling",
    BATTLE_THRILLING = {
        NAME = "Fighting, awesome!",
        DESC =
        "It's time to show true courage! I plan to challenge that terrifying Dragonfly to prove the unparalleled strength of pigmen. The glory of battle shall be ours! Do you dare to join me in this epic fight?",
        COND = "1. Pigmen unite to slay the Dragonfly.\n2. Avoid pigmen casualties.",
        FAIL = {
            NOT_DEFEAT_DRAGONFLY = "We haven't fought the Dragonfly yet!"
        }
    },
    -- "5MedalOfBraveryFromThePigmen",
    MEDAL_OF_BRAVERY_FROM_THE_PIGMAN = {
        NAME = "The Pigman's Medal of Valor.",
        DESC =
        "Legend has it that only a true warrior can defeat the Giant Deer. If you can slay the Giant Deer, then you are a warrior! You will earn the respect and rewards of the pigmen.",
        COND = { deerclops_eyeball = 1 },
    },
    -- "5TerrifyingShadows",
    TERRIFYING_SHADERS = {
        NAME = "The terrible shadow.",
        DESC =
        "Recently, an elusive shadowy creature has been lurking around the edge of the village, instilling fear in all the pigmen. We need a hero to help us, to vanquish this evil presence. Tell me, brave one, are you willing to accept this challenge?",
        COND = "Defeat the shadow creature.",
        FAIL = {
            NOT_DEFEAT_SHADOW = "Those Shadow Creatures are still there!"
        },
        GIVE_COIN = "I hope this can help you."
    },
    -- "5TheShadowOfThePigVillage"
    THE_SHADOW_OF_THE_PIG_VILLAGE = {
        NAME = "The Shadow of Pig Village.",
        DESC =
        "An unsettling shadow hangs over our village, and an evil altar is bringing nightmares to our pigmen. Brave traveler, we beseech you to destroy this altar and restore peace to our dreams.",
        COND = "Destroy the evil altar that plagues Pig Village.",
        FAIL = {
            VILLAGE_EXIST = "That evil altar is still there!",
            TARGETS_EXIST = "The evil creatures have not been eliminated!",
            STATUE_EXIST = "You must also destroy that ominous statue!"
        }
    },
}

----------------------------------------------------------------------------------------------------
STRINGS.PIG_TALK_LOOKATWILBA = {
    "Oh, noble Princess Wilba!",
    "Princess, you shine bright today!",
    "What can I do for you, Princess Wilba?",
    "Your crown outshines pig's ears, Princess!",
    "Oh, Princess Wilba, your smile is sweeter than meat!",
    "All pigs adore Princess Wilba!",
    "Greetings, Princess!",
    "Wilba the fairest!"
}
----------------------------------------------------------------------------------------------------
STRINGS.PTRIBE_FOOD_BUFFS_STOP = "The buff effect has ended."
STRINGS.PTRIBE_FOOD_BUFFS = {
    HEALTH = {
        "Gain 60 seconds of sustained recharge.",
        "Gain 60 seconds of sustained blood withholding."
    },
    HUNGER_MULT = {
        "Reduce your hunger rate throughout the day.",
        "Increase your hunger rate throughout the day.",
    },
    SPEED_MULT = {
        "Increased movement speed for 2 days.",
        "Reduced movement speed for two days.",
    },
    ATTACK_MULT = {
        "Increased attack rate for one day.",
        "Reduced attack rate per day.",
    },
    ATTACK_TAKEN_MULT = {
        "Improve your defenses for the day.",
        "Lower your defenses for the day.",
    },
    -- SPAWN_ITEM = {},
    RANDOM_TEMPERATURE = {
        "",
        "Temperature changes randomly in one minute.",
    },
    AFFECT_ENTITY = {
        "Get on the Pigman's good side in a minute.",
        "In a minute, the pig people hated it.",
    },
    WORMWOOD_VINED = {
        "Imprison the enemy when attacked within a day.",
    },
    SHADOWTENTACLE = {
        "Chance to summon tentacles to attack enemies within a day of attack."
    },
    WORK_MULT = {
        "Increase productivity in one day."
    },
    KILL_HEALTH_REGEN = {
        "Killing enemies within a day regenerates health."
    },
    FOOD_EAT_REGEN = {
        "Improve food recovery efficiency throughout the day.",
        "Reduced food recovery efficiency throughout the day."
    },
    BURNABLE_RESIST = {
        "Increase fire resistance in one day.",
        "Reduced flame resistance in one day."
    },
    FREEZABLE_RESIST = {
        "Increase freeze resistance in one day.",
        "Reduced freeze resistance in one day."
    },
    BUILD_INGREDIENTMOD = {
        "Half the amount of material used to make items in one minute.",
    },
    FAST_PICK = {
        "Get fast collection in one day."
    },
    ELECTRICATTACK = {
        "One day attack with lightning effect."
    },
    FAST_BUILD = {
        "Get quick production in one day."
    }
}

STRINGS.PTRIBE_PIG_CHEF_PLACE_POT = {
    "巧妇难为无米之炊。",
    "巧手莫为无面饼。",
    "要做饭，先架锅！",
}

STRINGS.PTRIBE_PIG_CHEF_DO_COOK = {
    "猪人喜欢做饭。",
    "自己做的饭最香！",
    "猪人是天生的大厨！",
    "没有动物能够抵御猪人做的美食。",
    "香，真香！"
}

STRINGS.PTRIBE_PIG_CHEF_WARN_EATER = {
    "再偷吃就不客气了。",
    "猪人要生气啦！",
    "那是猪人的食物。",
    "卑鄙的外来人。"
}

STRINGS.PTRIBE_PIG_FARMER_PLACE = {
    "猪人喜欢种地。",
    "倒萍越早，早稻越好。",
    "冬草肥田，春草肥禾。",
    "蚕豆不要粪，只要灰里困。",
    "好种出好苗，良种产量高。",
    "秋后不深耕，来年虫子生。",
    "锅底无柴难烧饭，田里无粪难增产。",
    "看田选种，看种种田。",
    "地里挑，场上选，忙半天，甜一年。",
    "旱锄地发暖，多锄地不板。",
    "种地无巧，粪水灌饱。",
    "谷子打苞，水淹齐腰。",
    "谷子黄，选种藏。",
    "年外不如年里，年里不如掩底。",
    "谷在犁上，菜在锄上。",
    "冬天比粪堆，来年比粮堆。",
    "春天粪堆密，秋后粮铺地。",
    "秧好一半禾，菜好半年粮。",
    "要想吃、好面，种麦泥里站。"
}

STRINGS.PTRIBE_PIG_FARMER_FERTILIZE = {
    "施肥了才能更好的生长。",
    "各肥混合用，不要胡乱壅。",
    "种地不上粪，好比瞎胡混。",
    "要想粮食打得多，就得水土不下坡。",
    "庄稼要好，施肥要巧。",
    "猪靠饭饱，田靠肥料。",
    "粪是土里虎，能增一石五。",
    "蚕豆一把灰，角角起堆堆。",
    "上粪上在劲头，锄地锄到地头。",
    "青草沤成粪，越长越有劲。",
    "要想庄稼旺，合理把粪上。",
    "灯无油不亮，稻无肥不长。",
    "草子三坐头，肥料就不愁。"
}
STRINGS.PTRIBE_PIG_FARMER_HARVEST = {
    "劳动最光荣！",
    "吃饭当节俭。",
    "粗茶淡饭，细水长流。",
    "粮食来之不易！",
}

STRINGS.PTRIBE_PIG_SCAVENGER_COLLECT = {
    "搜集物资。",
    "闻起来像宝贝！",
    "捡！捡！捡！猪人最喜欢捡东西！",
    "这个好闪亮！猪人喜欢！",
    "猪人不懂，但猪人喜欢！",
    "猪人找到秘密东西！",
    "猪人觉得这个重要！",
    "所有东西都是猪人的！"
}
STRINGS.PTRIBE_PIG_SCAVENGER_DISAPPEAR = "Backpack's full. Pigman's going home."

STRINGS.PTRIBE_WILD_PIG_NAMES = {
    "洛天依",
    "锡徐",
    "猪猪侠",
    "菲菲",
    "波比",
    "超人强",
    "猪猪侠",
    "小呆呆"
}
