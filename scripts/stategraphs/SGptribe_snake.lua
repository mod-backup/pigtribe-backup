require("stategraphs/commonstates")

local actionhandlers =
{
	ActionHandler(ACTIONS.EAT, "eat"),
	ActionHandler(ACTIONS.GOHOME, "gohome"),
}

local events =
{
	EventHandler("attacked",
		function(inst)
			if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then
				inst.sg
					:GoToState("hit")
			end
		end),
	EventHandler("death", function(inst) inst.sg:GoToState("death") end),
	EventHandler("doattack",
		function(inst, data)
			if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then
				inst.sg:GoToState("attack", data.target)
			end
		end),
	CommonHandlers.OnSleep(),
	CommonHandlers.OnLocomote(true, false),
	CommonHandlers.OnFreeze(),
}

local states =
{

	State {
		name = "gohome",
		tags = { "busy" },
		onenter = function(inst, playanim)
			if inst.components.homeseeker and
				inst.components.homeseeker.home and
				inst.components.homeseeker.home:IsValid() then
				inst.components.homeseeker.home.AnimState:PlayAnimation("chop", false)
			end
			inst:PerformBufferedAction()
		end,
	},

	State {
		name = "idle",
		tags = { "idle", "canrotate" },
		onenter = function(inst, playanim)
			-- inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/snake/idle")
			inst.Physics:Stop()
			if playanim then
				inst.AnimState:PlayAnimation(playanim)
				inst.AnimState:PushAnimation("idle", true)
			else
				inst.AnimState:PlayAnimation("idle", true)
			end
			inst.sg:SetTimeout(2 * math.random() + .5)
		end,

	},


	State {
		name = "attack",
		tags = { "attack", "busy" },

		onenter = function(inst, target)
			inst.sg.statemem.target = target
			inst.Physics:Stop()
			inst.components.combat:StartAttack()
			-- inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/snake/pre-attack")
			inst.AnimState:PlayAnimation("atk_pre")
			inst.AnimState:PushAnimation("atk", false)
			inst.AnimState:PushAnimation("atk_pst", false)
		end,

		timeline =
		{
			TimeEvent(8 * FRAMES, function(inst)
				if inst.components.combat.target then
					inst:ForceFacePoint(inst.components.combat.target:GetPosition())
				end
			end),

			TimeEvent(14 * FRAMES, function(inst)
				if inst.components.combat.target then
					inst:ForceFacePoint(inst.components.combat.target:GetPosition())
				end
				inst.SoundEmitter:PlaySound("dontstarve/bee/bee_attack")
			end),

			TimeEvent(20 * FRAMES, function(inst)
				if inst.components.combat.target then
					inst:ForceFacePoint(inst.components.combat.target:GetPosition())
				end
			end),

			TimeEvent(27 * FRAMES, function(inst)
				inst.components.combat:DoAttack(inst.sg.statemem.target)
				if inst.components.combat.target then
					inst:ForceFacePoint(inst.components.combat.target:GetPosition())
				end
			end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State {
		name = "eat",
		tags = { "busy" },

		onenter = function(inst, cb)
			inst.Physics:Stop()
			inst.components.combat:StartAttack()
			inst.AnimState:PlayAnimation("atk_pre")
			inst.AnimState:PushAnimation("atk", false)
			inst.AnimState:PushAnimation("atk_pst", false)
		end,

		timeline =
		{
			TimeEvent(14 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/bee/bee_attack") end),
			TimeEvent(24 * FRAMES, function(inst) if inst:PerformBufferedAction() then inst.components.combat:SetTarget(nil) end end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("taunt") end),
		},
	},

	State {
		name = "hit",
		tags = { "busy", "hit" },

		onenter = function(inst, cb)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve/bee/bee_hurt")
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State {
		name = "taunt",
		tags = { "busy" },

		onenter = function(inst, cb)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("taunt")
			-- inst.SoundEmitter:PlaySound("dontstarve_DLC002/creatures/snake/taunt")
		end,

		timeline =
		{
			TimeEvent(10 * FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/bee/bee_hurt")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State {
		name = "death",
		tags = { "busy" },

		onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/bee/bee_death")
			inst.AnimState:PlayAnimation("death")
			inst.Physics:Stop()
			RemovePhysicsColliders(inst)
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
		end,

	},
}

CommonStates.AddSleepStates(states,
	{
		sleeptimeline = {
			TimeEvent(30 * FRAMES,
				function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/sleep") end),
		},
	})


CommonStates.AddRunStates(states)
CommonStates.AddFrozenStates(states)


return StateGraph("ptribe_snake", states, events, "taunt", actionhandlers)
