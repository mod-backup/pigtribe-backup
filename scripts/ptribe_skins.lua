local FN = {}

---哈姆雷特猪镇房子皮肤
FN.PIGHOUSE_SKINS = { {
    build = "pig_townhouse1_pink_build",
    bank = "pig_townhouse"
}, {
    build = "pig_townhouse1_green_build",
    bank = "pig_townhouse"
}, {
    build = "pig_townhouse1_white_build",
    bank = "pig_townhouse"
}, {
    build = "pig_townhouse1_brown_build",
    bank = "pig_townhouse"
}, {
    build = "pig_townhouse5_beige_build",
    bank = "pig_townhouse5"
}, {
    build = "pig_townhouse6_red_build",
    bank = "pig_townhouse6"
}, {
    build = "pig_house_tropical", --野猪房
    bank = "pig_house_tropical"
} }


return FN
