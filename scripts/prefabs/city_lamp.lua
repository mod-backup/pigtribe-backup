local assets =
{
    Asset("ANIM", "anim/lamp_post2.zip"),
    Asset("ANIM", "anim/lamp_post2_city_build.zip"),
    Asset("ANIM", "anim/lamp_post2_yotp_build.zip"),
}

local INTENSITY = 0.6

-- 联机没有这个音乐
-- local LAMP_DIST = 16
-- local LAMP_DIST_SQ = LAMP_DIST * LAMP_DIST
-- local function UpdateAudio(inst)
--     local player = GetPlayer()

--     local instPosition = Vector3(inst.Transform:GetWorldPosition())
--     local playerPosition = Vector3(player.Transform:GetWorldPosition())
--     local lampIsNearby = (distsq(playerPosition, instPosition) < LAMP_DIST_SQ)

--     if TheWorld.state.isdusk and lampIsNearby and not inst.SoundEmitter:PlayingSound("onsound") then
--         inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/city_lamp/on_LP", "onsound")
--     elseif not lampIsNearby and inst.SoundEmitter:PlayingSound("onsound") then
--         inst.SoundEmitter:KillSound("onsound")
--     end
-- end

local function GetStatus(inst)
    return not inst.lighton and "ON" or nil
end

local function fadein(inst)
    inst.components.fader:StopAll()
    inst.AnimState:PlayAnimation("on")
    inst.AnimState:PushAnimation("idle", true)
    inst.Light:Enable(true)

    if inst:IsAsleep() then
        inst.Light:SetIntensity(INTENSITY)
    else
        inst.Light:SetIntensity(0)
        inst.components.fader:Fade(0, INTENSITY, 3 + math.random() * 2, function(v) inst.Light:SetIntensity(v) end)
    end
end

local function fadeout(inst)
    inst.components.fader:StopAll()
    inst.AnimState:PlayAnimation("off")
    inst.AnimState:PushAnimation("idle", true)

    if inst:IsAsleep() then
        inst.Light:SetIntensity(0)
    else
        inst.components.fader:Fade(INTENSITY, 0, .75 + math.random() * 1, function(v) inst.Light:SetIntensity(v) end)
    end
end

local function updatelight(inst)
    if TheWorld.state.isdusk or TheWorld.state.isnight or TheWorld.state.iscaveday then
        if not inst.lighton then
            inst:DoTaskInTime(math.random() * 2, fadein)
        else
            inst.Light:Enable(true)
            inst.Light:SetIntensity(INTENSITY)
        end
        inst.AnimState:Show("FIRE")
        inst.AnimState:Show("GLOW")
        inst.lighton = true
    else
        if inst.lighton then
            inst:DoTaskInTime(math.random() * 2, fadeout)
        else
            inst.Light:Enable(false)
            inst.Light:SetIntensity(0)
        end

        inst.AnimState:Hide("FIRE")
        inst.AnimState:Hide("GLOW")

        inst.lighton = false
    end
end

local function onhammered(inst)
    -- if not inst.components.fixable then
    inst.components.lootdropper:DropLoot()
    -- end

    SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())

    inst:Remove()
end

local function onhit(inst)
    inst.AnimState:PlayAnimation("hit")
    inst.AnimState:PushAnimation("idle", true)
    inst:DoTaskInTime(0.3, updatelight)
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle", true)
    inst:DoTaskInTime(0, updatelight)
end

-- 季节皮肤
-- local function OnEntityWake(inst)
--     if GetAporkalypse() and GetAporkalypse():GetFiestaActive() then
--         if inst.build == "lamp_post2_city_build" then
--             inst.build = "lamp_post2_yotp_build"
--             inst.AnimState:SetBuild(inst.build)
--         end
--     elseif inst.build == "lamp_post2_yotp_build" then
--         inst.build = "lamp_post2_city_build"
--         inst.AnimState:SetBuild(inst.build)
--     end
-- end


local function setobstical(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    TheWorld.Pathfinder:AddWall(x, y, z)
end

local function clearobstacle(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    TheWorld.Pathfinder:RemoveWall(x, y, z)
end

local function OnPhaseChange(inst)
    if TheWorld.state.isday then
        inst:DoTaskInTime(1 / 30, updatelight)
    elseif TheWorld.state.isdusk then
        inst:DoTaskInTime(1 / 30, updatelight)
    end
end

local function OnSave(inst, data)
    data.lighton = inst.lighton
end

local function OnLoad(inst, data)
    if not data then return end

    if data.lighton then
        fadein(inst)
        inst.Light:Enable(true)
        inst.Light:SetIntensity(INTENSITY)
        inst.AnimState:Show("FIRE")
        inst.AnimState:Show("GLOW")
        inst.lighton = true
    end
end

local function fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddPhysics()
    inst.entity:AddNetwork()
    inst.entity:AddLight()

    inst.Light:SetIntensity(INTENSITY)
    inst.Light:SetColour(197 / 255, 197 / 255, 10 / 255)
    inst.Light:SetFalloff(0.9)
    inst.Light:SetRadius(5)
    inst.Light:Enable(false)

    inst.build = "lamp_post2_city_build"
    inst.AnimState:SetBank("lamp_post")
    inst.AnimState:SetBuild(inst.build)
    inst.AnimState:PlayAnimation("idle", true)
    inst.AnimState:Hide("FIRE")
    inst.AnimState:Hide("GLOW")

    MakeObstaclePhysics(inst, 0.25)

    inst:AddTag("CITY_LAMP")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst.components.inspectable.getstatus = GetStatus

    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)

    inst:AddComponent("fader")

    -- 联机版没有这个音乐
    -- inst.audiotask = inst:DoPeriodicTask(1.0, UpdateAudio, math.random())

    -- inst:AddComponent("fixable")
    -- inst.components.fixable:AddRecinstructionStageData("rubble", "lamp_post", "lamp_post2_city_build")

    -- inst:AddComponent("gridnudger")

    inst.setobstical = setobstical
    -- 跟室内有关
    inst.returntointeriorscene = setobstical
    inst.removefrominteriorscene = clearobstacle

    inst:ListenForEvent("onbuilt", onbuilt)
    inst:ListenForEvent("onremove", clearobstacle)
    inst:WatchWorldState("phase", OnPhaseChange)
    OnPhaseChange(inst)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab("common/objects/city_lamp", fn, assets),
    MakePlacer("common/city_lamp_placer", "lamp_post", "lamp_post2_city_build", "idle", false, false, true)
