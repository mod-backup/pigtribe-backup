local Constructor = require("ptribe_utils/constructor")

local assets =
{
    Asset("ANIM", "anim/halberd.zip"),
    Asset("ANIM", "anim/swap_halberd.zip"),
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_halberd", "swap_halberd")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function CoomonInit(inst)
    MakeInventoryPhysics(inst)
    MakeInventoryFloatable(inst, "small", 0.1)

    inst:AddTag("sharp")
end

local function MasterInit(inst)
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/hamletinventoryimages.xml"

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.HALBERD_DAMAGE)

    inst:AddComponent("tool")
    inst.components.tool:SetAction(ACTIONS.CHOP)

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(TUNING.HALBERD_USES)
    inst.components.finiteuses:SetUses(TUNING.HALBERD_USES)
    inst.components.finiteuses:SetOnFinished(inst.Remove)
    inst.components.finiteuses:SetConsumption(ACTIONS.CHOP, 1)

    inst:AddComponent("inspectable")

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
end

return Constructor.MakePrefab("halberd", {
    assets = assets,
    coomonInit = CoomonInit,
    masterInit = MasterInit
})
