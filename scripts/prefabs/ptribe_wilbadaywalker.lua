local Constructor = require("ptribe_utils/constructor")

--- 薇尔芭变身
local function ptribe_Transform(inst, player)
    inst.wilba = player
    inst.entity:SetParent(player.entity)
    inst.Follower:FollowSymbol(player.GUID, "torso", 0, 50, 0)
end

local function ShouldRemove(inst)
    if not inst.wilba or IsEntityDeadOrGhost(inst.wilba) then
        inst:Remove()
    end
end

local function fn(inst)
    inst.entity:AddFollower()

    if not TheWorld.ismastersim then end

    inst.ptribe_Transform = ptribe_Transform

    inst:SetBrain()
    inst:SetStateGraph("ptribe_SGdaywalker")

    inst.wilba = nil
    inst:DoTaskInTime(0, ShouldRemove)

    inst.persists = false --由变身组件控制
end

return Constructor.CopyPrefab("ptribe_wilbadaywalker", "daywalker", { init = fn })
