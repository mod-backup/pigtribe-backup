local Constructor = require("ptribe_utils/constructor")

local assets =
{
    Asset("ANIM", "anim/waterdrop.zip"),
    Asset("ANIM", "anim/lifeplant.zip"),
}

local function CommonInit(inst)
    MakeInventoryPhysics(inst)
    MakeInventoryFloatable(inst)

    inst:AddTag("waterdrop")
end

local function ondeploy(inst, pt)
    local lifeplant = SpawnAt("lifeplant", pt)
    lifeplant.SoundEmitter:PlaySound("dontstarve_DLC003/common/crafted/flower_of_life/plant")
    if inst.finiteusesPer then
        lifeplant.components.finiteuses:SetPercent(inst.finiteusesPer)
    end
    inst:Remove()
end

local function OnSave(inst, data)
    data.finiteusesPer = inst.finiteusesPer
end

local function OnLoad(inst, data)
    inst.finiteusesPer = data and data.finiteusesPer
end

local function MasterInit(inst)
    inst:AddComponent("edible")
    inst.components.edible.foodtype = FOODTYPE.VEGGIE
    inst.components.edible.healthvalue = TUNING.HEALING_SUPERHUGE * 3
    inst.components.edible.hungervalue = TUNING.CALORIES_SUPERHUGE * 3
    inst.components.edible.sanityvalue = TUNING.SANITY_HUGE * 3

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/hamletinventoryimages_2.xml"

    inst:AddComponent("inspectable")

    inst:AddComponent("deployable")
    inst.components.deployable:SetDeployMode(DEPLOYMODE.PLANT)
    inst.components.deployable.ondeploy = ondeploy

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
end

return Constructor.MakePrefab("waterdrop", {
        assets = assets,
        bank = "waterdrop",
        build = "waterdrop",
        commonInit = CommonInit,
        masterInit = MasterInit
    }),
    MakePlacer("common/waterdrop_placer", "lifeplant", "lifeplant", "idle_loop")
