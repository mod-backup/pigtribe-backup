local Utils = require("ptribe_utils/utils")

local assets =
{
    Asset("ANIM", "anim/telescope.zip"),
    Asset("ANIM", "anim/telescope_long.zip"),
    Asset("ANIM", "anim/swap_telescope.zip"),
    Asset("ANIM", "anim/swap_telescope_long.zip"),
}

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function ReticuleTargetFn()
    return Vector3(ThePlayer.entity:LocalToWorldSpace(5, 0, 0))
end

local function oncast(inst, target, pos)
    local doer = inst.components.inventoryitem:GetGrandOwner()
    if not doer then return end

    local x, y, z = doer.Transform:GetWorldPosition()
    inst.components.finiteuses:Use(1)

    -- inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/use_spyglass_reveal")
    local numerodeitens = inst:HasTag("supertelescope") and TUNING.SUPERTELESCOPE_RANGE or TUNING.TELESCOPE_RANGE
    local dist = 1
    for _ = 1, numerodeitens do
        local angle = doer:GetRotation()
        dist = dist + 1
        local offset = Vector3(dist * math.cos(angle * DEGREES), 0, -dist * math.sin(angle * DEGREES))
        local pt = Vector3(x, y, z)
        local chestpos = pt + offset
        local x, y, z = chestpos:Get()

        -------------------coloca os itens------------------------
        TheWorld.minimap.MiniMap:ShowArea(x, y, z, 30)
        doer.player_classified.MapExplorer:RevealArea(x, 0, z)
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    -- MakeBlowInHurricane(inst, TUNING.WINDBLOWN_SCALE_MIN.MEDIUM, TUNING.WINDBLOWN_SCALE_MAX.MEDIUM)
    MakeInventoryFloatable(inst, "small", 0.1)

    inst:AddTag("nopunch")
    inst:AddTag("allow_action_on_impassable")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(TUNING.TELESCOPE_USES)
    inst.components.finiteuses:SetUses(TUNING.TELESCOPE_USES)
    inst.components.finiteuses:SetOnFinished(inst.Remove)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/hamletinventoryimages.xml"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("reticule")
    inst.components.reticule.targetfn = ReticuleTargetFn
    inst.components.reticule.ease = true

    inst:AddComponent("spellcaster")
    -- inst.components.spellcaster:SetAction(ACTIONS.PEER)
    inst.components.spellcaster:SetSpellFn(oncast)
    inst.components.spellcaster:SetCanCastFn(Utils.TrueFn)
    inst.components.spellcaster.canuseonpoint = true
    inst.components.spellcaster.canusefrominventory = false
    inst.components.spellcaster.quickcast = true
    inst.components.spellcaster.canuseonpoint_water = true

    return inst
end

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_telescope", "swap_object")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function onsuperequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_telescope_long", "swap_object")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function normalfn()
    local inst = fn()

    inst.AnimState:SetBank("telescope")
    inst.AnimState:SetBuild("telescope")
    inst.AnimState:PlayAnimation("idle")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.inventoryitem.imagename = "telescope"

    inst.components.equippable:SetOnEquip(onequip)

    return inst
end

local function superfn()
    local inst = fn()

    inst.AnimState:SetBank("telescope_long")
    inst.AnimState:SetBuild("telescope_long")
    inst.AnimState:PlayAnimation("idle")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.inventoryitem.imagename = "supertelescope"

    inst.components.equippable:SetOnEquip(onsuperequip)

    return inst
end

return Prefab("ptribe_telescope", normalfn, assets),
    Prefab("ptribe_supertelescope", superfn, assets)
