local Constructor = require("ptribe_utils/constructor")

local assets =
{
    Asset("ANIM", "anim/topiary_pigman.zip"),
    Asset("ANIM", "anim/topiary_werepig.zip"),
    Asset("ANIM", "anim/topiary_beefalo.zip"),
    Asset("ANIM", "anim/topiary_pigking.zip"),
}

local prefabs =
{
    "ash",
    "collapse_small",
    "green_leaves_chop"
}

local function CoomonInit(inst, frame)
    inst.entity:AddSoundEmitter()

    local minimap = inst.entity:AddMiniMapEntity()
    minimap:SetIcon("topiary_" .. frame .. ".png")

    if frame == "3" then
        MakeObstaclePhysics(inst, 0.75)
    elseif frame == "4" then
        MakeObstaclePhysics(inst, 1.75)
    else
        MakeObstaclePhysics(inst, .25)
    end

    inst:AddTag("structure")
end

----------------------------------------------------------------------------------------------------

local function onhammered(inst, worker)
    local x, y, z = inst.Transform:GetWorldPosition()
    for i = 1, math.random(3, 4) do
        local fx = SpawnPrefab("green_leaves_chop")
        fx.Transform:SetPosition(x + (math.random() * 2), y + math.random() * 0.5, z + (math.random() * 2))
        if math.random() < 0.5 then
            fx.Transform:SetScale(-1, 1, -1)
        end
    end

    -- if not inst.components.fixable then
    inst.components.lootdropper:DropLoot()
    -- end

    inst.SoundEmitter:PlaySound("dontstarve/common/destroy_straw")
    inst:Remove()
end

local function onhit(inst, worker)
    inst.AnimState:PlayAnimation("hit")
    inst.AnimState:PushAnimation("idle", false)

    local fx = SpawnPrefab("green_leaves_chop")
    local x, y, z = inst.Transform:GetWorldPosition()
    fx.Transform:SetPosition(x, y + math.random() * 0.5, z)
end

local function MasterInit(inst, frame)
    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)

    if frame == "3" or frame == "4" then
        MakeLargeBurnable(inst, nil, nil, true)
        MakeLargePropagator(inst)
    else
        MakeMediumBurnable(inst, nil, nil, true)
        MakeMediumPropagator(inst)
    end

    MakeSnowCovered(inst)

    -- inst:AddComponent("fixable")
    -- inst.components.fixable:AddRecinstructionStageData("burnt", build, build)
    -- inst.components.fixable:SetPrefabName("topiary")

    inst:SetPrefabNameOverride("topiary")
end

local function makeitem(name, build, frame)
    return Constructor.MakePrefab(name, {
        assets = assets,
        prefabs = prefabs,
        bank = build, --一样
        build = build,
        coomonInit = function(inst) CoomonInit(inst, frame) end,
        masterInit = function(inst) MasterInit(inst, frame) end,
    })
end

return
    makeitem("topiary_1", "topiary_pigman", "1"),
    makeitem("topiary_2", "topiary_werepig", "2"),
    makeitem("topiary_3", "topiary_beefalo", "3"),
    makeitem("topiary_4", "topiary_pigking", "4")
