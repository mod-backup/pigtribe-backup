local assets =
{
    Asset("ANIM", "anim/pig_house_sale.zip"),
    Asset("ANIM", "anim/player_small_house1.zip"),
    Asset("ANIM", "anim/player_large_house1.zip"),

    Asset("ANIM", "anim/player_large_house1_manor_build.zip"),
    Asset("ANIM", "anim/player_large_house1_villa_build.zip"),
    Asset("ANIM", "anim/player_small_house1_cottage_build.zip"),
    Asset("ANIM", "anim/player_small_house1_tudor_build.zip"),
    Asset("ANIM", "anim/player_small_house1_gothic_build.zip"),
    Asset("ANIM", "anim/player_small_house1_brick_build.zip"),
    Asset("ANIM", "anim/player_small_house1_turret_build.zip"),
}

local function onhammered(inst, worker)
    if inst:HasTag("fire") and inst.components.burnable then
        inst.components.burnable:Extinguish()
    end

    local pos = inst:GetPosition()
    SpawnPrefab("collapse_big").Transform:SetPosition(pos:Get())
    TheWorld.components.interiorspawner:OnHouseDestroy(inst, worker)
    inst.components.hamletdoor:SetCenterPos(nil)
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle")
    end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    -- inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/crafted/pighouse/wood_1")
    inst.AnimState:PushAnimation("idle")
end

local addprops = {
    {
        name = "playerhouse_city_exit_door",
        x_offset = 4.7,
        animdata = { bank = "pig_shop_doormats", build = "pig_shop_doormats", anim = "idle_old", background = true },
        init = function(inst, home)
            inst.components.teleporter:Target(home)
            home.components.teleporter:Target(inst)
        end
    },
    { name = "wallinteriorplayerhouse",      x_offset = -2.8, },
    { name = "deco_roomglow" },
    { name = "shelves_cinderblocks",         x_offset = -4.5,   z_offset = -15 / 3.5,                                      addtags = { "playercrafted" } },
    { name = "deco_antiquities_wallfish",    x_offset = -5,     z_offset = 3.9,                                            addtags = { "playercrafted" } },
    { name = "deco_antiquities_cornerbeam",  x_offset = -5,     z_offset = -15 / 2, },
    { name = "deco_antiquities_cornerbeam",  x_offset = -5,     z_offset = 15 / 2,                                         animdata = { flip = true } },
    { name = "deco_antiquities_cornerbeam2", x_offset = 4.7,    z_offset = -15 / 2 - 0.3, },
    { name = "deco_antiquities_cornerbeam2", x_offset = 4.7,    z_offset = 15 / 2 + 0.3,                                   animdata = { flip = true } },
    { name = "swinging_light_rope_1",        x_offset = -2,     y_offset = 1,                                              addtags = { "playercrafted" } },
    { name = "charcoal",                     x_offset = -3,     z_offset = -2 },
    { name = "charcoal",                     x_offset = 2,      z_offset = 3 },

    { name = "window_round_backwall",        z_offset = 15 / 2, animdata = { flip = true, bank = "interior_window_side" }, addtags = { "playercrafted" }, children = { "window_round_light" } },
    { name = "hamlet_interior_floor",        x_offset = -2.4 }
}

local function CreatInterior(inst)
    if inst.components.hamletdoor.centerPos then return end

    local pos = TheWorld.components.interiorspawner:GetPos(inst)
    inst.components.hamletdoor:SetCenterPos(pos)
    TheWorld.components.interiorspawner:SpawnHouseDoorInteriorPrefabs(pos.x, pos.z, addprops, inst)
end

local function OnActivate(inst, doer)
    if doer:HasTag("player") then
        -- doer.ptribe_playerCameraMode:set(1) --提前设置摄像机，缓动

        --Sounds are triggered in player's stategraph
    elseif inst.SoundEmitter ~= nil then
        inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
    end
end

local function OnActivateByOther(inst, source, doer)
    if doer ~= nil and doer.Physics ~= nil then
        doer.Physics:CollidesWith(COLLISION.WORLD)
    end
end

local function OnSave(inst, data)
    data.build = inst.build
    data.bank = inst.bank
    data.name = inst.name
    data.minimapicon = inst.minimapicon
end

local function OnLoad(inst, data)
    if not data then return end

    if data.build then
        inst.build = data.build
        inst.AnimState:SetBuild(data.build)
    end
    if data.bank then
        inst.AnimState:SetBank(data.bank)
        inst.bank = data.bank
    end
    if data.name then
        inst.name = data.name
        inst.name = STRINGS.NAMES[string.upper(data.name)]
    end
    if data.minimapicon then
        inst.minimapicon = data.minimapicon
        inst.MiniMapEntity:SetIcon(data.minimapicon)
    end
end

-- 如果屋里有玩家则房子不可燃，可以通过递归查询来检查是否有玩家在里面
local function OnIgniteFn(inst)
    if TheWorld.components.interiorspawner:InterioHasPlayer(inst) then
        inst:DoTaskInTime(0, function()
            if inst.components.burnable then
                inst.components.burnable:Extinguish()
            end
        end)
    end
end

local function fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    local minimap = inst.entity:AddMiniMapEntity()
    minimap:SetIcon("pig_house_sale.png")

    inst.Light:SetFalloff(1)
    inst.Light:SetIntensity(.5)
    inst.Light:SetRadius(1)
    inst.Light:Enable(false)
    inst.Light:SetColour(180 / 255, 195 / 255, 50 / 255)

    inst.AnimState:SetBuild("pig_house_sale")
    inst.AnimState:SetBank("pig_house_sale")
    inst.AnimState:SetScale(0.75, 0.75, 0.75)
    inst.AnimState:PlayAnimation("idle", true)

    MakeObstaclePhysics(inst, 1)

    inst:AddTag("renovatable") --可翻新
    inst:AddTag("structure")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)

    MakeMediumBurnable(inst, nil, nil, true)
    inst.components.burnable:SetOnIgniteFn(OnIgniteFn)

    -- 移除后可以修复，就是用锤子锤后可以再用锤子修回来
    -- inst:AddComponent("fixable")

    MakeSnowCovered(inst, .01)
    MakeLargePropagator(inst)

    -- 控制玩家是否可以进房，只有买了之后才能进
    -- inst:AddComponent("door")

    inst:AddComponent("hamletdoor")

    inst:AddComponent("teleporter")
    inst.components.teleporter.onActivate = OnActivate
    inst.components.teleporter.onActivateByOther = OnActivateByOther
    inst.components.teleporter.offset = 0
    inst.components.teleporter.travelcameratime = 0
    inst.components.teleporter.travelarrivetime = 0

    inst:DoTaskInTime(0, CreatInterior)

    inst:ListenForEvent("onbuilt", onbuilt)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

local function HideLayers(inst)
    inst.AnimState:Hide("snow")
    inst.AnimState:Hide("boards")
end

return Prefab("common/objects/playerhouse_city", fn, assets),
    MakePlacer("common/playerhouse_city_placer", "pig_house_sale", "pig_house_sale", "idle", nil, nil, nil, 0.75, nil,
        nil, nil, nil, nil, nil, nil, HideLayers)

-- 进入固定缩放大小，锁定滚轮
-- 墙壁不能因为角色移动而一直乱动
-- 可放置建筑，物品不会掉入“水”中
-- 不会落鸟
-- 屋内保温、防雨
-- 行走加上音效
-- 屋内回san
-- 允许房间里种植
