local GetPrefab = require("ptribe_utils/getprefab")

local assets =
{
    Asset("ANIM", "anim/flag_post_duster_build.zip"),
    -- Asset("ANIM", "anim/flag_post_perdy_build.zip"), --我要那么多旗帜没用
    -- Asset("ANIM", "anim/flag_post_royal_build.zip"),
    -- Asset("ANIM", "anim/flag_post_wilson_build.zip"),

    Asset("ANIM", "anim/pig_shop.zip"),
    Asset("ANIM", "anim/pig_tower_build.zip"),
    Asset("ANIM", "anim/pig_tower_royal_build.zip"),

    Asset("SOUND", "sound/pig.fsb"),
}

local prefabs =
{
    "pigman_royalguard",
    "pigman_royalguard_2",
}

local function onhammered(inst, worker)
    if inst:HasTag("fire") and inst.components.burnable then
        inst.components.burnable:Extinguish()
    end

    inst.components.lootdropper:DropLoot()
    SpawnPrefab("collapse_big").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst.SoundEmitter:PlaySound("dontstarve/common/destroy_stone")
    inst:Remove()
end

local function onhit(inst)
    if not inst:HasTag("burnt") then
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle")
    end
end

--- 根据等级初始化猪人守卫属性
---@param rate number 相较于10级最高级的百分比，因为黄金守卫最高级为15，所以这个值可能大于1
local function InitPigByLevel(pig, rate)
    rate = rate * TUNING.TRIBE_PIGMAN_CITY_MULT
    GetPrefab.SetMaxHealth(pig, TUNING.PIG_HEALTH * (1 + rate))
    pig.components.health:StartRegen(2 + (1 + rate * 2), 10)
    if rate >= 0.8 then
        pig.components.burnable:SetBurnTime(8 - math.min(6, rate * 3))
        pig.components.freezable:SetResistance(2 + math.min(3, rate * 2))
    end
    -- pig.components.combat:SetDefaultDamage(TUNING.PIG_DAMAGE * (1 + rate))
    pig.components.combat.externaldamagemultipliers:RemoveModifier(pig, "ptribe_pigUpgrade")
    pig.components.combat.externaldamagemultipliers:SetModifier(pig, 1 + 0.5 * rate, "ptribe_pigUpgrade")
    pig.components.locomotor.runspeed = TUNING.PIG_RUN_SPEED * (1 + math.ceil(0.5 * rate))
end

local function OnLevelChangeFn(inst)
    local pig = inst.components.spawner.child
    if pig then
        InitPigByLevel(pig, inst.components.ptribe_upgrade.current / 10)
    end
end

---初始化操作
local function onspawnedfn(inst, child)
    InitPigByLevel(child, inst.components.ptribe_upgrade.current / 10)

    local axe = SpawnPrefab("halberd")      --战戟
    child.components.inventory:Equip(axe)
    local armour = SpawnPrefab("armorwood") --木甲
    child.components.inventory:Equip(armour)
end

local function LightsOn(inst)
    if not inst:HasTag("burnt") then
        inst.Light:Enable(true)
        inst.AnimState:PlayAnimation("lit", true)
        inst.SoundEmitter:PlaySound("dontstarve/pig/pighut_lighton")
    end
end

local function LightsOff(inst)
    if not inst:HasTag("burnt") then
        inst.Light:Enable(false)
        inst.AnimState:PlayAnimation("idle", true)
        inst.SoundEmitter:PlaySound("dontstarve/pig/pighut_lightoff")
    end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle")
    inst.AnimState:AddOverrideBuild("flag_post_duster_build") --旗帜

    inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/crafted/pighouse/brick")
end

--- 虽然守卫晚上不回家，但是我还是想要屋子能有个灯光
local function OnPhaseChange(inst, phase)
    if phase == "night" or phase == "full" then
        LightsOn(inst)
    else
        LightsOff(inst)
    end
end

local function OnMaxLevelFn(inst, isLoading)
    inst.AnimState:Show("YOTP") -- 显示和隐藏两种状态
end

local function common(build, mapIcon, child, maxLevel)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    local minimap = inst.entity:AddMiniMapEntity()
    minimap:SetIcon(mapIcon)

    inst.Light:SetFalloff(1)
    inst.Light:SetIntensity(.5)
    inst.Light:SetRadius(1)
    inst.Light:SetColour(180 / 255, 195 / 255, 50 / 255)
    inst.Light:Enable(false)

    MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("pig_shop")
    inst.AnimState:SetBuild(build)
    inst.AnimState:PlayAnimation("idle", true)
    inst.AnimState:Hide("YOTP")

    -- inst:AddTag("guard_tower")
    inst:AddTag("structure")
    -- inst:AddTag("city_hammerable")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)

    inst:AddComponent("spawner")
    WorldSettings_Spawner_SpawnDelay(inst, TUNING.PIGHOUSE_SPAWN_TIME, TUNING.PIGHOUSE_ENABLED)
    inst.components.spawner:Configure(child, TUNING.PIGHOUSE_SPAWN_TIME)
    inst.components.spawner.onspawnedfn = onspawnedfn
    -- inst.components.spawner.onoccupied = onoccupied -- 这里设定皇家守卫不回家
    -- inst.components.spawner.onvacate = onvacate
    inst.components.spawner:SetWaterSpawning(false, true)
    inst.components.spawner:CancelSpawning()

    inst:AddComponent("inspectable")

    inst:AddComponent("ptribe_upgrade")
    inst.components.ptribe_upgrade:Configure(0, maxLevel, OnLevelChangeFn, OnMaxLevelFn)

    MakeSnowCovered(inst, .01)

    inst:ListenForEvent("onbuilt", onbuilt)
    inst:WatchWorldState("phase", OnPhaseChange)
    OnPhaseChange(inst)

    inst:DoTaskInTime(0, GetPrefab.SpawnerInitSpawnChild)

    return inst
end

local function fn1()
    return common("pig_tower_build", "pig_guard_tower.png", "pigman_royalguard", TUNING.PTRIBE_GUARD_TOWER_MAX_LEVEL)
end

local function fn2()
    return common("pig_tower_royal_build", "pig_tower_royal.png", "pigman_royalguard_2",
        math.ceil(TUNING.PTRIBE_GUARD_TOWER_MAX_LEVEL / 2 * 3))
end

return Prefab("common/objects/pig_guard_tower", fn1, assets, prefabs),
    Prefab("common/objects/pig_guard_tower_palace", fn2, assets, prefabs),
    MakePlacer("common/pig_guard_tower_placer", "pig_shop", "pig_tower_build", "idle"),
    MakePlacer("common/pig_guard_tower_palace_placer", "pig_shop", "pig_tower_royal_build", "idle")
