local Constructor = require("ptribe_utils/constructor")

local assets =
{
    Asset("ANIM", "anim/wildbore_build.zip"),
    Asset("ANIM", "anim/werepig_wildbore_build.zip"),
}

local function Init(inst)
    inst.build = "wildbore_build"
    inst.AnimState:SetBuild("wildbore_build")

    if not TheWorld.ismastersim then return end

    inst.prtibe_giveTime = GetTime() --给予道具的时间
end

return Constructor.CopyPrefab("ptribe_wildbore", "pigman", {
    assets = assets,
    init = Init
})
