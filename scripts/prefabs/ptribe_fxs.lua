local Constructor = require("ptribe_utils/constructor")

local fx = {}

-- 关闭特效
local function Close(inst)
    inst.AnimState:PlayAnimation("deactivate_fx")
    inst:DoTaskInTime(10 * FRAMES, inst.Remove)
end

table.insert(fx, Constructor.MakeFX("ptribe_terrarium_fx", {
    assets = {},
    bank = "terrarium",
    build = "terrarium",
    anim = "activate_fx",
    sound = "terraria1/terrarium/beam_shoot",
    commonPostInit = function(inst)
        inst:AddTag("DECOR")
        inst.AnimState:SetFinalOffset(3)
        inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
        inst.AnimState:SetSortOrder(3)
        inst.AnimState:SetLightOverride(1)

        inst.AnimState:PushAnimation("activated_idle_fx", true)
        inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

        inst:DoTaskInTime(34 * FRAMES, Close)
    end,
    -- masterPostInit = function(inst) --攻击逻辑放监听事件了，防止重复攻击和误伤
    -- inst.close = Close
    -- end
}))

return unpack(fx)
