local function SpawnPiso2()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddNetwork()

	inst:AddTag("NOBLOCK")
	inst:AddTag("NOCLICK")
	inst:AddTag("prototyper")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("prototyper")
	inst.components.prototyper.trees = TUNING.PROTOTYPER_TREES.PTRIBE_HOME

	inst.persists = false

	return inst
end

return Prefab("wallrenovation", SpawnPiso2) --科技
