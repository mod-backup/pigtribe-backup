local Constructor = require("ptribe_utils/constructor")

local function CoomonInit(inst)
    MakeInventoryFloatable(inst, "med", 0.1, 0.75)

    inst:AddTag("ptribe_upgrade")
end

local function MasterInit(inst)
    MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
    MakeSmallPropagator(inst)

    inst:AddComponent("stackable")
end

return Constructor.MakePrefab("ptribe_upgrade", {
    coomonInit = CoomonInit,
    masterInit = MasterInit,
    isInventoryitem = true
})
