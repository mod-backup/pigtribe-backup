local Utils = require("ptribe_utils/utils")

local assets =
{
    Asset("ANIM", "anim/deed.zip"),
    Asset("ANIM", "anim/pig_house_sale.zip"),
}

local function ondeploy(inst, pt)
    local casa = SpawnPrefab("playerhouse_city")
    casa.Transform:SetPosition(pt:Get())
    inst:Remove()
end

local function fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    MakeInventoryFloatable(inst)

    inst.AnimState:SetBank("deed")
    inst.AnimState:SetBuild("deed")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("deploykititem") --影响放置时提示文本

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.caminho = "images/inventoryimages/hamletinventoryimages.xml"

    MakeSmallBurnable(inst, TUNING.TINY_BURNTIME)
    MakeSmallPropagator(inst)
    MakeHauntableLaunchAndIgnite(inst)

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("deployable")
    inst.components.deployable.ondeploy = ondeploy
    inst.components.deployable:SetDeployMode(DEPLOYMODE.WALL)
    -- 虽然可以实现房套房，但是我并不希望，这反直觉
    Utils.FnDecorator(inst.components.deployable, "Deploy", function(self, pt)
        if pt.z >= 950 then
            return { false }, true
        end
    end)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/hamletinventoryimages.xml"
    inst.components.inventoryitem.foleysound = "dontstarve/movement/foley/jewlery"

    return inst
end

return Prefab("deed", fn, assets),
    MakePlacer("deed_placer", "pig_house_sale", "pig_house_sale", "idle", nil, nil, nil, 0.75)
