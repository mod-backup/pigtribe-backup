local GetPrefab = require("ptribe_utils/getprefab")
local Utils = require("ptribe_utils/utils")

local assets = {
    Asset("ANIM", "anim/pig_royalguard.zip"),
    Asset("ANIM", "anim/pig_royalguard_2.zip"), --其实还有两个废案也很好看
}

local prefabs = {
    "halberd", "armorwood"
}

local brain = require "brains/pigbrain"
local MAX_TARGET_SHARES = 5
local SHARE_TARGET_DIST = 30

local function ontalk(inst)
    inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
    -- inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/guard_alert") --沙哑的声音和猪人的声音参和在一起太违和了，除非重写一个sg
end

local function OnEat(inst, food)
    if food.components.edible ~= nil then
        if food.components.edible.foodtype == FOODTYPE.VEGGIE then
            SpawnPrefab("poop").Transform:SetPosition(inst.Transform:GetWorldPosition())
        end
    end
end

local function NormalKeepTargetFn(inst, target)
    return inst.components.combat:CanTarget(target)
        and target:IsInLight()
        and not (target.sg ~= nil and target.sg:HasStateTag("transform"))
end

local RETARGET_MUST_TAGS = { "_combat" }
local function NormalRetargetFn(inst)
    if inst:HasTag("NPC_contestant") then
        return nil
    end

    local exclude_tags = { "playerghost", "INLIMBO", "NPC_contestant" }
    if inst.components.follower.leader ~= nil then
        table.insert(exclude_tags, "abigail")
    end
    if inst.components.minigame_spectator ~= nil then
        table.insert(exclude_tags, "player") -- prevent spectators from auto-targeting webber
    end

    local oneof_tags = { "monster", "wonkey", "pirate" }
    if not inst:HasTag("merm") then
        table.insert(oneof_tags, "merm")
    end

    return not inst:IsInLimbo()
        and FindEntity(
            inst,
            TUNING.PIG_TARGET_DIST,
            function(guy)
                return guy:IsInLight() and inst.components.combat:CanTarget(guy)
            end,
            RETARGET_MUST_TAGS, -- see entityreplica.lua
            exclude_tags,
            oneof_tags
        )
        or nil
end

local function ShouldAcceptItem(inst, item, giver)
    --皇家守卫只有薇尔芭和佩戴了银项链的玩家可以雇佣
    return giver
        and (inst.prefab ~= "pigman_royalguard_2" or giver.prefab == "wilba" or giver:HasTag("ptribe_silvernecklace"))
        and ((item.components.equippable and item.components.equippable.equipslot)
            or inst.components.eater:CanEat(item))
end

local FOLLOW_PIG_MUST_TAGS = { "character", "pig" }
local FOLLOW_PIG_CANT_TAGS = { "werepig", "hostile" }
local function OnGetItemFromPlayer(inst, giver, item)
    --I eat food
    if item.components.edible ~= nil then
        --meat makes us friends (unless I'm a guard)
        if (item.components.edible.foodtype == FOODTYPE.MEAT or
                item.components.edible.foodtype == FOODTYPE.HORRIBLE
            ) and
            item.components.inventoryitem ~= nil and
            ( --make sure it didn't drop due to pockets full
                item.components.inventoryitem:GetGrandOwner() == inst or
                --could be merged into a stack
                (not item:IsValid() and
                    inst.components.inventory:FindItem(function(obj)
                        return obj.prefab == item.prefab
                            and obj.components.stackable ~= nil
                            and obj.components.stackable:IsStack()
                    end) ~= nil)
            ) then
            if inst.components.combat:TargetIs(giver) then
                inst.components.combat:SetTarget(nil)
            elseif giver.components.leader ~= nil and not (giver:HasTag("monster") or giver:HasTag("merm")) then
                if giver.components.minigame_participator == nil then
                    giver:PushEvent("makefriend")
                    giver.components.leader:AddFollower(inst)
                end
                inst.components.follower:AddLoyaltyTime(item.components.edible:GetHunger() *
                    TUNING.PIG_LOYALTY_PER_HUNGER)
                inst.components.follower.maxfollowtime =
                    giver:HasTag("polite")
                    and TUNING.PIG_LOYALTY_MAXTIME + TUNING.PIG_LOYALTY_POLITENESS_MAXTIME_BONUS
                    or TUNING.PIG_LOYALTY_MAXTIME
            end

            -- 薇尔芭
            if giver.prefab == "wilba" then
                local otherpigs = GetPrefab.GetOtherEnts(inst, {
                    radius = 16,
                    maxcount = TUNING.PTRIBE_WILBA_PIG_COUNT,
                    MUST_TAGS = FOLLOW_PIG_MUST_TAGS,
                    CANT_TAGS = FOLLOW_PIG_CANT_TAGS
                })

                for _, pig in ipairs(otherpigs) do
                    if giver.components.minigame_participator == nil then
                        giver:PushEvent("makefriend")
                        giver.components.leader:AddFollower(pig)
                    end
                    pig.components.follower:AddLoyaltyTime(item.components.edible:GetHunger() *
                        TUNING.PIG_LOYALTY_PER_HUNGER)
                    pig.components.follower.maxfollowtime = TUNING.PIG_LOYALTY_MAXTIME +
                        TUNING.PIG_LOYALTY_POLITENESS_MAXTIME_BONUS
                end
            end
        end
        if inst.components.sleeper:IsAsleep() then
            inst.components.sleeper:WakeUp()
        end
    end

    --I wear hats
    if item.components.equippable ~= nil then
        local current = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
        if current ~= nil then
            inst.components.inventory:DropItem(current)
        end
        inst.components.inventory:Equip(item)
        inst.AnimState:Show("hat")
    end
end

local function OnRefuseItem(inst, item)
    inst.sg:GoToState("refuse")
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local function CalcSanityAura(inst, observer)
    return (inst.prefab == "moonpig" and -TUNING.SANITYAURA_LARGE)
        or (inst.components.werebeast ~= nil and inst.components.werebeast:IsInWereState() and -TUNING.SANITYAURA_LARGE)
        or (inst.components.follower ~= nil and inst.components.follower.leader == observer and TUNING.SANITYAURA_SMALL)
        or 0
end

local CAMPFIRE_TAGS = { "campfire", "fire" }
local function NormalShouldSleep(inst)
    return DefaultSleepTest(inst)
        and (inst.components.follower == nil or inst.components.follower.leader == nil
            or (FindEntity(inst, 6, nil, CAMPFIRE_TAGS) ~= nil and inst:IsInLight()))
end

local function GetStatus(inst)
    return inst.components.sleeper:IsAsleep() and "SLEEPING" or "GUARD"
end

local function IsGuardPig(dude)
    return dude:HasTag("guard") and dude:HasTag("pig")
end

local function IsPig(dude)
    return dude:HasTag("pig")
end

local SUGGESTTARGET_MUST_TAGS = { "_combat", "_health", "pig" }
local SUGGESTTARGET_CANT_TAGS = { "werepig", "guard", "INLIMBO" }

local function OnAttackedByDecidRoot(inst, attacker)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, SpringCombatMod(SHARE_TARGET_DIST) * .5, SUGGESTTARGET_MUST_TAGS,
        SUGGESTTARGET_CANT_TAGS)
    local num_helpers = 0
    for i, v in ipairs(ents) do
        if v ~= inst and not v.components.health:IsDead() then
            v:PushEvent("suggest_tree_target", { tree = attacker })
            num_helpers = num_helpers + 1
            if num_helpers >= MAX_TARGET_SHARES then
                break
            end
        end
    end
end

local function OnAttacked(inst, data)
    --print(inst, "OnAttacked")
    local attacker = data.attacker
    inst:ClearBufferedAction()

    if attacker ~= nil then
        if attacker.prefab == "deciduous_root" and attacker.owner ~= nil then
            OnAttackedByDecidRoot(inst, attacker.owner)
        elseif attacker.prefab ~= "deciduous_root" and not attacker:HasTag("pigelite") then
            inst.components.combat:SetTarget(attacker)

            inst.components.combat:ShareTarget(attacker, SHARE_TARGET_DIST,
                attacker:HasTag("pig") and IsGuardPig or IsPig, MAX_TARGET_SHARES)
        end
    end
end

--- 猪人守卫会使用容器中的治疗道具
local function HealthDelta(inst, data)
    if data and data.newpercent < 0.5 then --会一直吃
        local item = GetPrefab.GetContainerFirstItemByType(inst, "heal", true)
        if item then
            if item.components.edible then
                inst:PushBufferedAction(BufferedAction(inst, item, ACTIONS.EAT))
            else
                inst.sg:GoToState("eat") --没吃的也不会报错
                inst:DoTaskInTime(10 * FRAMES, function()
                    if item.components.healer then
                        item.components.healer:Heal(inst)
                    end
                end)
            end
        end
    end
end

local equip = {
    { EQUIPSLOTS.BODY,  "armor" }, --虽然护甲看不见
    -- { EQUIPSLOTS.HANDS, "weapon" }, --降低点条件，能装备就装备，比如提灯和矿工帽
    -- { EQUIPSLOTS.HEAD,  "helmet" },
    { EQUIPSLOTS.HANDS, "hands" },
    { EQUIPSLOTS.HEAD,  "head" },
}
local function OnNewTarget(inst)
    for _, item in ipairs(equip) do
        if not inst.components.inventory:GetEquippedItem(item[1]) then
            local it = GetPrefab.GetContainerFirstItemByType(inst, item[2], true)
            if it then
                inst.components.inventory:Equip(it)
            end
        end
    end
end

--- 被雇佣时可以打开，这里就不判断是不是玩家了
local function StartFollowing(inst)
    inst.components.container.canbeopened = true
end

local function StopFollowing(inst)
    inst.components.container.canbeopened = false
    if not inst.components.follower.task then --玩家退出游戏会导致停止跟随，再次上线装备就会掉一地，通过这个判断是否是雇佣时间结束了
        inst.components.container:DropEverything()
    end
end

local function OnDeath(inst)
    if inst.components.container ~= nil then
        inst.components.container:DropEverything() --虽然官方都是放在sg里的
    end
end

local function onopen(inst)
    inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
end

local function OnEaten(inst, data)
    if inst == data.eater and inst.ptribe_giver and not IsEntityDeadOrGhost(inst.ptribe_giver) then
        OnGetItemFromPlayer(data.eater, inst.ptribe_giver, inst)
        inst.ptribe_giver = nil
    end
end

local function onclose(inst)
    inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")

    OnNewTarget(inst)

    local x, y, z = inst.Transform:GetWorldPosition()
    local player = FindClosestPlayerInRangeSq(x, y, z, 64, true)
    local isHealth = inst.components.health:GetPercent() < 1
    for _, food in ipairs(GetPrefab.GetAllContainerItem(inst, isHealth and "canEat" or "canEatNotHealth", true)) do
        inst.components.inventory:GiveItem(food)
        food.ptribe_giver = player --不用保存
        food:ListenForEvent("oneaten", OnEaten)
    end
end

local function Init(inst)
    inst.components.container.canbeopened = inst.components.follower.leader ~= nil
end

local function OnEntitySleep(inst)
    GetPrefab.EntitySleepGoHome(inst)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 50, .5)

    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()

    inst.AnimState:SetBank("townspig")
    inst.AnimState:SetBuild("pig_royalguard")
    inst.AnimState:PlayAnimation("idle_loop", true)
    inst.AnimState:Hide("hat")
    inst.AnimState:Hide("desk")
    inst.AnimState:Hide("ARM_carry")

    inst:AddTag("character")
    inst:AddTag("pig")
    inst:AddTag("scarytoprey")
    inst:AddTag("_named")
    inst:AddTag("trader")
    inst:AddTag("guard")

    inst:AddComponent("talker")
    inst.components.talker.fontsize = 35
    inst.components.talker.font = TALKINGFONT
    --inst.components.talker.colour = Vector3(133/255, 140/255, 167/255)
    inst.components.talker.offset = Vector3(0, -400, 0)
    inst.components.talker:MakeChatter()

    -- 玩家组件，防止mod装备给予猪人守卫时崩溃，但是伪造就需要伪造所有函数，不然insight这类的会报错
    -- Utils.FakeComponent(inst, "skilltreeupdater", TheWorld.ismastersim)
    -- Utils.FakeComponent(inst, "sanity", TheWorld.ismastersim)
    -- Utils.FakeComponent(inst, "hunger", TheWorld.ismastersim)
    -- Utils.FakeComponent(inst, "timer", TheWorld.ismastersim)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:RemoveTag("_named")

    inst.talkertype = "pigman_royalguard"

    inst.components.talker.ontalk = ontalk

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = TUNING.PIG_RUN_SPEED
    inst.components.locomotor.walkspeed = TUNING.PIG_WALK_SPEED
    inst.components.locomotor:SetAllowPlatformHopping(true)
    inst:AddComponent("embarker")
    inst:AddComponent("drownable")

    inst:AddComponent("bloomer")

    ------------------------------------------
    inst:AddComponent("eater")
    inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetCanEatRaw()
    inst.components.eater:SetStrongStomach(true) -- can eat monster meat!
    inst.components.eater:SetOnEatFn(OnEat)
    ------------------------------------------
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.PIG_HEALTH)
    inst.components.health:StartRegen(2, 10)

    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "pig_torso"
    inst.components.combat:SetDefaultDamage(TUNING.PIG_DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.PIG_ATTACK_PERIOD)
    inst.components.combat:SetNoAggroTags(nil)
    inst.components.combat:SetKeepTargetFunction(NormalKeepTargetFn)
    inst.components.combat:SetRetargetFunction(3, NormalRetargetFn)

    MakeMediumBurnableCharacter(inst, "pig_torso")

    inst:AddComponent("named")
    inst.components.named.possiblenames = JoinArrays(STRINGS.CITYPIGNAMES.UNISEX, STRINGS.CITYPIGNAMES.MALE)
    inst.components.named:PickNewName()

    ------------------------------------------
    -- MakeHauntablePanic(inst)

    ------------------------------------------
    inst:AddComponent("follower")
    inst.components.follower.maxfollowtime = TUNING.PIG_LOYALTY_MAXTIME
    ------------------------------------------

    inst:AddComponent("inventory")

    inst:AddComponent("container")
    -- inst.components.container:WidgetSetup("sacred_chest")
    inst.components.container:WidgetSetup("wobysmall") --沃比这个UI有点好看
    inst.components.container.onopenfn = onopen
    inst.components.container.onclosefn = onclose
    inst.components.container.skipclosesnd = true
    inst.components.container.skipopensnd = true
    ------------------------------------------

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetLoot({})
    inst.components.lootdropper:AddRandomLoot("meat", 3)
    inst.components.lootdropper:AddRandomLoot("pigskin", 1)
    inst.components.lootdropper.numrandomloot = 1
    ------------------------------------------

    inst:AddComponent("knownlocations")

    ------------------------------------------
    -- 其实添加container后，trader的动作就被覆盖掉了，为了稳妥这里不变
    inst:AddComponent("trader")
    inst.components.trader:SetAcceptTest(ShouldAcceptItem)
    inst.components.trader.onaccept = OnGetItemFromPlayer
    inst.components.trader.onrefuse = OnRefuseItem
    inst.components.trader.deleteitemonaccept = false

    ------------------------------------------

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = CalcSanityAura

    ------------------------------------------

    inst:AddComponent("sleeper")
    inst.components.sleeper.watchlight = true
    inst.components.sleeper:SetResistance(2)
    inst.components.sleeper:SetSleepTest(NormalShouldSleep)
    inst.components.sleeper:SetWakeTest(DefaultWakeTest)
    ------------------------------------------
    MakeMediumFreezableCharacter(inst, "pig_torso")

    ------------------------------------------

    inst:AddComponent("inspectable")
    inst.components.inspectable.getstatus = GetStatus
    ------------------------------------------

    inst:SetBrain(brain)
    inst:SetStateGraph("SGpig")

    inst:DoTaskInTime(0, Init)

    inst:ListenForEvent("healthdelta", HealthDelta)
    inst:ListenForEvent("unequip", function() inst:DoTaskInTime(0, OnNewTarget) end) --有可能是代码主动卸下装备换新装备，这里要延迟一下
    inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("newcombattarget", OnNewTarget)
    inst:ListenForEvent("startfollowing", StartFollowing)
    inst:ListenForEvent("stopfollowing", StopFollowing)
    inst:ListenForEvent("death", OnDeath)

    inst.OnEntitySleep = OnEntitySleep

    return inst
end

local function fn2()
    local inst = fn()

    inst.AnimState:SetBuild("pig_royalguard_2")

    if not TheWorld.ismastersim then
        return inst
    end
    inst.talkertype = "pigman_royalguard_2"

    return inst
end

return Prefab("common/objects/pigman_royalguard", fn, assets, prefabs),
    Prefab("common/objects/pigman_royalguard_2", fn2, assets, prefabs)
