local assets =
{
    Asset("ANIM", "anim/tree_jungle_build.zip"),
    Asset("ANIM", "anim/tree_jungle_normal.zip"),
    Asset("ANIM", "anim/tree_jungle_short.zip"),
    Asset("ANIM", "anim/tree_jungle_tall.zip"),
    -- Asset("ANIM", "anim/dust_fx.zip"),
    Asset("SOUND", "sound/forest.fsb"),
}

local prefabs =
{
    "log",
    "ptribe_jungletreeseed", --
    "charcoal",
    -- "treeguard",      --树精守卫
    "ptribe_snake",
    "ptribe_snake_poison",
    "cave_banana",
    "bird_egg",
}

local builds =
{
    normal = {
        file = "tree_jungle_build",
        prefab_name = "ptribe_jungletree",
        normal_loot = { "log", "log", "ptribe_jungletreeseed" },
        short_loot = { "log" },
        tall_loot = { "log", "log", "log", "ptribe_jungletreeseed", "ptribe_jungletreeseed" },
    }
}

local function makeanims(stage)
    return {
        idle = "idle_" .. stage,
        sway1 = "sway1_loop_" .. stage,
        sway2 = "sway2_loop_" .. stage,
        chop = "chop_" .. stage,
        fallleft = "fallleft_" .. stage,
        fallright = "fallright_" .. stage,
        stump = "stump_" .. stage,
        burning = "burning_loop_" .. stage,
        burnt = "burnt_" .. stage,
        chop_burnt = "chop_burnt_" .. stage,
        idle_chop_burnt = "idle_chop_burnt_" .. stage,
        blown1 = "blown_loop_" .. stage .. "1",
        blown2 = "blown_loop_" .. stage .. "2",
        blown_pre = "blown_pre_" .. stage,
        blown_pst = "blown_pst_" .. stage
    }
end

local short_anims = makeanims("short")
local tall_anims = makeanims("tall")
local normal_anims = makeanims("normal")

local function GetBuild(inst)
    return builds[inst.build] or builds["normal"]
end

local function chop_down_burnt_tree(inst)
    inst:RemoveComponent("workable")

    inst.SoundEmitter:PlaySound("dontstarve/forest/treeCrumble")
    inst.SoundEmitter:PlaySound("dontstarve/wilson/use_axe_tree")
    inst.AnimState:PlayAnimation(inst.anims.chop_burnt)

    RemovePhysicsColliders(inst)

    inst.persists = false

    inst:ListenForEvent("animover", inst.Remove)
    inst:ListenForEvent("entitysleep", inst.Remove)
    inst.components.lootdropper:SpawnLootPrefab("charcoal")
    inst.components.lootdropper:DropLoot()

    if inst.pineconetask then
        inst.pineconetask:Cancel()
        inst.pineconetask = nil
    end
end

local burnt_highlight_override = { .5, .5, .5 }
local function changes(inst)
    if inst.components.burnable then
        inst.components.burnable:Extinguish()
    end
    inst:RemoveComponent("burnable")
    inst:RemoveComponent("propagator")
    inst:RemoveComponent("growable")
    inst:RemoveTag("shelter")
    inst:RemoveTag("dragonflybait_lowprio")
    inst:RemoveTag("fire")
    inst:RemoveTag("gustable")

    inst.components.lootdropper:SetLoot({})

    if inst.components.workable then
        inst.components.workable:SetWorkLeft(1)
        inst.components.workable:SetOnWorkCallback(nil)
        inst.components.workable:SetOnFinishCallback(chop_down_burnt_tree)
    end
end

local function OnBurnt(inst, imm)
    if imm then
        changes(inst)
    else
        inst:DoTaskInTime(0.5, changes)
    end
    inst.AnimState:PlayAnimation(inst.anims.burnt, true)
    inst.MiniMapEntity:SetIcon("jungleTree_burnt.png")
    --inst.AnimState:SetRayTestOnBB(true);
    inst:AddTag("burnt")

    inst.highlight_override = burnt_highlight_override
end

local function tree_burnt(inst)
    OnBurnt(inst)
    inst.pineconetask = inst:DoTaskInTime(10,
        function()
            local pt = inst:GetPosition()
            if math.random(0, 1) == 1 then
                pt = pt + TheCamera:GetRightVec()
            else
                pt = pt - TheCamera:GetRightVec()
            end
            inst.components.lootdropper:DropLoot(pt)
            inst.pineconetask = nil
        end)
end

local function inspect_tree(inst)
    if inst:HasTag("burnt") then
        return "BURNT"
    elseif inst:HasTag("stump") then
        return "CHOPPED"
    end
end

local function chop_tree(inst, chopper)
    if chopper and chopper.components.beaverness and chopper.components.beaverness:IsBeaver() then
        inst.SoundEmitter:PlaySound("dontstarve/characters/woodie/beaver_chop_tree")
    else
        inst.SoundEmitter:PlaySound("dontstarve/wilson/use_axe_tree")
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    SpawnPrefab("green_leaves_chop").Transform:SetPosition(x, y + 2 + math.random() * 2, z)

    inst.AnimState:PlayAnimation(inst.anims.chop)
    inst.AnimState:PushAnimation(inst.anims.sway1, true)
end

local function dig_up_stump(inst)
    inst.components.lootdropper:SpawnLootPrefab("log")
    inst:Remove()
end

local function chop_down_tree(inst, chopper)
    inst:RemoveComponent("burnable")
    MakeSmallBurnable(inst)
    inst:RemoveComponent("propagator")
    MakeSmallPropagator(inst)
    inst:RemoveComponent("workable")
    inst:RemoveTag("shelter")
    inst:RemoveTag("gustable")
    inst.SoundEmitter:PlaySound("dontstarve/forest/treefall")
    local pt = inst:GetPosition()
    local hispos = chopper:GetPosition()

    local he_right = (hispos - pt):Dot(TheCamera:GetRightVec()) > 0

    if he_right then
        inst.AnimState:PlayAnimation(inst.anims.fallleft)
        inst.components.lootdropper:DropLoot(pt - TheCamera:GetRightVec())
    else
        inst.AnimState:PlayAnimation(inst.anims.fallright)
        inst.components.lootdropper:DropLoot(pt + TheCamera:GetRightVec())
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    SpawnPrefab("green_leaves_chop").Transform:SetPosition(x, y + 2 + math.random() * 2, z)

    -- make snakes attack
    for _, v in pairs(TheSim:FindEntities(x, y, z, 2, { "snake" })) do
        if v.components.combat then
            v.components.combat:SetTarget(chopper)
        end
    end

    if chopper:HasTag("player") and chopper.ShakeCamera then
        inst:DoTaskInTime(.4, function()
            local sz = (inst.components.growable and inst.components.growable.stage > 2) and .5 or .25
            chopper:ShakeCamera(CAMERASHAKE.FULL, 0.25, 0.03, sz, 6)
        end)
    end

    RemovePhysicsColliders(inst)
    inst.AnimState:PushAnimation(inst.anims.stump)
    inst.MiniMapEntity:SetIcon("jungleTree_stump.png")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetOnFinishCallback(dig_up_stump)
    inst.components.workable:SetWorkLeft(1)

    inst:AddTag("stump")
    if inst.components.growable then
        inst.components.growable:StopGrowing()
    end

    inst:AddTag("NOCLICK")
    inst:DoTaskInTime(2, function() inst:RemoveTag("NOCLICK") end)
end

local function PushSway(inst)
    if math.random() > .5 then
        inst.AnimState:PushAnimation(inst.anims.sway1, true)
    else
        inst.AnimState:PushAnimation(inst.anims.sway2, true)
    end
end

local function Sway(inst)
    if math.random() > .5 then
        inst.AnimState:PlayAnimation(inst.anims.sway1, true)
    else
        inst.AnimState:PlayAnimation(inst.anims.sway2, true)
    end
    inst.AnimState:SetTime(math.random() * 2)
end

local function SetShort(inst)
    inst.anims = short_anims

    if inst.components.workable then
        inst.components.workable:SetWorkLeft(TUNING.JUNGLETREE_CHOPS_SMALL)
    end

    inst.components.lootdropper:SetLoot(GetBuild(inst).short_loot)

    if math.random() < 0.5 then
        for i = 1, TUNING.SNAKE_JUNGLETREE_AMOUNT_SMALL do
            if math.random() < 0.5 and TheWorld.state.cycles >= TUNING.SNAKE_POISON_START_DAY then
                inst.components.lootdropper:AddChanceLoot("ptribe_snake_poison", TUNING.SNAKE_JUNGLETREE_POISON_CHANCE)
            else
                inst.components.lootdropper:AddChanceLoot("ptribe_snake", TUNING.SNAKE_JUNGLETREE_CHANCE)
            end
        end
    end

    Sway(inst)
end

local function GrowShort(inst)
    inst.AnimState:PlayAnimation("grow_tall_to_short")
    inst.SoundEmitter:PlaySound("dontstarve/forest/treeGrowFromWilt")
    PushSway(inst)
end

local function SetNormal(inst)
    inst.anims = normal_anims

    if inst.components.workable then
        inst.components.workable:SetWorkLeft(TUNING.JUNGLETREE_CHOPS_NORMAL)
    end

    inst.components.lootdropper:SetLoot(GetBuild(inst).normal_loot)

    if math.random() < 0.5 then
        for i = 1, TUNING.SNAKE_JUNGLETREE_AMOUNT_MED do
            if math.random() < 0.5 and TheWorld.state.cycles >= TUNING.SNAKE_POISON_START_DAY then
                inst.components.lootdropper:AddChanceLoot("ptribe_snake_poison", TUNING.SNAKE_JUNGLETREE_POISON_CHANCE)
            else
                inst.components.lootdropper:AddChanceLoot("ptribe_snake", TUNING.SNAKE_JUNGLETREE_CHANCE)
            end
        end
    else
        inst.components.lootdropper:AddChanceLoot("bird_egg", 1.0)
    end

    Sway(inst)
end

local function GrowNormal(inst)
    inst.AnimState:PlayAnimation("grow_short_to_normal")
    inst.SoundEmitter:PlaySound("dontstarve/forest/treeGrow")
    PushSway(inst)
end

local function SetTall(inst)
    inst.anims = tall_anims
    if inst.components.workable then
        inst.components.workable:SetWorkLeft(TUNING.JUNGLETREE_CHOPS_TALL)
    end
    inst.components.lootdropper:SetLoot(GetBuild(inst).tall_loot)

    if math.random() < 0.5 then
        for i = 1, TUNING.SNAKE_JUNGLETREE_AMOUNT_TALL do
            if math.random() < 0.5 and TheWorld.state.cycles >= TUNING.SNAKE_POISON_START_DAY then
                inst.components.lootdropper:AddChanceLoot("ptribe_snake_poison", TUNING.SNAKE_JUNGLETREE_POISON_CHANCE)
            else
                inst.components.lootdropper:AddChanceLoot("ptribe_snake", TUNING.SNAKE_JUNGLETREE_CHANCE)
            end
        end
    else
        if math.random() < 0.5 then
            inst.components.lootdropper:AddChanceLoot("bird_egg", 1.0)
        else
            inst.components.lootdropper:AddChanceLoot("cave_banana", 1.0)
        end
    end

    Sway(inst)
end

local function GrowTall(inst)
    inst.AnimState:PlayAnimation("grow_normal_to_tall")
    inst.SoundEmitter:PlaySound("dontstarve/forest/treeGrow")
    PushSway(inst)
end

local growth_stages = { {
    name = "short",
    time = function()
        return GetRandomWithVariance(TUNING.JUNGLETREE_GROW_TIME[1].base, TUNING.JUNGLETREE_GROW_TIME[1].random)
    end,
    fn = SetShort,
    growfn = GrowShort,
    leifscale = .7
}, {
    name = "normal",
    time = function()
        return GetRandomWithVariance(TUNING.JUNGLETREE_GROW_TIME[2].base, TUNING.JUNGLETREE_GROW_TIME[2].random)
    end,
    fn = SetNormal,
    growfn = GrowNormal,
    leifscale = 1
}, {
    name = "tall",
    time = function()
        return GetRandomWithVariance(TUNING.JUNGLETREE_GROW_TIME[3].base, TUNING.JUNGLETREE_GROW_TIME[3].random)
    end,
    fn = SetTall,
    growfn = GrowTall,
    leifscale = 1.25
} }


local function handler_growfromseed(inst)
    inst.components.growable:SetStage(1)
    inst.AnimState:PlayAnimation("grow_seed_to_short")
    inst.SoundEmitter:PlaySound("dontstarve/forest/treeGrow")
    PushSway(inst)
end

local function OnEntitySleep(inst)
    local fire = inst:HasTag("fire")

    inst:RemoveComponent("burnable")
    inst:RemoveComponent("propagator")
    inst:RemoveComponent("inspectable")
    if fire then
        inst:AddTag("fire")
    end
end

local function OnEntityWake(inst)
    if not inst:HasTag("burnt") and not inst:HasTag("fire") then
        if not inst.components.burnable then
            if inst:HasTag("stump") then
                MakeSmallBurnable(inst)
            else
                MakeLargeBurnable(inst)
                inst.components.burnable:SetFXLevel(5)
                inst.components.burnable:SetOnBurntFn(tree_burnt)
            end
        end

        if not inst.components.propagator then
            if inst:HasTag("stump") then
                MakeSmallPropagator(inst)
            else
                MakeLargePropagator(inst)
            end
        end
    elseif not inst:HasTag("burnt") and inst:HasTag("fire") then
        OnBurnt(inst, true)
    end

    if not inst.components.inspectable then
        inst:AddComponent("inspectable")
        inst.components.inspectable.getstatus = inspect_tree
    end
end

local function onsave(inst, data)
    data.burnt = inst:HasTag("burnt") or inst:HasTag("fire") or nil
    data.stump = inst:HasTag("stump") or nil
    data.build = inst.build ~= "normal" and inst.build or nil
end

local function onload(inst, data)
    if not data then return end

    if not data.build or builds[data.build] == nil then
        inst.build = "normal"
    else
        inst.build = data.build
    end

    if data.burnt then
        inst:AddTag("fire") -- Add the fire tag here: OnEntityWake will handle it actually doing burnt logic
        inst.MiniMapEntity:SetIcon("jungleTree_burnt.png")
    elseif data.stump then
        inst:RemoveComponent("burnable")
        MakeSmallBurnable(inst)
        inst:RemoveComponent("workable")
        inst:RemoveComponent("propagator")
        MakeSmallPropagator(inst)
        inst:RemoveComponent("growable")
        RemovePhysicsColliders(inst)
        inst.AnimState:PlayAnimation(inst.anims.stump)
        inst.MiniMapEntity:SetIcon("jungleTree_stump.png")
        inst:AddTag("stump")
        inst:RemoveTag("shelter")
        inst:RemoveTag("gustable")
        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.DIG)
        inst.components.workable:SetOnFinishCallback(dig_up_stump)
        inst.components.workable:SetWorkLeft(1)
    end
end

local function makefn(build, stage, data)
    if stage == 0 then
        stage = math.random(1, 3)
    end

    return function()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()
        
        MakeObstaclePhysics(inst, .25)

        local minimap = inst.entity:AddMiniMapEntity()
        minimap:SetIcon("jungleTree.png")
        minimap:SetPriority(-1)

        inst:AddTag("tree")
        inst:AddTag("workable")
        inst:AddTag("shelter")
        inst:AddTag("gustable")

        inst.build = build
        inst.AnimState:SetBuild(GetBuild(inst).file)
        inst.AnimState:SetBank("jungletree")
        local color = 0.5 + math.random() * 0.5
        inst.AnimState:SetMultColour(color, color, color, 1)
        inst.AnimState:SetTime(math.random() * 2)

        inst:SetPrefabName(GetBuild(inst).prefab_name)

        if data == "stump" then
            inst:RemoveTag("gustable")
            RemovePhysicsColliders(inst)
            inst.AnimState:PlayAnimation(inst.anims.stump)
            inst.MiniMapEntity:SetIcon("jungleTree_stump.png")
            inst:AddTag("stump")
        end

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        -------------------
        MakeLargeBurnable(inst)
        inst.components.burnable:SetFXLevel(5)
        inst.components.burnable:SetOnBurntFn(tree_burnt)

        MakeLargePropagator(inst)

        -------------------
        inst:AddComponent("inspectable")
        inst.components.inspectable.getstatus = inspect_tree
        -------------------
        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.CHOP)
        inst.components.workable:SetOnWorkCallback(chop_tree)
        inst.components.workable:SetOnFinishCallback(chop_down_tree)

        -------------------
        inst:AddComponent("lootdropper")
        ---------------------
        inst:AddComponent("growable")
        inst.components.growable.stages = growth_stages
        inst.components.growable:SetStage(stage)
        inst.components.growable.loopstages = true
        inst.components.growable.springgrowth = true
        inst.components.growable:StartGrowing()

        inst.growfromseed = handler_growfromseed

        ---------------------

        MakeSnowCovered(inst, .01)
        ---------------------

        if data == "burnt" then
            OnBurnt(inst)
        end

        if data == "stump" then
            inst:RemoveComponent("burnable")
            MakeSmallBurnable(inst)
            inst:RemoveComponent("workable")
            inst:RemoveComponent("propagator")
            MakeSmallPropagator(inst)
            inst:RemoveComponent("growable")
            inst:AddComponent("workable")
            inst.components.workable:SetWorkAction(ACTIONS.DIG)
            inst.components.workable:SetOnFinishCallback(dig_up_stump)
            inst.components.workable:SetWorkLeft(1)
        end

        inst.OnEntitySleep = OnEntitySleep
        inst.OnEntityWake = OnEntityWake
        inst.OnSave = onsave
        inst.OnLoad = onload

        return inst
    end
end

local function tree(name, build, stage, data)
    return Prefab(name, makefn(build, stage, data), assets, prefabs)
end

return tree("ptribe_jungletree", "normal", 0),
    tree("ptribe_jungletree_normal", "normal", 2),
    tree("ptribe_jungletree_tall", "normal", 3),
    tree("ptribe_jungletree_short", "normal", 1),
    tree("ptribe_jungletree_burnt", "normal", 0, "burnt"),
    tree("ptribe_jungletree_stump", "normal", 0, "stump")
