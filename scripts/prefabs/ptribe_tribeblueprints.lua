local BP = require("ptribe_blueprints")
local Utils = require("ptribe_utils/utils")

--猪人房
local function TargetCheck(inst, doer, target)
    return target and target.replica.ptribe_pigmanager and not target:HasTag("burnt")
end

local function OnUse(inst, doer, target)
    if target.components.ptribe_pigmanager:IsManager() then
        return false, "ALREADY_MANAGER"
    end

    target.components.ptribe_pigmanager:SetManager(inst.blueprintName)
    inst:Remove()

    return true
end

local function fn(name)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("blueprint_rare")
    inst.AnimState:SetBuild("blueprint_rare")
    inst.AnimState:PlayAnimation("idle")

    inst:AddComponent("ptribe_consumable")
    inst.components.ptribe_consumable.targetCheckFn = TargetCheck
    inst.components.ptribe_consumable.state = "give"
    inst.components.ptribe_consumable.str = "GIVE"

    inst:AddTag("_named")

    -- inst:SetPrefabName("blueprint")

    MakeInventoryFloatable(inst, "med", nil, 0.75)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.blueprintName = name

    inst:RemoveTag("_named")

    inst.components.ptribe_consumable.onUseFn = OnUse

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem:ChangeImageName("blueprint_rare")

    inst:AddComponent("erasablepaper") --可擦除

    inst:AddComponent("named")
    inst.components.named:SetName(subfmt(STRINGS.NAMES.BLUEPRINT_RARE, { item = name }))

    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = TUNING.SMALL_FUEL

    MakeHauntableLaunch(inst)

    return inst
end

local BLUEPRINTS = {}
for p, name in pairs(BP.BLUEPRINT_LIST) do
    STRINGS.NAMES[string.upper(p)] = name
    table.insert(BLUEPRINTS, Prefab(p, Utils.FnParameterExtend(fn, name)))
end
return unpack(BLUEPRINTS)
