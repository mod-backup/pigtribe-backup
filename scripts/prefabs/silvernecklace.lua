local assets =
{
    Asset("ANIM", "anim/silvernecklace.zip"),
    Asset("ANIM", "anim/torso_silvernecklace.zip"),
}

local function onequip(inst, owner)
    owner:AddTag("ptribe_silvernecklace")
    owner.AnimState:OverrideSymbol("swap_body", "torso_silvernecklace", "silvernecklace")
    inst.components.fueled:StartConsuming()
end

local function onunequip(inst, owner)
    owner:RemoveTag("ptribe_silvernecklace")
    owner.AnimState:ClearOverrideSymbol("swap_body")
    inst.components.fueled:StopConsuming()
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    MakeInventoryFloatable(inst, "small", 0.1)

    inst.entity:AddMiniMapEntity()
    inst.MiniMapEntity:SetIcon("silvernecklace.png")

    inst.AnimState:SetBank("silvernecklace")
    inst.AnimState:SetBuild("silvernecklace")
    inst.AnimState:PlayAnimation("silvernecklace")

    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    -- inst:AddTag("irreplaceable")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.NECK or EQUIPSLOTS.BODY --兼容五格
    inst.components.equippable.dapperness = TUNING.DAPPERNESS_SMALL
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/hamletinventoryimages.xml"
    inst.components.inventoryitem.foleysound = "dontstarve/movement/foley/jewlery"
    
    inst:AddComponent("fueled")
    inst.components.fueled.fueltype = FUELTYPE.USAGE
    inst.components.fueled:InitializeFuelLevel(TUNING.FEATHERHAT_PERISHTIME)
    inst.components.fueled:SetDepletedFn(inst.Remove)

 
    return inst
end

return Prefab("common/inventory/silvernecklace", fn, assets)
