require "prefabutil"

local prefabs = { "collapse_small" }

local function quantizeposition(pt)
	local retval = Vector3(math.floor(pt.x) + .5, 0, math.floor(pt.z) + .5)
	return retval
end

local function onhacked(inst, worker)
	if inst:HasTag("fire") and inst.components.burnable then
		inst.components.burnable:Extinguish()
	end

	inst.components.lootdropper:SpawnLootPrefab("clippings")
	inst.components.lootdropper:SpawnLootPrefab("clippings")

	local x, y, z = inst.Transform:GetWorldPosition()
	for i = 1, math.random(5, 10) do
		local fx = SpawnPrefab("yellow_leaves_chop")
		fx.Transform:SetPosition(x + (math.random() * 2) - 1, y + math.random() * 0.5, z + (math.random() * 2) - 1)
		if math.random() < 0.5 then
			fx.Transform:SetScale(-1, 1, -1)
		end
	end

	inst:Remove()
end

local function onhit(inst)
	local fx = SpawnPrefab("yellow_leaves_chop")
	local x, y, z = inst.Transform:GetWorldPosition()
	fx.Transform:SetPosition(x, y + math.random() * 0.5, z)

	inst.SoundEmitter:PlaySound("dontstarve/common/destroy_straw")
end

function MakeHedgeType(data)
	local assets = {
		Asset("ANIM", "anim/hedge.zip"),
		Asset("ANIM", "anim/hedge" .. data.hedgetype .. "_build.zip"),
	}

	local function ondeploywall(inst, pt)
		local wall = SpawnPrefab(data.name)
		if wall then
			pt = quantizeposition(pt)
			wall.Physics:SetCollides(false)
			wall.Physics:Teleport(pt.x, pt.y, pt.z)
			wall.Physics:SetCollides(true)
			inst.components.stackable:Get():Remove()

			-- TheWorld.Pathfinder:AddWall(pt.x, pt.y, pt.z) --我没加，先看看有没有什么问题
		end
	end

	local function itemfn()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()

		MakeInventoryPhysics(inst)

		inst.AnimState:SetBank("hedge")
		inst.AnimState:SetBuild("hedge" .. data.hedgetype .. "_build")
		inst.AnimState:PlayAnimation("idle")

		MakeInventoryFloatable(inst, "small", 0.1)

		inst:AddTag("wallbuilder")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

		inst:AddComponent("stackable")
		inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM

		inst:AddComponent("inspectable")

		inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.atlasname = "images/inventoryimages/hamletinventoryimages.xml"

		if data.flammable then
			MakeSmallBurnable(inst, TUNING.MED_BURNTIME)
			MakeSmallPropagator(inst)

			inst:AddComponent("fuel")
			inst.components.fuel.fuelvalue = TUNING.SMALL_FUEL
		end

		inst:AddComponent("deployable")
		inst.components.deployable:SetDeployMode(DEPLOYMODE.WALL)
		inst.components.deployable.ondeploy = ondeploywall
		-- inst.components.deployable.test = test_wall
		inst.components.deployable.min_spacing = 0

		return inst
	end

	local function fn()
		local inst = CreateEntity()
		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()

		inst.Transform:SetEightFaced()

		MakeObstaclePhysics(inst, .5)

		inst.Physics:SetDontRemoveOnSleep(true)
		inst.AnimState:SetBank("hedge")
		inst.AnimState:SetBuild("hedge" .. data.hedgetype .. "_build")
		inst.AnimState:PlayAnimation("growth1", false)

		inst.SoundEmitter:PlaySound("dontstarve/common/place_structure_straw")

		inst:AddTag("wall")
		inst:AddTag("structure")
		inst:AddTag("grass")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

		inst:AddComponent("inspectable")

		inst:AddComponent("lootdropper")

		inst:AddComponent("workable")
		inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
		inst.components.workable:SetWorkLeft(3)
		inst.components.workable:SetOnFinishCallback(onhacked)
		inst.components.workable:SetOnWorkCallback(onhit)

		inst:SetPrefabNameOverride("hedge")

		MakeSnowCovered(inst)
		MakeMediumBurnable(inst, nil, nil, true)
		MakeMediumPropagator(inst)

		inst:ListenForEvent("burntup", inst.Remove)

		return inst
	end

	local placer = "common/" .. data.name .. "_item_placer"
	return Prefab("common/" .. data.name, fn, assets, prefabs),
		Prefab("common/" .. data.name .. "_item", itemfn, assets, { data.name, placer, "collapse_small" }),
		MakePlacer(placer, "hedge", "hedge" .. data.hedgetype .. "_build", "growth1",
			false, false, true, nil, nil, "eight")
end

-- hedge_cone_item_placer
local hedgeprefabs = {}
local hedgedata = {
	{ name = "hedge_block",   hedgetype = 1 },
	{ name = "hedge_cone",    hedgetype = 2, flammable = true },
	{ name = "hedge_layered", hedgetype = 3, flammable = true },
}

-- 应该把单机的剪换成砍，支持可以砍树篱，每次砍伐都掉一些草

for _, v in pairs(hedgedata) do
	local hedge, item, placer = MakeHedgeType(v)
	table.insert(hedgeprefabs, hedge)
	table.insert(hedgeprefabs, item)
	table.insert(hedgeprefabs, placer)
end

return unpack(hedgeprefabs)
