local assets = {
    Asset("ANIM", "anim/acorn.zip"),
    Asset("ANIM", "anim/pig_shop_doormats.zip"),  --出口地毯
    Asset("ANIM", "anim/player_house_doors.zip"), --小屋房间门
}

local function OnActivate(inst, doer)
    if doer:HasTag("player") then
        if doer.components.talker ~= nil then
            doer.components.talker:ShutUp()
        end

        --Sounds are triggered in player's stategraph
    elseif inst.SoundEmitter ~= nil then
        inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
    end
end

local function StartTravelSound(inst, doer)
    inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
end

local function OnHaunt(inst, haunter)
    inst.components.teleporter:Activate(haunter)
end

local function OnSave(inst, data)
    data.side = inst.side
    data.initData = inst.initData
    data.hamlet_houseexit = inst:HasTag("hamlet_houseexit") or nil
end

local function OnLoad(inst, data)
    if data == nil then return end

    inst.side = data.side
    if data.initData then
        TheWorld.components.interiorspawner:InitHouseInteriorPrefab(inst, data.initData)
        inst.initData = data.initData
    end
    if data.hamlet_houseexit then
        inst:AddTag("hamlet_houseexit") --没有单机版的小地图，目前通过鼠标悬停时的“进入”和“离开”来判断出口
    end
end

local function onaccept(inst, giver, item)
    inst.components.teleporter:Activate(item)
end

local function common(bank, build, anim)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank(bank)
    inst.AnimState:SetBuild(build)
    if anim then
        inst.AnimState:PlayAnimation(anim)
    end
    inst.AnimState:SetSortOrder(0)
    -- inst.AnimState:SetLayer(LAYER_WORLD_BACKGROUND)

    inst:AddTag("NOBLOCK")
    inst:AddTag("trader")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.side = nil --门所在墙边

    inst:AddComponent("inspectable")

    inst:AddComponent("trader")
    inst.components.trader.acceptnontradable = true
    inst.components.trader.onaccept = onaccept
    inst.components.trader.deleteitemonaccept = false

    inst:AddComponent("teleporter")
    inst.components.teleporter.onActivate = OnActivate
    inst.components.teleporter.offset = 0
    inst.components.teleporter.travelcameratime = 0
    inst.components.teleporter.travelarrivetime = 0
    inst:ListenForEvent("starttravelsound", StartTravelSound) -- triggered by player stategraph

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetOnHauntFn(OnHaunt)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

local function playerHouseExitDoorFn()
    local inst = common("pig_shop_doormats", "pig_shop_doormats", "idle_old")
    inst:AddTag("hamlet_houseexit")
    return inst
end

local function OnBuilt(inst)
    inst.side = TheWorld.components.interiorspawner:TestWallOrnamentPos(inst, false, 7.5, 5, 7.5, 5.5)
    local anim
    if inst.side == 1 then
        anim = "_close_east"
    elseif inst.side == 2 then
        anim = "_close_north"
    elseif inst.side == 3 then
        anim = "_close_west"
    elseif inst.side == 4 then
        anim = "_close_south"
    end
    inst.AnimState:PlayAnimation(inst.prefab .. anim)
    inst.initData = { animdata = { anim = inst.prefab .. anim } }
end

local function onhammered(inst, worker)
    local pos = inst:GetPosition()
    SpawnPrefab("collapse_big").Transform:SetPosition(pos:Get())
    TheWorld.components.interiorspawner:OnHouseDestroy(inst, worker)
    inst.components.hamletdoor:SetCenterPos(nil)
    inst:Remove()
end

local function MakeHouseDoor(name)
    local function fn()
        local inst = common("player_house_doors", "player_house_doors")

        inst:AddTag("hamlet_housedoor")
        inst:AddTag("predoor") --可建造房屋

        if not TheWorld.ismastersim then
            return inst
        end

        inst.components.trader:Disable() --初始不可用，需要初始化

        inst:AddComponent("lootdropper")

        inst:AddComponent("hamletdoor")

        -- 出口不能敲毁，入口可敲，出口不能敲
        inst:DoTaskInTime(0, function()
            if not inst:HasTag("hamlet_houseexit") then
                inst:AddComponent("workable")
                inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
                inst.components.workable:SetWorkLeft(4)
                inst.components.workable:SetOnFinishCallback(onhammered)
            end
        end)

        inst:ListenForEvent("onbuilt", OnBuilt)

        return inst
    end

    return Prefab(name, fn, assets)
end

----------------------------------------------------------------------------------------------------

local function PlacerOnUpdateTransform(inst)
    local side, minDis = TheWorld.components.interiorspawner:TestWallOrnamentPos(inst, true, 7.5, 5, 7.5, 5.5)
    local anim
    if side and minDis < 4 then
        if side == 1 then
            anim = "_close_east"
        elseif side == 2 then
            anim = "_close_north"
        elseif side == 3 then
            anim = "_close_west"
        elseif side == 4 then
            anim = "_close_south"
        end
        inst.accept_placement = true
    else
        inst.accept_placement = false
    end

    if anim and anim ~= inst.lastAnim then
        inst.AnimState:PlayAnimation(inst.animName .. anim)
    end
    inst.lastAnim = anim

    if inst.accept_placement then
        -- 我还需要检测旁边是否有门
        local x, y, z = inst.Transform:GetWorldPosition()
        if #TheSim:FindEntities(x, 0, z, 4, { "hamlet_door" }) > 0 then
            inst.accept_placement = false
        end
    end
end

local function placer_override_testfn(inst)
    local can_build, mouse_blocked = true, false
    if inst.components.placer.testfn ~= nil then
        can_build, mouse_blocked = inst.components.placer.testfn(inst:GetPosition(), inst:GetRotation())
    end
    can_build = inst.accept_placement

    return can_build, mouse_blocked
end

local function PostInitPlacer(inst, name)
    inst.lastAnim = nil
    inst.animName = name

    inst.components.placer.onupdatetransform = PlacerOnUpdateTransform
    inst.components.placer.override_build_point_fn = inst.GetPosition
    inst.components.placer.override_testfn = placer_override_testfn
    inst.accept_placement = false
end

local function MakeHouseDoorPlacer(name, build, bank)
    return MakePlacer("common/inventory/" .. name .. "_placer", bank, build, name .. "_open_north", nil, nil,
        nil, nil, nil, nil, function(inst) PostInitPlacer(inst, name) end)
end

return
-- 室内门
    MakeHouseDoor("wood_door"),
    MakeHouseDoor("stone_door"),
    MakeHouseDoor("organic_door"),
    MakeHouseDoor("iron_door"),
    MakeHouseDoor("pillar_door"),
    MakeHouseDoor("curtain_door"),
    MakeHouseDoor("round_door"),
    MakeHouseDoor("plate_door"),
    MakeHouseDoorPlacer("wood_door", "player_house_doors", "player_house_doors"),
    MakeHouseDoorPlacer("stone_door", "player_house_doors", "player_house_doors"),
    MakeHouseDoorPlacer("organic_door", "player_house_doors", "player_house_doors"),
    MakeHouseDoorPlacer("iron_door", "player_house_doors", "player_house_doors"),
    MakeHouseDoorPlacer("pillar_door", "player_house_doors", "player_house_doors"),
    MakeHouseDoorPlacer("curtain_door", "player_house_doors", "player_house_doors"),
    MakeHouseDoorPlacer("round_door", "player_house_doors", "player_house_doors"),
    MakeHouseDoorPlacer("plate_door", "player_house_doors", "player_house_doors"),

    -- 出口门
    Prefab("playerhouse_city_exit_door", playerHouseExitDoorFn, assets)
-- 出口门anim：
-- idle_antiquities、idle_bank、idle_basic、idle_deli、idle_flag、idle_florist、idle_general、idle_giftshop、idle_hoofspa、idle_old、idle_produce、idle_tinker
