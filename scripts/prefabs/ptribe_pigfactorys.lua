local assets = {
    Asset("ANIM", "anim/pig_shop.zip"),
    Asset("ANIM", "anim/pig_shop_weapons.zip"),
}

local function OnCoolDown(inst)
    inst.startTime = GetTime()
end

----------------------------------------------------------------------------------------------------
local function onhammered(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end

    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_big")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle")
    end
end

local function onopen(inst)
    inst.SoundEmitter:PlaySound("dontstarve/wilson/chest_open")
end

local function onclose(inst)
    inst.SoundEmitter:PlaySound("dontstarve/wilson/chest_close")
end

local function weaponFn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("pig_shop_weapons.png")

    MakeObstaclePhysics(inst, 1.25)

    inst.AnimState:SetBank("pig_shop")
    inst.AnimState:SetBuild("pig_shop_weapons")
    inst.AnimState:PlayAnimation("idle", true)
    inst.AnimState:Hide("YOTP")

    inst:AddTag("structure")
    inst:AddTag("ptribe_pigfactory")

    MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheNet:IsDedicated() then
        inst.startTime = nil --开始计时的时间
        inst:ListenForEvent("ptribe_pigfactory.cooldown", OnCoolDown)
    end

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("ptribe_upgrade")
    inst.components.ptribe_upgrade:Configure(1, 5)

    inst:AddComponent("ptribe_pigfactory")

    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)

    inst:AddComponent("inspectable")

    inst:AddComponent("container")
    inst.components.container:WidgetSetup("ptribe_pigweaponfactory")
    inst.components.container.onopenfn = onopen
    inst.components.container.onclosefn = onclose
    inst.components.container.skipclosesnd = true
    inst.components.container.skipopensnd = true

    MakeSnowCovered(inst)

    MakeMediumBurnable(inst, nil, nil, true)
    MakeLargePropagator(inst)

    return inst
end

return Prefab("ptribe_pigweaponfactory", weaponFn, assets),
    MakePlacer("ptribe_pigweaponfactory_placer", "pig_shop", "pig_shop_weapons", "idle")
