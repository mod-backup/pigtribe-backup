local assets =
{
    Asset("ANIM", "anim/pig_bandit.zip"),
    Asset("ANIM", "anim/townspig_basic.zip"),
    Asset("ANIM", "anim/townspig_actions.zip"),
    Asset("ANIM", "anim/townspig_attacks.zip"),
    Asset("ANIM", "anim/townspig_sneaky.zip"),
    Asset("SOUND", "sound/pig.fsb"),
}
local prefabs =
{
    "collapse_big",
    "pigbanditexit",
    "meat",
    "stash_map",
    "bandithat",
}

SetSharedLootTable('pigbandit', {
    { 'meat',      1.0 },
    { 'stash_map', 1.0 },
    { 'bandithat', 1.0 },
})

local brain = require "brains/pigbanditbrain"

local function OnTalk(inst, script)
    inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
end

local ATTACK_MUST_TAGS = { "_combat" }
local ATTACK_CANT_TAGS = { "playerghost" }
local ATTACK_ONE_OF_TAGS = { "player", "pig" }
local function Retarget(inst)
    return FindEntity(inst, 16,
        function(guy)
            return inst.components.combat:CanTarget(guy)
                and guy.components.inventory
                and guy.components.inventory:HasItemWithTag("oinc", 1)
        end, ATTACK_MUST_TAGS, ATTACK_CANT_TAGS, ATTACK_ONE_OF_TAGS)
end

local function KeepTarget(inst, target)
    return inst.components.combat:CanTarget(target)
end

function FindOincs(inst)
    return inst.components.inventory and inst.components.inventory:GetItemsWithTag("oinc")
end

local function OnHitOtherFn(inst, other)
    local oincs = FindOincs(other)

    while oincs and (#oincs > 0) do
        for i, oinc in ipairs(oincs) do
            inst.components.thief:StealItem(other, oinc, nil)
        end

        oincs = FindOincs(other)
    end
end

local function OnAttacked(inst, data)
    inst:ClearBufferedAction()
    inst.attacked = true
    inst.components.combat:SetTarget(data and data.attacker)
end

local MAX_TARGET_SHARES = 5
local SHARE_TARGET_DIST = 30
local function OnNewTarget(inst, data)
    if inst:HasTag("werepig") then
        inst.components.combat:ShareTarget(data.target, SHARE_TARGET_DIST,
            function(dude) return dude:HasTag("werepig") end, MAX_TARGET_SHARES)
    end
end

local function OnRemove(inst)
    if inst.components.homeseeker and inst.components.homeseeker:GetHome() then
        inst.components.homeseeker:GetHome():Remove()
    end
end

local function onsave(inst, data)
    data.attacked = inst.attacked or nil
    data.monster = inst:HasTag("monster") or nil
end

local function onload(inst, data)
    inst.attacked = data.attacked
    if data.monster then
        inst:AddTag("monster")
    end
end

local function Disappear(inst)
    -- 移除之前把钱添加到宝物中
    local stash = TheWorld.components.piratespawner and TheWorld.components.piratespawner:GetCurrentStash()
    if stash then
        inst.components.inventory:ForEachItem(function(ent) stash:stashloot(ent) end)
    end

    inst:Remove()
end

local function AddMonster(inst)
    if not inst:HasTag("monster") then
        inst:AddTag("monster")
    end
end

local function fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddLightWatcher()
    inst.entity:AddNetwork()

    inst.DynamicShadow:SetSize(1.5, .75)

    inst.AnimState:SetBank("townspig")
    inst.AnimState:SetBuild("pig_bandit")
    inst.AnimState:PlayAnimation("idle", true)
    inst.AnimState:Hide("hat")
    inst.AnimState:Hide("ARM_carry")
    inst.Transform:SetFourFaced()

    MakeCharacterPhysics(inst, 50, .5)

    inst:AddTag("character")
    inst:AddTag("pig")
    inst:AddTag("scarytoprey")
    -- inst:AddTag("monster") -- 等捡了钱再添加
    -- inst:AddTag("sneaky")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("talker")
    inst.components.talker.ontalk = OnTalk
    inst.components.talker.fontsize = 35
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = 7
    inst.components.locomotor.walkspeed = 3

    inst:AddComponent("eater")

    MakeMediumBurnableCharacter(inst, "pig_torso")

    inst:AddComponent("homeseeker")

    inst:AddComponent("follower")
    inst.components.follower.maxfollowtime = TUNING.PIG_LOYALTY_MAXTIME

    inst:AddComponent("combat")
    inst.components.combat:SetRange(4)
    inst.components.combat:SetDefaultDamage(33)
    inst.components.combat:SetAttackPeriod(3)
    inst.components.combat:SetRetargetFunction(3, Retarget)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst.components.combat.hiteffectsymbol = "chest"
    inst.components.combat.onhitotherfn = OnHitOtherFn

    inst:AddComponent("thief")
    -- inst.components.thief:SetOnStolenFn(OnStolenFn)

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.PIG_HEALTH * 2) --因为猪人会帮忙打，所以血量不能原封不动照搬

    inst:AddComponent("inventory")

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('pigbandit')

    inst:AddComponent("sleeper")
    -- inst.components.sleeper.onlysleepsfromitems = true

    inst:AddComponent("inspectable")

    inst:AddComponent("vanish_on_sleep") --虽然会跑到一个位置再消失， 但是如果如果脱离加载范围则直接让其消失

    MakeMediumFreezableCharacter(inst, "pig_torso")

    inst:ListenForEvent("onmissother", AddMonster) --当强盗猪人攻击时，添加monster标签
    inst:ListenForEvent("onattackother", AddMonster)
    inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("newcombattarget", OnNewTarget)
    inst:ListenForEvent("onremove", OnRemove)

    inst.disappear = Disappear

    inst:SetBrain(brain)
    inst:SetStateGraph("SGpigbandit")

    inst.OnSave = onsave
    inst.OnLoad = onload

    return inst
end

return Prefab("common/characters/pigbandit", fn, assets, prefabs)
