local MakePlayerCharacter = require "prefabs/player_common"
local ResourceConvert = require("ptribe_resourceconvert")
local Utils = require("ptribe_utils/utils")
local ImageUtils = require("ptribe_imageutils")

TUNING.GAMEMODE_STARTING_ITEMS.DEFAULT.WILBA = { "deed", "silvernecklace" }
for _, v in ipairs(TUNING.GAMEMODE_STARTING_ITEMS.DEFAULT.WILBA) do
    TUNING.STARTING_ITEM_IMAGE_OVERRIDE[v] = ImageUtils.GetItemImage(v)
end

local assets =
{
    Asset("ANIM", "anim/wilba.zip"),
    Asset("ANIM", "anim/werewilba.zip"),
    Asset("ANIM", "anim/werewilba_actions.zip"),
    Asset("ANIM", "anim/werewilba_transform.zip"),
    Asset("ATLAS", "images/woodie.xml"),
    -- Asset("IMAGE", "images/colour_cubes/beaver_vision_cc.tex"),
    Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
}

local prefabs = {
    "silvernecklace",
    "deed",
}

local function common_postinit(inst)
    inst.AnimState:AddOverrideBuild("werewilba_transform")

    inst.entity:AddLight()
    inst.Light:Enable(false)
    inst.Light:SetRadius(5)
    inst.Light:SetFalloff(.5)
    inst.Light:SetIntensity(.6)
    inst.Light:SetColour(245 / 255, 40 / 255, 0 / 255)

    inst:AddTag("wilba")
    inst:AddTag("pigroyalty")
    inst:AddTag("pigelite") --打猪人不会还手，不过这个防不了对猪人的远程攻击
end

----------------------------------------------------------------------------------------------------
local function BreatheSounds(inst)
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/breath_in")
end

local function BreatheSounds_out(inst)
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/breath_out")
end

local function RandomGrunt(inst)
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/characters/werewilba/bark")
    inst.components.timer:StartTimer("ptribe_grunt", math.random(TUNING.TOTAL_DAY_TIME / 15, TUNING.TOTAL_DAY_TIME / 10))
end

local function StartStarving(inst)
    if inst.components.ptribe_wilbatransform.type ~= "" then
        inst.components.ptribe_wilbatransform:TransformToWilba2()
    end
end

local function OnTimerDone(inst, data)
    if data.name == "ptribe_grunt" then
        RandomGrunt(inst)
    elseif data.name == "ptribe_breath" then
        BreatheSounds(inst)
        inst.components.timer:StartTimer("ptribe_breath", 2)
    elseif data.name == "ptribe_breath2" then
        BreatheSounds_out(inst)
        inst.components.timer:StartTimer("ptribe_breath2", 2)
    end
end

local function OnKilledOther(inst, data)
    if data.victim and data.victim:HasTag("pig")
        and not data.victim:HasTag("werepig") and not data.victim:HasTag("hostile") and not inst:HasTag("monster") then
        inst.components.sanity:DoDelta(-10)
    end
end

local ALLGOODS = ResourceConvert.GetAllGoods()
local function BuildItem(inst, data)
    -- 在猪人摊位购买商品会返还部分幸运黄金（变相打折）
    if data.prototyper and data.prototyper.prefab == "ptribe_carnival_prizebooth" then
        local name = data.item.prefab
        if name == "blueprint" and data.item.recipetouse then --蓝图的预制体名全是blueprint
            name = data.item.recipetouse .. "_blueprint"
        end
        local cost = ALLGOODS[name]
        if cost then
            local ret = math.min(cost - 1, math.ceil(cost * 0.2))
            print(ret)
            if ret > 0 then
                local item = SpawnPrefab("oinc")
                item.components.stackable.stacksize = ret
                inst.components.inventory:GiveItem(item)
            end
        end
    end
end

---召唤四大护法，初始15秒，因为强度太高了
local function SummonPigEliteFighter(inst, attacker)
    for i = 1, 4 do
        inst:DoTaskInTime(0.5, function()
            local pos = inst:GetPosition()
            local elite = SpawnPrefab("ptribe_pigelitefighter" .. tostring(i))

            elite:AddTag("crazy")
            elite.Transform:SetPosition(pos.x,
                (inst.components.rider ~= nil and inst.components.rider:IsRiding()) and 3 or 0, pos.z)
            elite.components.follower:SetLeader(inst)
            elite.components.timer:StartTimer("ptribe_despawn_timer", 15)

            local theta = math.random() * PI2
            local offset = FindWalkableOffset(pos, theta, 2.5, 16, true, true, nil, false, true)
                or FindWalkableOffset(pos, theta, 2.5, 16, false, false, nil, false, true)
                or Vector3(0, 0, 0)

            pos.x, pos.y, pos.z = pos.x + offset.x, 0, pos.z + offset.z
            elite.sg:GoToState("spawnin", { dest = pos })
            elite.components.combat:SetTarget(attacker)
        end)
    end
end

local SHARE_TARGET_DIST = 30
local MAX_TARGET_SHARES = 5
local function IsNonWerePig(dude)
    return dude:HasTag("pig") and not dude:HasTag("werepig")
end
local function OnAttacked(inst, data)
    local attacker = data.attacker
    if not attacker or not attacker.components.health or attacker.components.health:IsDead()
        or not data.damage or data.damage <= 0 --没伤害就不召唤
    then
        return
    end

    -- 猪人帮忙
    if not attacker:HasTag("pig") or attacker:HasTag("hostile") then
        inst.components.combat:ShareTarget(attacker, SHARE_TARGET_DIST, IsNonWerePig, MAX_TARGET_SHARES)
    end

    -- 四大护法
    if not inst.components.timer:TimerExists("ptribe_wilbaSummonPig")
        and inst.components.leader:CountFollowers("pigelite") < 4 --如果已经有四个护法了就不召唤
    then
        SummonPigEliteFighter(inst, attacker)
        inst.components.timer:StartTimer("ptribe_wilbaSummonPig", 300)
    end
end

local function OnEquip(inst, data)
    if inst:HasTag("monster") then
        if data.item.prefab == "silvernecklace" then
            local type = inst.components.ptribe_wilbatransform.type
            if type == "daywalker" then
                inst.components.ptribe_wilbatransform:TransformToWilba2()
            else
                inst.sg:GoToState("wilba_back")
            end
            inst:DoTaskInTime(0, function()
                data.item.components.fueled:DoDelta(-math.huge)
            end)
        elseif data.eslot == EQUIPSLOTS.HANDS
            or (data.item.components.inventoryitem and data.item.components.inventoryitem.cangoincontainer)
        then
            inst:DoTaskInTime(0, function()
                inst.components.inventory:DropItem(data.item, true, true)
            end)
        end
    end
end

local function OnMounted(inst, data)
    if inst:HasTag("monster") then
        inst:DoTaskInTime(0, function()
            inst:PushEvent("bucked")
        end)
    end
end

---搬的player_common.lua代码，覆盖读书失败后的话
-- local function OnActionFailed(inst, data)
--     print("执行1")
--     if inst.components.talker ~= nil
--         and not data.action.action.silent_fail
--         and (data.reason ~= nil or
--             not data.action.autoequipped or
--             inst.components.inventory.activeitem == nil) then
--         local msg = GetActionFailString(inst, data.action.action.id, data.reason)
--         if msg == "only_used_by_waxwell_and_wicker" then
--             local oldPrefab = inst.prefab
--             inst.prefab = "wicker"
--             msg = GetActionFailString(inst, data.action.action.id, data.reason)
--             inst.prefab = oldPrefab
--         end
--         print("执行2")
--         inst.components.talker:Say(msg)
--     end
-- end

local function master_postinit(inst)
    inst.starting_inventory = TUNING.GAMEMODE_STARTING_ITEMS.DEFAULT.WILBA

    inst.soundsname = "wilba"
    inst.talker_path_override = "dontstarve_DLC003/characters/"

    inst.components.foodaffinity:AddPrefabAffinity("bonestew", TUNING.AFFINITY_15_CALORIES_HUGE)

    inst.components.sanity:SetMax(TUNING.WILBA_SANITY)
    inst.components.hunger:SetMax(TUNING.WILBA_HUNGER)
    inst.components.health:SetMaxHealth(TUNING.WILBA_HEALTH)

    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetCanEatRaw()
    inst.components.eater:SetStrongStomach(true) -- can eat monster meat!
    inst.components.eater:SetCanEatRawMeat(true)

    inst.components.sanity.night_drain_mult = 1.5

    inst:AddComponent("ptribe_wilbatransform")

    --背重物不减速
    inst:AddComponent("mightiness")
    inst.components.mightiness.current = inst.components.mightiness.max
    inst.components.mightiness.state = "normal"
    inst.components.mightiness.CanTransform = Utils.FalseFn
    inst.components.mightiness.GetPercent = Utils.ConstantFn(1)
    inst.components.mightiness.DoDelta = Utils.ConstantFn()

    --可以读书
    inst:AddComponent("reader")

    inst:ListenForEvent("startstarving", StartStarving)
    inst:ListenForEvent("timerdone", OnTimerDone)
    inst:ListenForEvent("killed", OnKilledOther)
    inst:ListenForEvent("builditem", BuildItem)
    inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("equip", OnEquip)
    inst:ListenForEvent("mounted", OnMounted)
    -- inst:ListenForEvent("actionfailed", OnActionFailed)
end


return MakePlayerCharacter("wilba", prefabs, assets, common_postinit, master_postinit),
    CreatePrefabSkin("wilba_none", {
        base_prefab = "wilba",
        --各种皮肤
        skins = {
            normal_skin = "wilba",
            ghost_skin = "ghost_wilba_build",
            were_skin = "werewilba",
        },
        tags = { "wilba", "CHARACTER" },
        type = "base",
        bigportrait = { symbol = "wilba.tex", build = "bigportraits/wilba.xml" },
        build_name_override = "wilba",
        rarity = "",
        skip_item_gen = true,
        skip_giftable_gen = true,
    })
