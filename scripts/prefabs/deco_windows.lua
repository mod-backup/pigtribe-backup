local DecoCreator = require "prefabs/deco_util"

-- 构建后我需要根据所在墙面初始化bank和旋转方向
local function OnBuilt(inst)
    local bank = inst.AnimState:GetCurrentBankName()
    local side = TheWorld.components.interiorspawner:TestWallOrnamentPos(inst)

    if side == 1 or side == 3 then
        inst.initData = {
            animdata = {
                bank = (side == 1 or side == 3) and bank .. "_side" or nil,
                flip = side == 3 or nil,
            }
        }
        TheWorld.components.interiorspawner:InitHouseInteriorPrefab(inst, inst.initData)

        inst.tempChildrens = { "window_round_light" }
    end
end

local function MasterInit(inst)
    inst:ListenForEvent("onbuilt", OnBuilt)
end

local function MakeWindow(name, build, bank, anim, data)
    data = data or {
        loopanim = true,
        decal = true,
        background = 3,
        dayevents = true,
        curtains = true,
        children = { "window_round_light_backwall" },
        tags = { "NOBLOCK", "wallsection" },
        onbuilt = true,
    }

    data.masterInit = MasterInit

    return DecoCreator:Create(name, build, bank, anim, data)
end

-- 我移除了窗帘的_side预制体，因为它与后墙的预制体只是bank和children不同
return
    MakeWindow("window_round_backwall", "interior_window", "interior_window", "day_loop"),
    MakeWindow("window_round_burlap_backwall", "interior_window_burlap", "interior_window_burlap", "day_loop"),
    MakeWindow("window_small_peaked_backwall", "interior_window_small", "interior_window_small", "day_loop"),
    MakeWindow("window_large_square_backwall", "interior_window_large", "interior_window", "day_loop"),
    MakeWindow("window_tall_backwall", "interior_window_tall", "interior_window_tall", "day_loop"),
    MakeWindow("window_round_arcane_backwall", "window_arcane_build", "interior_window_large", "day_loop"),
    MakeWindow("window_small_peaked_curtain_backwall", "interior_window_small", "interior_window", "day_loop"),
    MakeWindow("window_large_square_curtain_backwall", "interior_window_large", "interior_window_large", "day_loop"),
    MakeWindow("window_tall_curtain_backwall", "interior_window_tall", "interior_window_tall", "day_loop"),
    MakeWindow("window_square_weapons_backwall", "window_weapons_build", "interior_window_large", "day_loop"),
    MakeWindow("window_greenhouse_backwall", "interior_window_greenhouse_build", "interior_window_greenhouse", "day_loop",
        { loopanim = true, decal = nil, background = 3, dayevents = true, curtains = true, children = { "window_big_light_backwall" }, tags = { "NOBLOCK", "wallsection", "fullwallsection" }, onbuilt = true, }),

    -- 日光
    DecoCreator:Create("window_round_light", "interior_window", "interior_window_light_side", "day_loop",
        { loopanim = true, decal = true, light = true, dayevents = true, followlight = "natural", windowlight = true, dustzmod = 1.3, tags = { "NOBLOCK", "NOCLICK" } }),
    DecoCreator:Create("window_round_light_backwall", "interior_window", "interior_window_light", "day_loop",
        { loopanim = true, decal = true, light = true, dayevents = true, followlight = "natural", windowlight = true, dustxmod = 1.3, tags = { "NOBLOCK", "NOCLICK" } }),
    DecoCreator:Create("window_big_light", "interior_window_greenhouse_build", "interior_window_greenhouse_light_side",
        "day_loop",
        { loopanim = true, decal = true, light = true, dayevents = true, followlight = "natural", windowlight = true, dustzmod = 1.3, tags = { "NOBLOCK", "NOCLICK" } }),
    DecoCreator:Create("window_big_light_backwall", "interior_window_greenhouse_build",
        "interior_window_greenhouse_light", "day_loop",
        { loopanim = true, decal = true, light = true, dayevents = true, followlight = "natural", windowlight = true, dustxmod = 1.3, tags = { "NOBLOCK", "NOCLICK" } })
