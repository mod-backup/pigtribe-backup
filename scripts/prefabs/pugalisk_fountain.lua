local Constructor = require("ptribe_utils/constructor")

local assets = {
    Asset("ANIM", "anim/python_fountain.zip"), --不老泉
}

local prefabs = {
    "waterdrop",
    "lifeplant",
}

local function Recover(inst)
    inst.components.activatable.inactive = true
    inst.AnimState:PlayAnimation("flow_pre")
    inst.AnimState:PushAnimation("flow_loop", true)
    inst.SoundEmitter:KillSound("burble")
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/fountain_LP", "burble")
    inst.resettask = nil
end

-- 玩家采集不老泉水
local function OnActivate(inst, doer)
    inst.AnimState:PlayAnimation("flow_pst")
    inst.AnimState:PushAnimation("off", true)
    -- inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/resurrection")
    inst.SoundEmitter:KillSound("burble")
    inst.components.activatable.inactive = false

    local drop = SpawnPrefab("waterdrop")
    doer.components.inventory:GiveItem(drop, nil, Vector3(TheSim:GetScreenPos(inst.Transform:GetWorldPosition())))

    if inst.resettask then
        inst.resettask:Cancel()
        inst.resettask = nil
    end
    inst.resettask = inst:DoTaskInTime(TUNING.TOTAL_DAY_TIME * 20, Recover) --20天采集一次
end

local function CommonInit(inst)
    inst.entity:AddSoundEmitter()

    inst.AnimState:PlayAnimation("flow_loop", true)

    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/fountain_LP", "burble")

    MakeObstaclePhysics(inst, 2)

    local minimap = inst.entity:AddMiniMapEntity()
    minimap:SetIcon("pig_ruins_well.png")

    inst:AddTag("pugalisk_fountain")
    inst:AddTag("pugalisk_avoids")
end

local function OnSave(inst, data)
    data.remainTime = inst.resettask and GetTaskRemaining(inst.resettask) or nil
end

local function OnLoad(inst, data)
    if data.remainTime then
        inst.resettask = inst:DoTaskInTime(data.remainTime, Recover)
        inst.AnimState:PlayAnimation("off", true)
        inst.SoundEmitter:KillSound("burble")
        inst.components.activatable.inactive = false
    end
end

local function onhammered(inst)
    SpawnPrefab("collapse_big").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst:Remove()
end

local function MasterInit(inst)
    inst:AddComponent("activatable")
    inst.components.activatable.OnActivate = OnActivate
    inst.components.activatable.inactive = true

    inst:AddComponent("inspectable")
    inst.components.inspectable:RecordViews()

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
end

return Constructor.MakePrefab("pugalisk_fountain", {
        assets = assets,
        prefabs = prefabs,
        bank = "fountain",
        build = "python_fountain",
        playAnim = "flow_loop",
        coomonInit = CommonInit,
        masterInit = MasterInit
    }),
    MakePlacer("pugalisk_fountain_placer", "fountain", "python_fountain", "flow_loop")
