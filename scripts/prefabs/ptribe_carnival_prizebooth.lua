require "prefabutil"
local ResourceConvert = require("ptribe_resourceconvert")
local Utils = require("ptribe_utils/utils")

local prefabs = { "carnival_prizebooth" }
local kit_assets ={Asset("ANIM", "anim/carnival_prizebooth.zip"),}

local function RefreshGoods(inst)
    local manager  = FindEntity(inst, TUNING.TRIBE_MAX_DIS, function(ent)
        return ent.components.ptribe_pigmanager
            and ent.components.ptribe_pigmanager:IsManager()
            and ent.components.ptribe_pigmanager:IsValidManager()
    end, { "structure" })

    local rate     = manager and manager.components.ptribe_pigmanager.completeRate or 0 --部落完成率
    local newGoods = ResourceConvert.SelectGoods(rate)
    inst.goodsJson:set(json.encode(newGoods)) --同步商品
end

-- 本地执行，解析json
local function OnGoodsRefresh(inst)
    inst.goods = {}
    for _, name in ipairs(json.decode(inst.goodsJson:value())) do
        inst.goods[name] = true
    end
end

local function OnEntitySleep(inst)
    if inst.updateGoodsTask then
        inst.updateTime = GetTaskRemaining(inst.updateGoodsTask)
        inst.updateGoodsTask:Cancel()
        inst.updateGoodsTask = nil
    end
end
local function OnEntityWake(inst)
    if not inst.updateGoodsTask then --虽然感觉不判断也行
        inst.updateGoodsTask = inst:DoPeriodicTask(TUNING.TRIBE_SHOP_REFRESH_TIME, RefreshGoods,
            math.max(0, (inst.updateTime or 0) - GetTime()))
    end
end

local oldFn = Prefabs["carnival_prizebooth"].fn
local function fn(...)
    local inst = oldFn(...)

    --启用的商品列表，商品名列表
    inst.goodsJson = net_string(inst.GUID, "ptribe_carnival_prizebooth.goodsJson", "ptribe_carnival_prizebooth.goodsJson")
    inst.goods = nil --启用的配方名表

    if not TheNet:IsDedicated() then
        inst:ListenForEvent("ptribe_carnival_prizebooth.goodsJson", OnGoodsRefresh)
    end

    if not TheWorld.ismastersim then
        return inst
    end

    inst.components.prototyper.trees = TUNING.PROTOTYPER_TREES.PTRIBE_PIG_SHOP

    Utils.FnDecorator(inst, "OnEntitySleep", OnEntitySleep)
    Utils.FnDecorator(inst, "OnEntityWake", OnEntityWake)

    return inst
end

local deployable_data =
{
    deploymode = DEPLOYMODE.CUSTOM,
    custom_candeploy_fn = function(inst, pt, mouseover, deployer)
        local x, y, z = pt:Get()
        return TheWorld.Map:CanDeployAtPoint(pt, inst, mouseover) and TheWorld.Map:IsAboveGroundAtPoint(x, y, z, false)
    end,
    master_postinit = function (inst)
        -- inst.components.inventoryitem.atlasname = "images/inventoryimages1.xml" --默认会自己查找的
        inst.components.inventoryitem.imagename = "carnival_prizebooth_kit"
    end
}

return Prefab("ptribe_carnival_prizebooth", fn, nil, prefabs),
MakeDeployableKitItem("ptribe_carnival_prizebooth_kit", "ptribe_carnival_prizebooth", "carnival_prizebooth",
        "carnival_prizebooth","kit_item", kit_assets, { size = "med", scale = 0.77 }, nil, { fuelvalue = TUNING.MED_FUEL }, deployable_data),
    MakePlacer("ptribe_carnival_prizebooth_kit_placer", "carnival_prizebooth", "carnival_prizebooth", "idle")
