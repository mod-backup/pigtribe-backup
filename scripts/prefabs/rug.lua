local Constructor = require("ptribe_utils/constructor")

local assets =
{
    Asset("ANIM", "anim/rugs.zip"),
    Asset("ANIM", "anim/interior_wall_decals_mayorsoffice.zip"),
    Asset("ANIM", "anim/interior_wall_decals_palace.zip"),
}

local function CoomonInit(inst, data)
    inst.entity:AddSoundEmitter()

    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)

    if data.face == 2 then
        inst.Transform:SetTwoFaced()
    end

    inst:AddTag("OnFloor") --地毯特有标签
    inst:AddTag("NOBLOCK")
end

----------------------------------------------------------------------------------------------------
local function OnBuilt(inst)
    inst.SoundEmitter:PlaySound("dontstarve/wilson/dig")
    inst.rotate = inst.Transform:GetRotation() --初始是90
end

local function OnHammered(inst)
    local collapse_fx = SpawnPrefab("collapse_small")
    collapse_fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    collapse_fx:SetMaterial("wood")

    inst.components.lootdropper:DropLoot()
    inst:Remove()
end

local function Merge(inst)
    if inst.level >= 6 then
        return false, "MAXN"
    end

    inst.level = inst.level + 1

    local scale = 1 + inst.level * 0.5
    inst.AnimState:SetScale(scale, scale)
    return true
end

local function Rotate(inst)
    inst.rotate = inst.rotate + 90
    inst.Transform:SetRotation(inst.rotate)
end

local function OnSave(inst, data)
    data.level = inst.level ~= 0 and inst.level or nil
    data.rotate = inst.rotate ~= 0 and inst.rotate or nil
end

local function OnLoad(inst, data)
    if not data then return end

    if data.level then
        inst.level = data.level
        local scale = 1 + inst.level * 0.5
        inst.AnimState:SetScale(scale, scale)
    end

    if data.rotate then
        inst.rotate = data.rotate
        inst.Transform:SetRotation(inst.rotate)
    end
end

local function MasterInit(inst)
    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnWorkCallback(OnHammered)

    inst.level = 0 --大小级别，最大为6，最大缩放为4
    inst.Merge = Merge
    inst.rotate = 0
    inst.Rotate = Rotate

    inst:ListenForEvent("onbuilt", OnBuilt)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
end

local function MakeRug(name, anim, data)
    data = data or {}
    return Constructor.MakePrefab(name, {
        assets = assets,
        bank = data.bank or "rugs",
        build = data.build or "rugs",
        playAnim = anim or name,
        coomonInit = function(inst) CoomonInit(inst, data) end,
        masterInit = MasterInit,
    })
end

-- 牛毛可以旋转和升级地毯
return MakeRug("rug_round"),
    MakeRug("rug_oval"),
    MakeRug("rug_square"),
    MakeRug("rug_rectangle"),
    MakeRug("rug_leather"),
    MakeRug("rug_fur"),
    MakeRug("rug_circle", "half_circle"),
    MakeRug("rug_hedgehog"),
    MakeRug("rug_porcupuss", nil, { face = 2 }),
    MakeRug("rug_hoofprint", "rug_hoofprints"),
    MakeRug("rug_octagon"),
    MakeRug("rug_swirl"),
    MakeRug("rug_catcoon"),
    MakeRug("rug_rubbermat"),
    MakeRug("rug_web"),
    MakeRug("rug_metal"),
    MakeRug("rug_wormhole"),
    MakeRug("rug_braid"),
    MakeRug("rug_beard"),
    MakeRug("rug_nailbed"),
    MakeRug("rug_crime"),
    MakeRug("rug_tiles"),
    MakeRug("rug_cityhall_corners", "corner_back",
        { build = "interior_wall_decals_mayorsoffice", bank = "wall_decals_mayorsoffice" }),
    MakeRug("rug_palace_corners", "floortrim_corner",
        { build = "interior_wall_decals_palace", bank = "wall_decals_palace" }),
    MakeRug("rug_palace_runner", "rug_throneroom")
