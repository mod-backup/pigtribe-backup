local Utils = require("ptribe_utils/utils")
local GetPrefab = require("ptribe_utils/getprefab")

local assets = {
	Asset("ANIM", "anim/lifeplant.zip"),
	Asset("ANIM", "anim/lifeplant_fx.zip"),
}

local prefabs = { "collapse_small", "waterdrop" }

local INTENSITY = .5

local function fadein(inst)
	inst.components.fader:StopAll()
	inst.Light:Enable(true)
	if inst:IsAsleep() then
		inst.Light:SetIntensity(INTENSITY)
	else
		inst.Light:SetIntensity(0)
		inst.components.fader:Fade(0, INTENSITY, 3 + math.random() * 2, function(v) inst.Light:SetIntensity(v) end)
	end
end

local function onburnt(inst)
	local ash = SpawnPrefab("ash")
	ash.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst:Remove()
end

local function onsave(inst, data)
	if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
		data.burnt = true
	end
end

local function onload(inst, data)
	if data ~= nil and data.burnt then
		inst.components.burnable.onburnt(inst)
	end
end

local function onfar(inst)
	if inst.starvetask then
		inst.starvetask:Cancel()
		inst.starvetask = nil

		inst.starvetask2:Cancel()
		inst.starvetask2 = nil

		inst.SoundEmitter:KillSound("drainloop")
	end
end

local function onnear(inst, player)
	if not player then return end --player有可能不存在

	inst.starvetask = inst:DoPeriodicTask(0.5, function()
		if not player:IsValid() then
			onfar(inst)
			return
		end

		local fx = SpawnPrefab("lifeplant_sparkle")
		fx.Transform:SetPosition(player.Transform:GetWorldPosition())
		fx.lifeplant = inst
	end)
	inst.starvetask2 = inst:DoPeriodicTask(2, function()
		if not player:IsValid() then
			onfar(inst)
			return
		end

		player.components.hunger:DoDelta(-1)
	end)

	inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/crafted/flower_of_life/fx_LP", "drainloop") --其实这个声音挺好听的
end

local function dig_up(inst)
	local waterdrop = inst.components.lootdropper:SpawnLootPrefab("waterdrop")
	waterdrop.finiteusesPer = inst.components.finiteuses:GetPercent()
	inst:Remove()
end

local function manageidle(inst)
	local anim = "idle_gargle"
	if math.random() < 0.5 then
		anim = "idle_vanity"
	end

	inst.AnimState:PlayAnimation(anim)
	inst.AnimState:PushAnimation("idle_loop", true)

	inst:DoTaskInTime(8 + (math.random() * 20), inst.manageidle)
end

local function OnEntitySleep(inst)
	if inst.tendingCropsTask then
		inst.tendingCropsTask:Cancel()
		inst.tendingCropsTask = nil
	end
end

-- 对话、浇水、施肥
local RANDOM_FX = {
	"battlesong_healthgain_fx", "battlesong_sanitygain_fx", "battlesong_sanityaura_fx", "battlesong_fireresistance_fx",
	"battlesong_shadowaligned_fx", "battlesong_lunaraligned_fx"
}
local FARM_PLANT_TAGS = { "farm_plant", "farmplantstress" }
local function TendingCrops(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	local cost = 0
	local tiles = {} --同一块地皮每次只作用一次
	for _, v in ipairs(TheSim:FindEntities(x, y, z, 16, FARM_PLANT_TAGS)) do
		if v:HasTag("tendable_farmplant") and v.components.farmplanttendable ~= nil then
			v.components.farmplanttendable:TendTo(inst)
			cost = cost + 1
		end
		if TheWorld.components.farming_manager ~= nil then
			local vx, vy, vz = v.Transform:GetWorldPosition()
			local tile_x, tile_z = TheWorld.Map:GetTileCoordsAtPoint(vx, vy, vz)
			tiles[tile_x] = tiles[tile_x] or {}

			if not tiles[tile_x][tile_z] then
				local flag = false
				if GetPrefab.FarmingManagerGetMoisture(vx, vy, vz) < 90 then --潮湿度上限100
					flag = true
					TheWorld.components.farming_manager:AddSoilMoistureAtPoint(vx, vy, vz, 10)
					cost = cost + 1
				end

				local v1, v2, v3 = GetPrefab.FarmingManagerGetNutrients(vx, vy, vz)
				if v1 < 50 or v2 < 50 or v3 < 50 then
					flag = true
					TheWorld.components.farming_manager:AddTileNutrients(tile_x, tile_z,
						math.max(0, 60 - v1), math.max(0, 60 - v2), math.max(0, 60 - v3))
					cost = cost + 1
				end

				if flag then
					tiles[tile_x][tile_z] = true
				end
			end
		end
	end

	if cost > 0 then
		SpawnAt(RANDOM_FX[math.random(1, #RANDOM_FX)], inst)
		inst.components.finiteuses:Use(cost)
	end
end

local function OnEntityWake(inst)
	if inst.tendingCropsTask then
		inst.tendingCropsTask:Cancel()
		inst.tendingCropsTask = nil
	end
	inst.tendingCropsTask = inst:DoPeriodicTask(5, TendingCrops, math.random(1, 5))
end

-- 我需要在玩家作祟后手动删除
local function OnHauntFn(inst, doer)
	if doer:HasTag("playerghost") then
		inst:DoTaskInTime(0, function()
			SpawnAt("pocketwatch_ground_fx", inst)
			inst:Remove()
		end)
		return true
	end
end

local function OnRemove(inst)
	inst.SoundEmitter:KillSound("drainloop")
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddMiniMapEntity()
	inst.entity:AddSoundEmitter()
	inst.entity:AddLight()
	inst.entity:AddNetwork()

	MakeObstaclePhysics(inst, .3)

	inst.MiniMapEntity:SetIcon("lifeplant.png")

	inst.AnimState:SetBank("lifeplant")
	inst.AnimState:SetBuild("lifeplant")
	inst.AnimState:PlayAnimation("grow")
	inst.AnimState:PushAnimation("idle_loop", true)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	inst.AnimState:SetMultColour(0.9, 0.9, 0.9, 1)

	inst.Light:SetIntensity(INTENSITY)
	inst.Light:SetColour(180 / 255, 195 / 255, 150 / 255)
	inst.Light:SetFalloff(0.9)
	inst.Light:SetRadius(2)
	inst.Light:Enable(true)

	inst:AddTag("lifeplant")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("inspectable")

	inst:AddComponent("finiteuses")
	inst.components.finiteuses:SetOnFinished(inst.Remove)
	inst.components.finiteuses:SetMaxUses(2000)
	inst.components.finiteuses:SetUses(2000)

	-- 复活
	-- inst:AddComponent("resurrector")
	-- inst.components.resurrector.active = true
	-- inst.components.resurrector.doresurrect = doresurrect
	-- inst.components.resurrector.makeusedfn = makeused
	-- inst.components.resurrector.penalty = 1

	inst:AddComponent("lootdropper")

	inst:AddComponent("workable")
	inst.components.workable:SetWorkAction(ACTIONS.DIG)
	inst.components.workable:SetWorkLeft(1)
	inst.components.workable:SetOnFinishCallback(dig_up)

	MakeSnowCovered(inst, .01)

	inst:AddComponent("burnable")
	inst.components.burnable:SetFXLevel(3)
	inst.components.burnable:SetBurnTime(10)
	inst.components.burnable:AddBurnFX("fire", Vector3(0, 0, 0))
	inst.components.burnable:SetOnBurntFn(onburnt)

	MakeLargePropagator(inst)

	inst:AddComponent("fader")
	fadein(inst)

	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aurafn = Utils.ConstantFn(TUNING.SANITYAURA_MED)

	inst:AddComponent("playerprox")
	inst.components.playerprox:SetDist(6, 7)
	inst.components.playerprox:SetOnPlayerNear(onnear)
	inst.components.playerprox:SetOnPlayerFar(onfar)

	inst:AddComponent("hauntable")
	inst.components.hauntable:SetHauntValue(TUNING.HAUNT_INSTANT_REZ)
	inst.components.hauntable:SetOnHauntFn(OnHauntFn)

	inst.manageidle = manageidle
	inst:DoTaskInTime(8 + (math.random() * 20), inst.manageidle)

	inst:ListenForEvent("onremove", OnRemove)

	inst.OnSave = onsave
	inst.OnLoad = onload

	inst.OnEntitySleep = OnEntitySleep
	inst.OnEntityWake = OnEntityWake

	return inst
end

local function onspawn(inst)
	if inst.lifeplant and inst.lifeplant:IsValid() then
		local x2, y2, z2 = inst.lifeplant.Transform:GetWorldPosition()
		local angle = inst:GetAngleToPoint(x2, y2, z2)
		inst.Transform:SetRotation(angle)

		GetPrefab.SetFlightAttack(inst, {
			speed = 0.05,
			time = 2
		})
	else
		inst:Remove()
	end
end

local function sparklefn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	inst.entity:AddPhysics()

	inst.AnimState:SetBank("lifeplant_fx")
	inst.AnimState:SetBuild("lifeplant_fx")
	inst.AnimState:PlayAnimation("single" .. math.random(1, 3), true)
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

	inst:AddTag("flying")
	inst:AddTag("NOCLICK")
	inst:AddTag("FX")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst.lifeplant = nil --魔力花
	inst:DoTaskInTime(0, onspawn)

	inst.OnEntitySleep = inst.Remove
	inst.persists = false

	return inst
end

return Prefab("common/objects/lifeplant", fn, assets, prefabs),
	Prefab("common/objects/lifeplant_sparkle", sparklefn, assets, prefabs)
