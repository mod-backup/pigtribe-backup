local Constructor = require("ptribe_utils/constructor")

local function onbuilt(inst, data)
    inst.components.spawner:ReleaseChild()
end

local function fn(inst)
    inst:AddTag("wilbastructure") --猪人守卫判断这个

    if not TheWorld.ismastersim then
        return inst
    end

    inst.happy = true

    -- 猪火炬建造时放出猪人守卫
    inst:ListenForEvent("onbuilt", onbuilt)

    return inst
end

return Constructor.CopyPrefab("ptribe_pigtorch", "pigtorch", { init = fn, prefabs = { "pigguard" } }),
    MakePlacer("ptribe_pigtorch_placer", "pigtorch", "pig_torch", "idle")
