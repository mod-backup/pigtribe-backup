local Utils = require("ptribe_utils/utils")
local brain = require("brains/ptribe_pigelitefighterbrain")
local GetPrefab = require("ptribe_utils/getprefab")

local RETARGET_MUST_TAGS = { "_combat" }
local function NormalRetargetFn(inst)
    if inst:HasTag("NPC_contestant") then return nil end

    local exclude_tags = { "playerghost", "INLIMBO", "NPC_contestant" }
    if inst.components.follower.leader ~= nil then
        table.insert(exclude_tags, "abigail")
    end
    if inst.components.minigame_spectator ~= nil then
        table.insert(exclude_tags, "player") -- prevent spectators from auto-targeting webber
    end

    local oneof_tags = { "monster", "wonkey", "pirate" }
    if not inst:HasTag("merm") then
        table.insert(oneof_tags, "merm")
    end

    return not inst:IsInLimbo()
        and FindEntity(
            inst,
            TUNING.PIG_TARGET_DIST,
            function(guy)
                return guy:IsInLight() and inst.components.combat:CanTarget(guy)
            end,
            RETARGET_MUST_TAGS, -- see entityreplica.lua
            exclude_tags,
            oneof_tags
        )
        or nil
end

local function NormalKeepTargetFn(inst, target)
    --give up on dead guys, or guys in the dark, or werepigs
    return inst.components.combat:CanTarget(target) and target:IsInLight()
        and not (target.sg ~= nil and target.sg:HasStateTag("transform"))
end

local function ShouldAcceptItem(inst, item)
    if item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD then
        return true
    elseif item.components.edible then
        local foodtype = item.components.edible.foodtype
        if foodtype == FOODTYPE.MEAT or foodtype == FOODTYPE.HORRIBLE then
            return inst.components.follower.leader == nil or
                inst.components.follower:GetLoyaltyPercent() <= TUNING.PIG_FULL_LOYALTY_PERCENT
        end
        return true
    end
end

local FOLLOW_PIG_MUST_TAGS = { "character", "pig" }
local FOLLOW_PIG_CANT_TAGS = { "werepig", "hostile" }
local MAX_FOLLOW_TIME = TUNING.PIG_LOYALTY_MAXTIME / 2 * TUNING.TRIBE_KUNGFU_EMPLOY_EFFIC
local function OnGetItemFromPlayer(inst, giver, item)
    if item.components.edible ~= nil then
        --meat makes us friends (unless I'm a guard)
        if (item.components.edible.foodtype == FOODTYPE.MEAT or
                item.components.edible.foodtype == FOODTYPE.HORRIBLE)
            and item.components.inventoryitem ~= nil
            and ( --make sure it didn't drop due to pockets full
                item.components.inventoryitem:GetGrandOwner() == inst or
                --could be merged into a stack
                (not item:IsValid() and
                    inst.components.inventory:FindItem(function(obj)
                        return obj.prefab == item.prefab
                            and obj.components.stackable ~= nil
                            and obj.components.stackable:IsStack()
                    end) ~= nil)
            ) then
            if inst.components.combat:TargetIs(giver) then
                inst.components.combat:SetTarget(nil)
            elseif giver.components.leader ~= nil and not (inst:HasTag("guard") or giver:HasTag("monster") or giver:HasTag("merm")) then
                if giver.components.minigame_participator == nil then
                    giver:PushEvent("makefriend")
                    giver.components.leader:AddFollower(inst)
                end

                local left = inst.components.timer:GetTimeLeft("ptribe_despawn_timer")
                if left then
                    inst.components.timer:StopTimer("ptribe_despawn_timer")
                else
                    left = 0
                end
                inst.components.timer:StartTimer("ptribe_despawn_timer", math.min(
                    left + item.components.edible:GetHunger() * TUNING.PIG_ELITE_FIGHTER_DESPAWN_TIME / 2 *
                    TUNING.TRIBE_KUNGFU_EMPLOY_EFFIC, MAX_FOLLOW_TIME))
            end

            -- 薇尔芭
            if giver.prefab == "wilba" then
                local otherpigs = GetPrefab.GetOtherEnts(inst, {
                    radius = 16,
                    maxcount = TUNING.PTRIBE_WILBA_PIG_COUNT,
                    MUST_TAGS = FOLLOW_PIG_MUST_TAGS,
                    CANT_TAGS = FOLLOW_PIG_CANT_TAGS
                })

                for _, pig in ipairs(otherpigs) do
                    if giver.components.minigame_participator == nil then
                        giver:PushEvent("makefriend")
                        giver.components.leader:AddFollower(pig)
                    end
                    pig.components.follower:AddLoyaltyTime(item.components.edible:GetHunger() *
                        TUNING.PIG_LOYALTY_PER_HUNGER)
                    pig.components.follower.maxfollowtime = TUNING.PIG_LOYALTY_MAXTIME +
                        TUNING.PIG_LOYALTY_POLITENESS_MAXTIME_BONUS
                end
            end
        end
        if inst.components.sleeper:IsAsleep() then
            inst.components.sleeper:WakeUp()
        end
    end

    --I wear hats
    if item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD then
        local current = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
        if current ~= nil then
            inst.components.inventory:DropItem(current)
        end
        inst.components.inventory:Equip(item)
        inst.AnimState:Show("hat")
    end
end

local function OnRefuseItem(inst, item)
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local function OnTimerDone(inst, data)
    if data.name == "ptribe_despawn_timer" then
        inst.ptribe_should_despawn = true --新定义一个变量控制，不受原预制体影响
        inst.persists = false
    end
end

local OnAttacked = Utils.ChainFindUpvalue(Prefabs["pigman"].fn, "common", "OnAttacked")
local function MakePrefab(olaName)
    local oldFn = Prefabs[olaName].fn

    local function fn(...)
        local inst = oldFn(...)

        inst:AddTag("ptribe_pig")

        if not TheWorld.ismastersim then
            return inst
        end

        inst:RemoveAllEventCallbacks() --原对象监听的太多了，动不动就停止跟随了

        inst:SetBrain(brain)
        -- inst:SetStateGraph("ptribe_SGpigelitefighter")

        inst:AddComponent("named")
        inst.components.named.possiblenames = STRINGS.PIGNAMES
        inst.components.named:PickNewName()

        -- 把猪人的抄过来
        inst.components.combat:SetRetargetFunction(3, NormalRetargetFn)
        inst.components.combat:SetKeepTargetFunction(NormalKeepTargetFn)

        inst:AddComponent("inventory")

        inst:AddComponent("trader")
        inst.components.trader:SetAcceptTest(ShouldAcceptItem)
        inst.components.trader.onaccept = OnGetItemFromPlayer
        inst.components.trader.onrefuse = OnRefuseItem
        inst.components.trader.deleteitemonaccept = false

        inst:ListenForEvent("timerdone", OnTimerDone)
        inst:ListenForEvent("attacked", OnAttacked)
        return inst
    end

    return Prefab("ptribe_" .. olaName, fn, nil, { olaName })
end

return MakePrefab("pigelitefighter1"),
    MakePrefab("pigelitefighter2"),
    MakePrefab("pigelitefighter3"),
    MakePrefab("pigelitefighter4")
