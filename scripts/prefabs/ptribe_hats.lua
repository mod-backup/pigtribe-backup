local Constructor = require("ptribe_utils/constructor")
local HATS = {}

----------------------------------------------------------------------------------------------------
-- 假猪面具
local CHECK_TAGS = { "monster", "wonkey", "pirate", "merm" }
local function OnDisguiseEquipFn(inst, owner)
    Constructor.OpenTopOnEquip(owner)
    owner.AnimState:OverrideSymbol("swap_hat", "hat_disguise", "swap_hat")

    inst.tags = {}
    for _, v in ipairs(CHECK_TAGS) do
        if owner:HasTag(v) then
            table.insert(inst.tags, v)
            owner:RemoveTag(v)
        end
    end
end

local function OnDisguiseUnequipPostFn(inst, owner)
    for _, v in ipairs(inst.tags) do
        owner:AddTag(v)
    end
    inst.tags = nil
end

table.insert(HATS, Prefab("disguisehat", Constructor.MakeHatFn("disguisehat", "hat_disguise", {
    playAnim = "anim",
    imageAtlasName = "images/inventoryimages/hamletinventoryimages.xml",
    onEquipFn = OnDisguiseEquipFn,
    onUnequipPostFn = OnDisguiseUnequipPostFn
}), {
    Asset("ANIM", "anim/hat_disguise.zip"),
}))
----------------------------------------------------------------------------------------------------
-- 鬼祟帽

local function OnBanditEquipPostFn(inst, owner)
    owner:AddTag("sneaky")
end
local function OnBanditUnequipPostFn(inst, owner)
    owner:RemoveTag("sneaky")
end

local function BanditMasterInit(inst)
    inst.components.equippable.dapperness = TUNING.DAPPERNESS_SMALL

    inst:AddComponent("fueled")
    inst.components.fueled.fueltype = "USAGE"
    inst.components.fueled:InitializeFuelLevel(TUNING.TOTAL_DAY_TIME)
    inst.components.fueled:SetDepletedFn(inst.Remove)
end

table.insert(HATS, Prefab("bandithat", Constructor.MakeHatFn("bandithat", "hat_bandit", {
    playAnim = "anim",
    imageAtlasName = "images/inventoryimages/hamletinventoryimages.xml",
    masterInit = BanditMasterInit,
    onEquipPostFn = OnBanditEquipPostFn,
    onUnequipPostFn = OnBanditUnequipPostFn
}), {
    Asset("ANIM", "anim/hat_bandit.zip"),
}))

return unpack(HATS)
