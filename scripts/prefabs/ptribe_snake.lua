local assets =
{
    Asset("ANIM", "anim/snake_build.zip"),
    Asset("ANIM", "anim/snake_yellow_build.zip"),
    Asset("ANIM", "anim/snake_basic.zip"),
    Asset("ANIM", "anim/dragonfly_fx.zip"),
    Asset("SOUND", "sound/hound.fsb"),
}

local prefabs =
{
    "monstermeat",
    -- "snakeskin",  --蛇皮
    -- "venomgland", --毒腺
    "ash",
    "charcoal",
}

local brain = require "brains/ptribe_snakebrain"

local notags = { "FX", "NOCLICK", "INLIMBO", "wall", "snake", "structure" }
local function retargetfn(inst)
    return FindEntity(inst, TUNING.SNAKE_TARGET_DIST, function(guy)
        return inst.components.combat:CanTarget(guy)
    end, nil, notags)
end

local function KeepTarget(inst, target)
    return inst.components.combat:CanTarget(target) and
        inst:GetDistanceSqToInst(target) <= (TUNING.SNAKE_KEEP_TARGET_DIST * TUNING.SNAKE_KEEP_TARGET_DIST)
end

local function SanityAura(inst, observer)
    return observer.prefab == "webber" and 0 or -TUNING.SANITYAURA_SMALL
end

local function DoReturn(inst)
    if inst.components.homeseeker then
        -- inst.components.homeseeker:ForceGoHome() -- 通过home的组件回家，不过现在没有蛇窝
    end
end

local function OnEntitySleep(inst)
    if TheWorld.state.isday then
        DoReturn(inst)
    end
end

local function OnNewTarget(inst)
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local SHARE_TARGET_DIST = 30
local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST,
        function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 5)
end

local function OnAttackOther(inst, data)
    inst.components.combat:ShareTarget(data.target, SHARE_TARGET_DIST,
        function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 5)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddPhysics()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.Transform:SetFourFaced()

    MakeCharacterPhysics(inst, 10, .5)

    inst.AnimState:SetBank("snake")
    inst.AnimState:SetBuild("snake_build")
    inst.AnimState:PlayAnimation("idle")
    inst.AnimState:SetRayTestOnBB(true)

    inst:AddTag("scarytoprey")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("snake")
    inst:AddTag("canbetrapped")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("knownlocations")

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = TUNING.SNAKE_SPEED

    inst:AddComponent("follower")

    inst:AddComponent("eater")
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater.strongstomach = true -- can eat monster meat!

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.SNAKE_HEALTH)

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.SNAKE_DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.SNAKE_ATTACK_PERIOD)
    inst.components.combat:SetRetargetFunction(3, retargetfn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst.components.combat:SetHurtSound("dontstarve/creatures/together/deer/hit")
    inst.components.combat:SetRange(2, 3)

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:AddRandomLoot("monstermeat", 1.00)

    inst:AddComponent("inspectable")

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = SanityAura

    inst:AddComponent("sleeper")
    inst.components.sleeper:SetNocturnal(true)

    inst:SetStateGraph("SGptribe_snake")
    inst:SetBrain(brain)

    MakeMediumFreezableCharacter(inst, "hound_body")

    inst.OnEntitySleep = OnEntitySleep

    inst:ListenForEvent("newcombattarget", OnNewTarget)
    inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("onattackother", OnAttackOther)

    return inst
end

local function commonfn()
    local inst = fn()

    if not TheWorld.ismastersim then
        return inst
    end

    MakeMediumBurnableCharacter(inst, "hound_body")

    return inst
end

local function poisonfn()
    local inst = fn()

    inst.AnimState:SetBuild("snake_yellow_build")

    if not TheWorld.ismastersim then
        return inst
    end

    MakeMediumBurnableCharacter(inst, "hound_body")

    return inst
end

local function firefn()
    local inst = fn()

    inst.AnimState:SetBuild("snake_yellow_build")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.last_spit_time = nil
    inst.last_target_spit_time = nil
    inst.spit_interval = math.random(20, 30)
    inst.num_targets_vomited = 0

    inst.components.health.fire_damage_scale = 0

    inst.components.lootdropper.numrandomloot = 3
    inst.components.lootdropper:AddRandomLoot("ash", .25)
    inst.components.lootdropper:AddRandomLoot("charcoal", .25)

    MakeLargePropagator(inst)
    inst.components.propagator.decayrate = 0

    return inst
end

return Prefab("ptribe_snake", commonfn, assets, prefabs),
    Prefab("ptribe_snake_poison", poisonfn, assets, prefabs),
    Prefab("ptribe_snake_fire", firefn, assets, prefabs)
