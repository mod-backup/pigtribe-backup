local Utils = require("ptribe_utils/utils")

local assets =
{
    Asset("ANIM", "anim/quagmire_coins.zip"),
}

local prefabs =
{
    "quagmire_coin_fx",
}

local function fxfn()
    local inst = CreateEntity()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    if not TheWorld.ismastersim then
        inst.entity:SetCanSleep(false)
    end
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddFollower()

    inst.AnimState:SetBank("quagmire_coins")
    inst.AnimState:SetBuild("quagmire_coins")
    inst.AnimState:PlayAnimation("opal_loop", true)
    inst.AnimState:SetLightOverride(0.1)
    -- inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    return inst
end

local function MakeCoin(data)
    local id = data.id
    local hasfx = data.hasfx

    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    inst.AnimState:SetBank("quagmire_coins")
    inst.AnimState:SetBuild("quagmire_coins")
    inst.AnimState:PlayAnimation("idle")

    if id > 1 then
        inst.AnimState:OverrideSymbol("coin01", "quagmire_coins", "coin0" .. tostring(id))
        inst.AnimState:OverrideSymbol("coin_shad1", "quagmire_coins", "coin_shad" .. tostring(id))
    end

    inst.pickupsound = "metal"

    inst:AddTag("oinc")

    inst.entity:SetPristine()

    if not TheNet:IsDedicated() then
        if hasfx then
            inst.oincFx = fxfn()
            inst.oincFx.entity:SetParent(inst.entity)
            inst.oincFx.Follower:FollowSymbol(inst.GUID, "coin_shad1", nil, nil, nil, true)
        end
    end

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("edible")
    inst.components.edible.foodtype = FOODTYPE.ELEMENTAL
    inst.components.edible.hungervalue = math.pow(10, id - 1)

    inst:AddComponent("tradable")
    inst.components.tradable.goldvalue = math.pow(10, id - 1)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem:SetSinks(true) --也许我不应该让钱掉水里
    inst.components.inventoryitem.atlasname = "images/inventoryimages2.xml"
    inst.components.inventoryitem.imagename = "quagmire_coin" .. id

    inst:AddComponent("stackable")

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("common/inventory/oinc", Utils.FnParameterExtend(MakeCoin, { id = 1 }), assets),
    Prefab("common/inventory/oinc10", Utils.FnParameterExtend(MakeCoin, { id = 2 }), assets),
    Prefab("common/inventory/oinc100", Utils.FnParameterExtend(MakeCoin, { id = 3 }), assets),
    Prefab("common/inventory/oinc1000", Utils.FnParameterExtend(MakeCoin, { id = 4, hasfx = true }), assets, prefabs)
-- Prefab("quagmire_coin_fx", fxfn, assets)
