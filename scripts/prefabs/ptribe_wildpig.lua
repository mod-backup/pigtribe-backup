local Utils = require("ptribe_utils/utils")
local Constructor = require("ptribe_utils/constructor")
local WildPigUtils = require("ptribe_wildpigutils")

local brain = require "brains/ptribe_wildpig"

local function ShouldAcceptItem(inst, item)
    return false
end

local function SetType(inst, type)
    inst.type = type
    inst:AddTag("ptribe_wildpig_" .. type)

    local d = WildPigUtils.PIGS[type]
    if d.Init then
        d.Init(inst)
    end

    if d.AcceptTest then
        inst.components.trader:SetAcceptTest(d.AcceptTest)
    end
    if d.OnAccept then
        inst.components.trader:SetOnAccept(d.OnAccept)
    end
end

--- 消失
local function OnRemove(inst)
    if not inst.type then return end

    local d = WildPigUtils.PIGS[inst.type]
    if d and d.Remove then
        d.Remove(inst)
    end
end

--- 疯猪变回来
local function SetnormalAfter(retTab, inst)
    inst:SetBrain(brain)
end

--- 晚上自己吃一个发光浆果
local function OnIsNight(inst, isnight)
    if isnight
        and not IsEntityDead(inst)
        and not inst.components.inventory:IsFull() then
        local item = SpawnPrefab("wormlight")
        inst.components.inventory:GiveItem(item)
    end
end

local function OnSaveAfter(retTab, inst, data)
    data.type = inst.type
end

local function OnLoadAfter(retTab, inst, data)
    if not data then return end
    if data.type then
        inst.type = data.type
        inst:DoTaskInTime(0, SetType, inst.type)
    end
end

local function NameDirty(inst)
    local name = inst.replica.named._name:value()

    if not inst.Label then
        inst.entity:AddLabel()
        inst.Label:SetFontSize(24)
        inst.Label:SetFont(BODYTEXTFONT)
        inst.Label:SetWorldOffset(0, 3, 0)
        inst.Label:SetColour(1, 1, 1)
        inst.Label:Enable(true)
    end
    inst.Label:SetText(name)
end

local function Init(inst)
    inst:SetPrefabNameOverride("pigman")

    if not TheNet:IsDedicated() then
        inst:ListenForEvent("namedirty", NameDirty)
    end

    if not TheWorld.ismastersim then return end

    inst.type = nil --类型
    inst.SetType = SetType

    inst.components.named.possiblenames = STRINGS.PTRIBE_WILD_PIG_NAMES
    inst.components.named:PickNewName()

    inst:AddComponent("entitytracker")

    inst.components.trader:SetAcceptTest(ShouldAcceptItem)
    -- inst.components.trader.onaccept = OnGetItemFromPlayer
    inst.components.trader.deleteitemonaccept = true

    inst:AddComponent("vanish_on_sleep")
    -- inst.components.vanish_on_sleep.vanishfn = Vanish

    Utils.FnDecorator(inst.components.werebeast, "onsetnormalfn", nil, SetnormalAfter)

    inst:ListenForEvent("onremove", OnRemove) --不管被打死还是脱离加载范围移除
    inst:WatchWorldState("isnight", OnIsNight)

    inst:SetBrain(brain)

    inst.persists = false

    Utils.FnDecorator(inst, "OnSave", nil, OnSaveAfter)
    Utils.FnDecorator(inst, "OnLoad", nil, OnLoadAfter)
end

return Constructor.CopyPrefab("ptribe_wildpig", "pigman", {
    init = Init
})
