-- 基地投影兼容
-- 即便不兼容了，该文件也不应该报错
-- 基地投影的蓝图数据转换格式后的文件以ptribe_为前缀，我只能创建，无法主动删除，虽然占不了多大空间，不过不是很舒服

local Utils = require("ptribe_utils/utils")
local BP = require("ptribe_blueprints")

local FN = {}

local ERRMSG = {} --错误日志，找个时机发送给玩家

local function GetBluePrintSize(blueprint)
    if #blueprint.data <= 0 then return { 0, 0 } end

    local minX, maxX, minZ, maxZ = blueprint.data[1].x, blueprint.data[1].x, blueprint.data[1].z, blueprint.data[1].z
    for _, p in ipairs(blueprint.data) do
        minX = math.min(minX, p.x)
        maxX = math.max(maxX, p.x)
        minZ = math.min(minZ, p.z)
        maxZ = math.max(maxZ, p.z)
    end

    return { math.ceil((maxZ - minZ) / 4), math.ceil((maxX - minX) / 4) }
end

-- 这里要获取的数据有：预制体名、预制体的x和z，并且检验是否有猪人房
-- 两种蓝图数据格式之间的转换，如果基地投影改变格式，该函数也应该重写
local function ConvertBlueprint(blueprint)
    if #blueprint.data <= 0 then return nil end

    local hasPigHouse = false
    -- 计算蓝图范围
    local minX, maxX, minZ, maxZ = blueprint.data[1].x, blueprint.data[1].x, blueprint.data[1].z, blueprint.data[1].z
    for _, p in ipairs(blueprint.data) do
        hasPigHouse = hasPigHouse or p.prefab == "pighouse"

        minX = math.min(minX, p.x)
        maxX = math.max(maxX, p.x)
        minZ = math.min(minZ, p.z)
        maxZ = math.max(maxZ, p.z)
    end

    if not hasPigHouse then
        return "蓝图 " .. blueprint.name .. " 没有猪人房，无法导入" --也许我应该抛异常
    end

    -- 不应该从最低点的坐标开始，而且最低点所在地皮的最低点开始
    minX = math.floor((minX - 2) / 4) * 4 + 2
    minZ = math.floor((minZ - 2) / 4) * 4 + 2

    -- 把预制体划分到不同的模块中
    local row = math.ceil((maxX - minX) / 4)
    local col = math.ceil((maxZ - minZ) / 4)

    local tab = Utils.CreateEmptyTable(row, col, BP.CM) --空模块
    for _, p in ipairs(blueprint.data) do
        local dx = p.x - minX
        local dz = p.z - minZ
        local r = math.clamp(math.ceil(dx / 4), 1, row)
        local c = math.clamp(math.ceil(dz / 4), 1, col)

        -- print("判断", row, col, r, c, tab[r] and tab[r][c] and tab[r][c].prefabs)
        -- print("添加预制体", p.prefab, r, dx, dx % 4, p.x, minX)
        local x = dx % 4 == 0 and (dx >= 4 and 4 or 0) or dx % 4
        local z = dz % 4 == 0 and (dz >= 4 and 4 or 0) or dz % 4
        table.insert(tab[r][c].prefabs, BP.CP(p.prefab, 1, nil, { x, z }))
    end
    return BP.AddBluePrint(blueprint.name, tab)
end

--- 初始化，读取PigmanTribeBluePrint/blueprints.json中记录的蓝图数据，添加到全局蓝图库中
function FN.ReadPigmanTribeBluePrintDir()
    local status, result = Utils.ReadJsonFile("PigmanTribeBluePrint/blueprints.json")
    if status then
        for _, filename in ipairs(result) do
            -- 先看有没有已经转换过的文件
            local status2, file = Utils.ReadJsonFile("PigmanTribeBluePrint/ptribe_" .. filename)
            if status2 and file then
                BP.AddBluePrint(file.name, file.blueprint)
            else
                -- 重新解析并转换
                status2, file = Utils.ReadJsonFile("PigmanTribeBluePrint/" .. filename)
                if status2 then
                    local status3, data = pcall(ConvertBlueprint, file)
                    if status3 then
                        if type(data) == "table" then
                            -- 生成转换后文件，下次不再转换
                            local wf = io.open("PigmanTribeBluePrint/ptribe_" .. filename, "w")
                            if wf then
                                wf:write(json.encode(data))
                                wf:close()
                            end
                        else
                            print(data) --不合格的蓝图
                            table.insert(ERRMSG, data)
                        end
                    else
                        local msg = "基地投影的蓝图解析失败，请联系mod作者修复。" .. data
                        print(msg)
                        table.insert(ERRMSG, msg)
                    end
                else
                    print(file)
                    table.insert(ERRMSG, file)
                end
            end
        end
    else
        -- 默认下没有该文件，找不到索引文件不进行处理
        -- print(result)
        -- table.insert(ERRMSG, result)
    end
end

----------------------------------------------------------------------------------------------------
-- 检查蓝图是否有猪人房
local function CheckBlueprint(blueprint)
    for _, p in ipairs(blueprint.data) do
        if p.prefab == "pighouse" then
            return true
        end
    end
    return false
end

---读取内存中的蓝图数据，需要注意调用时机
function FN.GetBlueprintBaseInfo()
    local res = {}
    if BSPJ then
        -- 预设蓝图，虽然没有猪人房也没用，不过这里还是读取一下吧
        if BSPJ.BLUE_PRINTS then
            for _, blueprint in ipairs(BSPJ.BLUE_PRINTS) do
                if CheckBlueprint(blueprint) then
                    local size = GetBluePrintSize(blueprint)
                    res[blueprint.name] = {
                        width = size[1],
                        height = size[2],
                        enable = false --服务端初始没有该蓝图
                    }
                end
            end
        end
        -- 缓存文件中读取的蓝图
        if BSPJ.DATA.RECORDS then
            for _, blueprint in ipairs(BSPJ.DATA.RECORDS) do
                if CheckBlueprint(blueprint) then
                    local size = GetBluePrintSize(blueprint)
                    res[blueprint.name] = {
                        width = size[1],
                        height = size[2],
                        enable = false
                    }
                end
            end
        end
    end

    return res
end

---通过蓝图名获取格式转换后的本地蓝图数据
function FN.GetBlueprintByName(name)
    if not BSPJ then return end

    if BSPJ.DATA.RECORDS then
        for _, blueprint in ipairs(BSPJ.DATA.RECORDS) do
            if blueprint.name == name then
                return ConvertBlueprint(blueprint).blueprint --该操作也会将蓝图数据缓存进全局蓝图中
            end
        end
    end

    if BSPJ.BLUE_PRINTS then
        for _, blueprint in ipairs(BSPJ.BLUE_PRINTS) do
            if blueprint.name == name then
                return ConvertBlueprint(blueprint).blueprint --该操作也会将蓝图数据缓存进全局蓝图中
            end
        end
    end
end

function FN.PopErrorMessage()
    local res = ERRMSG
    ERRMSG = {}
    return res
end

return FN
