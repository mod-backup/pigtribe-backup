local GetPrefab = require("ptribe_utils/getprefab")

local FN = {}

---任务先分五级吧，一级到五级越来越难，奖励越来越丰富
FN.TASKS = {}
--- 每一阶的任务数量统计
FN.TASK_LEVEL_COUNT = { 0, 0, 0, 0, 0 }

local function DefaultGetTargetPos(self)
    return self.pig:GetPosition()
end

local function DefaultFinishTask(self)
    if FN.TryRemoveInventoryItem(self, shallowcopy(self.task.condition)) then
        return true
    else
        return false, STRINGS.PTRIBE_PIG_TASK.GENERIC.INSUFFICIENT_MATERIAL
    end
end

local function SortByLevel(a, b)
    return a.level < b.level
end

-- 每个任务可把数据保存在self.taskData中，组件会默认保存和读取数据，取消时会自动丢弃数据，因此不能保存实体对象，不应该保存大量数据
-- 很多任务都是在玩家靠近生成点时才生成的，多人游戏时队友也行会发现建筑和敌人突然就出现了
-- OnInit = OnInit, OnSave = OnSave, GetTargetPos = GetTargetPos, OnUpdate = OnUpdate, FinishTask = FinishTask,
-- OnEnd = OnEnd

---添加任务
---@param level number 任务等级，1-5
---@param name string 任务名
---@param desc string 任务描述，猪人的口吻
---@param condition string|table 任务达成条件
---@param reward table 奖励
---@param data table|nil {time, OnInit, OnSave, GetTargetPos, OnUpdate, FinishTask, OnEnd}
function FN.AddTask(level, name, desc, condition, reward, data)
    data = data or {}
    table.insert(FN.TASKS, {
        level = level,
        name = name,
        desc = desc,
        condition = condition,
        reward = reward,
        time = data.time or TUNING.TOTAL_DAY_TIME * 2, --任务关闭限时，默认两天

        --玩家接受任务时/游戏加载时,(self,isLoading) -> (status,msg)
        OnInit = data.OnInit,
        --保存时，在staskData保存前执行
        OnSave = data.OnSave,
        --获取任务目标位置函数，默认执行发布任务的猪人，(self) -> (status,msg)，status为false时取消任务
        GetTargetPos = data.GetTargetPos or DefaultGetTargetPos,
        --刷帧，用于判断任务是否触发
        OnUpdate = data.OnUpdate,
        --任务完成函数，默认判断玩家物品栏道具是否满足条件要求，(self) -> (status,msg)
        FinishTask = data.FinishTask or DefaultFinishTask,
        --当任务结束时，取消任务和完成任务都会调用，(self, taskFinished, isInValid)
        OnEnd = data.OnEnd,
    })

    FN.TASK_LEVEL_COUNT[level] = FN.TASK_LEVEL_COUNT[level] + 1

    -- 重新排序
    table.sort(FN.TASKS, SortByLevel)
end

---尝试几次找生成点，如果找不到就算了，返回nil，任务不执行
function FN.SpawnPos(pos, radius)
    local spawnPos
    for i = 0, 4 do
        spawnPos = GetPrefab.GetSpawnPoint(pos, radius + i * 8, 12)
        if spawnPos then
            return spawnPos
        end
    end
end

---尝试移除玩家物品栏物品，如果足够就移除并返回true，否则就返回false
function FN.TryRemoveInventoryItem(self, prefabs)
    return GetPrefab.TryRemoveInventoryItem(self.inst.components.inventory, prefabs)
end

---init中初始化一个pos
---@param self Component
---@param isLoading boolean
---@param radius number|nil 默认200~300
---@param msg string|nil 默认“没有找到合适的生成位置”
function FN.DefaultInit(self, isLoading, loadInitFn, radius, msg)
    if isLoading then
        if loadInitFn then
            loadInitFn(self)
        end
        return true
    end

    self.taskData.pos = FN.SpawnPos(self.inst:GetPosition(), radius or math.random(200, 300))
    if self.taskData.pos then
        return true
    else
        return false, msg or STRINGS.PTRIBE_PIG_TASK.GENERIC.NOT_POS
    end
end

function FN.DefaultGetTargetPos(self, name)
    if self.taskData.isSpawned then
        local target = self.pig.components.entitytracker:GetEntity(name)
        if target then
            return target:GetPosition()
        end

        return self.pig:GetPosition()
    else
        return self.taskData.pos
    end
end

---默认生成函数
---@param self Component
---@param spawnFn function
---@param disSq number|nil
function FN.DefaultOnUpdate(self, spawnFn, disSq)
    if self.taskData.isSpawned then return end

    if self.inst:GetPosition():DistSq(self.taskData.pos) < (disSq or math.random(700, 900)) then
        spawnFn(self, self.taskData.pos)
        self.taskData.isSpawned = true
    end
end

function FN.DefaultFinishTask(self, unSpawnMsg, survivalMsg, name)
    if not self.taskData.isSpawned then
        return false, unSpawnMsg or STRINGS.PTRIBE_PIG_TASK.GENERIC.TARGET_NOT_DEFEATED
    end

    local ent = self.pig.components.entitytracker:GetEntity(name)
    if ent then
        return false, survivalMsg or STRINGS.PTRIBE_PIG_TASK.GENERIC.TARGET_NOT_DEFEATED
    end

    return true
end

function FN.RemoveEntity(self, name)
    local ent = self.pig.components.entitytracker:GetEntity(name)
    if ent then
        self.pig.components.entitytracker:ForgetEntity(name)
        if not ent:IsAsleep() then --作为是否在加载范围内的判断
            SpawnAt("small_puff", ent)
        end
        ent:Remove()
    end
end

function FN.DefaultOnEnd(self)
    if self.pig:IsValid() then
        GetPrefab.EntityTrackerForgetAllEntity(self.pig.components.entitytracker)
    end
end

return FN
