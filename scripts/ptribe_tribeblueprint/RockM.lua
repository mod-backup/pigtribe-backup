return {
    name = "【预设】巨岩氏族中",
    blueprint = {
        tab = {
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "ptribe_pigtorch",
                            pos = {
                                3.5,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                3.5,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                0.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_daywalker",
                            pos = {
                                3.975997924805,
                                0.027954101559999
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                3.5,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                0.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar",
                            pos = {
                                3,
                                3
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                0.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "punchingbag",
                            pos = {
                                4,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                3.643371582031,
                                0.016693115229998
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "punchingbag",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "ptribe_pigtorch",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                3.833927154541,
                                3.72932434082
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pigshrine",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                3.89058303833,
                                0.15774536133
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_daywalker",
                            pos = {
                                3.927749633789,
                                0.72425842285
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "ptribe_pigtorch",
                            pos = {
                                4,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_daywalker",
                            pos = {
                                0.098949432372997,
                                3.90760803223
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "punchingbag",
                            pos = {
                                4,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pigskin",
                            pos = {
                                0.904777526855,
                                0.32960510254
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "punchingbag",
                            pos = {
                                4,
                                3
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                0.06042098999,
                                3.98794555664
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                3.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar",
                            pos = {
                                1,
                                3
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar",
                            pos = {
                                1,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                3.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.5,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                3.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_daywalker",
                            pos = {
                                0.175846099854,
                                0.079330444340002
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.5,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "ptribe_pigtorch",
                            pos = {
                                0.5,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_moonrock",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            }
        },
        optionalPoints = {
            {
                row = 3,
                col = 3
            },
            {
                row = 3,
                col = 8
            },
            {
                row = 5,
                col = 3
            },
            {
                row = 5,
                col = 7
            },
            {
                row = 8,
                col = 3
            },
            {
                row = 8,
                col = 8
            }
        },
        height = 10,
        width = 10
    }
}
