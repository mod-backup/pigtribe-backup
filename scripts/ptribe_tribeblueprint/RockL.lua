return {
    name = "【预设】巨岩氏族大完",
    blueprint = {
        tab = {
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_bearger",
                            pos = {
                                3.503000259399,
                                0.024002075200002
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar",
                            pos = {
                                2.145000457764,
                                1.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                0.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                0.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                3.826000213623,
                                0.033004760739999
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                3.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                3.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar",
                            pos = {
                                2.145000457764,
                                1.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                3.780000686646,
                                3.91200256348
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                0.030000686646002,
                                3.88400268555
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                0.031000137328999,
                                3.73300170898
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                0.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "deco_lamp_ceramic",
                            pos = {
                                2.145000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                3.895000457764,
                                3.94900512695
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                0.177000045776,
                                0.03500366211
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "deco_lamp_ceramic",
                            pos = {
                                2.145000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                0.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                0.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                0.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                0.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                0.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                0.125,
                                3.88200378418
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cave_banana_tree",
                            pos = {
                                0.379001617432,
                                0.16900634766
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                3.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                3.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                3.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                0.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "oasis_cactus",
                            pos = {
                                3.993000030518,
                                3.90600585937
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_daywalker",
                            pos = {
                                3.798000335693,
                                3.95700073242
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "ptribe_pigtorch",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mighty_gym",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "basalt_pillar",
                            pos = {
                                3.881000518799,
                                3.70100402832
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "ptribe_pigtorch",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "oasis_cactus",
                            pos = {
                                0.236000061035,
                                3.81700134277
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cactus",
                            pos = {
                                3.875,
                                3.84300231934
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cactus",
                            pos = {
                                0.034000396728999,
                                3.41700744629
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lava_pond_rock4",
                            pos = {
                                3.575241088867,
                                1.59306335449
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lava_pond_rock5",
                            pos = {
                                2.883068084717,
                                2.01937866211
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pigshrine",
                            pos = {
                                3.999000549316,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_daywalker",
                            pos = {
                                0.034000396728999,
                                0.33500671387
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_antlion",
                            pos = {
                                0.013000488281001,
                                2.63000488281
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "oasis_cactus",
                            pos = {
                                3.956001281738,
                                0.10600280762
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "oasis_cactus",
                            pos = {
                                0.22200012207,
                                3.73100280762
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cactus",
                            pos = {
                                0.111000061035,
                                3.69000244141
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mighty_gym",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_daywalker",
                            pos = {
                                3.916000366211,
                                3.70100402832
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "basalt_pillar",
                            pos = {
                                0.187000274658,
                                3.79200744629
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lava_pond",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_daywalker",
                            pos = {
                                3.957000732422,
                                0.072006225590002
                            }
                        },
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_deerclops",
                            pos = {
                                0.048000335692997,
                                2.17300415039
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cactus",
                            pos = {
                                0.111000061035,
                                3.49000549316
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "oasis_cactus",
                            pos = {
                                0.152000427246,
                                3.90600585937
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "ptribe_pigtorch",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "basalt_pillar",
                            pos = {
                                0.229000091553,
                                3.92100524902
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "ptribe_pigtorch",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                3.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                0.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                0.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                0.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.645000457764,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mighty_gym",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                3.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                3.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                3.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                3.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                3.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "deco_lamp_ceramic",
                            pos = {
                                2.145000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.645000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "deco_lamp_ceramic",
                            pos = {
                                2.145000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                3.645000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                1.645000457764,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                0.145000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.643997192383,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                0.145000457764,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.643997192383,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.643997192383,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                0.145000457764,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.643997192383,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.643997192383,
                                1.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                0.145000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                0.145000457764,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.645000457764,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar",
                            pos = {
                                2.143997192383,
                                1.52000427246
                            }
                        },
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                0.143997192383,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.643997192383,
                                2.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.143997192383,
                                3.52000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.643997192383,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.643997192383,
                                2.02000427246
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                0.143997192383,
                                3.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.643997192383,
                                1.02000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_ruins",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar",
                            pos = {
                                2.143997192383,
                                1.52000427246
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "chesspiece_moosegoose",
                            pos = {
                                0.09700012207,
                                3.31100463867
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            }
        },
        optionalPoints = {
            {
                row = 3,
                col = 3
            },
            {
                row = 3,
                col = 12
            },
            {
                row = 4,
                col = 7
            },
            {
                row = 5,
                col = 5
            },
            {
                row = 5,
                col = 10
            },
            {
                row = 10,
                col = 5
            },
            {
                row = 10,
                col = 10
            },
            {
                row = 12,
                col = 3
            },
            {
                row = 12,
                col = 7
            },
            {
                row = 12,
                col = 12
            }
        },
        height = 17,
        width = 15
    }
}
