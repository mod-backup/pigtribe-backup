return {
    name = "【预设】风语氏族中",
    blueprint = {
        tab = {
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 13,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                4,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_light2",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 9,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                3.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                3
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                3.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                3.5,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                3.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 9,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_light2",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 10,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                4,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.5,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                2,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 9,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                4,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                2,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lawnornament_1",
                            pos = {
                                3.998462677002,
                                3.98902893066
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                4,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                0.015871047973999,
                                0.0097503662099996
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                4,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lawnornament_1",
                            pos = {
                                3.977130889893,
                                0.011840820309999
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 9,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                4,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                2,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                4,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                4,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                0.5
                            }
                        }
                    }
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pigshrine",
                            pos = {
                                4,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.97766494751,
                                0.59443664551
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                4,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    }
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                0.061229705811002,
                                0.1949005127
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                4,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                2.5
                            }
                        }
                    }
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                4,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                4,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                4,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                4,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                4,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                4,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    }
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 9,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                2,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                4,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_light2",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lawnornament_1",
                            pos = {
                                0.04878616333,
                                3.61393737793
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.877140045166,
                                0.0002593994099982
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_light2",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lawnornament_1",
                            pos = {
                                0.232303619385,
                                0.024978637700002
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 6,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                2,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 11,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                2,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 9,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                0.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                0.5,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                4,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                0.5,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_farm",
                            pos = {
                                0.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 10,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "sapling",
                            pos = {
                                3.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                4,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            }
        },
        optionalPoints = {
            {
                row = 4,
                col = 5
            },
            {
                row = 4,
                col = 6
            },
            {
                row = 5,
                col = 4
            },
            {
                row = 5,
                col = 7
            },
            {
                row = 6,
                col = 5
            },
            {
                row = 6,
                col = 6
            }
        },
        height = 10,
        width = 10
    }
}
