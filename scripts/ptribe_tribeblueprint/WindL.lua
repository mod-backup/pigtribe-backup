return {
    name = "【预设】风语氏族大完",
    blueprint = {
        tab = {
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.832000732422,
                                3.99000549316
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.631000518799,
                                3.99000549316
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.798000335693,
                                0.22500610352
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.832000732422,
                                3.99000549316
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                0.078002929690001
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.798000335693,
                                0.22500610352
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.631000518799,
                                3.99000549316
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                3.935001373291,
                                3.88800048828
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_light2",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "moon_tree",
                            pos = {
                                3.916000366211,
                                3.75500488281
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "saltbox",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                3.416000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "saltbox",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                0.198001861572,
                                0.22500610352
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "moon_tree",
                            pos = {
                                3.998001098633,
                                3.97500610352
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_light2",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush",
                            pos = {
                                3.916000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush",
                            pos = {
                                1.916000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                3.916000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.416000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.416000366211,
                                2.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.416000366211,
                                0.078002929690001
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.416000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                2.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                3.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                3.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                1.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                2.416000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pond_cave",
                            pos = {
                                3.860000610352,
                                3.60800170898
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_plant",
                            pos = {
                                3.38204574585,
                                1.70321655273
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pigshrine",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "oceantree_pillar",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                0.416000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                0.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                0.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                0.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                2.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush",
                            pos = {
                                3.916000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                3.916000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_plant",
                            pos = {
                                0.764083862305,
                                1.78956604004
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                2.916000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.916000366211,
                                2.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                3.416000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.416000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "moon_tree",
                            pos = {
                                3.993000030518,
                                3.89100646973
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                2.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.916000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "moon_tree",
                            pos = {
                                3.993000030518,
                                0.070007324220001
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower",
                            pos = {
                                1.416000366211,
                                3.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_light2",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 7,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 9,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "madscience_lab",
                            pos = {
                                0.416000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 8,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                3.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushroom_light2",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 14,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                2.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 13,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 10,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                3.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                3.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                0.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 14,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 14,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 15,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 10,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                3.416000366211,
                                3.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                3.07800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                3.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                2.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 7,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                0.078002929690001
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 8,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                3.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 8,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                2.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                3.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                1.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                1.916000366211,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "grass",
                            pos = {
                                0.91600036621099,
                                0.57800292969
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_wood",
                            pos = {
                                1.416000366211,
                                3.07800292969
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            }
        },
        optionalPoints = {
            {
                row = 4,
                col = 7
            },
            {
                row = 5,
                col = 3
            },
            {
                row = 5,
                col = 9
            },
            {
                row = 6,
                col = 2
            },
            {
                row = 6,
                col = 10
            },
            {
                row = 10,
                col = 2
            },
            {
                row = 10,
                col = 10
            },
            {
                row = 11,
                col = 3
            },
            {
                row = 11,
                col = 9
            },
            {
                row = 12,
                col = 6
            }
        },
        height = 14,
        width = 13
    }
}
