return {
    name = "【预设】巨岩氏族小",
    blueprint = {
        tab = {
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                2.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                2.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                4,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pigshrine",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                1.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_stone",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_fruit_sprout_sapling",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack",
                            pos = {
                                4,
                                4
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "rock_avocado_bush",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.ROCKY
                }
            }
        },
        optionalPoints = {
            {
                row = 3,
                col = 5
            },
            {
                row = 4,
                col = 5
            },
            {
                row = 5,
                col = 5
            }
        },
        height = 8,
        width = 8
    }
}
