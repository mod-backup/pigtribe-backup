return {
    name = "【预设】幽噬氏族中",
    blueprint = {
        tab = {
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                3.816226959229,
                                0.086654663090002
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                3.848617553711,
                                3.87442016602
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                0.0092945098880008,
                                3.96322631836
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                0.10933303833,
                                3.51707458496
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                0.171459197998,
                                3.65074157715
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                3.94372177124,
                                3.87738037109
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                3.875675201416,
                                3.97061157227
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                0.030885696411001,
                                3.75787353516
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                0.238872528076,
                                0.068740844730002
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                0.01615524292,
                                0.065292358400001
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wormlight_plant",
                            pos = {
                                0.249294281006,
                                3.62474060059
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "atrium_light",
                            pos = {
                                3.596015930176,
                                3.88055419922
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wormlight_plant",
                            pos = {
                                0.075012207031001,
                                3.44592285156
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack_hermit",
                            pos = {
                                3.075981140137,
                                0.86640930176
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack_hermit",
                            pos = {
                                0.549892425537,
                                3.60020446777
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "atrium_light",
                            pos = {
                                3.98819732666,
                                3.7922668457
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                3.947929382324,
                                3.52220153809
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                0.079750061035,
                                0.20774841309
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wormlight_plant",
                            pos = {
                                0.057006835937997,
                                0.039199829099999
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighead",
                            pos = {
                                0.066253662108998,
                                3.43562316895
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "atrium_light",
                            pos = {
                                0.022399902343999,
                                0.36061096191
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighead",
                            pos = {
                                0.0018081665039986,
                                3.65849304199
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wormlight_plant",
                            pos = {
                                0.055068969727003,
                                3.98612976074
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                3.871795654297,
                                3.85661315918
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wormlight_plant",
                            pos = {
                                3.980369567871,
                                0.0057830810499979
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "atrium_light",
                            pos = {
                                3.884311676025,
                                0.24333190918
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighead",
                            pos = {
                                3.981224060059,
                                3.73390197754
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "atrium_overgrowth",
                            pos = {
                                0.197212219238,
                                3.33047485352
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "atrium_light",
                            pos = {
                                3.586570739746,
                                3.9810333252
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighead",
                            pos = {
                                0.088226318358998,
                                3.53318786621
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "atrium_light",
                            pos = {
                                0.0073394775389986,
                                3.98170471191
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wormlight_plant",
                            pos = {
                                0.055335998535,
                                3.8631439209
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack_hermit",
                            pos = {
                                3.513244628906,
                                0.58387756348
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                0.010704040527003,
                                3.99851989746
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                3.72437286377,
                                3.98706054687
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack_hermit",
                            pos = {
                                0.749645233154,
                                3.70436096191
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                0.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wormlight_plant",
                            pos = {
                                3.960582733154,
                                3.5065612793
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                3.854156494141,
                                0.0010223388700012
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                3.938606262207,
                                0.033172607419999
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                3.92431640625,
                                3.96627807617
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                0.098793029785,
                                3.97079467773
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wormlight_plant",
                            pos = {
                                0.174919128418,
                                3.86097717285
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                3.885288238525,
                                0.23472595215
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                0.087253570557003,
                                3.96293640137
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                0.04838180542,
                                3.48846435547
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                3.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                0.021072387695,
                                3.53904724121
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0.5,
                                1.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "marsh_tree",
                            pos = {
                                0.180610656738,
                                3.58995056152
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            }
        },
        optionalPoints = {
            {
                row = 3,
                col = 4
            },
            {
                row = 3,
                col = 6
            },
            {
                row = 5,
                col = 3
            },
            {
                row = 5,
                col = 7
            },
            {
                row = 7,
                col = 4
            },
            {
                row = 7,
                col = 6
            }
        },
        height = 12,
        width = 11
    }
}
