return {
    name = "【预设】幽噬氏族大完",
    blueprint = {
        tab = {
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 5,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                0 }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                3
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                3
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "birdcage",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "bookstation",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack_hermit",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                0,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                2,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar_dreadstone",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar_dreadstone",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "monkeyisland_portal",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.OCEAN_START
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "tentacle_pillar",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar_dreadstone",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "support_pillar_dreadstone",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "meatrack_hermit",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "livingtree_halloween",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "flower_cave_triple",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "mushtree_tall_webbed",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "researchlab3",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 4,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                0
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                0
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {},
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "nightlight",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                2,
                                2
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                3
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                3
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                3
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                1,
                                1
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_dreadstone",
                            pos = {
                                3,
                                1
                            }
                        }
                    },
                    tile = WORLD_TILES.MARSH
                }
            }
        },
        optionalPoints = {
            {
                row = 1,
                col = 4
            },
            {
                row = 1,
                col = 8
            },
            {
                row = 5,
                col = 5
            },
            {
                row = 5,
                col = 7
            },
            {
                row = 6,
                col = 1
            },
            {
                row = 6,
                col = 11
            },
            {
                row = 7,
                col = 5
            },
            {
                row = 7,
                col = 7
            },
            {
                row = 11,
                col = 4
            },
            {
                row = 11,
                col = 8
            }
        },
        height = 11,
        width = 12
    }
}
