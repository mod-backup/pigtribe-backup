return {
    name = "【预设】风语氏族小",
    blueprint = {
        tab = {
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                2.033756256104,
                                1.93794250488
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush2",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lawnornament_1",
                            pos = {
                                3.5,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush2",
                            pos = {
                                2,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush2",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lawnornament_1",
                            pos = {
                                3.5,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                2.077899932861,
                                1.93916320801
                            }
                        }
                    }
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                2.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                2.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                2.5,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                2.5,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                2.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                2.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "acorn_sapling",
                            pos = {
                                1.718845367432,
                                0.55950927734
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "acorn_sapling",
                            pos = {
                                3.732677459717,
                                2.97320556641
                            }
                        }
                    }
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                4,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                0.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                0.5,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.5,
                                0.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pigshrine",
                            pos = {
                                4,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "pighouse",
                            pos = {
                                3.5,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                0.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                4,
                                2
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                3.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "cookpot",
                            pos = {
                                4,
                                4
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                3.5,
                                1.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush_juicy",
                            pos = {
                                2,
                                2
                            }
                        }
                    }
                }
            },
            {
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                0.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                1.5,
                                3.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                1.5,
                                2.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                1.5,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                1.5,
                                3.5
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                1.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                0.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "wall_hay",
                            pos = {
                                1.5,
                                0.5
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                }
            },
            {
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                2.045673370361,
                                2.07691955566
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 3,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush2",
                            pos = {
                                3.5,
                                2.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush2",
                            pos = {
                                2,
                                4
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lawnornament_1",
                            pos = {
                                0.5,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 2,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "berrybush2",
                            pos = {
                                3.5,
                                1.5
                            }
                        },
                        {
                            priority = 1,
                            num = 1,
                            prefab = "lawnornament_1",
                            pos = {
                                0.5,
                                2
                            }
                        }
                    }
                },
                {
                    hasPos = false,
                    priority = -1,
                    num = 0,
                    prefabs = {}
                },
                {
                    hasPos = true,
                    priority = -1,
                    num = 1,
                    prefabs = {
                        {
                            priority = 1,
                            num = 1,
                            prefab = "fast_farmplot",
                            pos = {
                                1.977199554443,
                                1.87721252441
                            }
                        }
                    }
                }
            }
        },
        optionalPoints = {
            {
                row = 3,
                col = 3
            },
            {
                row = 3,
                col = 4
            }
        },
        height = 6,
        width = 6
    }
}
