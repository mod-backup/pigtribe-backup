local ResourceConvert = require("ptribe_resourceconvert")

local FN = {}

local day = TUNING.TOTAL_DAY_TIME

--- 默认武器数据，只限制武器
local WEAPONS = { {
    -- 1
    axe = {},              --斧子
    goldenaxe = {},        --黄金斧头
    hammer = {},           --锤子
    spear_wathgrithr = {}, --战斗长矛

    spear = {              --长矛
        coolDown = day / 2
    },
    halberd = { --战戟
        coolDown = day / 2
    }
}, {
    -- 2
    whip = {},            --三尾猫鞭
    hambat = {},          --火腿棒
    boomerang = {},       --回旋镖
    brush = {},           --刷子
    bugnet = {},          --捕虫网
    cutless = {},         --木头短剑
    farm_hoe = {},        --园艺锄
    golden_farm_hoe = {}, --黄金园艺锄
    fence_rotator = {},   --栅栏击剑
    fishingrod = {},      --淡水钓竿
}, {
    -- 3
    batbat = {},        --蝙蝠棒
    tentaclespike = {}, --触手尖刺
    firestaff = {},     --火魔杖
    icestaff = {},      --冰魔杖
    bullkelp_root = {}, --公牛海带茎
    moonglassaxe = {}   --月光玻璃斧

}, {
    -- 4
    nightsword = {},                 --暗夜剑
    wathgrithr_shield = {},          --战斗圆盾
    glasscutter = {},                --玻璃刀
    spear_wathgrithr_lightning = {}, --奔雷矛
    pocketwatch_weapon = {},         --警钟
    waterplant_bomb = {              --种壳
        coolDown = day / 2
    },
    cane = {},    --步行手杖
    firepen = {}, --火焰笔
}, {
    -- 5
    blowdart_pipe = { --吹箭
        coolDown = day
    },
    multitool_axe_pickaxe = {},              --多用斧镐
    nightstick = {},                         --晨星锤
    ruins_bat = {},                          --铥矿棒
    sword_lunarplant = {},                   --亮茄剑
    voidcloth_scythe = {},                   --暗影收割者
    spear_wathgrithr_lightning_charged = {}, --充能奔雷矛
    houndstooth_blowpipe = {},               --嚎弹炮
    staff_lunarplant = {},                   --亮茄魔杖
    gunpowder = {                            --火药
        coolDown = day
    },
    cannonball_rock_item = { --炮弹
        coolDown = day
    },
    bomb_lunarplant = { --亮茄炸弹
        coolDown = day * 3
    },
    ----------------------------------------------------------------------------------------------------
    shieldofterror = { --恐怖盾牌
        coolDown = day * 10
    },
    trident = { --刺耳三叉戟
        coolDown = day * 12
    },
} }
--- 一个快速查找的索引表
local WEAPON_MIN_LEVEL = {}

FN.ALL_LEVEL = #WEAPONS
FN.MAX_COST = 0
FN.MAX_COOL_DOWN = 0
for level, tab in ipairs(WEAPONS) do
    for name, d in pairs(tab) do
        -- 默认
        d.cost = d.cost
            or (math.ceil(ResourceConvert.WEAPONS[name] * 1.43))                          --假定WEAPONS中的武器在ResourceConvert中一定定义了
        d.coolDown = d.coolDown or day * level
        d.minResource = d.minResource or TUNING.PTRIBE_SPAWN_WEAPON_MIN_RESOURCE + d.cost --启动资源

        WEAPON_MIN_LEVEL[name] = level
        FN.MAX_COST = math.max(FN.MAX_COST, d.cost)
        FN.MAX_COOL_DOWN = math.max(FN.MAX_COOL_DOWN, d.coolDown)
    end
end

--- 默认花费，采用全武器最高花费最高冷却
FN.DEFAULT_DATA = {
    cost = FN.MAX_COST,
    coolDown = FN.MAX_COOL_DOWN,
    minResource = TUNING.PTRIBE_SPAWN_WEAPON_MIN_RESOURCE + FN.MAX_COST
}

--- 自定义武器数据
local function GetWeaponCustomData(name)
    if string.starts(name, "blowdart_") then
        -- 吹箭
        return {
            cost = 12,
            coolDown = day,
            minResource = TUNING.PTRIBE_SPAWN_WEAPON_MIN_RESOURCE + 12
        }
    end

    return FN.DEFAULT_DATA
end

--- 获取生产该武器的相关数据
function FN.GetWeaponData(name)
    local level = WEAPON_MIN_LEVEL[name]
    return WEAPONS[level] and WEAPONS[level][name] or GetWeaponCustomData(name)
end

--- 当前等级能否生成指定武器
function FN.IsAllowSpawn(name, level)
    local minLevel = WEAPON_MIN_LEVEL[name]
    return (minLevel and level >= minLevel)
        or GetWeaponCustomData(name, level) ~= FN.DEFAULT_DATA
        or level >= FN.ALL_LEVEL
end

return FN
