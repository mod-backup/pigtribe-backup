local Utils = require("ptribe_utils/utils")

--- 蓝图表，二维表的列表，每个元素表示一个地皮上的建设，即一个长方形范围
local BLUEPRINTS = {}
--- 预制体名和蓝图名的映射表
local BLUEPRINT_LIST = {}
--- 部落最大大小，根据蓝图最大小计算,width >= height
local MAX_SIZE = {
    width = 1,
    height = 1
}

-- 蓝图库格式样例
-- local blueprints = {
--     -- 每个蓝图
--     ["蓝图名"] = {
--         tab = {
--             {
--                 {
--                     -- 每个模块
--                     prefabs = {
--                         {
--                             prefab = "pighouse",
--                             num = 2,      --预制体数量
--                             priority = 1, --预制体优先级
--                             pos = { 1, 1 }
--                         },
--                     },
--                     tile = WORLD_TILES.GRASS, --地皮
--                     priority = 1,             --模块优先级
--                     num = 1,                  --模块预制体总数
--                     hasPos = true,
--                 },
--             },
--         },
--         width = 1,         --蓝图宽度
--         height = 1,        --蓝图高度
--         optionalPoints = { -- 蓝图的管理者可用点
--             {
--                 row = 1,
--                 col = 1,
--             },
--         }
--     },
-- }

---预制体
---@param prefab string 预制体名
---@param num number|nil 数量，默认1
---@param priority number|nil 优先级，默认1
---@param pos table|nil 预制体位置，pos[1]为x，pos[2]为z
local function CP(prefab, num, priority, pos)
    return {
        prefab = prefab,
        num = num or 1,
        priority = priority or 1,
        pos = pos
    }
end

--- 一个地皮上的预制体表示一个模块
local function CM(prefabs, tile, priority)
    return {
        prefabs = prefabs or {},
        tile = tile,
        priority = prefabs and #prefabs > 0 and (priority or 1) or -1, --优先级默认为1，空的模块优先级最低，为-1
    }
end


local function ModulePrefabsSort(a, b)
    if a.priority == b.priority then
        return a.num < b.num
    else
        return a.priority > b.priority
    end
end

local function AddBluePrint(name, tab)
    local blueprint
    if tab.tab then
        blueprint = tab
    else
        blueprint = {
            tab = tab,
            optionalPoints = {} -- 管理者可用点
        }
        local hei = #tab
        local wid = 0
        for i, row in ipairs(tab) do
            wid = math.max(wid, #row)

            for j, module in ipairs(row) do
                -- 检查是否指定了pos
                local hasPos = false
                for _, p in ipairs(module.prefabs) do
                    if p.pos then
                        hasPos = true
                        break
                    end
                end

                if hasPos then
                    for _, p in ipairs(module.prefabs) do
                        assert(p.pos and p.num == 1, --如果存在pos的prefab，该模块的每个prefab必须都指定pos，并且num为1
                            "blueprint name = " .. name
                            .. "module index = [" .. tostring(i) .. "][" .. tostring(j)
                            .. "] ,The module must specify a pos for all prefab in the module, and the number must be 1")
                    end
                end

                module.hasPos = hasPos
                module.num = module.tile and 1 or 0
                for _, p in pairs(module.prefabs) do
                    module.num = module.num + p.num
                    if p.prefab == "pighouse" then
                        table.insert(blueprint.optionalPoints, { row = i, col = j })
                        break
                    end
                end

                table.sort(module.prefabs, ModulePrefabsSort) --优先级高的预制体排前面
            end
        end

        assert(#blueprint.optionalPoints > 0, "blueprint name = " .. name
            .. " The blueprint must have at least one pighouse, The blueprint indexed ") --至少一个猪人房

        blueprint.width = wid
        blueprint.height = hei
    end

    MAX_SIZE.width = math.max(MAX_SIZE.width, blueprint.width)
    MAX_SIZE.height = math.max(MAX_SIZE.height, blueprint.height)

    BLUEPRINTS[name] = blueprint

    -- 部落蓝图名没有写英文版本的名字，预制体名可能有点儿乱，哈希计算得到的，希望不要重复
    local p = Utils.ChineseToVariable(name, "ptribe_bp")
    BLUEPRINT_LIST[p] = name

    return {
        name = name,
        blueprint = blueprint,
    }
end

----------------------------------------------------------------------------------------------------
--  也许我应该找时间把蓝图名也本地化一下

-- 以命名为索引，因此命名不应该重复
-- 指定位置的模块，其中每一个预制体都应该指定模块并且数量为1，位置为一个地皮左上角为原点，长宽都在[0,4]
-- 蓝图的模块形状必须为矩形，对于某一行即便不需要了也要填充空模块补足缺失的

-- 基本地皮
-- WORLD_TILES.WOODFLOOR 木板地皮
-- WORLD_TILES.DECIDUOUS 桦树林的落叶地皮
-- WORLD_TILES.GRASS 长草地皮
-- WORLD_TILES.CARPET 地毯地皮（牛毛）
-- WORLD_TILES.ROAD 卵石路地皮
-- WORLD_TILES.FARMING_SOIL 可耕地皮
-- WORLD_TILES.SHELLBEACH 贝壳海滩地皮
-- WORLD_TILES.OCEAN_START 浅海地皮
-- WORLD_TILES.MARSH 沼泽地皮
-- WORLD_TILES.ROCKY 岩石地皮

-- 3 * 3 猪人房、避雷针、多汁浆果
AddBluePrint("【预设】猪人房、避雷针、多汁浆果", { {
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
}, {
    CM({ CP("berrybush_juicy", 4) }, WORLD_TILES.GRASS),
    CM({ CP("lightning_rod") }, WORLD_TILES.ROAD),
    CM({ CP("berrybush_juicy", 4) }, WORLD_TILES.GRASS),
}, {
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
} })

-- 3 * 5
AddBluePrint("【预设】猪人房、浆果、草", { {
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("lightning_rod") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
}, {
    CM({ CP("berrybush", 2) }),
    CM({ CP("berrybush", 2) }),
    CM(nil, WORLD_TILES.CARPET),
    CM({ CP("berrybush", 2) }),
    CM({ CP("berrybush", 2) }),
}, {
    CM({ CP("grass", 2) }),
    CM({ CP("grass", 2) }),
    CM(nil, WORLD_TILES.CARPET),
    CM({ CP("grass", 2) }),
    CM({ CP("grass", 2) }),
} })

-- 7 * 7 房子、雪球机、草、树枝、锅、农田
AddBluePrint("【预设】猪人房、雪球机、草、树枝、锅、农田", { {
    CM(),
    CM(),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM(),
    CM(),
}, {
    CM(),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM(),
}, {
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
}, {
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("firesuppressor") }, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
}, {
    CM({ CP("berrybush", 4) }, WORLD_TILES.CARPET),
    CM({ CP("berrybush", 4) }, WORLD_TILES.CARPET),
    CM({ CP("berrybush", 4) }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("fast_farmplot") }, WORLD_TILES.CARPET),
    CM({ CP("fast_farmplot") }, WORLD_TILES.CARPET),
    CM({ CP("fast_farmplot") }, WORLD_TILES.CARPET),
}, {
    CM(),
    CM({ CP("grass", 4) }, WORLD_TILES.CARPET),
    CM({ CP("grass", 4) }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("sapling", 4) }, WORLD_TILES.CARPET),
    CM({ CP("sapling", 4) }, WORLD_TILES.CARPET),
    CM(),
}, {
    CM(),
    CM(),
    CM({ CP("cookpot") }, WORLD_TILES.CARPET),
    CM({ CP("cookpot") }, WORLD_TILES.CARPET),
    CM({ CP("cookpot") }, WORLD_TILES.CARPET),
    CM(),
    CM(),
} })

-- 3 * 7，虽然规模小，但是非常富有
AddBluePrint("【预设】短小精悍", { {
    CM({ CP("mushroom_light") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("lightning_rod") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("mushroom_light") }, WORLD_TILES.CARPET),
}, {
    CM({ CP("pigtorch") }, WORLD_TILES.CARPET),
    CM({ CP("pigtorch") }, WORLD_TILES.CARPET),
    CM({ CP("treasurechest") }, WORLD_TILES.ROAD),
    CM({ CP("mushroom_light") }, WORLD_TILES.ROAD),
    CM({ CP("treasurechest") }, WORLD_TILES.ROAD),
    CM({ CP("pigtorch") }, WORLD_TILES.CARPET),
    CM({ CP("pigtorch") }, WORLD_TILES.CARPET),
}, {
    CM({ CP("berrybush", 4) }, WORLD_TILES.CARPET),
    CM({ CP("grass", 4) }, WORLD_TILES.CARPET),
    CM({ CP("cookpot") }, WORLD_TILES.CARPET),
    CM({ CP("icebox") }, WORLD_TILES.CARPET),
    CM({ CP("cookpot") }, WORLD_TILES.CARPET),
    CM({ CP("sapling", 4) }, WORLD_TILES.CARPET),
    CM({ CP("rock_avocado_bush", 4) }, WORLD_TILES.CARPET),
}, })

AddBluePrint("【预设】大杂烩", { {
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("treasurechest", 2) }, WORLD_TILES.CARPET),
    CM({ CP("icebox", 2) }, WORLD_TILES.CARPET),
}, {
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("saltbox", 2) }, WORLD_TILES.CARPET),
    CM({ CP("cookpot") }, WORLD_TILES.CARPET),
}, {
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("mushroom_light") }, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
}, {
    CM({ CP("slow_farmplot") }, WORLD_TILES.CARPET),
    CM({ CP("fast_farmplot") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("madscience_lab") }, WORLD_TILES.CARPET),
    CM({ CP("birdcage") }, WORLD_TILES.CARPET),
}, {
    CM({ CP("mushroom_farm") }, WORLD_TILES.CARPET),
    CM({ CP("wood_table_round") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("wintersfeastoven") }, WORLD_TILES.CARPET),
    CM({ CP("table_winters_feast") }, WORLD_TILES.CARPET),
}, {
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("mushroom_light") }, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
}, {
    CM({ CP("meatrack") }, WORLD_TILES.CARPET),
    CM({ CP("meatrack_hermit") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.CARPET),
    CM({ CP("winter_treestand") }, WORLD_TILES.CARPET),
}, {
    CM({ CP("ptribe_carnival_prizebooth") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("wormlight_plant", 4) }, WORLD_TILES.CARPET),
    CM({ CP("oasis_cactus", 2), CP("cactus", 2) }, WORLD_TILES.CARPET),
}, {
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("mushroom_light") }, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.ROAD),
}, {
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("sapling", 4) }, WORLD_TILES.CARPET),
    CM({ CP("grass", 4) }, WORLD_TILES.CARPET),
}, {
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM({ CP("pighouse") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.ROAD),
    CM({ CP("berrybush", 4) }, WORLD_TILES.CARPET),
    CM({ CP("cave_banana_tree", 4) }, WORLD_TILES.CARPET),
} })


AddBluePrint("【预设】猪人科研区", { {
    CM({ CP("pighouse") }, WORLD_TILES.WOODFLOOR),
    CM({ CP("pighouse") }, WORLD_TILES.WOODFLOOR),
    CM({ CP("pighouse") }, WORLD_TILES.WOODFLOOR)
}, {
    CM(nil, WORLD_TILES.DECIDUOUS),
    CM(nil, WORLD_TILES.DECIDUOUS),
    CM(nil, WORLD_TILES.DECIDUOUS)
}, {
    CM({ CP("researchlab") }, WORLD_TILES.WOODFLOOR),
    CM({ CP("researchlab2") }, WORLD_TILES.WOODFLOOR),
    CM({ CP("researchlab3") }, WORLD_TILES.WOODFLOOR)
}
})

AddBluePrint("【预设】猪人农业社区", { {
    CM({ CP("pighouse") }, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("slow_farmplot") }, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
}, {
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("fast_farmplot") }, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("pighouse") }, WORLD_TILES.GRASS),
}, {
    CM({ CP("pighouse") }, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("mushroom_farm") }, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
}, {
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("meatrack") }, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("pighouse") }, WORLD_TILES.GRASS),
} })

AddBluePrint("【预设】猪人集市广场", { {
    CM({ CP("pighouse") }, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("ptribe_carnival_prizebooth") }, WORLD_TILES.ROAD),
    CM({ CP("ptribe_carnival_prizebooth") }, WORLD_TILES.ROAD),
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("pighouse") }, WORLD_TILES.ROAD),
}, {
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("wood_table_round") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("wood_table_square") }, WORLD_TILES.CARPET),
    CM(nil, WORLD_TILES.GRASS),
} })

AddBluePrint("【预设】猪人生态村", { {
    CM({ CP("pighouse") }, WORLD_TILES.GRASS),
    CM({ CP("slow_farmplot") }, WORLD_TILES.GRASS),
    CM({ CP("fast_farmplot") }, WORLD_TILES.GRASS),
    CM({ CP("pighouse") }, WORLD_TILES.GRASS),
}, {
    CM({ CP("mushroom_farm") }, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
    CM(nil, WORLD_TILES.GRASS),
    CM({ CP("mushroom_farm") }, WORLD_TILES.GRASS),
}, {
    CM({ CP("pighouse") }, WORLD_TILES.GRASS),
    CM({ CP("meatrack") }, WORLD_TILES.GRASS),
    CM({ CP("cookpot") }, WORLD_TILES.GRASS),
    CM({ CP("pighouse") }, WORLD_TILES.GRASS),
} })

----------------------------------------------------------------------------------------------------
return {
    BLUEPRINTS = BLUEPRINTS,
    BLUEPRINT_LIST = BLUEPRINT_LIST,
    MAX_SIZE = MAX_SIZE,
    CP = CP,
    CM = CM,
    AddBluePrint = AddBluePrint,
}
