local WildPigUtils = require("ptribe_wildpigutils")

local FN = {}
local PIGS_LIST = WildPigUtils.PIGS_LIST
local PIGS = WildPigUtils.PIGS

local PIG_GROUPS = {
    scavenger = function(player, pos)
        local pigs = {}

        local leader = SpawnAt("ptribe_wildpig", pos)
        leader:SetType("scavenger")
        table.insert(pigs, leader)

        local hat = SpawnPrefab("strawhat")
        leader.components.inventory:Equip(hat)

        for i = 1, math.random(3, 5) do
            local pig = SpawnAt("ptribe_wildpig", pos)
            pig.components.follower:SetLeader(leader)
            pig:SetType("scavenger")
            table.insert(pigs, pig)
        end
        return pigs
    end
}

---随机野外猪人组
--- TODO 目前只生成单个野外猪人
function FN.SpawnRandomPigGroup(player, pos)
    local type = PIGS_LIST[math.random(1, #PIGS_LIST)]

    local pigs
    if PIG_GROUPS[type] then
        pigs = PIG_GROUPS[type](player, pos)
    else
        local pig = SpawnAt("ptribe_wildpig", pos)
        pig:SetType(type)
        pigs = { pig }
    end

    for _, pig in ipairs(pigs) do
        local d = PIGS[pig.type]
        if d.Init then
            d.Init(pig)
        end
    end

    return pigs
end

return FN
