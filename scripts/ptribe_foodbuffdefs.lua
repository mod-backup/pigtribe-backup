--- 猪人厨子料理buff配置，buff都为buffmanagerutils中定义的buff
return {
    {
        type = "health",
        data = {
            {
                -- 正面
                time = 60,
                amount = 1,
                period = 1,
            },
            {
                -- 负面
                time = 60,
                amount = -1,
                period = 1,
            }
        }
    },
    {
        type = "hunger_mult",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                mult = 0.75
            },
            {
                time = TUNING.TOTAL_DAY_TIME,
                mult = 1.25
            }
        }
    },
    {
        type = "speed_mult",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME * 2,
                mult = 1.1
            },
            {
                time = TUNING.TOTAL_DAY_TIME * 2,
                mult = 0.9
            }
        }
    },
    {
        type = "attack_mult",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                mult = 1.2
            },
            {
                time = TUNING.TOTAL_DAY_TIME,
                mult = 0.75
            }
        }
    },
    {
        type = "attack_taken_mult",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                mult = 0.8
            },
            {
                time = TUNING.TOTAL_DAY_TIME,
                mult = 1.25
            }
        }
    },
    {
        type = "spawn_item",
        data = {
            nil,
            {
                time = 60,
                damage = 1,
                period = 2,
                prefab = "poop"
            }
        }
    },
    {
        type = "spawn_item",
        data = {
            nil,
            {
                time = 60,
                period = 2,
                prefab = "gunpowder_ignite"
            }
        }
    },
    {
        type = "random_temperature",
        data = {
            nil,
            {
                time = 60,
                period = 2,
                minVal = 1,
                maxVal = 5
            }
        }
    },
    {
        type = "affect_entity",
        data = {
            {
                time = 60,
                radius = 16,
                isAttack = false,
                duration = 60,
                mustTags = { "pig" },
            },
            {
                time = 60,
                radius = 16,
                isAttack = true,
                duration = 60,
                mustTags = { "pig" },
            }
        }
    },
    {
        type = "wormwood_vined",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
            }
        }
    },
    {
        type = "shadowtentacle",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                random = 0.5,
            }
        }
    },
    {
        type = "work_mult",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                mult = 2.5,
            }
        }
    },
    {
        type = "kill_health_regen",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                percent = 0.1
            }
        }
    },
    {
        type = "food_eat_regen",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                healthabsorption = 2,
                hungerabsorption = 2,
                sanityabsorption = 2
            },
            {
                time = TUNING.TOTAL_DAY_TIME,
                healthabsorption = 0.5,
                hungerabsorption = 0.5,
                sanityabsorption = 0.5
            }
        }
    },
    {
        type = "burnable_resist",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                fxLevel = 4, --这里直接设置值，不会还是削弱吧
                burntime = 5
            },
            {
                time = TUNING.TOTAL_DAY_TIME,
                fxLevel = 2, --这里直接设置值，不会还是削弱吧
                burntime = 40
            }
        }
    },
    {
        type = "freezable_resist",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                shatterFXLevel = 5,
                resistance = 4
            },
            {
                time = TUNING.TOTAL_DAY_TIME,
                shatterFXLevel = 1,
                resistance = 1
            }
        }
    },
    {
        type = "build_ingredientmod",
        data = {
            {
                time = 60,
                percent = 0.5,
            }
        }
    },
    {
        type = "fast_pick",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
            }
        }
    },
    {
        type = "electricattack",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
            }
        }
    },
    {
        type = "fast_build",
        data = {
            {
                time = TUNING.TOTAL_DAY_TIME,
                timeout = 0.1
            }
        }
    }
}
