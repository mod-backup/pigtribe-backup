local function DaywalkerActionString(inst, action)
    -- local item = action.invobject --暂时设置成所有装备都不能装备
    return (action.action == ACTIONS.PTRIBE_USE_WILBA_SKILL and STRINGS.ACTIONS.PTRIBE_USE_WILBA_SKILL.SMASH) --重锤
        or (action.action == ACTIONS.DROP and STRINGS.ACTIONS.DROP.GENERIC)                                   --丢弃
        or (action.action == ACTIONS.EQUIP and STRINGS.ACTIONS.EQUIP)                                         --装备
        -- or (action.action == ACTIONS.EAT and STRINGS.ACTIONS.EAT)                                                --吃
        -- or (action.action == ACTIONS.EQUIP and item and (item.prefab == "silvernecklace" or
        --     not item.prefab.components.inventoryitem
        --     or not item.prefab.components.inventoryitem.cangoincontainer) and STRINGS.ACTIONS.EQUIP) --只有项链能装备
        or STRINGS.ACTIONS.ATTACK.GENERIC
        ,
        (action.action == ACTIONS.PTRIBE_USE_WILBA_SKILL)
        or nil
end

local DAYWALKER_LMB_ACTIONS = { "CHOP", "MINE", "HAMMER" }
local DAYWALKER_ACTION_TAGS = {}
for i, v in ipairs(DAYWALKER_LMB_ACTIONS) do
    table.insert(DAYWALKER_ACTION_TAGS, v .. "_workable")
end
local BEAVER_TARGET_EXCLUDE_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO", "catchable", "sign" }
local function GetDaywalkerAction(inst, target)
    for i, v in ipairs(DAYWALKER_LMB_ACTIONS) do
        if target:HasTag(v .. "_workable") then
            return not target:HasTag("sign") and ACTIONS[v] or nil
        end
    end

    if target:HasTag("walkingplank") and target:HasTag("interactable") then
        return (inst:HasTag("on_walkable_plank") and ACTIONS.ABANDON_SHIP) or
            (target:HasTag("plank_extended") and ACTIONS.MOUNT_PLANK) or
            ACTIONS.EXTEND_PLANK
    end
end

local function DaywalkerActionButton(inst, force_target)
    if inst.components.playercontroller:IsDoingOrWorking() then return end

    if force_target == nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x, y, z, inst.components.playercontroller.directwalking and 3 or 6, nil,
            BEAVER_TARGET_EXCLUDE_TAGS, DAYWALKER_ACTION_TAGS)
        for i, v in ipairs(ents) do
            if v ~= inst and v.entity:IsVisible() and CanEntitySeeTarget(inst, v) then
                local action = GetDaywalkerAction(inst, v)
                if action ~= nil then
                    return BufferedAction(inst, v, action)
                end
            end
        end
    elseif inst:GetDistanceSqToInst(force_target) <= (inst.components.playercontroller.directwalking and 9 or 36) then
        local action = GetDaywalkerAction(inst, force_target)
        if action ~= nil then
            return BufferedAction(inst, force_target, action)
        end
    end
end

local function DaywalkerLeftClickPicker(inst, target)
    if not target or target == inst then return end

    if inst.replica.combat:CanTarget(target) then
        return (not target:HasTag("player") or inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK))
            and inst.components.playeractionpicker:SortActionList({ ACTIONS.ATTACK }, target, nil)
            or nil
    end
    --特殊处理，允许噩梦疯猪攻击，支持砍、挖、锤，因为攻击是飞扑，所以需要对方有碰撞体积，防止飞过头
    for _, v in ipairs(DAYWALKER_LMB_ACTIONS) do
        if target:HasTag(v .. "_workable") then
            return not target:HasTag("sign")
                and inst.components.playeractionpicker:SortActionList({ ACTIONS.PTRIBE_WILBA_MONSTER_WORK }, target, nil)
                or nil
        end
    end
end

local function DaywalkerRightClickPicker(inst, target, pos)
    return target ~= nil
        and target ~= inst
        and (target:HasTag("HAMMER_workable") and
            inst.components.playeractionpicker:SortActionList({ ACTIONS.PTRIBE_WILBA_MONSTER_WORK }, target, nil))
        or (not inst.components.playercontroller.isclientcontrollerattached and
            inst.components.playeractionpicker:SortActionList({ ACTIONS.PTRIBE_USE_WILBA_SKILL }, target or pos, nil))
end

local function DaywalkerAndGoosePointSpecialActions(inst, pos, useitem, right)
    return
        right
        and inst.components.playercontroller:IsEnabled()
        and inst.components.ptribe_wilbatransform.type == "daywalker"
        and { ACTIONS.PTRIBE_USE_WILBA_SKILL } or {}
end
----------------------------------------------------------------------------------------------------
local function ReticuleTargetFn(inst)
    return Vector3(inst.entity:LocalToWorldSpace(1.5, 0, 0))
end

local function ReticuleUpdatePositionFn(inst, pos, reticule, ease, smoothing, dt)
    local x, y, z = inst.Transform:GetWorldPosition()
    reticule.Transform:SetPosition(x, 0, z)
    local rot = -math.atan2(pos.z - z, pos.x - x) / DEGREES
    if ease and dt ~= nil then
        local rot0 = reticule.Transform:GetRotation()
        local drot = rot - rot0
        rot = Lerp((drot > 180 and rot0 + 360) or (drot < -180 and rot0 - 360) or rot0, rot, dt * smoothing)
    end
    reticule.Transform:SetRotation(rot)

    if inst.components.reticule ~= nil then
        inst.components.reticule.ease = reticule.entity:IsVisible()
    end
end

local function EnableReticule(inst, enable)
    if enable then
        if inst.components.reticule == nil then
            inst:AddComponent("reticule")
            inst.components.reticule.reticuleprefab = "reticuleline2"
            inst.components.reticule.targetfn = ReticuleTargetFn
            inst.components.reticule.updatepositionfn = ReticuleUpdatePositionFn
            inst.components.reticule.ease = true
            if inst.components.playercontroller ~= nil and inst == ThePlayer then
                inst.components.playercontroller:RefreshReticule()
            end
        end
    elseif inst.components.reticule ~= nil then
        inst:RemoveComponent("reticule")
        if inst.components.playercontroller ~= nil and inst == ThePlayer then
            inst.components.playercontroller:RefreshReticule()
        end
    end
end

local function SetWereActions(inst, type)
    if type == "daywalker" then
        inst.ActionStringOverride = DaywalkerActionString
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller.actionbuttonoverride = DaywalkerActionButton
        end
        if inst.components.playeractionpicker ~= nil then
            inst.components.playeractionpicker.leftclickoverride = DaywalkerLeftClickPicker
            inst.components.playeractionpicker.rightclickoverride = DaywalkerRightClickPicker --鼠标悬停
            inst.components.playeractionpicker.pointspecialactionsfn = DaywalkerAndGoosePointSpecialActions
        end
        EnableReticule(inst, false)
    else
        inst.ActionStringOverride = nil
        if inst.components.playercontroller ~= nil then
            inst.components.playercontroller.actionbuttonoverride = nil
        end
        if inst.components.playeractionpicker ~= nil then
            inst.components.playeractionpicker.leftclickoverride = nil
            inst.components.playeractionpicker.rightclickoverride = nil
            inst.components.playeractionpicker.pointspecialactionsfn = nil
        end
        EnableReticule(inst, false)
    end
end
----------------------------------------------------------------------------------------------------

local function OnTypeChanged(inst)
    local type = inst.replica.ptribe_wilbatransform:GetType()
    SetWereActions(inst, type)
end

local WilbaTransform = Class(function(self, inst)
    self.inst = inst

    self.type = net_string(inst.GUID, "ptribe_wilbatransform.type", "ptribe_wilbatransform.type") --变身形态

    --也许操作要区分一下主客机，不过既然没出问题就不管了(￣ー￣)
    inst:ListenForEvent("ptribe_wilbatransform.type", OnTypeChanged)
end)

function WilbaTransform:SetType(type)
    self.type:set(type)
end

function WilbaTransform:GetType()
    return self.type:value()
end

return WilbaTransform
