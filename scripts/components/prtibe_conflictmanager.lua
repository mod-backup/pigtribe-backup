local ResourceConvert = require("ptribe_resourceconvert")
local Shapes = require("ptribe_utils/shapes")
local Utils = require("ptribe_utils/utils")
local Timer = require("ptribe_utils/timer")

local ConflictManager = Class(function(self, inst)
    self.inst = inst

    self.minDay = 30

    self.attackTime = 0 --下次攻击时间，其实这个初始化值可有可无，因为有一个最短世界天数限制
    self.attackers = {} --攻击者
    self.targets = {}
    self.attackerManager = nil
    self.targetManager = nil
    self.updateTask = nil

    self.inst:DoPeriodicTask(30, function() self:OnPeriodicUpdate() end)
end)

---评估部落战斗力
--- 20资源 == 一个帽子 == 一个猪人
--- 一个特殊猪人 == 两个猪人
local function EvaluationCombatEffective(pigs, resource)
    local targetPigCount = #pigs
    local score = targetPigCount + math.min(resource / 20, targetPigCount)

    for _, p in ipairs(pigs) do
        if p.components.ptribe_pigability and p.components.ptribe_pigability.type then
            score = score + 1
        end
    end

    return score
end

local function OnPigDeath(inst, data)
    local afflicter = data and data.afflicter
    if afflicter and afflicter:HasTag("player") then
        inst.ptribe_playerKill = true --玩家击杀
    end
end

--- 如果是玩家击杀的就掉落，否则不掉落
local function DropLootBefore(self)
    if self.inst:HasTag("ptribe_tribeinvader") or self.inst:HasTag("ptribe_tribevictim") then
        return nil, not self.inst.ptribe_playerKill
    end
end

local RETARGET_MUST_TAGS = { "_combat" }
local RETARGET_CANT_TAGS = { "ptribe_tribeinvader" }
local RETARGET_ONE_OF_TAGS = { "ptribe_tribevictim", "player" }
local function RetargetBeforeFn(inst)
    if inst:HasTag("ptribe_tribeinvader") then
        local target = FindEntity(inst, 35, function(guy)
                return inst.components.combat:CanTarget(guy)
            end,
            RETARGET_MUST_TAGS,
            RETARGET_CANT_TAGS,
            RETARGET_ONE_OF_TAGS
        )
        return target and { target } or nil, true
    end
end

local function KeepTargetBeforeFn(inst, target)
    if inst:HasTag("ptribe_tribeinvader") then
        local keep = inst.components.combat:CanTarget(target)
            and (target:HasTag("ptribe_tribeinvader")
                or inst:GetDistanceSqToInst(target) <= 256)
        return { keep }, true
    end
end

local function ShareTargetBefore(self, target)
    if self.inst:HasTag("ptribe_tribeinvader") then
        return nil, true --简单点，侵略者被攻击者不扩散仇恨
    end
end

local function InitPig(attackers, targets, targetManager)
    for _, pig in ipairs(attackers) do
        pig.AnimState:SetMultColour(0, 0, 0, 0.85)

        pig:AddTag("ptribe_tribeinvader")
        pig:AddTag("hostile") --可以按F攻击
        pig:AddTag("pirate")  --被猪人仇视
        pig:AddTag("werepig") --普通猪人被攻击时不会让侵略者受影响

        -- 这些追加的内容我不用移除，只要移除标签就行，在下次加载游戏时就恢复正常了
        Utils.FnDecorator(pig.components.combat, "targetfn", RetargetBeforeFn)
        Utils.FnDecorator(pig.components.combat, "keeptargetfn", KeepTargetBeforeFn)
        pig:ListenForEvent("death", OnPigDeath)
        if pig.components.lootdropper then
            Utils.FnDecorator(pig.components.lootdropper, "DropLoot", DropLootBefore)
        end
        Utils.FnDecorator(pig.components.combat, "ShareTarget", ShareTargetBefore)

        local home = pig.components.homeseeker and pig.components.homeseeker:GetHome()
        if home and home.components.ptribe_pigmanager then
            Utils.FnDecorator(pig.components.homeseeker, "GetHomePos", function(self) --把目标村长房暂时当成自己家
                if self.inst:HasTag("ptribe_tribeinvader") then
                    return { targetManager and targetManager:IsValid() and targetManager:GetPosition() or nil }, true
                end
            end)
            home.components.ptribe_pigmanager:CancelTask()
        end
    end

    for _, pig in ipairs(targets) do
        pig:AddTag("ptribe_tribevictim")
        pig:ListenForEvent("death", OnPigDeath)
        if pig.components.lootdropper then
            Utils.FnDecorator(pig.components.lootdropper, "DropLoot", DropLootBefore)
        end
    end
end

--- 结束战斗
function ConflictManager:FinishAttack()
    for _, pig in ipairs(self.attackers) do
        if pig:IsValid() then
            pig.AnimState:SetMultColour(1, 1, 1, 1)

            pig.ptribe_asleepTime = nil

            pig:RemoveTag("ptribe_tribeinvader")
            pig:RemoveTag("hostile") --可以按F攻击
            pig:RemoveTag("pirate")  --被猪人仇视
            pig:RemoveTag("werepig")

            pig:RemoveEventCallback("death", OnPigDeath)

            local home = pig.components.homeseeker and pig.components.homeseeker:GetHome()
            if home then
                if pig.entity:IsVisible() then
                    SpawnPrefab("small_puff").Transform:SetPosition(pig.Transform:GetWorldPosition())
                end

                home.components.spawner:GoHome(pig)
                home.components.spawner:ReleaseChild()
            end
        end
    end

    for _, pig in ipairs(self.targets) do
        if pig:IsValid() then
            pig:RemoveTag("ptribe_tribevictim")

            pig:RemoveEventCallback("death", OnPigDeath)
        end
    end

    self.attackers = {}
    self.targets = {}
    self.attackerManager = nil
    self.targetManager = nil
    if self.updateTask then
        self.updateTask:Cancel()
        self.updateTask = nil
    end
end

local RANDOM_HATS = { "wathgrithrhat", "footballhat", "cookiecutterhat", "woodcarvedhat", }

local function WearHat(manager, pigs)
    for _, v in ipairs(pigs) do
        local hat = v.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
        if not hat or not hat.components.armor then
            if hat then
                v.components.inventory:DropItem(hat):Remove()
            end

            hat = SpawnPrefab(RANDOM_HATS[math.random(1, #RANDOM_HATS)])
            hat:AddTag("ptribe_pigmanbuild") --加上特殊标签
            v.components.inventory:Equip(hat)
            v.AnimState:Show("hat")
            manager.components.ptribe_pigmanager:ResourceDoDelta(-(math.max(20, ResourceConvert.GetResource(hat.prefab))))

            if manager.components.ptribe_pigmanager.resource < 20 then break end
        end
    end
end

local offset = { { -1, -1 }, { -1, -1 }, { -1, -1 }, { -1, -1 } }
local function TransferPigman(count, pos, findRadius, attackers)
    local spawnPos
    for i = 1, 15 do
        spawnPos = Shapes.GetRandomLocation(pos, findRadius, findRadius + 10)
        if TheWorld.Map:IsAboveGroundAtPoint(spawnPos:Get()) then
            break
        end
        spawnPos = nil
    end

    if not spawnPos and findRadius > 0 then --哪怕贴着村长房刷也要找到合适的位置
        findRadius = math.max(0, findRadius - 10)
        TransferPigman(count, pos, findRadius - 10, attackers)
    end

    if not spawnPos then return end

    local x, y, z = TheWorld.Map:GetTileCenterPoint(spawnPos:Get())
    for i = (count - 1) * 4 + 1, math.min(count * 4, #attackers) do
        local index = i % 4 + 1
        attackers[i].Transform:SetPosition(x + offset[index][1], y, z + offset[index][2])
        SpawnPrefab("small_puff").Transform:SetPosition(x + offset[index][1], y, z + offset[index][2])
    end
end

--- 更新状态，判断战斗是否结束
local function UpdateState(inst)
    local self = inst.components.prtibe_conflictmanager

    local minSleepTime = math.huge
    local isExist = false
    local isWin = false
    local cur = GetTime()
    for _, pig in ipairs(self.attackers) do
        if pig:IsValid() then
            isExist = true
            if pig:IsAsleep() then
                pig.ptribe_asleepTime = pig.ptribe_asleepTime or cur
                minSleepTime = math.max(minSleepTime, cur - pig.ptribe_asleepTime)
            else
                pig.ptribe_asleepTime = nil
                if pig.components.combat.target then return end --正在战斗
                isWin = true
            end
        end
    end

    if isExist and not isWin then
        -- 所有猪人允许休眠范围
        if minSleepTime <= 60 then return end

        -- 猪人长时间脱离加载范围，计为战斗结束
        local count1, count2 = 0, 0
        for _, pig in ipairs(self.attackers) do
            count1 = count1 + (pig:IsValid() and 1 or 0)
        end
        for _, pig in ipairs(self.targets) do
            count2 = count2 + (pig:IsValid() and 1 or 0)
        end
        if count1 > count2 then
            isWin = true
        end
    end

    -- 战斗结束
    local rate = isWin and 0.3 or 0.1 --30%
    local resource = math.max(self.targetManager.components.ptribe_pigmanager.resource * rate, 200)
    if isWin then                     --进攻者胜利
        self.targetManager.components.ptribe_pigmanager:ResourceDoDelta(-resource)
        self.attackerManager.components.ptribe_pigmanager:ResourceDoDelta(resource)
    else
        self.targetManager.components.ptribe_pigmanager:ResourceDoDelta(resource)
        self.attackerManager.components.ptribe_pigmanager:ResourceDoDelta(-resource)
    end

    self:FinishAttack()
end

---进攻！
---资源换算成防具，
function ConflictManager:AttackTribe(attackerManager, attackers, targetManager, targets)
    SpawnPrefab("krampuswarning_lvl2").Transform:SetPosition(targetManager.Transform:GetWorldPosition()) --音效

    WearHat(attackerManager, attackers)
    InitPig(attackers, targets, targetManager)

    -- 猪人先出房间
    for _, pig in ipairs(attackers) do
        local pighouse = pig.parent
        if pighouse and pighouse.components.spawner then --在房间里
            pighouse.components.spawner:ReleaseChild()
        end
    end

    self.inst:DoTaskInTime(2, function()
        WearHat(targetManager, targets)
        for _, pig in ipairs(targets) do
            local pighouse = pig.parent
            if pighouse and pighouse.components.spawner then --在房间里
                pighouse.components.spawner:ReleaseChild()
            end
        end
    end)

    -- 传送敌人，四猪一个队，一个地皮
    -- 要不整个传送门特效？
    local findRadius = 25
    local pos = targetManager:GetPosition()
    local titleCount = math.ceil(#targets / 4)
    Timer.PeriodicExecutor(self.inst, function(count)
        if count >= titleCount then
            return
        end
        return math.random() * 2 + 2
    end, function(count)
        TransferPigman(count, pos, findRadius, attackers)
    end)

    self.updateTask = self.inst:DoPeriodicTask(5, UpdateState)
end

function ConflictManager:OnPeriodicUpdate()
    if GetTime() < self.attackTime
        or TheWorld.state.cycles < self.minDay
        or TheWorld.state.isfullmoon
    then
        return
    end

    local prob = 1
    if TheWorld.state.isdusk then
        prob = prob / 2
    elseif TheWorld.state.isnight then
        prob = prob / 3
    end
    if math.random() >= prob then return end

    for _, player in ipairs(AllPlayers) do
        local manager = player.components.ptribe_entnearby:FindEntityByPrefab("ptribe_nearby_manager", "pighouse")
        if not player:HasTag("playerghost")
            and manager and manager:IsValid()
            and manager.components.ptribe_pigmanager.resource >= 200 --没钱就不打了
        then
            -- 查找合适的进攻者
            local managers = {}
            local x, _, z = manager.Transform:GetWorldPosition()
            for _, v in ipairs(TheSim:FindEntities(x, _, z, TUNING.TRIBE_CONFLICT_FIND_RAIDUS)) do
                if v.prefab == "pighouse" then
                    local ma = v.components.ptribe_pigmanager:GetManager() or nil
                    if ma and ma.components.ptribe_pigmanager:IsValidManager() then
                        if not managers[ma] then
                            managers[ma] = {
                                resource = ma.components.ptribe_pigmanager.resource,
                                pigs = {}
                            }
                        end
                        local pig = v.components.spawner and v.components.spawner.child or nil
                        if pig and (not pig.components.follower or not pig.components.follower.leader) then
                            table.insert(managers[ma].pigs, v.components.spawner.child)
                        end
                    end
                end
            end

            -- 挑选一个进攻，需要平衡一下战斗力
            local targets = managers[manager] and managers[manager].pigs
            if targets and #targets > 0 then --没存活的猪人就算了
                local score = EvaluationCombatEffective(targets, managers[manager].resource)
                local attackerManager, data
                local minDif = math.huge
                managers[manager] = nil
                for ma, d in pairs(managers) do
                    local sco = EvaluationCombatEffective(d.pigs, d.resource)
                    local dif = math.abs(sco - score)
                    if dif < minDif then
                        minDif = dif
                        attackerManager = ma
                        data = d
                    end
                end

                if attackerManager then
                    self.attackers = data.pigs
                    self.targets = targets
                    self.attackerManager = attackerManager
                    self.targetManager = manager

                    self:AttackTribe(self.attackerManager, self.attackers, self.targetManager, targets)

                    self.attackTime = GetTime() + TUNING.TRIBE_CONFLICT_ATTACK_PERIOD
                    return
                end
            end
        end
    end
end

function ConflictManager:OnSave()
    local data = {}
    local references = {}

    data.attackTime = self.attackTime and (self.attackTime - GetTime()) or nil

    for _, v in ipairs(self.attackers) do
        if data.attackers == nil then
            data.attackers = { v.GUID }
        else
            table.insert(data.attackers, v.GUID)
        end

        table.insert(references, v.GUID)
    end
    for _, v in ipairs(self.targets) do
        if data.targets == nil then
            data.targets = { v.GUID }
        else
            table.insert(data.targets, v.GUID)
        end

        table.insert(references, v.GUID)
    end

    if self.attackerManager then
        data.attackerManager = self.attackerManager.GUID
        table.insert(references, self.attackerManager.GUID)
        data.targetManager = self.targetManager.GUID
        table.insert(references, self.targetManager.GUID)
    end

    return data, references
end

function ConflictManager:OnLoad(data)
    self.attackTime = data.attackTime or self.attackTime
end

function ConflictManager:LoadPostPass(ents, data)
    if data.attackers then
        for _, v in ipairs(data.attackers) do
            local child = ents[v]
            if child ~= nil then
                table.insert(self.attackers, child.entity)
            end
        end
        for _, v in ipairs(data.targets) do
            local child = ents[v]
            if child ~= nil then
                table.insert(self.targets, child.entity)
            end
        end

        self.attackerManager = ents[data.attackerManager] and ents[data.attackerManager].entity
        self.targetManager = ents[data.targetManager] and ents[data.targetManager].entity

        InitPig(self.attackers, self.targets, self.targetManager)
        self.updateTask = self.inst:DoPeriodicTask(5, UpdateState)
    end
end

return ConflictManager
