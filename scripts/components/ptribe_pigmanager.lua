local Utils = require("ptribe_utils/utils")
local GetPrefab = require("ptribe_utils/getprefab")
local Shapes = require("ptribe_utils/shapes")
local BluePrintsUtils = require("ptribe_blueprintsutils")
local ResourceConvert = require("ptribe_resourceconvert")
local PigTasksUtils = require("ptribe_pigtasksutils")
local Timer = require("ptribe_utils/timer")

--- 验证管理者是否可用
local function CheckManager(inst)
    local manager = inst.components.ptribe_pigmanager:GetManager()
    return manager and manager.components.ptribe_pigmanager:IsValidManager()
    -- and inst:GetDistanceSqToInst(manager) <= TUNING.TRIBE_MAX_DIS * TUNING.TRIBE_MAX_DIS
end

--- 检验任务
local function TryDoTask(inst, manager, data)
    if not inst.components.spawner then return end --好像有时候没有这个组件，不知道是为什么

    local task = data.task
    if task == "changeManager" then
        if inst ~= manager then
            inst.components.timer:StartTimer("changeManager", 10 / inst:GetDistanceSqToInst(manager)) -- 最远的猪人房查找最优先
        end
    elseif task == "managerRemove" then
        inst.components.ptribe_pigmanager.resource = data.resource
    elseif task == "selectPigTask" then
        if data.dirty then
            --判断猪人能否发布任务
            local child = inst.components.spawner.child
            if not (child and child:IsValid()
                    and child.components.ptribe_pigtask and child.components.ptribe_pigtask.taskId:value() == 0
                    and (not child.components.follower or not child.components.follower.leader)) then
                child = nil
            end

            if child then
                data.dirty = nil
                child.components.ptribe_pigtask.taskId:set(data.taskId)                                 --任务剩下的数据直接读取文件就行
                manager.components.timer:StopTimer(data.cdName)
                manager.components.timer:StartTimer(data.cdName, data.deadline + TUNING.TOTAL_DAY_TIME) --任务截止后也要冷却一天
            end
        end
    end
end

--- 检查管理者
local function UpdateManager(inst)
    local x, y, z = inst.Transform:GetWorldPosition()

    if z > 950 then -- 小房子里的猪人房禁止发展部落
        inst:AddTag("ptribe_forbidTribe")
        return
    end

    -- 猪人房附近有肉块雕像时不会生成部落
    if FindEntity(inst, 30, function(ent) return ent.prefab == "resurrectionstatue" end) then return end

    if CheckManager(inst) then
        --重新绑定关系
        inst.components.ptribe_pigmanager:SetNewManager(inst.components.ptribe_pigmanager:GetManager())
        return
    end

    -- 查找最近的的管理者
    local newManager
    local pighouseCount = 0
    local closestDis = math.huge
    for _, v in pairs(TheSim:FindEntities(x, y, z, TUNING.TRIBE_MAX_DIS * 2, { "structure" })) do
        if v.components.ptribe_pigmanager and v ~= inst then
            pighouseCount = pighouseCount + 1

            local dis = math.sqrt(inst:GetDistanceSqToInst(v))
            if CheckManager(v)
                and dis < closestDis
                and dis <= (math.sqrt(v.components.ptribe_pigmanager:GetMaxDisSq()) + TUNING.TRIBE_MAX_DIS) --距离补偿，保证两部落不相交
            then
                closestDis = dis
                newManager = v
            end
        end
    end

    if not newManager then
        if pighouseCount < TUNING.TRIBE_TRIBE_MIN_PIGHOUSE_COUNT then --附近猪人房数量限制
            return
        end

        if not inst:HasTag("ptribe_forbidTribe") then
            newManager = inst
        end
    end

    if newManager then
        inst.components.ptribe_pigmanager:SetNewManager(newManager)
    end
end

----------------------------------------------------------------------------------------------------
local function IsBuildPathMap(buildPath, pos)
    for _, part in ipairs(buildPath) do
        if part.mapPos and part.mapPos.x - pos.x < Utils.EPSILON and part.mapPos.z - pos.z < Utils.EPSILON then
            return true
        end
    end
    return false
end

--- 维护模块完成率100%的模块，不包含tasks中待建设的模块
-- local TITLE_CHECK_RANGE = math.sqrt(8)
local TITLE_CHECK_CANT_TAGS = { "FX" }

-- 收集任务，三次判断：第一次分配任务时判断、第二次brain中猪人执行时判断、第三次action执行时判断
local function PostCollectTask(inst)
    local self = inst.components.ptribe_pigmanager

    local tasksNum = self:GetCollectTasksNum()
    if tasksNum >= TUNING.TRIBE_TASKS_LENGTH then return end

    local cur = GetTime()
    local treeNum = 0                               --不能让树太多了
    local pos = inst:GetPosition()
    local range = math.sqrt(self:GetMaxDisSq()) + 4 --确保覆盖整个蓝图，再加一个地皮

    -- 先查找可收集资源，地上的垃圾也捡一捡
    local tasks = {}
    for _, v in ipairs(TheSim:FindEntities(pos.x, 0, pos.z, range, nil, TITLE_CHECK_CANT_TAGS)) do
        if v.prefab and (v.prefab == "acorn_sapling" or string.match(v.prefab, "^deciduoustree")) then
            treeNum = treeNum + 1
        end

        local vPos = v:GetPosition()
        if tasksNum < TUNING.TRIBE_TASKS_LENGTH
            and TheWorld.Map:IsAboveGroundAtPoint(vPos:Get()) --海上的不要
        then
            local workAction = v.components.workable and v.components.workable:GetWorkAction()
            local type = nil --进一步区分

            if v.components.inventoryitem and not v.components.inventoryitem:IsHeld() and ResourceConvert.IsPickUpGarbage(v) then
                type = "item"
            elseif v.ptribe_lastInteractTime and cur - v.ptribe_lastInteractTime > v.ptribe_interactCd
                and ResourceConvert.PrefabIsInteractive(v) then
                -- 优先考虑交互，交互有可能消耗资源也有可能增加资源
                type = "interact"
                v.ptribe_lastInteractTime = cur
            elseif (not v:HasTag("ptribe_pigmanbuild") or v:HasTag("burnt")) --猪人建筑
                and workAction == ACTIONS.CHOP and v.components.workable:CanBeWorked()
                and v:HasTag("plant") and v:HasTag("tree")
                and (not v.components.inventoryitem or v.components.inventoryitem:IsHeld()) --有些mod的道具可以砍，比如单机移植的椰子
                and (not v.components.growable or (v.components.growable.stage >= #v.components.growable.stages - 1))
                and not v:HasTag("winter_tree")                                             --圣诞树不要砍
                and not string.starts(v.prefab, "oceantree")                                --水中木
            then
                type = "work"
            end

            if type then
                table.insert(tasks, {
                    id = -1, --与建设任务id区分开
                    prefab = v,
                    pos = vPos,
                    type = type
                })
                tasksNum = tasksNum + 1
            end
        end
    end

    -- TODO 对于猪人公主来说，猪人给的垃圾可能会很多，必须把地上的垃圾捡一下，等更新后就加入机器人来捡垃圾，这个判断就上面去
    if self.resource >= TUNING.TRIBE_RESOURCE_MAX then
        self:AddCollectTask(tasks)
        return
    end

    -- 再挑选区域种树，这里简单一点儿，只是围绕蓝图边缘的一个地皮种树，而且随机取值
    if tasksNum < TUNING.TRIBE_TASKS_LENGTH and treeNum < 10 then
        local trialNum = (TUNING.TRIBE_TASKS_LENGTH - tasksNum) * 2 --试错次数
        local oldTaskNum = tasksNum
        while tasksNum < TUNING.TRIBE_TASKS_LENGTH and trialNum > 0 do
            local pose = Shapes.GetRandomLocation(pos, range - 4, range) --种植范围要在上面查找资源的范围内

            if TheWorld.Map:IsAboveGroundAtPoint(pose:Get()) and
                #TheSim:FindEntities(pose.x, pose.y, pose.z, 0.5, nil, TITLE_CHECK_CANT_TAGS) <= 0 then
                table.insert(tasks, {
                    id = -1,
                    prefab = "acorn_sapling", --种树苗
                    pos = pose,
                    type = "spawn"
                })
                tasksNum = tasksNum + 1
                -- treeNum = treeNum + 1 --允许树不够时超过10
            end
            trialNum = trialNum - 1
        end

        if oldTaskNum == tasksNum then
            -- 没找到可种植地区，给点儿资源作为补偿
            self:ResourceDoDelta(1)
        end
    end

    self:AddCollectTask(tasks)
end

--- 管理者发布任务，一定有蓝图，每次处理一个模块
-- 根据当前进度选择下一个模块，根据优先级决定模块预制体建造顺序，根据资源决定是否立即执行
local DEL_POS = { { 0, -1 }, { 1, 0 }, { 0, 1 }, { -1, 0 } }
local function PostBuildTask(inst)
    local self = inst.components.ptribe_pigmanager

    if self.cancheBuildTask then
        -- 资源不够就不执行
        if self.resource >= self.cancheBuildTask.need then
            self:AddBuildTask(self.cancheBuildTask.tasks)
            self.cancheBuildTask = nil
        end
        return
    end

    if self:GetBuildTasksNum() >= TUNING.TRIBE_TASKS_LENGTH then return end

    local centerPos = Vector3(TheWorld.Map:GetTileCenterPoint(inst.Transform:GetWorldPosition()))
    local blueprint = self._blueprint
    local pos1 = blueprint.pos

    local len = #blueprint.buildPath
    for _ = 1, len do
        self.buildpathIndex = (self.buildpathIndex + 1) % (len + 1)
        self.buildpathIndex = self.buildpathIndex ~= 0 and self.buildpathIndex or 1
        local part = blueprint.buildPath[self.buildpathIndex]

        part.mapPos = nil
        local pos
        if part.mapPos then
            pos = part.mapPos
        else
            local pos2 = part.pos
            pos = Vector3((pos2[1] - pos1[1]) * 4, 0, (pos2[2] - pos1[2]) * 4) + centerPos --该模块对应地皮中心位置
        end

        local tasks = BluePrintsUtils.GetTasksByModule(self.buildpathIndex, part.module, pos, self.buildTasks,
            blueprint.angle)
        if #tasks > 0 then
            local need = 0 --需要的资源量
            for _, task in ipairs(tasks) do
                if type(task.prefab) == "string" then
                    need = need + ResourceConvert.GetResource(task.prefab)
                elseif task.tile then
                    need = need + 1
                end
            end
            if self.resource < need then --资源不够时，任务先缓存起来
                self.cancheBuildTask = { tasks = tasks, need = need }
            else
                self:AddBuildTask(tasks)
            end
            part.mapPos = pos
            break
        elseif part.module.existNum == 0 and TUNING.TRIBE_ALLOW_MISMATCH then --如果原位置符合条件的预制体一个都没有才查找相邻地皮
            -- TODO 找机会测一下这个功能是否正常
            for i = 1, #DEL_POS do
                local newx = part.pos[1] + DEL_POS[i][1]
                local newz = part.pos[2] + DEL_POS[i][2]
                if newx >= 1 and newx <= blueprint.height and newz >= 1 and newz <= blueprint.width
                    and #blueprint.tab[newx][newz].prefabs == 0 then --空模块才行
                    pos = Vector3((newx - pos1[1]) * 4, 0, (newz - pos1[2]) * 4) + centerPos
                    -- print("查找相邻地皮")
                    -- print(newx, newz, pos, blueprint.height, blueprint.width)

                    if TheWorld.Map:IsAboveGroundAtPoint(pos.x, 0, pos.z)     --地皮可用
                        and not IsBuildPathMap(blueprint.buildPath, pos) then -- 不是其他模块的map
                        tasks = BluePrintsUtils.GetTasksByModule(self.buildpathIndex, part.module, pos, self.buildTasks,
                            blueprint.angle)
                        if #tasks > 0 then
                            local need = 0 --需要的资源量
                            for _, task in ipairs(tasks) do
                                if type(task.prefab) == "string" then
                                    need = need + ResourceConvert.GetResource(task.prefab)
                                elseif task.tile then
                                    need = need + 1
                                end
                            end
                            if self.resource < need then
                                self.cancheBuildTask = { tasks = tasks, need = need }
                            else
                                self:AddBuildTask(tasks)
                            end
                            part.mapPos = pos
                            break
                        end
                    end
                end
            end
        end
    end
end

-- 更新部落完成率
local function UpdateCompleteRate(inst)
    local self = inst.components.ptribe_pigmanager

    local num = 0
    local existNum = 0
    for _, part in ipairs(self._blueprint.buildPath) do
        existNum = existNum + (part.module.existNum and part.module.existNum or 0)
        num = num + part.module.num
    end
    self.completeRate = math.min(1, existNum / num) --existNum有个小bug，可能会超过1
end

local function ClearTimer(inst)
    local timer = inst.components.timer
    if not timer then return end

    timer:StopTimer("changeManager")
    timer:StopTimer("ptribe_selectPigTask1_cd")
    timer:StopTimer("ptribe_selectPigTask2_cd")
    timer:StopTimer("ptribe_selectPigTask3_cd")
    timer:StopTimer("ptribe_findTask_cd")
end

local function CancelManager(inst)
    ClearTimer(inst)
    inst:AddTag("ptribe_forbidTribe")

    local self = inst.components.ptribe_pigmanager
    if self then
        if self.postTask then
            self.postTask:Cancel()
            self.postTask = nil
        end

        self.task = nil
        self._blueprint = nil
        self.favorability = 0 --刷新一下本地Label
    end
end

--管理者被摧毁时，资源量继承
local function OnManagerRemove(inst)
    inst:DoTaskInTime(0, CancelManager) --烧毁

    -- 猪人房附近有肉块雕像时不会生成部落
    if FindEntity(inst, 30, function(ent) return ent.prefab == "resurrectionstatue" end) then return end

    inst:PushEvent("ptribe_postTask", {
        task = "managerRemove",
        resource = inst.components.ptribe_pigmanager.resource
    })
end

local function OnStartDay(inst)
    local self = inst.components.ptribe_pigmanager
    if self.favorability <= -100
        or self.favorability >= 100
        or math.abs(self.favorability) <= 20
    then
        return
    end

    local unit = self.favorability > 20 and -5 or 2
    self:FavorabilityDelta(unit)
end

local function AcceptTest(inst)
    return not inst:HasTag("burnt")
end

local function OnGetItemFromPlayer(inst, giver, item)
    local self = inst.components.ptribe_pigmanager
    local resource = ResourceConvert.GetResource(item.prefab) * GetStackSize(item)
    self:ResourceDoDelta(resource)

    self:FavorabilityDelta(resource / 4)

    if TUNING.TRIBE_LUCK_GOLD_FROM_PIGMAN then
        --给予奖励
        local count = math.floor(resource / 5)
        if count <= 0 and math.random() < 0.3 then
            count = 1
        end
        if count > 0 then
            GetPrefab.ReturnMaterial("oinc", inst, { count = count, target = giver, isGiveTarget = true })
        end
    end
end

---初始化管理者，只初始化一次
local function InitManager(inst)
    if TheWorld.components.ptribe_tribemanager then
        TheWorld.components.ptribe_tribemanager:AddManager(inst)
    end

    inst.AnimState:SetBank("pig_cityhall")
    inst.AnimState:SetBuild("pig_cityhall")
    inst.MiniMapEntity:SetIcon("pig_shop_cityhall.png")

    local self = inst.components.ptribe_pigmanager
    -- 管理者变量
    self.collectTasks = Utils.CreateEmptyTable(TUNING.TRIBE_TASKS_LENGTH) --资源采集任务表、包含种树、砍树、做饭、收获等
    self.buildTasks = Utils.CreateEmptyTable(TUNING.TRIBE_TASKS_LENGTH)   --建设任务表

    inst.components.entitytracker:TrackEntity("manager", inst)

    -- inst:DoPeriodicTask(60, UpdateModuleExistNum)         --维护 existNum = num 的模块
    self.postTask = inst:DoPeriodicTask(30, Utils.MergeFn(PostCollectTask, PostBuildTask, UpdateCompleteRate), 0) --定期发布任务

    --挑选猪人任务
    local timer = inst.components.timer
    if not timer:TimerExists("ptribe_selectPigTask1_cd") then
        timer:StartTimer("ptribe_selectPigTask1_cd", math.random(1, 120))
    end
    if not timer:TimerExists("ptribe_selectPigTask2_cd") then
        timer:StartTimer("ptribe_selectPigTask2_cd", math.random(121, 240))
    end
    if not timer:TimerExists("ptribe_selectPigTask3_cd") then
        timer:StartTimer("ptribe_selectPigTask3_cd", math.random(241, 360))
    end

    -- 玩家可给予物品，增加资源
    if not inst.components.trader then
        inst:AddComponent("trader")
    end
    inst.components.trader:SetAcceptTest(AcceptTest)
    inst.components.trader.onaccept = OnGetItemFromPlayer
    inst.components.trader.acceptnontradable = true
    inst.components.trader:SetAcceptStacks() --整组给予

    -- 把范围内无管理者的猪人房纳入管理
    local x, y, z = inst.Transform:GetWorldPosition()
    for _, v in pairs(TheSim:FindEntities(x, y, z, math.sqrt(self:GetMaxDisSq()), { "structure" })) do
        if v ~= inst and v.components.ptribe_pigmanager and not CheckManager(v) then
            v.components.ptribe_pigmanager:SetNewManager(inst)
        end
    end

    inst:ListenForEvent("onremove", OnManagerRemove)
    inst:WatchWorldState("startday", OnStartDay)
    inst:ListenForEvent("onburnt", OnManagerRemove)
end

-- 重新选择任务
local function PushPigTaskEvent(manager, cdName)
    local self = manager.components.ptribe_pigmanager
    local newTask, taskId = PigTasksUtils.SelectTaskByTribe(self.completeRate)
    -- print("重新选择任务", newTask.time, taskId)

    manager:PushEvent("ptribe_postTask", {
        task = "selectPigTask",
        taskId = taskId,
        dirty = true,
        cdName = cdName,
        deadline = newTask.time + GetTime()
    })
end

local function OnTimerDone(inst, data)
    if not data then return end

    local self = inst.components.ptribe_pigmanager
    if data.name == "changeManager" then
        local manager = self:GetManager()
        if manager.components.ptribe_pigmanager:IsManager() then
            if not inst:HasTag("ptribe_forbidTribe") then
                self._blueprint = BluePrintsUtils.GetRandomBluePrint(inst)
                if self._blueprint then
                    -- 交换管理者身份
                    manager.components.ptribe_pigmanager:SetNewManager(inst)
                    InitManager(inst)
                end
            end
        else
            --如果找到后阻止其他猪人房继续查找
            self:SetNewManager(manager.components.ptribe_pigmanager:GetManager())
        end
    elseif data.name == "ptribe_selectPigTask1_cd" or data.name == "ptribe_selectPigTask2_cd" or data.name == "ptribe_selectPigTask3_cd" then
        if inst:IsAsleep() then --加载外的猪人房不需要一直查找
            self.pigTaskEvent = self.pigTaskEvent or {}
            table.insert(self.pigTaskEvent, data.name)
        else
            inst.components.timer:StartTimer(data.name, TUNING.TOTAL_DAY_TIME) --找到之前先开始冷却，找到后再重新计时
            PushPigTaskEvent(inst, data.name)
        end
    end
end

local function OnPigHouseRemove(inst)
    inst.components.ptribe_pigmanager:CancelTask()
end

local function on_blueprint(self, _blueprint)
    self.inst.replica.ptribe_pigmanager:SetBlueprint(_blueprint and _blueprint.name or "")
end

local function onresource(self, resource)
    self.inst.replica.ptribe_pigmanager:SetResource(resource)
end

local function onfavorability(self, favorability)
    self.inst.replica.ptribe_pigmanager:SetFavorability(math.floor(favorability)) --主机存小数，客机存整数
end

-- 部落管理者组件
local PigmanTribe = Class(function(self, inst)
    self.inst = inst

    if not TheWorld.ismastersim then
        return
    end

    -- 通用变量
    self.task = nil
    -- local task = {
    --     id = 1,      --buildPath中索引，表示一个存在的任务
    --     prefabId = 1,--模块中所在预制体编号，只有蓝图建设时有编号
    --     prefab = "", --预制体
    --     pos = {},    --生成位置
    --     tile = 1,    --地皮
    --     worker = 1,  --工作工人数量，类似一个锁
    --     startTime = nil, --开始时间
    --     endTime = nil,   --完成时间
    --     priority = nil --优先级，为nil时默认3级
    --     count = 0, -- 执行过的次数，超过一定次数将不再执行
    --     onFinish --执行完后的处理
    -- }

    self._blueprint = nil   --部落蓝图
    self.completeRate = 0   --部落完成率
    self.resource = 0       --资源量
    self.favorability = 0   --部落对玩家的好感度
    self.buildpathIndex = 0 --查找循环索引，不需要保存
    self.findBuildTaskIndex = 0
    self.findCollectTasksIndex = 0

    inst:DoTaskInTime(0, function()
        if not inst.ptribe_built then --玩家建造的已经在onbuilt中初始化过了
            self:Init()               --等设置好猪人房的位置后再初始化
        end
    end)
    inst:ListenForEvent("timerdone", OnTimerDone)
    inst:ListenForEvent("onremove", OnPigHouseRemove)
end, nil, {
    _blueprint = on_blueprint,
    resource = onresource,
    favorability = onfavorability
})

function PigmanTribe:OnEntitySleep()
    if self:IsValidManager() then
        self.inst.prtibe_start_sleep_time = GetTime()
        if self.postTask then
            self.postTask:Cancel()
            self.postTask = nil
        end
    end

    if self.doTask then
        self.doTask:Cancel()
        self.doTask = nil
    end
end

function PigmanTribe:OnEntityWake()
    local inst = self.inst
    if inst:HasTag("burnt") then return end

    -- 猪人发布任务
    if self.pigTaskEvent then
        for _, cdName in ipairs(self.pigTaskEvent) do
            PushPigTaskEvent(inst, cdName)
        end
        self.pigTaskEvent = {}
    end

    if inst.prtibe_start_sleep_time ~= nil then
        self:ResourceDoDelta(math.floor((GetTime() - inst.prtibe_start_sleep_time) / 30)) --30秒一个资源作为补偿
        self.postTask = inst:DoPeriodicTask(30, Utils.MergeFn(PostCollectTask, PostBuildTask, UpdateCompleteRate),
            math.random(5, 10))
        inst.prtibe_start_sleep_time = nil
    end

    local manager = self:GetManager()
    if manager and manager.components.ptribe_pigmanager:IsValidManager() and not self.doTask then
        self.doTask = inst:DoPeriodicTask(15, function() self:FindTask() end, math.random(1, 15)) --执行任务的周期函数
    end
end

--- 初始化，在onload之后
function PigmanTribe:Init()
    local inst = self.inst
    if inst:HasTag("burnt") then
        ClearTimer(inst)
        return
    end

    UpdateManager(inst)

    -- 管理者初始化蓝图
    if self:IsManager() then
        if not self._blueprint
            and not inst:HasTag("ptribe_forbidTribe") then
            self._blueprint = BluePrintsUtils.GetRandomBluePrint(inst)
            if not self._blueprint then
                --没有找到合适的蓝图时将不再查找
                inst:AddTag("ptribe_forbidTribe")
                inst:PushEvent("ptribe_postTask", {
                    task = "changeManager", --任务名
                })
            end
        end

        if self._blueprint then
            InitManager(inst)
        end
    end
end

function PigmanTribe:GetManager()
    return self.inst.components.entitytracker and self.inst.components.entitytracker:GetEntity("manager")
end

function PigmanTribe:LostManager()
    local oldManager = self:GetManager()
    if self.doTask then
        self.doTask:Cancel()
        self.doTask = nil
    end
    if oldManager then
        GetPrefab.RemoveEventCallback(self.inst, "ptribe_postTask", nil, oldManager)
        self.inst.components.entitytracker:ForgetEntity("manager")
    end
end

function PigmanTribe:SetNewManager(newManager)
    self:LostManager()
    local inst = self.inst

    -- 建立新的绑定
    inst.components.entitytracker:TrackEntity("manager", newManager)
    inst:ListenForEvent("ptribe_postTask", function(manager, data) TryDoTask(inst, manager, data) end, newManager)

    if not self.doTask then
        self.doTask = inst:DoPeriodicTask(15, function() self:FindTask() end, math.random(1, 15))
    end
end

function PigmanTribe:IsManager()
    return self:GetManager() == self.inst
end

--- 设置当前猪人房为村长房
function PigmanTribe:SetManager(blueprintName)
    if self:IsManager() then return end --不应该发生

    self:LostManager()

    self._blueprint = BluePrintsUtils.GetRandomBluePrint(self.inst, {
        name = blueprintName
    }, true)

    InitManager(self.inst)
end

---只有管理着有蓝图
function PigmanTribe:IsValidManager()
    return self._blueprint ~= nil and not self.inst:HasTag("burnt")
end

---部落蓝图的最大半径
function PigmanTribe:GetMaxDisSq()
    if not self._blueprint then return -1 end
    return BluePrintsUtils.GetMaxDisSq(self._blueprint.name)
end

local function addTask(tasks, newTasks)
    for _, newTask in ipairs(newTasks) do
        for i = 1, TUNING.TRIBE_TASKS_LENGTH do
            if not tasks[i].id or tasks[i].endTime then
                tasks[i] = newTask
                break
            end
        end
    end
end

function PigmanTribe:AddBuildTask(newTasks)
    if self.buildTasks then
        addTask(self.buildTasks, newTasks)
    end
end

function PigmanTribe:AddCollectTask(newTasks)
    if self.collectTasks then --好像有时候为空的也会调用
        addTask(self.collectTasks, newTasks)
    end
end

function PigmanTribe:FindTask()
    if self.inst:HasTag("burnt")
        or self.inst.components.timer:TimerExists("ptribe_findTask_cd")
    then
        return
    end
    self.inst.components.timer:StartTimer("ptribe_findTask_cd", 30) --加一个冷却，不管找没找到

    local child = self.inst.components.spawner and self.inst.components.spawner.child
    if not child
        or not child:IsValid()
        or self.inst.components.spawner:IsOccupied()                        --在房子里
        or child:HasTag("ptribe_tribeinvader")                              --部落入侵
        or (child.components.follower and child.components.follower.leader) --被雇佣就不计数了
    then
        return
    end

    -- 正在执行任务
    local cur = GetTime()
    if self.task then
        if cur - self.task.startTime > TUNING.TRIBE_DO_TASK_TIMEOUT then
            self:CancelTask() -- 超时就放弃当前任务，等下一次计时
        end
        return
    end

    -- 管理者不存在或没有蓝图
    local manager = self:GetManager()
    if not manager or not manager.components.ptribe_pigmanager:IsValidManager() then
        self:LostManager()
        return
    end

    -- 挑选任务，优先建设，然后采集资源
    local newTask
    local buildTasks = manager.components.ptribe_pigmanager.buildTasks
    for _ = 1, #buildTasks do
        self.findBuildTaskIndex = (self.findBuildTaskIndex + 1) % (#buildTasks + 1)
        self.findBuildTaskIndex = self.findBuildTaskIndex ~= 0 and self.findBuildTaskIndex or 1

        local task = buildTasks[self.findBuildTaskIndex]
        if task.id
            and not task.endTime
            and (not task.worker or task.worker <= 0) then
            newTask = task
            newTask.worker = 1 --类似一个锁
            newTask.startTime = cur
            break
        end
    end

    if not newTask then
        local collectTasks = manager.components.ptribe_pigmanager.collectTasks
        for _ = 1, #collectTasks do
            self.findCollectTasksIndex = (self.findCollectTasksIndex + 1) % (#collectTasks + 1)
            self.findCollectTasksIndex = self.findCollectTasksIndex ~= 0 and self.findCollectTasksIndex or 1

            local task = collectTasks[self.findCollectTasksIndex]
            if task.id and not task.endTime and (not task.worker or task.worker <= 0) then
                newTask = task
                newTask.worker = 1
                newTask.startTime = cur
                break
            end
        end
    end

    if newTask then
        self.task = newTask
        TheWorld.components.ptribe_tribemanager:TrySetPigCanSleep(child, false)
    end

    -- 剩下的交给猪人
    return newTask
end

local function GetTasksNum(tasks)
    local num = 0
    for _, task in ipairs(tasks) do
        if task.id and not task.endTime then
            num = num + 1
        end
    end
    return num
end

--- 获取待执行的任务数量
function PigmanTribe:GetBuildTasksNum()
    return GetTasksNum(self.buildTasks or {}) --虽然感觉这里不用判断，但是有mod喜欢将村长房打包？
end

function PigmanTribe:GetCollectTasksNum()
    return GetTasksNum(self.collectTasks or {})
end

function PigmanTribe:CancelTask()
    local child = self.inst.components.spawner and self.inst.components.spawner.child
    if child and child:IsValid() then
        TheWorld.components.ptribe_tribemanager:TrySetPigCanSleep(child, true)
    end

    if not self.task then return end

    self.task.startTime = nil
    self.task.worker = self.task.worker - 1
    self.task.count = 1 + (self.task.count and self.task.count or 0) --执行次数+1

    -- 不再执行
    if self.task.count > TUNING.TRIBE_TASK_MAX_COUNT then
        self.task.endTime = GetTime()

        if self.task.id and self.task.prefabId then
            local manager = self:GetManager()
            local managerSelf = manager and manager.components.ptribe_pigmanager
            if managerSelf and managerSelf:IsValidManager()
                and managerSelf._blueprint.buildPath[self.task.id] and managerSelf._blueprint.buildPath[self.task.id][self.task.prefabId] then
                managerSelf._blueprint.buildPath[self.task.id][self.task.prefabId].fail = true --失败标记，不再执行
            end
        end
    end

    self.task = nil
end

function PigmanTribe:FinishTask()
    local child = self.inst.components.spawner and self.inst.components.spawner.child
    if child and child:IsValid() then
        TheWorld.components.ptribe_tribemanager:TrySetPigCanSleep(child, true)
    end

    if not self.task then return end

    for k in pairs(self.task) do
        self.task[k] = nil
    end
    self.task = nil
end

function PigmanTribe:GetManagerResource()
    local manager = self:GetManager()
    if manager and manager.components.ptribe_pigmanager:IsValidManager() then
        return manager.components.ptribe_pigmanager.resource
    end
    return 0
end

function PigmanTribe:ResourceDoDelta(amount)
    self.resource = math.clamp(self.resource + amount, -32766, 32766)
    self.inst:PushEvent("ptribe_triberesourcedelta", { amount = amount })
end

function PigmanTribe:ManagerResourceDoDelta(amount)
    local manager = self:GetManager()
    if manager and manager.components.ptribe_pigmanager:IsValidManager() then
        manager.components.ptribe_pigmanager:ResourceDoDelta(amount)
    end
end

local RANDOM_CELEBRATE_FX = { "confetti_fx", "rabbit_confetti_fx" }
--- 放烟花
local function SpawnConfettiFx(inst)
    local pos = inst:GetPosition()
    Timer.ScheduleRepeatingTasks(inst, function()
        local spawnPos = Shapes.GetRandomLocation(pos, 2, 12)
        SpawnAt(RANDOM_CELEBRATE_FX[math.random(1, #RANDOM_CELEBRATE_FX)], spawnPos)
    end, math.random(8, 12), {
        minGap = 0.7,
        maxGap = 1.6
    })
end

function PigmanTribe:FavorabilityDelta(amount)
    local oldFav = self.favorability
    self.favorability = math.clamp(self.favorability + amount, -100, 100)

    if oldFav < 100 and self.favorability >= 100 then
        self.inst.AnimState:Show("YOTP")

        SpawnConfettiFx(self.inst)
    elseif oldFav >= 100 and self.favorability < 100 then
        self.inst.AnimState:Hide("YOTP")
    end
end

function PigmanTribe:ManagerFavorabilityDelta(amount)
    local manager = self:GetManager()
    if manager and manager.components.ptribe_pigmanager:IsValidManager() then
        manager.components.ptribe_pigmanager:FavorabilityDelta(amount)
    end
end

-- 部落完成率
function PigmanTribe:GetCompleteRate()
    local manager = self:GetManager()
    return manager and manager.components.ptribe_pigmanager.completeRate or 0
end

function PigmanTribe:OnSave()
    --不保存过大数据
    local bp = nil
    if self._blueprint then
        bp = shallowcopy(self._blueprint)
        bp.tab = nil
        bp.buildPath = nil
    end

    return {
        ptribe_forbidTribe = self.inst:HasTag("ptribe_forbidTribe") and true or false,
        _blueprint = bp,
        -- tasks = tasks, --不保存
        resource = self.resource,
        favorability = self.favorability,
        completeRate = self.completeRate,
    }
end

function PigmanTribe:OnLoad(data)
    if not data then return end

    if data.ptribe_forbidTribe then
        self.inst:AddTag("ptribe_forbidTribe")
    elseif data._blueprint then
        self._blueprint = BluePrintsUtils.GetRandomBluePrint(self.inst, data._blueprint) --重新获取tab、buildPath
    end

    self.resource = data.resource or self.resource
    self.favorability = data.favorability or self.favorability
    self.completeRate = data.completeRate or self.completeRate
end

return PigmanTribe
