local Skins = require("ptribe_skins")

local function OnPlayerJoined(inst, player)
    if not player.userid then return end
    local self = inst.components.ptribe_pigskinmanager

    for prefab, skins in pairs(PREFAB_SKINS) do
        for _, skin in ipairs(skins) do
            if TheInventory:CheckClientOwnership(player.userid, skin) then
                self.skins[prefab] = self.skins[prefab] or {}
                self.skins[prefab][skin] = self.skins[prefab][skin] or {}
                self.skins[prefab][skin][player.userid] = true
            end
        end
    end
end

local function OnPlayerLeft(inst, player)
    if not player.userid then return end

    local self = inst.components.ptribe_pigskinmanager
    for _, skins in pairs(self.skins) do
        for _, skin in pairs(skins) do
            skin[player.userid] = nil
        end
    end
end

local PigSkinManager = Class(function(self, inst)
    self.inst = inst

    self.skins = {} --[预制体名][皮肤名] = {持有的玩家id = true}

    if TUNING.TRIBE_PLAYER_SKIN_ENABLE then
        inst:ListenForEvent("ms_playerjoined", OnPlayerJoined)
        inst:ListenForEvent("ms_playerleft", OnPlayerLeft)
    end
end)

--- 随机换皮，参考清洁扫把
function PigSkinManager:RandomSkin(target)
    if not target:IsValid() then return end

    local prefab_to_skin = target.prefab
    local is_beard = false
    if target.components.beard ~= nil and target.components.beard.is_skinnable then
        prefab_to_skin = target.prefab .. "_beard"
        is_beard = true
    end

    local skins = self.skins[prefab_to_skin] or {}

    -- 可用皮肤
    local available = {}
    for name, skin in pairs(skins) do
        local userid = next(skin)
        if userid then
            available[name] = userid
        end
    end

    -- 房子皮肤有两套，一套anim，一套idle，只使用anim可能有点浪费了，但是要换就得覆盖源码，包括燃烧播放动画、受击动画
    -- 哈姆雷特猪镇房子皮肤
    if target.prefab == "pighouse"
        and target.components.ptribe_saveskin then
        local pigcity = #Skins.PIGHOUSE_SKINS
        local all = GetTableSize(available) + pigcity
        if all > 0 and math.random() < (pigcity / all) then
            local i = math.random(1, #Skins.PIGHOUSE_SKINS)
            target.components.ptribe_saveskin:Set({
                bank = Skins.PIGHOUSE_SKINS[i].bank,
                build = Skins.PIGHOUSE_SKINS[i].build,
                index = i, --额外数据
            })
            target.components.ptribe_saveskin:Enable(true)
            return
        end
    end

    if next(available) then
        local cached_skin, userid = GetRandomItemWithIndex(available)

        if is_beard then
            target.components.beard:SetSkin(cached_skin)
        else
            TheSim:ReskinEntity(target.GUID, target.skinname, cached_skin, nil, userid)
        end
    end
end

return PigSkinManager
