local Utils = require("ptribe_utils/utils")
local ResourceConvert = require("ptribe_resourceconvert")

local PIG_MUST_TAGS = { "character", "pig" }
local function MakePigmanAttackPlayer(player, isOneAttack)
    local x, y, z = player.Transform:GetWorldPosition()
    for _, v in ipairs(TheSim:FindEntities(x, y, z, 20, PIG_MUST_TAGS)) do
        if v.components.combat and not v.components.combat.target
            and (not v.components.follower or not v.components.follower.leader or not v.components.follower.leader:HasTag("player")) then
            v.components.combat:SetTarget(player)
            if isOneAttack then
                break
            end
        end
    end
end

local function Init(inst, self)
    if inst.prefab == "pighouse"
        or inst:HasTag("ptribe_pigfactory")
        or inst:HasTag("ptribe_pigmanbuild")
    then
        self:Init(true)
    end
end

local PigManBuild = Class(function(self, inst)
    self.inst = inst

    inst:DoTaskInTime(0, Init, self)
end)

--- 禁止熄灭名单
local FORBID_EXTINCTS = {
    ptribe_pigtorch = true,
    pigtorch = true,
    pigking_pigtorch = true                                          --永不妥协
}
local PIG_CANT_TAGS = { "guard", "moonbeast", "werepig", "hostile" } --猪人守卫不要
local function OnIgnite(inst)
    if FORBID_EXTINCTS[inst.prefab] then return end

    -- 选择最近的猪人（有房子）去灭火
    local pig = FindEntity(
        inst,
        16,
        function(guy)
            local home = guy.components.homeseeker and guy.components.homeseeker:GetHome()
            return home and home.components.ptribe_pigmanager
                and (not home.components.ptribe_pigmanager.task or home.components.ptribe_pigmanager.task.priority ~= 1)
        end,
        PIG_MUST_TAGS,
        PIG_CANT_TAGS)

    if pig then
        local home = pig.components.homeseeker and pig.components.homeseeker:GetHome()
        home.components.ptribe_pigmanager.task = {
            id = 0,
            prefab = inst,
            priority = 1,          --一级优先级
            worker = 1,
            startTime = GetTime(), --这些属性用不到，只是为了和一般task格式兼容
            type = "extinguish"
        }
        home.components.timer:StopTimer("ptribe_findTask_cd")
        home.components.timer:StartTimer("ptribe_findTask_cd", 30)
    end
end

--- 当玩家收获部落的资源的时候，如果随从中有部落的猪人，猪人就不会攻击，否则最近的一个猪人攻击玩家
---@param influencedByFollower boolean|nil 是否判断随从为同一部落，如果判断则可以通过减少跟随时间来补偿
---@param isDirectAttack boolean|nil 是否直接攻击，非玩家肯定直接攻击
local function OnPlayerStealProperty(inst, worker, loot, influencedByFollower, isDirectAttack)
    -- print("判断", inst:HasTag("burnt"), not worker or not worker.components.combat, worker:HasTag("pig"), #loot <= 0)
    if not inst:HasTag("ptribe_pigmanbuild")
        or inst:HasTag("burnt")
        or not worker or not worker.components.combat
        or worker:HasTag("pig")
        or #loot <= 0 then
        return
    end


    if worker:HasTag("sneaky")                                                                 -- 鬼祟帽逻辑
        or worker.prefab == "wilba"
        or (worker.components.leader and worker.components.leader:CountFollowers("wilba") > 0) --兼容我的队友mod
    then
        return
    end

    local resources = 0
    for _, item in ipairs(loot) do
        local prefab, count
        if type(item) == "string" then
            prefab = item
            count = 1
        else
            prefab = item.prefab
            count = GetStackSize(item)
        end
        resources = resources + ResourceConvert.GetResource(prefab) * count
    end

    -- 判断玩家所处部落的村长房
    local manager = worker.components.ptribe_entnearby
        and worker.components.ptribe_entnearby:FindEntityByPrefab("ptribe_nearby_manager", "pighouse")

    -- 非玩家或特殊行为直接攻击
    if not worker:HasTag("player") or isDirectAttack then
        MakePigmanAttackPlayer(worker, isDirectAttack) -- 直接攻击
        if worker:HasTag("player") and manager then
            manager.components.ptribe_pigmanager:FavorabilityDelta(-math.max(2, resources * 2))
        end
        return
    end

    if not manager then
        -- 组件还不能移除，有些不可拆除的建筑需要每次都添加workable
        -- if not FindEntity(inst, TUNING.TRIBE_MAX_DIS, function(ent) return ent.components.ptribe_pigmanager ~= nil end, PIGHOUSE_MUST_TAGS) then
        --     -- 如果附近没有猪人房了说明，该建筑已经不归猪人管了，其实这里只是针对玩家行为做了判断，如果是其他的没有判断是否还有部落
        --     inst:RemoveComponent("ptribe_pigmanbuild")
        -- end
        return
    end

    -- 检查是否可以用忠诚度来抵消淘气值的增加
    if influencedByFollower then
        for k, _ in pairs(worker.components.leader.followers) do
            local home = k.components.homeseeker and k.components.homeseeker:GetHome()
            if home and home.components.ptribe_pigmanager and home.components.ptribe_pigmanager:GetManager() == manager then
                -- 虽然不攻击了，这里减少雇佣时长
                k.components.follower:AddLoyaltyTime(-6 * resources)
                return
            end
        end
    end

    manager.components.ptribe_pigmanager:FavorabilityDelta(-math.max(2, resources * 2))
end

local function OnFinishBefore(inst, worker)
    OnPlayerStealProperty(inst, worker, { inst })
end

local function OnPickedFnBefore(inst, picker, loot)
    OnPlayerStealProperty(inst, picker, { loot }, true)
end

local function OnHarvestBefore(inst, product, harvester)
    OnPlayerStealProperty(inst, harvester, { product }, false, true)
end

local function HarvestBefore(self, harvester)
    if not self.chef_id then --如果是玩家做的菜猪人不打
        OnPlayerStealProperty(self.inst, harvester, { self.product }, false, true)
    end
end

local function OnHarvestFnBefore(inst, picker, produce)
    local products = {}
    for _ = 1, produce, 1 do
        table.insert(products, inst.components.harvestable.product)
    end
    OnPlayerStealProperty(inst, picker, products, false, true)
end

-- 太乱了，等执行onharvest时我已经拿不到产物了，只能直接覆盖Harvest
local function OnDryerHarvestBefore(self, harvester)
    if not self:IsDone() or harvester == nil or harvester.components.inventory == nil then
        return
    end
    OnPlayerStealProperty(self.inst, harvester, { self.product }, false, true)
end

-- 当猪人房受到攻击时，猪人都会冲出来攻击攻击者
local function OnHouseWorked(inst, data)
    local worker = data.worker
    if worker
        and worker.prefab ~= "wilba"
        and inst.components.spawner --烧焦后就没spawner组件了
    then
        local child = inst.components.spawner.child
        if child and child.components.combat:CanTarget(worker) then
            if inst.components.spawner:IsOccupied() then
                inst.components.spawner:ReleaseChild()
            end
            child.components.combat:SetTarget(worker)
        end
    end
end

-- 管理者被摧毁时，猪人将陷入疯狂状态，短时间的绝对仇恨，并且猪人将获得移速强化
local PIG_MUST_TAGS = { "character", "pig" }
local function OnManagerFinishBefore(inst, worker)
    if inst:HasTag("burnt")         --已经烧毁
        or not worker
        or worker.prefab == "wilba" --猪猪公主
        or worker:HasTag("sneaky")  --鬼祟帽
    then
        return
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    for _, v in ipairs(TheSim:FindEntities(x, y, z, 20, PIG_MUST_TAGS)) do
        if v.components.combat and (not v.components.follower
                or not v.components.follower.leader or not v.components.follower.leader:HasTag("player")) then
            v.components.combat:SetTarget(worker)
            --绝对仇恨，不过也只有村长房的猪人会一直追玩家，其他猪人还是不会远离猪人房
            v.oldDropTarget = v.components.combat.DropTarget
            v.components.combat.DropTarget = Utils.EmptyFn

            v:DoTaskInTime(30, function(v, task)
                if task then task:Cancel() end
                v.components.combat.DropTarget = v.oldDropTarget
            end, v:DoPeriodicTask(0, function()
                v.components.locomotor:PushTempGroundSpeedMultiplier(2, WORLD_TILES.MUD)
            end))
        end
    end
end

-- 猪人享有终生所有权
function PigManBuild:Init(isLoading)
    local inst = self.inst

    if isLoading then
        ResourceConvert.PrefabLoad(self.inst)
    end

    inst:AddTag("ptribe_pigmanbuild")

    if inst.components.burnable
        and inst.prefab ~= "pigtorch" then                                    --猪火炬不用灭火
        inst:ListenForEvent("onignite", OnIgnite)                             --点燃
        Utils.FnDecorator(inst.components.burnable, "onsmoldering", OnIgnite) --冒烟
    end

    local cd = ResourceConvert.GetInteractCd(inst.prefab)
    if cd then
        inst.ptribe_interactCd = cd
        inst.ptribe_lastInteractTime = GetTime() - cd + math.random(5, 60)
    end

    if TUNING.TRIBE_STEAL_PIGMAN_PROPERTY_PUHISH then
        -- 监听玩家的偷窃行为，中途添加的组件我不管
        -- 官方各个组件写法不统一，针对每个组件挺麻烦的，这里没有采用监听事件的原因是因为预制体可能在onXXX中就移除了，就不会再调用监听回调
        if inst.components.workable then
            if inst.prefab == "pighouse" then
                inst:ListenForEvent("worked", OnHouseWorked)
                Utils.FnDecorator(inst.components.workable, "onfinish",
                    inst.components.ptribe_pigmanager:IsValidManager() and OnManagerFinishBefore or OnFinishBefore)
            elseif not inst:HasTag("tree") then --树木拆除无惩罚
                Utils.FnDecorator(inst.components.workable, "onfinish", OnFinishBefore)
            end
        end
        if inst.components.pickable then
            Utils.FnDecorator(inst.components.pickable, "onpickedfn", OnPickedFnBefore)
        end
        if inst.components.crop then --农场
            Utils.FnDecorator(inst.components.crop, "onharvest", OnHarvestBefore)
        end
        if inst.components.stewer then --烹饪锅
            Utils.FnDecorator(inst.components.stewer, "Harvest", HarvestBefore)
        end
        if inst.components.harvestable then
            Utils.FnDecorator(inst.components.harvestable, "onharvestfn", OnHarvestFnBefore)
        end
        if inst.components.dryer then
            Utils.FnDecorator(inst.components.dryer, "Harvest", OnDryerHarvestBefore)
        end
    end
end

function PigManBuild:OnRemoveFromEntity()
    self.inst:RemoveTag("ptribe_pigmanbuild")

    self.inst:RemoveEventCallback("onignite", OnIgnite)
    self.inst:RemoveEventCallback("worked", OnHouseWorked)
end

function PigManBuild:OnSave()
    return {
        ptribe_pigmanbuild = self.inst:HasTag("ptribe_pigmanbuild")
    }
end

function PigManBuild:OnLoad(data)
    if data.ptribe_pigmanbuild then
        self.inst:AddTag("ptribe_pigmanbuild")
    end
end

return PigManBuild
