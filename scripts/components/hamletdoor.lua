local function OnRemove(inst)
    if inst.components.hamletdoor.centerPos then
        TheWorld.components.interiorspawner:OnHouseDestroy(inst, nil, true)
    end
end

local function OnDoorChange(self, centerPos)
    if centerPos then
        if not self.inst:HasTag("hamlet_door") then
            self.inst:AddTag("hamlet_door")
        end
    else
        self.inst:RemoveTag("hamlet_door")
    end
end

---室内数据
local HamletDoor = Class(function(self, inst)
    self.inst = inst

    self.centerPos = nil --室内中心位置

    -- 这里只写移除事件，锤毁不是监听事件，在预制体里单独处理
    self.inst:ListenForEvent("onremove", OnRemove)
end, nil, {
    centerPos = OnDoorChange
})

function HamletDoor:SetCenterPos(pos)
    self.centerPos = pos
end

function HamletDoor:OnSave()
    return {
        centerPos = self.centerPos,
        hamlet_door = self.inst:HasTag("hamlet_door") or nil
    }
end

function HamletDoor:OnLoad(data)
    if not data then return end

    self.centerPos = data.centerPos
    if data.hamlet_door then
        self.inst:AddTag("hamlet_door")
    end
end

return HamletDoor
