local GetPrefab = require("ptribe_utils/getprefab")
local PigFactoryUtils = require("ptribe_pigfactoryutils")

local function CheckContainer(inst)
    local self = inst.components.ptribe_pigfactory
    local weapon = GetPrefab.GetContainerFirstItem(inst.components.container)
    local tribe = self:GetTribe()
    local isFull = inst.components.container:IsFull()

    if isFull then
        inst.AnimState:Show("YOTP")
    else
        inst.AnimState:Hide("YOTP")
    end

    if self.spawnItemTask then
        self.spawnItemTask:Cancel()
        self.spawnItemTask = nil
    end

    if weapon
        and not isFull
        and tribe then
        self.product = weapon.prefab
        self.data = PigFactoryUtils.GetWeaponData(weapon.prefab)
        self.cooldown = self.tempRemainTime or
            (self.data.coolDown * (1 - 0.5 * tribe.components.ptribe_pigmanager.completeRate))
        self.spawnItemTask = inst:DoTaskInTime(self.cooldown, function() self:SpawnItem() end)
    else
        self.product = ""
    end
    self.tempRemainTime = nil
end

local function OnClose(inst)
    local self = inst.components.ptribe_pigfactory
    local weapon = GetPrefab.GetContainerFirstItem(inst.components.container)
    if weapon then
        if weapon.prefab ~= self.product or not self.spawnItemTask then
            CheckContainer(inst)
        end
    else
        if self.spawnItemTask then
            self.spawnItemTask:Cancel()
            self.spawnItemTask = nil
        end
    end
end

local function onproduct(self, product)
    self.inst.replica.ptribe_pigfactory:SetProduct(product)
end

local function oncooldown(self, cooldown)
    self.inst.replica.ptribe_pigfactory:SetCooldown(cooldown)
end

local PigFactory = Class(function(self, inst)
    self.inst = inst

    self.tribe = nil         --所属部落
    self.product = ""        --产物，同时表示是否正在生产
    self.data = nil          --生产数据
    self.cooldown = 0        --下次生成所需时间
    self.spawnItemTask = nil --生成任务
    self.taskStartTime = nil
    self.tempRemainTime = nil

    inst:DoTaskInTime(0, CheckContainer)

    inst:AddTag("ptribe_pigfactory")

    inst:ListenForEvent("onclose", OnClose)
end, nil, {
    product = onproduct,
    cooldown = oncooldown
})

function PigFactory:OnEntityWake()
    --这段代码主要为客机正确显示倒计时服务，每次重新进入加载范围客机都会自动调用一次监听回调，不知道为什么
    if self.spawnItemTask then
        self.cooldown = GetTaskRemaining(self.spawnItemTask)
    end
end

--- 验证管理者是否可用
local function CheckManager(inst)
    local manager = inst.components.ptribe_pigmanager:GetManager()
    return manager and manager.components.ptribe_pigmanager:IsValidManager()
end

local function OnResourceChanged(self, data)
    if data and data.amount > 0 and not self.spawnItemTask then
        CheckContainer(self.inst)
    end
end

--- 获取附近的村长房
function PigFactory:GetTribe()
    if self.tribe then --村长房没有取消的设定，所以这里不用判断
        if self.tribe:IsValid() then
            return self.tribe
        else
            self.tribe = nil
        end
    end

    local x, y, z = self.inst.Transform:GetWorldPosition()
    for _, v in pairs(TheSim:FindEntities(x, y, z, TUNING.TRIBE_MAX_DIS)) do
        if v.components.ptribe_pigmanager
            and CheckManager(v)
            and self.inst:GetDistanceSqToInst(v) <= v.components.ptribe_pigmanager:GetMaxDisSq()
        then
            self.tribe = v
            self.inst:ListenForEvent("ptribe_triberesourcedelta", function(inst, data)
                OnResourceChanged(self, data)
            end, self.tribe)
            return v
        end
    end
end

--- 制作一把武器放入容器
function PigFactory:SpawnItem()
    local inst = self.inst
    self.spawnItemTask = nil

    local isFull = inst.components.container:IsFull()
    if isFull then
        inst.AnimState:Show("YOTP")
        self.product = ""
        return
    end

    local tribe = self:GetTribe()
    if not tribe then
        self.product = ""
        return
    end

    if tribe.components.ptribe_pigmanager.resource >= self.data.minResource then
        local weapon = SpawnPrefab(self.product)
        weapon:AddTag("ptribe_pigmanbuild")
        if math.random() < TUNING.TRIBE_CHANGE_SKIN_PROB then
            TheWorld.components.ptribe_pigskinmanager:RandomSkin(weapon)
        end
        inst.components.container:GiveItem(weapon)

        local x, y, z = inst.Transform:GetWorldPosition()
        SpawnPrefab("carnival_sparkle_fx").Transform:SetPosition(x + math.random(-1, 1), y + 8,
            z + math.random(-1, 1))

        tribe.components.ptribe_pigmanager:ResourceDoDelta(-self.data.cost)
        CheckContainer(inst)
    else
        self.product = ""
    end
end

function PigFactory:OnSave()
    return {
        tempRemainTime = self.spawnItemTask and GetTaskRemaining(self.spawnItemTask) or nil
    }
end

function PigFactory:OnLoad(data)
    if not data then return end

    self.tempRemainTime = data.tempRemainTime or self.tempRemainTime
end

return PigFactory
