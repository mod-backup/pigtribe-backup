local PigmanTribe = Class(function(self, inst)
    self.inst = inst

    self.blueprint = net_string(inst.GUID, "ptribe_pigmanager.blueprint", "ptribe_pigmanager.blueprint")        --蓝图名
    self.resource = net_shortint(inst.GUID, "ptribe_pigmanager.resource", "ptribe_pigmanager.resource")         --[-32767..32767]，资源是可以为负数的
    self.favorability = net_byte(inst.GUID, "ptribe_pigmanager.favorability", "ptribe_pigmanager.favorability") --[0..255]，好感度0这里对应-100
end)

function PigmanTribe:GetBlueprint()
    return self.blueprint:value()
end

function PigmanTribe:GetResource()
    return self.resource:value()
end

function PigmanTribe:GetFavorability()
    return self.favorability:value() - 100
end

function PigmanTribe:SetBlueprint(blueprint)
    self.blueprint:set(blueprint)
end

function PigmanTribe:SetResource(resource)
    self.resource:set(resource)
end

function PigmanTribe:SetFavorability(favorability)
    self.favorability:set(favorability + 100)
end

return PigmanTribe
