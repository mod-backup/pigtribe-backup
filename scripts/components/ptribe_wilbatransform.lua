local GetPrefab = require("ptribe_utils/getprefab")

local MONSTERMEAT_COUNT = 4
local MONSTER_COOLDOWN = TUNING.TOTAL_DAY_TIME * 2 / MONSTERMEAT_COUNT

local function CheckWere(inst)
    local self = inst.components.ptribe_wilbatransform
    inst:DoTaskInTime(0, function() self:SetWere(self:CanGoWere()) end)
end

local function OnStartDay(inst)
    local self = inst.components.ptribe_wilbatransform
    if self.ready_to_transform and not self.from_food then
        self.ready_to_transform = false
    end
    CheckWere(inst)
end

local function OnFullmoon(inst, isnight)
    if isnight then
        CheckWere(inst)
    end
end

local function cooldown_monster_count(inst)
    local self = inst.components.ptribe_wilbatransform

    self.monster_count = self.monster_count - 1
    if self.monster_count <= 0 then
        self.monster_count = 0

        if self.ready_to_transform then
            inst.components.talker:Say(GetString(inst.prefab, "ANNOUNCE_NECKLACE_INACTIVE"))
        end

        self.ready_to_transform = false
        self.from_food = false
        CheckWere(inst)
    else
        self.cooldown_schedule = (self.monster_cooldown - self.cooldown_schedule) % MONSTER_COOLDOWN
        if self.cooldown_schedule == 0 then
            self.cooldown_schedule = MONSTER_COOLDOWN
        end

        self.transform_task, self.trans_task_info = inst:ResumeTask(self.cooldown_schedule, cooldown_monster_count)
    end
end

local function OnEat(inst, data)
    local self = inst.components.ptribe_wilbatransform
    if data.food.prefab == "waterdrop" then
        inst.components.timer:StopTimer("ptribe_wilbaCanToDaywalker")
        inst.components.timer:StartTimer("ptribe_wilbaCanToDaywalker", 3 * 60)
    elseif (data.food.prefab == "monsterlasagna" or data.food.prefab == "monstertartare")
        and self.were
        and (TheWorld.state.isfullmoon or inst.components.timer:TimerExists("ptribe_wilbaCanToDaywalker")) then
        inst.sg:GoToState("transform_to_werewilba2", "daywalker")
    end

    if data.food:HasTag("monstermeat") then
        self.monster_count = self.monster_count + 1
        self.monster_cooldown = self.monster_cooldown + MONSTER_COOLDOWN

        if self.transform_task then
            self.transform_task:Cancel()
            self.transform_task = nil
        end

        self.cooldown_schedule = self.monster_cooldown % MONSTER_COOLDOWN
        if self.cooldown_schedule == 0 then
            self.cooldown_schedule = MONSTER_COOLDOWN
        end

        self.transform_task, self.trans_task_info = inst:ResumeTask(self.cooldown_schedule, cooldown_monster_count)

        if self.monster_count >= MONSTERMEAT_COUNT then
            self.from_food = true
        end

        CheckWere(inst)
    end
end

local function onresurrect(inst)
    local self = inst.components.ptribe_wilbatransform

    self.ready_to_transform = false
    self.from_food = false
    self.monster_count = 0
    self.monster_cooldown = 0
    self.cooldown_schedule = 0

    if self.transform_task then
        self.transform_task:Cancel()
        self.transform_task = nil
        self.trans_task_info = nil
    end

    if self.were then
        self:TransformToWilba()
        self.were = false
    end
end

local function OnDeath(inst)
    local self = inst.components.ptribe_wilbatransform
    if self.were then
        self:TransformToWilba2()
    end
end

local function ontype(self, type)
    self.inst.replica.ptribe_wilbatransform:SetType(type)
end

---薇尔芭变身组件
local WilbaTransform = Class(function(self, inst)
    self.inst = inst

    self.ready_to_transform = false
    self.from_food = false
    self.were = false --可以用这个判断，也可以用monster标签判断
    self.monster_count = 0
    self.monster_cooldown = 0
    self.cooldown_schedule = 0

    self.type = "" --变身形态

    inst:WatchWorldState("startday", OnStartDay)
    inst:WatchWorldState("isfullmoon", OnFullmoon)
    inst:ListenForEvent("oneat", OnEat)
    inst:ListenForEvent("respawnfromghost", onresurrect)
    inst:ListenForEvent("death", OnDeath)
end, nil, {
    type = ontype
})

----------------------------------------------------------------------------------------------------

function WilbaTransform:CanGoWere()
    return self.monster_count >= MONSTERMEAT_COUNT
        or self.from_food
        or TheWorld.state.isfullmoon
end

local function NecklaceComment(inst)
    local self = inst.components.ptribe_wilbatransform
    if self.comment_task then
        self.comment_task:Cancel()
        self.comment_task = nil
    end

    inst.components.talker:Say(GetString(inst.prefab, "ANNOUNCE_NECKLACE_ACTIVE"))

    if self.ready_to_transform and not self.were then
        self.comment_task = inst:DoTaskInTime(math.random(TUNING.TOTAL_DAY_TIME / 15, TUNING.TOTAL_DAY_TIME / 10),
            NecklaceComment)
    end
end

function WilbaTransform:SetWere(active)
    if active then
        if not self.were then
            self.ready_to_transform = true
            self.inst:PushEvent("ready_to_transform")

            local current = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
            if not current or current.prefab ~= "silvernecklace" then
                self.inst.components.talker:Say(GetString(self.inst.prefab, "ANNOUNCE_TRANSFORM"))
                self.inst:PushEvent("transform_to_werewilba")
            else
                NecklaceComment(self.inst)
            end
        end
    else
        self.ready_to_transform = false
        self.inst:PushEvent("end_ready_to_transform")
        if self.were then
            self.inst:PushEvent("transform_to_wilba")
        end
    end
end

local function SettHungerMulti(inst, val)
    inst.components.hunger.burnratemodifiers:RemoveModifier(inst, "werewilba")
    if val and val ~= 1 then
        inst.components.hunger.burnratemodifiers:SetModifier(inst, val, "werewilba")
    end
end

local function StopRegen(inst)
    inst.components.health:StopRegen()
end

local function StartRegen(inst)
    inst.components.health:StartRegen(0.5, 1)
end

---从其他形态变回来
function WilbaTransform:TransformToWilba()
    self.were = false
    self.type = ""
    self.monster_count = 0 --累积值清零
    self.from_food = false

    local inst = self.inst
    inst.AnimState:SetBuild("wilba")
    inst.AnimState:SetBank("wilson")
    inst.AnimState:AddOverrideBuild("werewilba_transform")

    inst.components.mightiness.state = "normal"

    SettHungerMulti(inst)
    GetPrefab.SetMaxHealth(inst, TUNING.WILBA_HEALTH)
    inst.components.sanity.night_drain_mult = 1.5

    inst.components.locomotor.walkspeed = TUNING.WILSON_WALK_SPEED
    inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED
    inst.components.locomotor.allow_platform_hopping = true

    inst.components.combat:SetDefaultDamage(TUNING.UNARMED_DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.WILSON_ATTACK_PERIOD)
    inst.components.combat:SetRange(TUNING.DEFAULT_ATTACK_RANGE)
    inst.components.combat.hiteffectsymbol = "torso"

    inst:RemoveEventCallback("startstarving", StopRegen)
    inst:RemoveEventCallback("stopstarving", StartRegen)
    StopRegen(inst)

    inst:RemoveComponent("worker")

    inst:RemoveTag("monster")
    inst:AddTag("pigelite")
    inst:AddTag("pigroyalty")

    inst.components.temperature:SetTemp(nil)

    inst.Light:Enable(false)

    inst.SoundEmitter:KillSound("werepigmusic")
    inst.soundsname = "wilba"

    inst.components.timer:StopTimer("ptribe_grunt")
    inst.components.timer:StopTimer("ptribe_breath")
    inst.components.timer:StopTimer("ptribe_breath2")
end

--- 先凑合一下，直接调用，不需要sg
function WilbaTransform:TransformToWilba2()
    self.inst.components.ptribe_wilbatransform:TransformToWilba()
    SpawnAt("dreadstone_spawn_fx", self.inst)
    self.inst.sg:GoToState("idle")
end

---变疯猪
function WilbaTransform:TransformToWere()
    for _, v in pairs(EQUIPSLOTS) do
        local euqip = self.inst.components.inventory:GetEquippedItem(v)
        if euqip
            and euqip.components.inventoryitem
            and (euqip.components.inventoryitem.cangoincontainer or v == EQUIPSLOTS.HANDS) --这里友好点，不能放入物品栏的不会脱下
        then
            self.inst.components.inventory:DropItem(euqip, true, true)
        end
    end
end

function WilbaTransform:TransformToWerePost()
    self.ready_to_transform = false
    self.were = true

    local inst = self.inst
    inst.AnimState:SetBuild("werewilba")
    inst.AnimState:SetBank("wilson")

    inst.components.mightiness.state = "mighty"

    SettHungerMulti(inst, 5)

    GetPrefab.SetMaxHealth(inst, TUNING.WEREWILBA_HEALTH)

    if not inst.components.hunger:IsStarving() then
        StartRegen(inst)
    end

    inst:ListenForEvent("startstarving", StopRegen)
    inst:ListenForEvent("stopstarving", StartRegen)

    inst.components.sanity.night_drain_mult = 0

    inst.components.combat:SetDefaultDamage(TUNING.WEREWILBA_DAMAGE)
    inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED * 1.5

    -- inst.components.playercontroller.actionbuttonoverride = WereActionButton
    -- inst.components.playeractionpicker.leftclickoverride = LeftClickPicker
    -- inst.components.playeractionpicker.rightclickoverride = function() return {} end

    inst:AddComponent("worker")
    inst.components.worker:SetAction(ACTIONS.DIG)
    inst.components.worker:SetAction(ACTIONS.CHOP, 2)
    inst.components.worker:SetAction(ACTIONS.MINE)
    inst.components.worker:SetAction(ACTIONS.HAMMER)

    inst:AddTag("monster")
    inst:RemoveTag("pigelite")
    inst:RemoveTag("pigroyalty")

    inst.SoundEmitter:PlaySound("dontstarve_DLC003/music/werepig_of_london", "werepigmusic")

    inst.components.temperature:SetTemp(20)
    inst.components.timer:StartTimer("ptribe_grunt", math.random(TUNING.TOTAL_DAY_TIME / 15, TUNING.TOTAL_DAY_TIME / 5))
    inst.components.timer:StartTimer("ptribe_breath", 2)
    inst.components.timer:StartTimer("ptribe_breath2", 5)

    inst.Light:Enable(true)
    inst.soundsname = "werewilba"
end

--- 变噩梦疯猪，通过sg来执行
function WilbaTransform:TransformToDaywalker()
    local inst = self.inst

    inst.AnimState:SetBank("daywalker")
    inst.AnimState:SetBuild("daywalker_build")

    self.type = "daywalker"
    self.canToDaywalker = false

    inst.components.locomotor.walkspeed = TUNING.DAYWALKER_WALKSPEED
    inst.components.locomotor.runspeed = TUNING.DAYWALKER_RUNSPEED
    inst.components.locomotor.allow_platform_hopping = false --禁止上下船

    GetPrefab.SetMaxHealth(inst, TUNING.DAYWALKER_HEALTH)
    inst.components.combat:SetDefaultDamage(TUNING.DAYWALKER_DAMAGE)
    inst.components.combat:SetAttackPeriod(2)
    inst.components.combat:SetRange(TUNING.DAYWALKER_ATTACK_RANGE)
    inst.components.combat.hiteffectsymbol = "ww_body"

    SettHungerMulti(inst, 10)

    -- inst.components.pinnable.canbepinned = false
end

function WilbaTransform:TransformByType()
    if self.type == "daywalker" then
        self:TransformToDaywalker()
    end
end

function WilbaTransform:OnSave()
    return {
        were = self.were,
        type = self.type,
        were_health = self.inst.components.health:GetPercent(), --这里顺便记录一下
        monster_count = self.monster_count,
        monster_cooldown = self.monster_cooldown,
        cooldown_schedule = self.cooldown_schedule,
        from_food = self.from_food,
        ready_to_transform = self.ready_to_transform,
        timeleft = self.trans_task_info and self.inst:TimeRemainingInTask(self.trans_task_info) or nil
    }
end

function WilbaTransform:OnLoad(data)
    if data.were then
        self.inst:DoTaskInTime(0, function()
            self:TransformToWere()
            self:TransformToWerePost() --设置build需要一个延迟
            if data.type then
                self.type = data.type
                self:TransformByType()
            end

            if data.were_health then
                self.inst.components.health:SetPercent(data.were_health)
            end
        end)
    end

    self.monster_count = data.monster_count or self.monster_count
    self.monster_cooldown = data.monster_cooldown or self.monster_cooldown
    self.cooldown_schedule = data.cooldown_schedule or self.cooldown_schedule
    self.from_food = data.from_food or self.from_food
    self.ready_to_transform = data.ready_to_transform or self.ready_to_transform

    if data.timeleft then
        self.transform_task, self.trans_task_info = self.inst:ResumeTask(data.timeleft, cooldown_monster_count)
    end
end

return WilbaTransform
