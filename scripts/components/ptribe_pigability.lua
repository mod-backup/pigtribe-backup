local Utils = require("ptribe_utils/utils")
local Shapes = require("ptribe_utils/shapes")
local GetPrefab = require("ptribe_utils/getprefab")

-- local ATTACK_MUST_TAGS = { "_combat", "_health" }
-- local function OnAttackOther(inst, data)
--     local target = data.target
--     if not target then return end

--     -- 要攻击直线上的敌人，就需要每次攻击都要扫描攻击范围内的所有单位，然后判断敌人是否在该路径上，或者在该路径上进行多次查询，我不知道哪个方法更优
--     local pos = inst:GetPosition()
--     for _, v in ipairs(TheSim:FindEntities(pos.x, pos.y, pos.z, 16, ATTACK_MUST_TAGS)) do
--         if not v.components.health:IsDead()
--             and (GetPrefab.TargetTestForAll(inst, v) or v:HasTag("wonkey") or v:HasTag("pirate") or (not inst:HasTag("merm") and v:HasTag("merm")))
--             and Shapes.IsInRectangle(pos, v:GetPosition(), inst:GetRotation(), 1.5, 16) then
--             local damage = inst.components.combat.defaultdamage
--             if v == target then
--                 damage = damage * math.max(TUNING.TRIBE_TERRARIA_DAMAGE_MULT - 1, 0)
--             end

--             if damage > 0 then
--                 v.components.combat:GetAttacked(inst, damage)
--             end
--         end
--     end
-- end

-- local function RegenPig(inst)
--     if not inst.components.combat.target then
--         if inst.ptribe_nurseTask then
--             inst.ptribe_nurseTask:Cancel()
--             inst.ptribe_nurseTask = nil
--         end
--         return
--     end

--     local pos = inst:GetPosition()
--     local flag = false
--     for _, v in ipairs(TheSim:FindEntities(pos.x, pos.y, pos.z, 16, PIG_MUST_TAGS)) do
--         if not v.components.health:IsDead()
--             and v.components.health:GetPercent() < 0.5
--             and not GetPrefab.TargetTestForAll(inst, v) then
--             flag = true
--             v.components.health:DoDelta(math.max(v.components.health.maxhealth * 0.3, 150), true)
--         end
--     end
--     if flag then
--         inst.components.health:DoDelta(-100)
--         SpawnAt("spider_heal_ground_fx", pos)
--     end
-- end

-- local function OnNurseDeath(inst)
--     local x, y, z = inst.Transform:GetWorldPosition()
--     for _, v in ipairs(TheSim:FindEntities(x, y, z, 16, PIG_MUST_TAGS)) do
--         if v ~= inst and not v.components.health:IsDead()
--             and v.components.health:GetPercent() < 0.5
--             and not GetPrefab.TargetTestForAll(inst, v) then
--             v.components.health:DoDelta(math.max(v.components.health.maxhealth * 0.5, 500), true)
--         end
--     end
-- end

----------------------------------------------------------------------------------------------------

-- ptribe_pigmanager的配套组件，根据部落的建设情况来强化猪人
local PigmanBility = Class(function(self, inst)
    self.inst = inst

    self.type = nil   --特殊能力
    self.inited = nil -- 是否已经初始化，不保存
end)

-- 配合覆盖combat来修改伤害判定
local PIG_MUST_TAGS = { "character", "pig", "_health" }
local function AbsorbDamage(inst)
    inst.damageThreshold = 0 --伤害阈值
    if not inst.components.combat.target then
        if inst.ptribe_armorTask then
            inst.ptribe_armorTask:Cancel()
            inst.ptribe_armorTask = nil
        end
        return
    end

    local pos = inst:GetPosition()
    SpawnAt("groundpoundring_fx", pos)
    for _, v in ipairs(TheSim:FindEntities(pos.x, pos.y, pos.z, 16, PIG_MUST_TAGS)) do
        if not v.components.health:IsDead()
            and not v.damageThreshold
            and not GetPrefab.TargetTestForAll(inst, v) then
            v.ptribe_absorbDamagePig = inst
            v:DoTaskInTime(8, function() v.ptribe_absorbDamagePig = nil end)
        end
    end
end

local function DropMite(inst)
    local target = inst.components.combat.target
    if not target then
        if inst.ptribe_blastMiteTask then
            inst.ptribe_blastMiteTask:Cancel()
            inst.ptribe_blastMiteTask = nil
        end
        return
    end
    if not target:IsValid() then return end

    SpawnAt("ptribe_fused_shadeling_bomb", inst:GetPosition())
        :PushEvent("setexplosiontarget", { caster = inst, target = target })
end

local PIG_NAME_SUFFIXS = {
    -- terraria = PTRIBE_LAGUAGE_LOC("(Terraria)", "（泰拉）"),
    -- nurse = PTRIBE_LAGUAGE_LOC("(Nurse)", "（护士）"),
    kungfu = PTRIBE_LAGUAGE_LOC("(Kung Fu)", "（功夫）"),
    armor = PTRIBE_LAGUAGE_LOC("(Armor)", "（铁甲）"),
    blastMite = PTRIBE_LAGUAGE_LOC("(Mite)", "（爆螨）"),
    space = PTRIBE_LAGUAGE_LOC("(Space)", "（空间）"),
    blowdart = PTRIBE_LAGUAGE_LOC("(Blowdart)", "（吹箭）"),
    poop = PTRIBE_LAGUAGE_LOC("(Poop)", "（便便）"),
}

local function SetNameByType(child, type)
    local name = child.components.named.name

    local suffix = PIG_NAME_SUFFIXS[type]
    if suffix then
        local flag = string.match(name, "（.+）") or string.match(name, "%(.+%)")
        if flag then return end
        child.components.named:SetName(name .. suffix)
    end
end

local RANDOM_BLOWDARTS = {
    -- "blowdart_fire",
    "blowdart_sleep",
    "blowdart_yellow"
}
local function OnBlowDartNewTarget(inst)
    if not inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
        local completeRate = inst.ptribe_completeRate or 0

        local prefab = "blowdart_pipe"
        if math.random() < completeRate then
            prefab = RANDOM_BLOWDARTS[math.random(1, #RANDOM_BLOWDARTS)]
        end
        local blowdart = SpawnPrefab(prefab)
        inst.components.inventory:Equip(blowdart)
    end
end

----------------------------------------------------------------------------------------------------

-- 便便发射器
local function EquipWeapons(inst)
    if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then return end

    local thrower = CreateEntity()
    thrower.name = "Thrower"
    thrower.entity:AddTransform()
    thrower:AddComponent("weapon")
    thrower.components.weapon:SetDamage(TUNING.MONKEY_RANGED_DAMAGE)
    thrower.components.weapon:SetRange(TUNING.MONKEY_RANGED_RANGE)
    thrower.components.weapon:SetProjectile("monkeyprojectile")
    -- thrower.components.weapon:SetOnProjectileLaunch(onthrow)
    thrower:AddComponent("inventoryitem")
    thrower.persists = false
    thrower.components.inventoryitem:SetOnDroppedFn(thrower.Remove)
    thrower:AddComponent("equippable")
    thrower:AddTag("nosteal")
    inst.components.inventory:GiveItem(thrower)
    inst.components.inventory:Equip(thrower)
end

--- 猪人的强化，可能会执行多次，需要判重
function PigmanBility:InitChild(child)
    if not child:IsValid() --虽然感觉不用判断，但好像有mod会导致猪人直接没了
        or child._ptribe_abilityInited then
        return
    end
    child._ptribe_abilityInited = true --初始化标记，不需要保存

    if not self.type then
        self.inst.components.spawner.childfn(self.inst) --房子刚建好第一个猪人需要手动调用
    end

    child = child or self.inst.components.spawner.child
    local completeRate = self.inst.components.ptribe_pigmanager:GetCompleteRate()
    -- if not child or not completeRate or completeRate == 0 then return end

    -- 属性强化
    child.components.combat:SetDefaultDamage(TUNING.PIG_DAMAGE * (1 + completeRate * 0.8)) --33 * 2
    if self.type == "kungfu"
    then
        -- 功夫猪人不改攻速
    elseif self.type == "blowdart" then
        child.components.combat:SetAttackPeriod(math.floor(TUNING.PIG_BLOWDART_ATTACK_PERIOD / (1 + completeRate)))
    else
        child.components.combat:SetAttackPeriod(math.floor(TUNING.PIG_ATTACK_PERIOD / (1 + completeRate)))
    end
    child.components.locomotor.runspeed = math.floor(TUNING.PIG_RUN_SPEED * (1 + completeRate * 0.5))
    child.components.locomotor.walkspeed = math.floor(TUNING.PIG_WALK_SPEED * (1 + completeRate * 0.5))
    child.components.health:SetMaxHealth(math.ceil(TUNING.PIG_HEALTH * (1 + completeRate))) --上限500

    -- 改名
    SetNameByType(child, self.type)

    -- 类型强化
    if self.type == "kungfu" then
        -- 功夫猪人
    elseif self.type == "armor" then
        -- 铁甲猪人
        GetPrefab.AddFx(child, "lunar_grazer_core_fx", { offset = Vector3(0, 1, 0) })
        child.components.health.externalabsorbmodifiers:SetModifier(child, 0.3, "ptribe_armorPig")

        child:ListenForEvent("newcombattarget", function(inst)
            if not inst.ptribe_armorTask then
                inst.ptribe_armorTask = inst:DoPeriodicTask(5, AbsorbDamage, 0)
            end
        end)
    elseif self.type == "blastMite" then
        -- 爆螨猪人
        child:ListenForEvent("newcombattarget", function(inst)
            if not inst.ptribe_blastMiteTask then
                inst.ptribe_blastMiteTask = inst:DoPeriodicTask(TUNING.TRIBE_MITE_COOLDOWN, DropMite, math.random(1, 3))
            end
        end)
    elseif self.type == "space" then
        -- 空间猪人
    elseif self.type == "blowdart" then
        -- 吹箭猪人
        child.ptribe_completeRate = completeRate

        OnBlowDartNewTarget(child)
        child:ListenForEvent("newcombattarget", OnBlowDartNewTarget)
        child:ListenForEvent("unequip", function() child:DoTaskInTime(0, OnBlowDartNewTarget) end) --有可能是代码主动卸下装备换新装备，这里要延迟一下
    elseif self.type == "poop" then
        -- 便便猪人
        child.components.combat:SetRange(TUNING.MONKEY_MELEE_RANGE)
        child.components.combat:SetAttackPeriod(TUNING.MONKEY_ATTACK_PERIOD)
        child.components.combat:SetDefaultDamage(0) --This doesn't matter, monkey uses weapon damage

        EquipWeapons(child)
    end

    -- 被废弃的设定
    -- elseif self.type == "terraria" then
    --     -- 泰拉猪人
    --     child.components.combat:SetRange(16)
    --     child:ListenForEvent("onattackother", OnAttackOther)
    -- elseif self.type == "nurse" then
    --     -- 护士猪人
    --     child.components.health:StartRegen(5, 3)

    --     child:ListenForEvent("death", OnNurseDeath)

    --     child:ListenForEvent("newcombattarget", function(inst)
    --         if not inst.ptribe_nurseTask then
    --             inst.ptribe_nurseTask = inst:DoPeriodicTask(5, RegenPig, math.random(1, 3))
    --         end
    --     end)
end

function PigmanBility:OnSave()
    return {
        type = self.type,
    }
end

function PigmanBility:OnLoad(data)
    if not data then return end

    self.type = data.type or self.type
end

return PigmanBility
