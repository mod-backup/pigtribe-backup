local GetPrefab = require("ptribe_utils/getprefab")

---强盗猪人生成管理器
local Banditmanager = Class(function(self, inst)
	self.inst = inst
	self.bandit = nil
	self.respawnTime = 0

	self.inst:DoPeriodicTask(10, function() self:OnPeriodicUpdate() end)
end)

function Banditmanager:OnSave()
	local refs = {}
	local data = {}

	data.respawnTime = self.respawnTime and (self.respawnTime - GetTime()) or nil
	if self.bandit then
		data.bandit = self.bandit.GUID
		table.insert(refs, self.bandit.GUID)
	end

	return data, refs
end

function Banditmanager:OnLoad(data)
	self.respawnTime = data.respawnTime
end

function Banditmanager:LoadPostPass(ents, data)
	if data.bandit and ents[data.bandit] then
		self.bandit = ents[data.bandit].entity
	end
end

function Banditmanager:OnPeriodicUpdate()
	if self.bandit then
		if self.bandit:IsValid() then return end

		-- 已经死亡
		self.bandit = nil
		self.respawnTime = GetTime() + TUNING.TOTAL_DAY_TIME * 1.5
		return
	end

	if self.respawnTime then
		if GetTime() < self.respawnTime then return end
		self.respawnTime = nil --复活
	end

	for _, player in ipairs(AllPlayers) do
		local pt = player:GetPosition()
		if not player:HasTag("playerghost") and TheWorld.Map:IsAboveGroundAtPoint(pt:Get())
			and player.ptribe_pigmanager and player:GetDistanceSqToInst(player.ptribe_pigmanager) <= player.ptribe_pigmanager.components.ptribe_pigmanager:GetMaxDisSq() then
            local value = 0
			
			for _, item in ipairs(player.components.inventory:GetItemsWithTag("oinc")) do
				value = value + GetStackSize(item) * (item.components.tradable.goldvalue or 0)
			end
	
			if TheWorld.state.isdusk then
				value = value * 1.5
			elseif TheWorld.state.isnight then
				value = value * 3
			end

			local chance = 1 / 100
			if value >= 150 then
				chance = 1 / 5
			elseif value >= 100 then
				chance = 1 / 10
			elseif value >= 50 then
				chance = 1 / 20
			elseif value >= 10 then
				chance = 1 / 40
			elseif value == 0 then
				chance = 0
			end

			-- print("判断价值", player, value, chance)
			if math.random() < chance and self:SpawnBandit(player) then
				return --一次只生成一个
			end
		end
	end
end

local PIGHOUSE_MUST_TAGS = { "structure" }

--- 距离玩家最远的猪人房生成，原版好像是从玩家房子生成的就很离谱
--- 这里只负责生成，不会记录猪人死亡，猪人死亡由
function Banditmanager:SpawnBandit(player)
	local pighouse, maxDisSq = nil, 0

	local x, _, z = player.Transform:GetWorldPosition()
	for _, v in ipairs(TheSim:FindEntities(x, _, z, 30, PIGHOUSE_MUST_TAGS)) do
		if v.prefab == "pighouse" then
			local vx, _, vz = v.Transform:GetWorldPosition()
			local disSq = distsq(x, z, vx, vz)
			if disSq > maxDisSq then
				pighouse = v
				maxDisSq = disSq
			end
		end
	end

	if not pighouse then return false end --前面判断过部落，所以这里不应该找不到

	self.bandit = SpawnPrefab("pigbandit")
	GetPrefab.ReleaseChild(self.bandit, pighouse)
	if pighouse.SoundEmitter then
		pighouse.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
	end

	self.bandit.components.combat:SetTarget(player)
end

return Banditmanager
