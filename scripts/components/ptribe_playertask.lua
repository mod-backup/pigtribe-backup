local PigTasksUtils = require("ptribe_pigtasksutils")
local GetPrefab = require("ptribe_utils/getprefab")

local function OnUpdate(inst)
    local self = inst.components.ptribe_playertask

    --检查任务发布者有效性
    if not self.pig.components.ptribe_pigtask:IsTaskExist() then
        if self.inst.components.talker then
            self.inst.components.talker:Say("任务结束了")
        end
        self:CancelTask(true)
        return
    end

    -- 更新箭头，也可能取消任务
    local pos, msg = self.task.GetTargetPos(self)
    if pos and pos.Get then
        self.beacon.Transform:SetRotation(self.beacon:GetAngleToPoint(pos) - 90 - self.inst:GetRotation())
    else
        if self.inst.components.talker then
            self.inst.components.talker:Say("任务结束了，" .. (msg or ""))
        end
        self:CancelTask()
    end

    -- 更新，也可能取消任务
    if self.task and self.task.OnUpdate then
        self.task.OnUpdate(self)
    end
end

local function OnTimeDone(inst, data)
    local self = inst.components.ptribe_playertask
    if data.name == "ptribe_updateTask" then
        if self.taskId and self.pig and self.pig:IsValid() then
            OnUpdate(inst)
            inst.components.timer:StartTimer("ptribe_updateTask", 0.5)
        end
    elseif data.name == "ptribe_cancelTask" then
        self:CancelTask()
    end
end

--主机组件，主客机交互太麻烦了
local PlayerTask = Class(function(self, inst)
    self.inst = inst

    self.task = nil
    self.pig = nil
    self.cachePig = nil --上一次交互的猪人，有可能是发布任务的猪人，是否有任务以taskId为准
    self.taskId = 0

    inst:ListenForEvent("timerdone", OnTimeDone) --timer虽然方便，但是性能不知道会不会比DoPeriodicTask差太多
end)

function PlayerTask:CancelTask(isInvalid)
    if self.task and self.task.OnEnd then
        self.task.OnEnd(self, self.taskFinished == true, isInvalid)
    end
    self.taskFinished = nil

    if self.beacon then
        self.beacon:Remove()
        self.beacon = nil
    end

    if self.pig then
        if self.pig:IsValid() then
            self.pig.components.ptribe_pigtask.taskPlayers[self.inst.userid] = nil
        end
        self.pig = nil
    end

    self.taskId = 0
    self.task = nil
    self.taskData = nil
    -- self.inst:StopUpdatingComponent(self)
    -- if self.updateTask then
    --     self.updateTask:Cancel()
    -- end

    self.inst.components.timer:StopTimer("ptribe_cancelTask")
    self.inst.components.timer:StopTimer("ptribe_updateTask")
end

---添加任务，如果taskId为0则表示取消原有任务
function PlayerTask:AddTask(taskId, isLoading)
    if taskId == 0 or not self.cachePig then return end

    if not isLoading then
        self:CancelTask()
        -- print("玩家接受任务", self.inst.userid)
        self.cachePig.components.ptribe_pigtask.taskPlayers[self.inst.userid] = true --让猪人与玩家绑定
    end

    self.pig = self.cachePig
    self.cachePig = nil

    self.taskId = taskId
    self.task = PigTasksUtils.GetTaskById(taskId)
    self.taskData = self.taskData or {} --OnLoad可能有数据
    if self.task.OnInit then
        local status, msg = self.task.OnInit(self, isLoading)
        if not status then
            self.pig = nil
            self.taskId = 0
            self.task = nil
            self.taskData = nil
            if self.inst.components.talker then
                self.inst.components.talker:Say("任务初始化失败," .. msg)
            end
            return
        end
    end

    self.beacon = SpawnPrefab("boatrace_checkpoint_indicator")
    self.inst:AddChild(self.beacon) -- 玩家经常转向的话会导致箭头一直变换

    if not isLoading then
        self.inst.components.timer:StartTimer("ptribe_cancelTask", self.pig.components.ptribe_pigtask:GetTaskRemaining())
        self.inst.components.timer:StartTimer("ptribe_updateTask", 0.5)
    end


    --更新任务，感觉用StartUpdatingComponent有点儿浪费
    -- self.updateTask = self.inst:DoPeriodicTask(0.5, OnUpdate, 0)
    -- self.inst:StartUpdatingComponent(self)

    self.inst:ListenForEvent("onremove", function()
        self:CancelTask(true)
    end, self.pig)
end

function PlayerTask:FinishTask()
    for name, num in pairs(self.task.reward) do
        GetPrefab.ReturnMaterial(name, self.pig, {
            count = num,
            target = self.inst,
            isGiveTarget = true, --怕被猪人吃了
        })
    end

    local manager = self.pig.GetTribeManager and self.pig:GetTribeManager()
    if manager then
        manager.components.ptribe_pigmanager:FavorabilityDelta(self.task.level * 5)
    end

    self.pig.components.ptribe_pigtask.taskId:set(0)
    self.taskFinished = true
    self:CancelTask()
end

function PlayerTask:SetPlayerTaskId(taskId)
    if taskId == 0 then
        --取消任务
        self:CancelTask()
    else
        if self.taskId == taskId then
            --完成任务
            local status, msg = self.task.FinishTask(self)
            if status then
                self:FinishTask()
                if msg and self.pig.components.talker then --猪人应该有talker组件的
                    self.pig.components.talker:Say(say)
                end
            else
                if self.pig.components.talker then --猪人应该有talker组件的
                    self.pig.components.talker:Say("不行，" .. (msg or ""))
                end
            end
        else
            --接受任务
            self:AddTask(taskId)
        end
    end
end

function PlayerTask:OnSave()
    local data = {}

    if self.task and self.task.OnSave then
        self.task.OnSave(self, data)
    end

    if self.taskId ~= 0 then
        data.taskId = self.taskId
        data.taskData = self.taskData
    end

    return data
end

function PlayerTask:OnLoad(data)
    if data.taskId then
        self.taskData = data.taskData
        self.taskId = data.taskId
    end
end

return PlayerTask
