local PigFactory = Class(function(self, inst)
    self.inst = inst

    self.product = net_string(inst.GUID, "ptribe_pigfactory.product")
    self.cooldown = net_ushortint(inst.GUID, "ptribe_pigfactory.cooldown", "ptribe_pigfactory.cooldown")
end)

function PigFactory:SetCooldown(cooldown)
    self.cooldown:set(cooldown)
end

function PigFactory:GetCooldown()
    return self.cooldown:value()
end

function PigFactory:SetProduct(product)
    self.product:set(product)
end

function PigFactory:GetProduct()
    return self.product:value()
end

return PigFactory
