--- 保存动画数据并在加载时重新设置
--- data属性会在保存时一起保存，因此可便捷的保存一些简单的额外变量，加载后也能获取
local SaveSkin = Class(function(self, inst)
    self.inst = inst

    self.enable = false
    self.oldData = nil --为了能够还原原来的外观
    self.data = nil

    self.onSetFn = nil
end)

function SaveSkin:ClearOldData()
    self.oldData = nil
end

--- 设置外观
function SaveSkin:Set(data, oldData)
    assert(data)

    self.enable = false --脏标记
    self.data = data
    self.oldData = oldData or {
        bank = self.inst.AnimState:GetCurrentBankName(),
        build = self.inst.AnimState:GetBuild(),
    }

    if self.onSetFn then
        self.onSetFn(self.inst, data)
    end
end

local function SetAnim(inst, data)
    if not data then return end

    if data.bank then
        inst.AnimState:SetBank(data.bank)
    end
    if data.build then
        inst.AnimState:SetBuild(data.build)
    end
    if data.anim then
        inst.AnimState:PlayAnimation(data.anim, data.isLoopPlayAnim)
    end
end

function SaveSkin:Enable(enable)
    if self.enable == enable then return end

    if enable then
        SetAnim(self.inst, self.data)
    else
        SetAnim(self.inst, self.oldData)
    end

    self.enable = enable
end

function SaveSkin:OnSave()
    return {
        enable = self.enable,
        data = self.data,
        oldData = self.oldData,
    }
end

function SaveSkin:OnLoad(data)
    if not data then return end

    self.data = data.data
    self.oldData = data.oldData
    if data.enable then
        self.inst:DoTaskInTime(0, function() self:Enable(true) end)
    end
end

return SaveSkin
