local PigTasksUtils = require("ptribe_pigtasksutils")

local DEFAULT_SCALE = 2
local HUD_INDICATOR_DATA = { image = "poi_question.tex", atlas = "images/avatars.xml" }

local function _CommonIndicator(data)
    local inst = CreateEntity()

    --[[Non-networked entity]]
    if not TheWorld.ismastersim then
        inst.entity:SetCanSleep(false)
    end

    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst:AddTag("CLASSIFIED")
    inst:AddTag("NOCLICK")
    inst:AddTag("FX")

    inst.AnimState:SetBank(data.bank)
    inst.AnimState:SetBuild(data.build)
    inst.AnimState:PlayAnimation(data.anim)

    inst.Transform:SetScale(DEFAULT_SCALE, DEFAULT_SCALE, DEFAULT_SCALE)

    inst.alpha = 1
    inst.scale = 1

    return inst
end

local function PrefabIndicator()
    return _CommonIndicator({ bank = "poi_marker", build = "poi_marker", anim = "idle" }) --问号
end

local function CreateRing()
    local inst = _CommonIndicator({ bank = "poi_marker", build = "poi_marker", anim = "ring" }) --只有一个圈

    inst.entity:AddFollower()

    return inst
end

local function Stand()
    return _CommonIndicator({ bank = "poi_stand", build = "flint", anim = "idle" })
end

------------------------------------------------------------------------------------------------------------------------

local function OnTaskIdChanged(inst)
    local self = inst.components.ptribe_pigtask
    local taskId = self.taskId:value()

    if taskId == 0 then
        -- 解除任务
        self.task = nil
        self.endTime = nil
        self.taskPlayers = {}

        if self.cancelTask then
            self.cancelTask:Cancel()
            self.cancelTask = nil
        end

        if not TheNet:IsDedicated() then
            self:TriggerPulse()
            inst:StartUpdatingComponent(self)
        end

        self.inst:RemoveEventCallback("ms_newplayerspawned", self._OnPlayerJoined, TheWorld)
        self.inst:RemoveEventCallback("ms_playerjoined", self._OnNewPlayerSpawned, TheWorld)
    else
        -- 添加任务
        self:StartTask(taskId)
    end
end

local function InitPlayer(self, player)
    -- print("玩家进入", self.taskPlayers[player.userid], player.components.ptribe_playertask, GetTableSize(self.taskPlayers))
    local ptribe_playertask = player.components.ptribe_playertask
    ptribe_playertask.cachePig = self.inst --记录任务猪人，用于ptribe_playertask初始化
    if ptribe_playertask.taskId ~= 0 then
        ptribe_playertask:AddTask(ptribe_playertask.taskId, true)
    end
end

---猪人任务组件，猪人不会自己发布任务，由ptribe_pigmanager主动发布
local PigTask = Class(function(self, inst)
    self.inst = inst
    self.height = 0

    self.taskId = net_byte(inst.GUID, "ptribe_pigtask.taskId", "ptribe_pigtask.taskId") --[0..255]
    self.task = nil                                                                     --主客机共享变量
    self.taskPlayers = {}

    self._OnPlayerJoined = function(world, player) InitPlayer(self, player) end
    self._OnNewPlayerSpawned = function(world, player) InitPlayer(self, player) end

    inst:ListenForEvent("ptribe_pigtask.taskId", OnTaskIdChanged)

    -- inst:DoTaskInTime(0, function(inst) self:StartTask() end)
end)

function PigTask:SetHeight(height)
    self.height = height
end

local function CancelTask(inst)
    local self = inst.components.ptribe_pigtask
    self.taskId:set(0)
    self.cancelTask = nil
end

function PigTask:CreateWorldIndicator()
    self.stand = Stand()
    self.inst:AddChild(self.stand)

    self.marker = PrefabIndicator()
    self.marker.entity:AddFollower()

    self.marker.Follower:FollowSymbol(self.stand.GUID, "marker", 0, self.height, 0)
end

local THINGS_TO_REMOVE = { "ring1", "ring2", "marker", "stand", }

function PigTask:StartTask(taskId)
    if self.isLoading then
        self.isLoading = nil
    else
        self.taskPlayers = {}
    end

    taskId = taskId or self.taskId:value()
    self.task = PigTasksUtils.GetTaskById(taskId)

    if not TheNet:IsDedicated() then
        self:CreateWorldIndicator()
        self.endTime = GetTime() + self.task.time --客机读取剩余时间的
        self.inst:ListenForEvent("onremove", function()
            -- self.marker.Follower:FollowSymbol(self.stand.GUID, "marker", 0, self.height, 0)
            for _, k in ipairs(THINGS_TO_REMOVE) do
                if self[k] ~= nil then
                    ErodeAway(self[k])
                    self[k] = nil
                end
            end
        end)
    end

    if TheWorld.ismastersim then
        self.cancelTask = self.inst:DoTaskInTime(self.task.time, CancelTask)
    end
end

function PigTask:UpdateRing(ring, dt)
    if ring ~= nil then
        ring.scale = ring.scale + (1.05 * dt)
        ring.alpha = ring.alpha - (2.00 * dt)

        if ring.scale > 2 then
            ring:Remove()
            ring = nil
        else
            local _scale = ring.scale * DEFAULT_SCALE

            ring.Transform:SetScale(_scale, _scale, _scale)
            ring.AnimState:SetMultColour(1, 1, 1, ring.alpha)
        end
    end
end

function PigTask:RemoveEverything()
    if self._updating then
        self._updating = false
        self.inst:StopUpdatingComponent(self)
    end

    self._removing = false

    for _, k in ipairs(THINGS_TO_REMOVE) do
        if self[k] ~= nil then
            self[k]:Remove()
            self[k] = nil
        end
    end
end

function PigTask:UpdateRemovePulse(dt)
    self:UpdateRing(self.ring1, dt)
    self:UpdateRing(self.ring2, dt)

    if self.marker and self.marker.target ~= nil then
        self.marker.scale = self.marker.scale - (0.75 * dt)
        self.marker.Transform:SetScale(self.marker.scale * DEFAULT_SCALE, self.marker.scale * DEFAULT_SCALE,
            self.marker.scale * DEFAULT_SCALE)

        if self.loops == 2 then
            local marker_alpha = Remap(self.marker.scale, 1, self.marker.target, 1.5, 0)

            self.marker.AnimState:SetMultColour(1, 1, 1, marker_alpha)
        end

        if self.marker.scale < self.marker.target then
            self.loops = self.loops + 1

            if self.loops == 1 then
                self.marker.scale = 1.3
                self.marker.target = 1

                self.ring1 = CreateRing()
                self.ring1.Follower:FollowSymbol(self.stand.GUID, "marker", 0, self.height, 0)
            elseif self.loops == 2 then
                self.marker.scale = 1
                self.marker.target = 0.5

                self.ring2 = CreateRing()
                self.ring2.Follower:FollowSymbol(self.stand.GUID, "marker", 0, self.height, 0)
            else
                self:RemoveEverything()
            end
        end
    end
end

function PigTask:TriggerPulse()
    if self.marker == nil then
        return
    end

    TheFocalPoint.SoundEmitter:PlaySound("dontstarve/HUD/poi_register")

    self.marker.AnimState:PlayAnimation("dark")

    self._removing = true

    self.marker.scale = 1
    self.marker.target = 0.7
    self.loops = 0
end

function PigTask:OnUpdate(dt)
    self:UpdateRemovePulse(dt)
end

function PigTask:GetTaskRemaining()
    return GetTaskRemaining(self.cancelTask)
end

---延长任务时间
function PigTask:AddTaskTime(time)
    if self.cancelTask then
        local remain = self:GetTaskRemaining()
        self.cancelTask:Cancel()
        self.cancelTask = self.inst:DoTaskInTime(GetTime() + remain + time, CancelTask)
    end
end

---判断任务是否存在，这里用taskId也行
function PigTask:IsTaskExist()
    return self.taskId:value() ~= 0 and self.cancelTask ~= nil
end

function PigTask:OnSave()
    -- print("保存玩家数据", GetTableSize(self.taskPlayers))
    return {
        taskId = self.taskId:value() ~= 0 and self.taskId:value() or nil,
        remainingTime = self.cancelTask and GetTaskRemaining(self.cancelTask) or
            self.endTime and (self.endTime - GetTime()) or nil,
        taskPlayers = GetTableSize(self.taskPlayers) > 0 and self.taskPlayers or nil
    }
end

function PigTask:OnLoad(data)
    if not data then return end

    if TheWorld.ismastersim and data.taskId then
        self.isLoading = true --笨方法
        self.taskId:set(data.taskId)
    end

    if data.taskPlayers then
        self.taskPlayers = data.taskPlayers
        -- print("玩家数据初始化", GetTableSize(self.taskPlayers))
        self.inst:ListenForEvent("ms_playerjoined", self._OnPlayerJoined, TheWorld)
        self.inst:ListenForEvent("ms_newplayerspawned", self._OnNewPlayerSpawned, TheWorld)
    end

    if data.remainingTime then
        self.cancelTask = self.inst:DoTaskInTime(data.remainingTime, CancelTask)
        self.endTime = GetTime() + data.remainingTime
    end
end

return PigTask
