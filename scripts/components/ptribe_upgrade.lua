local componentPre = debug.getinfo(1, 'S').source:match("([^/]+)_upgrade%.lua$") --当前文件前缀
local componentName = componentPre .. "_upgrade"
local upgradableTag = componentPre .. "_upgradable"

local function OnChanged(self)
    if self.current and self.maxLevel and self.current < self.maxLevel then
        self.inst:AddTag(upgradableTag)
    else
        self.inst:RemoveTag(upgradableTag)
    end
end

local function oncurrent(self, current)
    self.inst.replica[componentName]:SetLevel(current)

    OnChanged(self)
end

--- 简单通用升级组件，只保存当前等级，其他东西都需要每次设置
local Upgrade = Class(function(self, inst)
    self.inst = inst

    self.initLevel = 0                                 --初始等级
    self.current = self.initLevel                      --当前等级
    self.maxLevel = 0                                  --满级
    self.onLevelChangeFn = nil                         --当等级改变时
    self.onMaxLevelFn = nil                            --当最高级时

    self.upSound = "dontstarve/wilson/equip_item_gold" --升级音效
end, nil, {
    current = oncurrent,
    maxLevel = OnChanged
})

---初始配置
---@param initLevel number|nil 初始等级
---@param maxLevel number 最高级
---@param onLevelChangeFn function|nil 当等级改变时,(inst,doer,count,isLoading)
---@param onMaxLevelFn function|nil 当最高级时,(inst,isLoading)
function Upgrade:Configure(initLevel, maxLevel, onLevelChangeFn, onMaxLevelFn)
    self.initLevel = initLevel or self.initLevel
    self.maxLevel = maxLevel or self.maxLevel
    self.current = self.initLevel
    self.onMaxLevelFn = onMaxLevelFn
    self.onLevelChangeFn = onLevelChangeFn
end

function Upgrade:Upgrade(doer, count)
    if count > 0 and self.current >= self.maxLevel then
        return false, "MAX_LEVEL" --不应该发生，升级前应该判断标签
    elseif count < 0 and self.current <= self.initLevel then
        return false, "MIN_LEVEL"
    end

    count = count or 1
    self.current = math.min(self.current + count, self.maxLevel) --虽然这里判断了，但是主要判断应该在调用之前，因为同时也涉及到物品的消耗数量

    if self.onLevelChangeFn then
        self.onLevelChangeFn(self.inst, doer, count, false)
    end

    if self.onMaxLevelFn and self.current >= self.maxLevel then
        self.onMaxLevelFn(self.inst, false)
    end

    if self.upSound then
        if doer and doer.SoundEmitter then
            doer.SoundEmitter:PlaySound(self.upSound)
        elseif self.inst.SoundEmitter then
            self.inst.SoundEmitter:PlaySound(self.upSound)
        end
    end

    return true
end

function Upgrade:GetNeed()
    return self.maxLevel - self.current
end

function Upgrade:GetPercent()
    return self.current / self.maxLevel
end

function Upgrade:IsFull()
    return self.current >= self.maxLevel
end

function Upgrade:OnSave()
    return {
        current = self.current
    }
end

function Upgrade:OnLoad(data)
    if not data then return end
    if data.current then
        self.current = data.current
        if self.current > self.initLevel then
            self.inst:DoTaskInTime(0, function() --一般初始化都需要等其他东西加载后
                if self.onLevelChangeFn then
                    self.onLevelChangeFn(self.inst, nil, self.current, true)
                end
                if self.current >= self.maxLevel and self.onMaxLevelFn then
                    self.onMaxLevelFn(self.inst, true)
                end
            end)
        end
    end
end

return Upgrade
