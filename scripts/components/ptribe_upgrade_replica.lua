local componentPre = debug.getinfo(1, 'S').source:match("([^/]+)_upgrade_replica%.lua$") --当前文件前缀
local componentName = componentPre .. "_upgrade"

local Upgrade = Class(function(self, inst)
    self.inst = inst

    self.current = net_smallbyte(inst.GUID, componentName .. ".current") --[0..63]
end)

function Upgrade:SetLevel(level)
    self.current:set(level)
end

function Upgrade:GetLevel()
    return self.current:value()
end

return Upgrade
