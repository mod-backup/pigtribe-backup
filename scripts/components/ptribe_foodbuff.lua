local FoodBuffDefs = require("ptribe_foodbuffdefs")

local function OnEaten(inst, data)
    local eater = data.eater
    if not eater then return end

    local self = inst.components.ptribe_foodbuff
    local buff = self.buff
    local d = FoodBuffDefs[buff][self.index]
    if d then
        d.isSave = true --重新游戏继续
        eater:ptribe_AddDebuff(buff, nil, d)
    end

    if d.str and eater.components.talker then
        eater:DoTaskInTime(0.5, function()
            if eater.components.talker then
                eater.components.talker:Say(STRINGS.PTRIBE_FOOD_BUFFS[string.upper(buff)][self.index])
            end
        end)
    end
end

local function onbuff(self, buff)
    self.inst:RemoveEventCallback("oneaten", OnEaten)
    if buff then
        self.inst:ListenForEvent("oneaten", OnEaten)
    end
end

local FoodBuff = Class(function(self, inst)
    self.inst = inst

    self.buff = nil --buff类型
    self.index = 1  --buff类型对应的索引，1是好buff，2是坏buff
end, nil, {
    buff = onbuff
})

function FoodBuff:OnSave()
    return {
        buff = self.buff,
        index = self.index
    }
end

function FoodBuff:OnLoad(data)
    if not data then return end
    self.buff = data.buff
    self.index = data.index or self.index
end

return FoodBuff
