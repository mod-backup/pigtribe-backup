local GetPrefab = require("ptribe_utils/getprefab")

local WildPigBuild = Class(function(self, inst)
    self.inst = inst

    inst.persists = false
    GetPrefab.SetNoLoot(inst, true)

    self.pig = nil --野外猪人
    self._onPigRemove = function() ErodeAway(inst) end

    self.canInteractFn = nil
    self.interactFn = nil
    self.cooldown = 0         --交互冷却时间
    self.lastTime = GetTime() --上次交互时间
end)

function WildPigBuild:InCooldown()
    return (GetTime() - self.lastTime) < self.cooldown
end

function WildPigBuild:CanInteract()
    return not self:InCooldown() and self.canInteractFn and self.canInteractFn(self.inst)
end

function WildPigBuild:Interact(pig)
    self.interactFn(self.inst, pig)
    self.lastTime = GetTime()
end

-- 应该只调用一次
function WildPigBuild:Init(pig, cooldown, canInteractFn, interactFn)
    self.pig = pig
    self.inst:RemoveEventCallback("onremove", self._onPigRemove, pig)
    self.inst:ListenForEvent("onremove", self._onPigRemove, pig)

    self.cooldown = cooldown or self.cooldown
    self.canInteractFn = canInteractFn or self.canInteractFn
    self.interactFn = interactFn or self.interactFn
end

return WildPigBuild
