local BASE_OFF = 1000
local ROOM_SIZE = 60
local ROW_COUNT = BASE_OFF / ROOM_SIZE * 2

---这里选择和哈姆雷特的组件同名，不过代码不一样，除了count外也许更像一个工具函数集合
local InteriorSpawner = Class(function(self, inst)
    self.inst = inst

    self.count = 0 --主机数据
end)

function InteriorSpawner:GetDis()
    return 12 --半径
end

-- 房间边界
function InteriorSpawner:getSpawnOrigin()
    return {
        dx = { -6.5, 5.5 }, --上下
        dz = { -9, 8 }      --左右
    }
end

--- 当房屋被摧毁时把屋内的掉落物扔出来
--- 如果是remove就移除屋内道具，如果是敲毁就返还材料，只包括lootdropper和inventoryitem
function InteriorSpawner:OnHouseDestroy(house, destroyer, isRemove)
    local centerPos = house.components.hamletdoor and house.components.hamletdoor.centerPos
    if not centerPos then return end

    local dis = self:GetDis()
    local hx, hy, hz = house.Transform:GetWorldPosition()
    if not isRemove then
        --摧毁室内建筑，生成掉落物
        for _, v in ipairs(TheSim:FindEntities(centerPos.x, centerPos.y, centerPos.z, dis)) do
            if not v:HasTag("hamlet_housefloor") and v ~= house then  --地板下面再删除
                if v.components.workable then
                    v.components.workable:Destroy(destroyer or floor) --包括其他的房间
                elseif v.components.health and not v.components.health:IsDead() and not v.components.locomotor
                    and v.components.lootdropper then
                    v.components.lootdropper:DropLoot() --一般是在死亡的sg中生成，我杀死直接移除不会生成额外的掉落物
                    v.components.health:Kill()
                end
            end
        end
    end

    --传送掉落物，移除地板
    for _, v in ipairs(TheSim:FindEntities(centerPos.x, centerPos.y, centerPos.z, dis)) do
        if not isRemove and v.components.inventoryitem then
            house.components.lootdropper:FlingItem(v) --借用lootdropper组件抛出物品
        elseif v.components.health and v.components.locomotor then
            if v:HasTag("player") then
                -- 玩家落水处理
                v.sg:GoToState("sink_fast")
            else
                v.Transform:SetPosition(hx, hy, hz)
            end
        else
            v:Remove()
        end
    end
end

---从地图左上角开始，从左到右，从上到下累积
function InteriorSpawner:GetPos()
    local x = (self.count % ROW_COUNT) * ROOM_SIZE - BASE_OFF
    local z = BASE_OFF + math.ceil(self.count / ROW_COUNT) * ROOM_SIZE
    self.count = self.count + 1
    return Vector3(x, 0, z)
end

--- 不可见墙，两层，里侧阻挡玩家移动，外侧限制建造和摆放位置
function InteriorSpawner:SpawnWall(x, z)
    local origin = self:getSpawnOrigin()
    for dx = origin.dx[1], origin.dx[2] do --因为两侧墙角度不一样，所以墙壁并不是对称的
        for dz = origin.dz[1], origin.dz[2] do
            if dx == origin.dx[1] or dx == origin.dx[2] or dz == origin.dz[1] or dz == origin.dz[2] then
                local part = SpawnPrefab("wall_tigerpond")
                part:AddTag("NOBLOCK")
                part.Transform:SetPosition(x + dx + 0.5, 0, z + dz + 0.5)
                -- SpawnAt("deco_plantholder_basic", Vector3(x + dx + 0.5, 0, z + dz + 0.5))
            end
        end
    end
    -- 可能会影响墙上贴纸的建设
    -- for dx = origin.dx[1] - 2, origin.dx[2] + 2 do
    --     for dz = origin.dz[1] - 2, origin.dz[2] + 2 do
    --         if dx == (origin.dx[1] - 2) or dx == (origin.dx[2] + 2) or dz == (origin.dz[1] - 2) or dz == (origin.dz[2] + 2) then
    --             local part = SpawnPrefab("wall_tigerpond")
    --             part.Transform:SetPosition(x + dx + 0.5, 0, z + dz + 0.5)
    --         end
    --     end
    -- end
end

function InteriorSpawner:OnSave()
    return {
        count = self.count,
    }
end

function InteriorSpawner:OnLoad(data)
    if not data then return end
    self.count = data.count or self.count
end

---根据格式初始化室内装饰，并保存初始化数据
function InteriorSpawner:InitHouseInteriorPrefab(p, data)
    if data.children then
        p.tempChildrens = data.children
    end
    if data.rotation then
        p.Transform:SetRotation(data.rotation)
    end
    if data.animdata then
        if data.animdata.flip then
            p.AnimState:SetScale(-1, 1)
        end
        if data.animdata.bank then
            p.AnimState:SetBank(data.animdata.bank)
        end
        if data.animdata.build then
            p.AnimState:SetBuild(data.animdata.build)
        end
        if data.animdata.anim then
            p.AnimState:PlayAnimation(data.animdata.anim)
        end
        if data.animdata.background then
            p.AnimState:SetLayer(LAYER_WORLD_BACKGROUND)
            -- p.AnimState:SetOrientation(ANIM_ORIENTATION.RotatingBillboard)
            p.AnimState:SetSortOrder(3)
        end
    end

    if data.addtags then
        for _, tag in ipairs(data.addtags) do
            p:AddTag(tag)
        end
    end
end

-- 初始化装饰物的子对象，只需初始化部分数据就行
function InteriorSpawner:InitHouseInteriorPrefabChild(p, data)
    if data.rotation then
        p.Transform:SetRotation(data.rotation)
    end
    if data.animdata and data.animdata.flip then
        p.AnimState:SetScale(-1, 1)
    end
end

-- 清理目标区域的空间
-- 虽然房子坐标是累增的，但是防止一些特殊情况，比如mod中途移除再添加导致count重新计数，或者其他mod也有小房子，房子生成位置冲突
function InteriorSpawner:ClearSpace(x, z)
    for _, ent in ipairs(TheSim:FindEntities(x, 0, z, self:GetDis())) do
        ent:Remove()
    end
end

---根据格式初始化室内装饰
function InteriorSpawner:SpawnHouseDoorInteriorPrefabs(x, z, addprops, house)
    self:ClearSpace(x, z)

    self:SpawnWall(x, z)

    for _, data in ipairs(addprops) do
        data = shallowcopy(data)

        local p = SpawnPrefab(data.name)
        -- print("生成" .. data.name, p, x + (data.x_offset or 0), z + (data.z_offset or 0))

        if x and z and (data.x_offset or data.z_offset) then
            p.Transform:SetPosition(x + (data.x_offset or 0), data.y_offset or 0, z + (data.z_offset or 0))
            data.x_offset = nil
            data.y_offset = nil
            data.z_offset = nil
        end

        self:InitHouseInteriorPrefab(p, data)

        if data.init and house then
            data.init(p, house)
            data.init = nil
        end

        p.initData = data
    end
end

function InteriorSpawner:GetHouseCenterPos(target)
    local x, _, z = target.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, 0, z, TheWorld.components.interiorspawner:GetDis(), { "hamlet_housewall" }) --以墙壁作为参考
    if #ents > 0 then
        local pos = ents[1]:GetPosition()
        pos.x = pos.x + 2.8 --中心位置
        return pos
    end
end

---根据目标对象位置计算距离最近的墙面
---side =1 左侧； =2 顶部； =3 右侧； =4 底部
function InteriorSpawner:TestWallOrnamentPos(target, isSetPos, left, top, right, bottom)
    left = left or 7.5
    top = top or 5
    right = right or 7.5

    local pos = target:GetPosition()
    local centerPos = self:GetHouseCenterPos(target)

    if centerPos then
        local x, y, z = centerPos:Get()
        local dx, dz = pos.x - x, pos.z - z

        -- 寻找距离最近的一侧墙
        local side = 1
        local minDis = math.abs(dz + left) --左

        local tem = math.abs(dx + top)
        if tem < minDis then --中
            minDis = tem
            side = 2
        end

        tem = math.abs(dz - right)
        if tem < minDis then --右
            minDis = tem
            side = 3
        end

        if bottom then
            tem = math.abs(dx - bottom)
            if tem < minDis then --下
                minDis = tem
                side = 4
            end
        end
        -- print("最小距离", minDis, dz + 8.5, dx + 5, dz - 7.5)

        if isSetPos and minDis < 4 then
            if side == 1 then
                target.Transform:SetPosition(pos.x, 0, z - left)
            elseif side == 2 then
                target.Transform:SetPosition(x - top, 0, pos.z)
            elseif side == 3 then
                target.Transform:SetPosition(pos.x, 0, z + right)
            elseif side == 4 then
                target.Transform:SetPosition(x + bottom, 0, pos.z)
            end
        end

        return side, minDis, x, z
    end
end

---判断柱子所在哪个角
function InteriorSpawner:TestBeam(target)
    local pos = target:GetPosition()
    local ents = TheSim:FindEntities(pos.x, 0, pos.z, self:GetDis(),
        { "hamlet_housewall" }) --以墙壁作为参考

    if #ents > 0 then
        local x, y, z = ents[1].Transform:GetWorldPosition()
        x = x + 2.8                 --中心位置

        return pos.x < x, pos.z < z --isCorner,isLeft
    end
end

---传入hamletdoor组件对象，递归检测内部是否存在玩家
local TNTERIOR_ONE_OF_TAGS = { "player", "hamlet_door" }
function InteriorSpawner:InterioHasPlayer(door)
    local centerPos = door.components.hamletdoor and door.components.hamletdoor.centerPos

    if not centerPos then return false end

    local doors = {}
    for _, v in ipairs(TheSim:FindEntities(centerPos.x, 0, centerPos.z, self:GetDis(), nil, nil, TNTERIOR_ONE_OF_TAGS)) do
        if v:HasTag("player") then
            return true
        else
            table.insert(door, v)
        end
    end

    for _, d in ipairs(doors) do
        if self:InterioHasPlayer(d) then
            return true
        end
    end

    return false
end

---获取门对室内中心的相对位置
function InteriorSpawner:GetDoorRelativePosition(door)
    local centerPos = self:GetHouseCenterPos(door)
    return door:GetPosition() - centerPos
end

return InteriorSpawner
