local WildPigGroupUtils = require("ptribe_wildpiggrouputils")
local GetPrefab = require("ptribe_utils/getprefab")

local function OnTimerDone(inst, data)
    local self = inst.components.ptribe_wildpigmanager
    if data.name == "ptribe_spawnWildPig" then
        if self.pigs then
            self:UpdatePigs()
        end

        if not self.pigs then --同时只存在一组
            local players = {}
            for _, player in ipairs(AllPlayers) do
                if not IsEntityDeadOrGhost(player)
                    and not FindEntity(player, 30, nil, nil, { "structure" }) --不在建筑附近生成，我不希望拾荒者小队在玩家基地附近生成
                then
                    table.insert(players, player)
                end
            end

            if #players > 0 then
                players = shuffleArray(players)
                for _, player in ipairs(players) do
                    local spawnPos = GetPrefab.GetSpawnPoint(player:GetPosition(), 30, 12)
                    if spawnPos then
                        self:SpawnWildPig(player, spawnPos)
                        inst.components.timer:StartTimer("ptribe_spawnWildPig",
                            TUNING.PTRIBE_SPAWN_WILD_PIG_PERIOD + math.random(0, TUNING.TOTAL_DAY_TIME))
                        return
                    end
                end
            end
        end

        inst.components.timer:StartTimer("ptribe_spawnWildPig", math.random(0, TUNING.TOTAL_DAY_TIME)) --随机一个时间后再次尝试
    end
end

local function Init(inst)
    if not inst.components.timer:TimerExists("ptribe_spawnWildPig") then
        inst.components.timer:StartTimer("ptribe_spawnWildPig", TUNING.PTRIBE_SPAWN_WILD_PIG_PERIOD)
    end
end

--- 野外猪人管理器
local WildPigManager = Class(function(self, inst)
    self.inst = inst

    self.pigs = nil

    inst:DoTaskInTime(0, Init)
    inst:ListenForEvent("timerdone", OnTimerDone)
end)

function WildPigManager:SpawnWildPig(player, pos)
    self.pigs = WildPigGroupUtils.SpawnRandomPigGroup(player, pos)
end

--- 更新猪人状态，判断野外猪人是否死亡
function WildPigManager:UpdatePigs()
    if not self.pigs then return end

    local isExist = false
    for _, v in ipairs(self.pigs) do
        if v:IsValid() then
            isExist = true
            break
        end
    end

    if not isExist then
        self.pigs = nil
    end
end

function WildPigManager:OnSave()
    local data = {}
    local references = {}

    if self.pigs then
        data.pigs = {}
        for _, v in ipairs(self.pigs) do
            if v:IsValid() then
                table.insert(data.pigs, v.GUID)
                table.insert(references, v.GUID)
            end
        end
    end

    return data, references
end

function WildPigManager:OnLoad(data)
end

function WildPigManager:LoadPostPass(newents, data)
    if data.pigs then
        local pigs = {}
        for _, v in ipairs(data.pigs) do
            local pig = newents[v] and newents[v].entity
            if pig then
                table.insert(pigs, pig)
            end
        end
        if #pigs > 0 then
            self.pigs = pigs
        end
    end
end

return WildPigManager
