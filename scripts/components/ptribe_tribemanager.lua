--- 加载范围外部落建设
-- local function OutOfRangeBuild(inst)
-- TODO 先放一放，计算还是有点儿麻烦的
-- local self = inst.components.ptribe_tribemanager
-- if not next(self.managers) then return end

-- local selected = {}

-- for manager, _ in pairs(self.managers) do
--     if manager:IsValid() and manager.components.ptribe_pigmanager and manager.components.ptribe_pigmanager:IsValidManager() then
--         if manager:IsAsleep() and manager.components.ptribe_pigmanager.resource >= ResourceConvert.MAX_NEED then
--             table.insert(selected, manager)
--         end
--     else
--         self.managers[manager] = nil
--     end
-- end

-- if #selected <= 0 then return end

-- local manager = selected[math.random(1, #selected)]
-- end

local TribeManager = Class(function(self, inst)
    self.inst = inst

    self.managers = {}
    self.activePigs = {} --活动的猪人
    self.pigking = nil

    -- inst:DoPeriodicTask(TUNING.TOTAL_DAY_TIME / 2, OutOfRangeBuild, math.random() * TUNING.TOTAL_DAY_TIME / 2)
end)

function TribeManager:AddManager(manager)
    -- self.managers[manager] = true
    table.insert(self.managers, manager)
end

--- 尝试设置猪人的休眠
function TribeManager:TrySetPigCanSleep(pig, canSleep)
    -- 让猪人可以计算寻路，但是这个只能对地皮和寻路墙起作用，对于没有寻路强的建筑不起效果
    pig.components.locomotor:SetTriggersCreep(canSleep)

    if canSleep then
        self.activePigs[pig] = nil
        pig.entity:SetCanSleep(true)
        return true
    end

    local count = 0
    for p, _ in pairs(self.activePigs) do
        if not p:IsValid() then
            self.activePigs[pig] = true
            pig.entity:SetCanSleep(false)
            return true
        end
        count = count + 1
    end

    -- 不能超出数量上限
    if count < TUNING.PTRIBE_MAX_AVTIVE_PIG_COUNT then
        self.activePigs[pig] = true
        pig.entity:SetCanSleep(false)
        return true
    end

    return false
end

--- 获取猪王对象，考虑到mod能打包猪王的特点，但是不考虑多个猪王的情况
function TribeManager:GetPigking()
    if self.inst:HasTag("cave") then --地下默认没有猪王，不考虑mod
        self.pigking = nil
        return
    end

    if self.pigking and self.pigking:IsValid() then
        return self.pigking
    end

    for _, v in pairs(Ents) do
        if v.prefab == "pigking" then
            self.pigking = v
            return v
        end
    end

    self.pigking = nil
end

return TribeManager
