-- 房屋扩建
local RoomBuilder = Class(function(self, inst)
    self.inst = inst
end)

local addprops = {
    { name = "wallinteriorplayerhouse",      x_offset = -2.8, },
    { name = "deco_roomglow" },
    { name = "deco_antiquities_cornerbeam",  x_offset = -5,   z_offset = -15 / 2, },
    { name = "deco_antiquities_cornerbeam",  x_offset = -5,   z_offset = 15 / 2,        animdata = { flip = true } },
    { name = "deco_antiquities_cornerbeam2", x_offset = 4.7,  z_offset = -15 / 2 - 0.3, },
    { name = "deco_antiquities_cornerbeam2", x_offset = 4.7,  z_offset = 15 / 2 + 0.3,  animdata = { flip = true } },
    { name = "swinging_light_rope_1",        x_offset = -2,   y_offset = 1,             addtags = { "playercrafted" } },
    { name = "hamlet_interior_floor",        x_offset = -2.4 },
}

function RoomBuilder:CreateNewRoom(door)
    if door.components.hamletdoor.centerPos then return false end
    local interiorspawner = TheWorld.components.interiorspawner

    local dpos = interiorspawner:GetDoorRelativePosition(door)
    if not dpos or not door.side then return false end --不可能

    local pos = interiorspawner:GetPos(door)
    door.components.hamletdoor:SetCenterPos(pos)
    interiorspawner:SpawnHouseDoorInteriorPrefabs(pos.x, pos.z, addprops, door)
    -- 还要装一个对应的门
    local doorAnim, newDoorAnim
    local newDoor = SpawnPrefab(door.prefab)
    if door.side == 1 then
        newDoor.side = 3
        newDoor.Transform:SetPosition(pos.x + dpos.x, 0, pos.z + 7.5)
        doorAnim = "_open_east"
        newDoorAnim = "_open_west"
    elseif door.side == 2 then
        newDoor.side = 4
        newDoor.Transform:SetPosition(pos.x + 5.5, 0, pos.z + dpos.z)
        doorAnim = "_open_north"
        newDoorAnim = "_open_south"
    elseif door.side == 3 then
        newDoor.side = 1
        newDoor.Transform:SetPosition(pos.x + dpos.x, 0, pos.z - 7.5)
        doorAnim = "_open_west"
        newDoorAnim = "_open_east"
    elseif door.side == 4 then
        newDoor.side = 2
        newDoor.Transform:SetPosition(pos.x - 5.5, 0, pos.z + dpos.z)
        doorAnim = "_open_south"
        newDoorAnim = "_open_north"
    end

    door.AnimState:PlayAnimation(door.prefab .. doorAnim)
    door.initData.animdata.anim = door.prefab .. doorAnim
    door.components.trader:Enable()
    newDoor.AnimState:PlayAnimation(door.prefab .. newDoorAnim)
    newDoor.initData = { animdata = { anim = door.prefab .. newDoorAnim } }
    newDoor.components.trader:Enable()

    door:RemoveTag("predoor")
    newDoor:RemoveTag("predoor")
    newDoor:AddTag("hamlet_houseexit")

    door.components.teleporter:Target(newDoor)
    newDoor.components.teleporter:Target(door)

    if door.SoundEmitter then
        door.SoundEmitter:PlaySound("dontstarve/creatures/together/klaus/lock_break")
    end

    return true
end

return RoomBuilder
