local PigTasks = require("ptribe_pigtasks")

local FN = {}

for _, path in ipairs({
    "1ExpansionOfPigTown",
    "1ItsGold",
    "1LoyalSubjectsOfTheKing",
    "1PigmanIsAfraidOfPain",
    "1PigmanLikesToPicture",
    "1TasteOfSpringFestival",
    "1TheInsecurePigman",
    "1TheKingsMeatFeast",
    "1TheLostPigman",
    "1TheTestOfTheMysticPotion",
    "1ToProtectTheEnvironment",
    "1WantToEatBerries",
    "1WillYouGiveMeTheBranchAndGrass",

    "2DeliciousCactusFlowers",
    "2ImAfraidOfTheDark",
    "2RowdyHound",
    "2ThoseWhoCanCookAreGoodPeople",

    "3PickerGuard",
    "3PigmanAndMermFight",
    "3PigmanWhoWantsToTravel",
    "3RecoverStolenProperty",
    "3TheForestCounterattack",
    "3TrueStrengthLiesWithinOneself",

    "4Braveheart",
    "4FallInto",
    "4NastyVillage",
    "4PigmanJewelDream",
    "4PigmenHateSpider",

    "5BattleThrilling",
    "5MedalOfBraveryFromThePigmen",
    "5TerrifyingShadows",
    "5TheShadowOfThePigVillage"
}) do
    require("prtibe_tasks/" .. path)
end

---根据部落完成率挑选任务，完成率越高挑选的任务越难，奖励越丰富
---@param rate number 部落完成率
function FN.SelectTaskByTribe(rate)
    local index = math.clamp(math.ceil(rate * 2 * math.random() * #PigTasks.TASKS), 1, #PigTasks.TASKS) --加一点儿随机性
    return PigTasks.TASKS[index], index
end

---根据id获取task
function FN.GetTaskById(taskId)
    return PigTasks.TASKS[taskId]
end

return FN
