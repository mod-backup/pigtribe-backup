local ResourceConvert = require("ptribe_resourceconvert")

local FN = {}

-- 自定义物品要设置图标
local hamlet = "images/inventoryimages/hamletinventoryimages.xml"
local ITEM_IMAGE = {
    deed = {
        atlas = hamlet,
        image = "deed.tex"
    },
    ptribe_carnival_prizebooth_kit = {
        image = "carnival_prizebooth_kit.tex"
    },
    oinc = {
        atlas = hamlet,
        image = "oinc.tex"
    },
    silvernecklace = {
        atlas = hamlet,
        image = "silvernecklace.tex"
    },
    ptribe_jungletreeseed = {
        atlas = hamlet,
        image = "jungletreeseed.tex"
    },
    ptribe_telescope = {
        atlas = hamlet,
        image = "telescope.tex"
    },
    ptribe_supertelescope = {
        atlas = hamlet,
        image = "supertelescope.tex"
    },
    ptribe_pigweaponfactory = {
        atlas = hamlet,
        image = "pig_shop_weapons.tex"
    }
}

-- 蓝图
for name, _ in pairs(ResourceConvert.RANDOM_BLUEPRINTS) do
    ITEM_IMAGE[name] = { image = "blueprint.tex" }
end

function FN.GetItemImage(name, isRemoveTex)
    local img = ITEM_IMAGE[name]
    if not img then return end

    if isRemoveTex then
        img.image = string.gsub(img.image, "%.tex$", "")
    end

    return img
end

return FN
