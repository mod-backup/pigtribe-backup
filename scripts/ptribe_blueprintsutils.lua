local Utils = require("ptribe_utils/utils")
local BP = require("ptribe_blueprints")
local BPU = require("ptribe_baseprojectionutils")

----------------------------------------------------------------------------------------------------
BPU.ReadPigmanTribeBluePrintDir() --加载基地投影mod中的蓝图数据
for _, name in ipairs({
    "DarkL",
    "DarkM",
    "DarkS",
    "RockL",
    "RockM",
    "RockS",
    "WindL",
    "WindM",
    "WindS",
}) do
    local file = require("ptribe_tribeblueprint/" .. name)
    if type(file) == "table" then
        BP.AddBluePrint(file.name, file.blueprint)
    end
end

----------------------------------------------------------------------------------------------------

--重新更新这个变量
TUNING.TRIBE_MAX_DIS = 2 * math.sqrt(BP.MAX_SIZE.width * BP.MAX_SIZE.width + BP.MAX_SIZE.height * BP.MAX_SIZE.height)

local BLUEPRINTS = BP.BLUEPRINTS
local MAX_SIZE = BP.MAX_SIZE

local fileBlueprintInfo = {} -- 本地文件蓝图基本数据，这个在游戏中不会改变，enable的改动主客机一致
----------------------------------------------------------------------------------------------------
-- 相关函数

local available_land_factor = 0.7 --可用地皮加权因子，多次测试得出
local size_factor = 0.18          --尺寸加权因子，多次测试得出
local minScore = 2

local FN = {}


local DEL_POS = { { 0, -1 }, { 1, 0 }, { 0, 1 }, { -1, 0 } }
--- 可用地皮，查找范围MAX_SIZE最长值组成的正方形
local function FindValidTitleTab(inst)
    local centrePos = inst:GetPosition()
    local range = math.max(MAX_SIZE.width, MAX_SIZE.height) - 1
    local size = range * 2 + 1 --正方形边长
    local validTileTab = {}
    for i = 1, size do validTileTab[i] = {} end
    local px = centrePos.x - range * 4 - 4 --左上角对应坐标
    local pz = centrePos.z - range * 4 - 4

    -- print("初始位置", centrePos.x, centrePos.z)
    -- print("左上角", px, pz)

    -- 不能只是根据地皮可用来判断，而应该从当前地皮出发，使用BFS向外探索来计算可用地皮
    -- for i = 1, size do
    --     local col = {}
    --     for j = 1, size do
    --         table.insert(col, TheWorld.Map:IsAboveGroundAtPoint(px + i * 4, 0, pz + j * 4))
    --     end
    --     table.insert(validTileTab, col)
    -- end

    local queue = LinkedList()
    validTileTab[range + 1][range + 1] = true --起始点
    queue:Append({ range + 1, range + 1 })
    while queue:Count() > 0 do
        local pos = queue:Head()
        queue:Remove(pos)
        for i = 1, #DEL_POS do
            local newx = pos[1] + DEL_POS[i][1]
            local newz = pos[2] + DEL_POS[i][2]

            -- print("新点", newx, newz, px + newx * 4, pz + newz * 4)
            -- if newx >= 1 and newx <= size and newz >= 1 and newz <= size then
            --     print(validTileTab[newx][newz] == nil, TheWorld.Map:IsAboveGroundAtPoint(px + newx * 4, 0, pz + newz * 4))
            -- end

            if newx >= 1 and newx <= size and newz >= 1 and newz <= size and validTileTab[newx][newz] == nil then
                local res = TheWorld.Map:IsAboveGroundAtPoint(px + newx * 4, 0, pz + newz * 4)
                if res then
                    validTileTab[newx][newz] = true
                    queue:Append({ newx, newz })
                else
                    validTileTab[newx][newz] = false
                end
            end
        end
    end

    -- 把nil填充false
    for i = 1, size do
        for j = 1, size do
            validTileTab[i][j] = validTileTab[i][j] or false
        end
    end

    return validTileTab, size
end

local function BuildPathSort(a, b)
    if a.module.priority == b.module.priority then
        return a.tempId < b.tempId
    else
        return a.module.priority > b.module.priority
    end
end

---规划建设路径，根据蓝图和可用地皮、每个模块的优先级给出一个蓝图建设路线出来
---@param blueprint table 旋转后的蓝图表，独立数据
---@param validTitle table 旋转后的可用地皮表，该表会被修改
---@return table {module, pos} module为每个模块，pos为该模块在蓝图中的行列位置，不包含空模块
local function CalcBuildPath(blueprint, validTitle, startPos)
    local width, height = #blueprint[1], #blueprint
    local buildPath = {}

    -- BFS
    local queue = LinkedList()
    table.insert(buildPath, { module = deepcopy(blueprint[startPos[1]][startPos[2]]), pos = startPos })
    validTitle[startPos[1]][startPos[2]] = false
    buildPath[#buildPath].tempId = #buildPath --临时id，排序需要
    queue:Append(startPos)
    while queue:Count() > 0 do
        local pos = queue:Head()
        queue:Remove(pos)
        for i = 1, #DEL_POS do
            local newx = pos[1] + DEL_POS[i][1]
            local newz = pos[2] + DEL_POS[i][2]

            if newx >= 1 and newx <= height and newz >= 1 and newz <= width and validTitle[newx][newz] then
                validTitle[newx][newz] = false
                if blueprint[newx][newz].tile or #blueprint[newx][newz].prefabs > 0 then
                    table.insert(buildPath, { module = deepcopy(blueprint[newx][newz]), pos = { newx, newz } })
                    buildPath[#buildPath].tempId = #buildPath --临时id，排序需要
                end
                queue:Append({ newx, newz })
            end
        end
    end

    -- 优先级排序
    local managerModule = buildPath[1].module
    local temp = managerModule.priority
    managerModule.priority = 99999 --管理者所在地皮优先级最高
    table.sort(buildPath, BuildPathSort)
    for _, v in ipairs(buildPath) do v.tempId = nil end
    managerModule.priority = temp --虽然不复原也可以

    -- 移除管理者自己的猪人房，不需要建设
    for i = 1, #managerModule.prefabs do
        if managerModule.prefabs[i].prefab == "pighouse" then
            if managerModule.prefabs[i].num > 1 then
                managerModule.prefabs[i].num = managerModule.prefabs[i].num - 1
            else
                table.remove(managerModule.prefabs, i)
            end
            break
        end
    end

    return buildPath
end

---根据猪人房周围的环境随机选择一个规模合适的蓝图，多个蓝图、多个起始点、多个旋转角度
---@param loadBluePrintData table|nil Onload时传入的参数，为原始的返回值
---@return table|nil {id, tab, pos, angle, buildPath, width, height} id为蓝图库蓝图编号，tab为已经旋转过的蓝图，pos为蓝图中管理者所在位置，
---angle为初始蓝图旋转过的角度，当所有得分过低或蓝图里都没有猪人房时返回nil，buildPath为一维表，为该管理者规划的蓝图建设路线
function FN.GetRandomBluePrint(inst, loadBluePrintData, forceEnable)
    math.randomseed(os.time())
    -- print("------------------------------------------------------------------------")
    local validTileTab, size = FindValidTitleTab(inst)
    -- print("validTileTab----------")
    -- pt(validTileTab)
    -- print("----------")

    local res = nil
    local maxScore = 0   --最大得分
    local isFull = false --我不会精确控制得分，对于占地较小的蓝图不太公平
    local blueprintName = loadBluePrintData and loadBluePrintData.name

    -- 这里多判断一下，如果临时更改蓝图名会导致找不到原来的蓝图
    if loadBluePrintData and BLUEPRINTS[blueprintName]
        and loadBluePrintData.pos --玩家指定的猪人房没有pos
        and (forceEnable or fileBlueprintInfo[blueprintName].enable) then
        res = loadBluePrintData
        maxScore = minScore
    else
        res = {}
        -- 预处理
        local cumSum = {} --cumSum[i][j] = 从1行1列开始到i行j列的矩形范围内1的个数
        for i = 0, size do
            cumSum[i] = {}
            for j = 0, size do
                cumSum[i][j] = 0
            end
        end
        for i = 1, size do
            for j = 1, size do
                cumSum[i][j] = (validTileTab[i][j] and 1 or 0) + cumSum[i - 1][j] + cumSum[i][j - 1] -
                    cumSum[i - 1][j - 1]
            end
        end


        local selects
        if blueprintName then
            selects = { blueprintName }
        else
            -- 计算最优值
            local enabledBlueprints = {}
            local enabledCount = 0
            for name, _ in pairs(BLUEPRINTS) do
                if FN.IsEnable(name) then --需要从已启用的蓝图里选择
                    enabledBlueprints[name] = 1
                    enabledCount = enabledCount + 1
                end
            end
            selects = weighted_random_choices(enabledBlueprints, math.min(10, enabledCount)) --每次从10个中评分
        end

        local base = math.floor(#validTileTab / 2) + 1 --中点
        local total = 0.1

        for _, name in ipairs(selects) do
            local blueprint = BLUEPRINTS[name]

            if forceEnable or fileBlueprintInfo[name].enable then
                for _, point in ipairs(blueprint.optionalPoints) do
                    for angle = 0, 3 do --旋转后也要进行计算得分
                        local row, col
                        if angle == 0 then
                            row = point.row
                            col = point.col
                        elseif angle == 1 then
                            row = point.col
                            col = blueprint.height - point.row + 1
                        elseif angle == 2 then
                            row = blueprint.height - point.row + 1
                            col = blueprint.width - point.col + 1
                        elseif angle == 3 then
                            row = blueprint.width - point.col + 1
                            col = point.row
                        end

                        local bri = base + blueprint.height - row --右下角坐标
                        local brj = base + blueprint.width - col
                        local totalOnes = cumSum[bri][brj]
                            - cumSum[bri - blueprint.height][brj]
                            - cumSum[bri][brj - blueprint.width] +
                            cumSum[bri - blueprint.height][brj - blueprint.width]

                        -- 蓝图评分，可惜这里计算用的是蓝图的全部区域，不是蓝图的有效区域，加3是想提高小蓝图概率
                        local score = totalOnes / (blueprint.width * blueprint.height) * available_land_factor +
                            totalOnes * size_factor + 3
                        -- print(string.format(
                        --     "name: %s, manager size: %d * %d\tpos: {%d, %d} -%d-> {%d, %d}\t\ttotalOnes: %d\tscore: %f",
                        --     name, blueprint.width, blueprint.height, point.row, point.col, angle, row, col, totalOnes,
                        --     score))

                        total = total + score

                        -- 随机选择一个蓝图
                        if math.random() < (score / total) then --score应该不为零
                            isFull = totalOnes == blueprint.width * blueprint.height
                            maxScore = score
                            res.name = name
                            res.angle = angle
                            res.pos = { row, col }
                        end

                        -- 如果当前角度所有地皮可用，则不再旋转
                        if totalOnes == blueprint.width * blueprint.height then break end
                    end
                end
            end
        end
    end

    if not blueprintName and maxScore < minScore and not isFull then
        return nil -- 最大得分太小，该位置无法发展部落
    end

    if not res.name then
        return nil
    end

    local blueprint = BLUEPRINTS[res.name]
    res.tab = blueprint.tab
    -- res.score = maxScore
    local width = blueprint.width
    local height = blueprint.height
    size = math.floor(size / 2) + 1
    local basex, basey = size - res.pos[1] + 1, size - res.pos[2] + 1
    if res.angle ~= 0 then
        res.tab = Utils.RotateTable(res.tab, res.angle * 90)
        if res.angle == 1 or res.angle == 3 then --其实也能从res.tab上获取
            width = blueprint.height
            height = blueprint.width
        end
    end

    res.width = width
    res.height = height

    -- 可用地皮也返回
    local validTitle = {}

    for i = 1, height do
        local row = {}
        for j = 1, width do
            table.insert(row, validTileTab[basex + i - 1][basey + j - 1])
        end
        table.insert(validTitle, row)
    end
    res.buildPath = CalcBuildPath(res.tab, validTitle, { res.pos[1], res.pos[2] }) --buildPath数据独立
    -- print("最终结果")
    -- print(string.format("name = %s, pos = (%d,%d), angle = %d, score = %f",
    --     res.name, res.pos[1], res.pos[2], res.angle, maxScore))
    -- pt(res.buildPath, function(module) return string.format("(%d,%d), ", module.pos[1], module.pos[2]) end)
    -- print("----------")

    return res
end

-- 在砍、挖、凿工作下排除一些特殊对象，函数返回true则表示可以被摧毁
local PREFAB_ACTIONS = {
    -- 猪人建造的农场生成的作物不能摧毁
    plant_normal = function(inst)
        return not inst.components.crop or not inst.components.crop.grower
            or not inst.components.crop.grower:HasTag("ptribe_pigmanbuild")
    end
}

--- 根据地皮上对象和任务表中生成新的任务，{prefab,pos,tile}
local TITLE_CHECK_RANGE = math.sqrt(8)
local PIGKING_MIN_DIS_SQ = 256 --16
local TITLE_CHECK_CANT_TAGS = { "FX" }
function FN.GetTasksByModule(index, module, pos, oldTasks, angle)
    local tasks = {}

    -- 猪王附近不建造
    local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
    if pigking and pos:DistSq(pigking:GetPosition()) < PIGKING_MIN_DIS_SQ then
        return tasks
    end

    local n = math.ceil(math.sqrt(math.max(0, module.num - (module.tile and 1 or 0)))) --每行个数
    local interval = n == 0 and 0 or 4 / n                                             --每个物品的长宽
    module.existNum = module.num                                                       --满值

    -- 处理地皮
    if module.tile then
        local tile = TheWorld.Map:GetTileAtPoint(pos:Get())
        local taskExist = false
        for _, task in ipairs(oldTasks) do
            if task.id == index and task.tile then
                taskExist = true
                break
            end
        end
        if not (tile == module.tile or taskExist) then
            table.insert(tasks, {
                id = index,
                tile = module.tile,
                pos = pos,
                type = "spawn",
            })
        end
    end

    if module.hasPos then
        -- 对每一个预制体的位置判断
        for prefabId, p in ipairs(module.prefabs) do
            if not p.fail then --没有失败标记
                -- 虽然支持pos,但是蓝图旋转后pos没有跟着旋转
                local modx, modz = Utils.RotateRectanglePos(4, 4, p.pos[1], p.pos[2], angle * 90)

                local px = pos.x - 2 + modx
                local pz = pos.z - 2 + modz

                local needTask = true
                -- 查找半径为0.5，懒得统计每个预制体的半径了
                for _, v in ipairs(TheSim:FindEntities(px, 0, pz, 0.5, nil, TITLE_CHECK_CANT_TAGS)) do
                    if v.prefab and not (v.components.locomotor or (v.components.inventoryitem and not v:HasTag("heavy"))) then
                        if p.prefab == v.prefab then
                            needTask = false
                        else
                            -- 检查是否可砍、凿、挖，即便目标在collectTasks中也无所谓，执行时会再检查
                            local workAction = v.components.workable and v.components.workable:GetWorkAction()
                            if not v:HasTag("ptribe_pigmanbuild") and workAction and v.components.workable:CanBeWorked()
                                and ((workAction == ACTIONS.CHOP
                                        and not string.match(v.prefab, "^winter_")
                                        and not string.starts(v.prefab, "oceantree"))
                                    or workAction == ACTIONS.MINE
                                    or workAction == ACTIONS.DIG)
                                and (not PREFAB_ACTIONS[v.prefab] or PREFAB_ACTIONS[v.prefab](v)) then -- 特殊检查
                                table.insert(tasks, {
                                    id = index,                                                        --模块buildpath索引
                                    prefab = v,
                                    pos = v:GetPosition(),
                                    type = "work"
                                })
                            else
                                needTask = false
                                if not string.match(v.prefab, "^winter_") then
                                    module.existNum = module.existNum - 1 --放弃
                                end
                            end
                        end
                    end
                end

                -- 检查任务列表
                for _, task in ipairs(oldTasks) do
                    if task.id == index and task.prefabId == prefabId then
                        needTask = false
                        break
                    end
                end

                if needTask then
                    table.insert(tasks, {
                        id = index,
                        prefabId = prefabId,
                        prefab = p.prefab,
                        pos = Vector3(px, 0, pz),
                        type = "spawn"
                    })
                end
            end
        end
    elseif n ~= 0 then
        -- 采用分区的方法来计算，首先把一个地皮大小按蓝图预制体数量等分并划分，再用排除法，依次排除已有对象和任务表中的对象
        -- 根据蓝图规划位置
        local cur = 0
        local grid = Utils.CreateEmptyTable(n + 1, n + 1)
        for _, p in ipairs(module.prefabs) do
            if not p.fail then --没有失败标记
                for i = 1, p.num do
                    cur = cur + 1
                    local row = math.ceil(cur / n)
                    local col = cur % n + (cur % n == 0 and n or 0)
                    grid[row][col] = {
                        prefabId = i,
                        prefab = p.prefab,
                        pos = { row, col }
                    }
                end
            end
        end

        -- 检查地皮上对象
        for _, v in ipairs(TheSim:FindEntities(pos.x, 0, pos.z, TITLE_CHECK_RANGE, nil, TITLE_CHECK_CANT_TAGS)) do
            local x, _, z = v.Transform:GetWorldPosition()
            if math.abs(x - pos.x) < 2 and math.abs(z - pos.z) < 2
                and not (v.components.locomotor or (v.components.inventoryitem and not v:HasTag("heavy")))
                and (not PREFAB_ACTIONS[v.prefab] or PREFAB_ACTIONS[v.prefab](v)) then
                local row = math.ceil(math.abs(x - (pos.x - 2)) / interval)
                local col = math.ceil(math.abs(z - (pos.z - 2)) / interval)
                -- print("检查", #grid, #grid[1], row, col, x, z, pos.x, pos.z, interval)
                local g = grid[row][col]
                if g.prefab == v.prefab and not v:HasTag("burnt") then
                    if not g.pos then
                        module.existNum = module.existNum + 1 --一片区域可能有多个对象，其中一个对象正好是自己需要的
                    end
                    g.pos = nil                               --移除标志
                else
                    if g.pos then
                        module.existNum = module.existNum - 1
                        g.pos = nil --移除标志
                    end

                    -- 检查是否可砍、凿、挖，即便目标在collectTasks中也无所谓，执行时会再检查
                    local workAction = v.components.workable and v.components.workable:GetWorkAction()
                    if (not v:HasTag("ptribe_pigmanbuild") or v:HasTag("burnt"))
                        and workAction and v.components.workable:CanBeWorked()
                        and ((workAction == ACTIONS.CHOP and not string.match(v.prefab, "^winter_"))
                            or workAction == ACTIONS.MINE
                            or workAction == ACTIONS.DIG
                            or (workAction == ACTIONS.HAMMER and v:HasTag("burnt"))) then
                        -- 特殊检查

                        table.insert(tasks, {
                            id = index, --模块buildpath索引
                            prefab = v,
                            pos = v:GetPosition(),
                            type = "work"
                        })
                    end
                end
            end
        end

        -- 最后是oldTasks
        for _, task in ipairs(oldTasks) do
            if task.id == index and task.prefab then
                local row = math.ceil((task.pos.x - (pos.x - 2)) / interval)
                local col = math.ceil((task.pos.z - (pos.z - 2)) / interval)
                -- print(task.id, task.prefab, #grid, #grid[1], row, col, task.pos.x, pos.x)
                grid[row][col].pos = nil
            end
        end

        for _, r in ipairs(grid) do
            for _, m in ipairs(r) do
                if m.pos then
                    -- print("pos", m.prefab, pos.x, pos.z, (m.pos[1] - 0.5) * interval - 2 + pos.x,
                    --     (m.pos[2] - 0.5) * interval - 2 + pos.z)
                    table.insert(tasks, {
                        id = index, --模块buildpath索引
                        prefabId = m.prefabId,
                        prefab = m.prefab,
                        pos = Vector3((m.pos[1] - 0.5) * interval - 2, 0, (m.pos[2] - 0.5) * interval - 2) + pos,
                        type = "spawn"
                    })
                end
            end
        end
    end

    return tasks
end

----------------------------------------------------------------------------------------------------

for name, blueprint in pairs(BLUEPRINTS) do
    fileBlueprintInfo[name] = {
        width = blueprint.width,
        height = blueprint.height,
        enable = true --enable的值等主机读取数据后再设置
    }
end

local disableBlueprint = {} --仅保存使用
local DATA_FILE = "mod_config_data/ptribe_disableBlueprint"
TheSim:GetPersistentString(DATA_FILE, function(load_success, str)
    if load_success and #str > 0 then
        local run_success, data = RunInSandboxSafe(str)
        if run_success then
            for name, _ in pairs(data) do
                if fileBlueprintInfo[name] then
                    fileBlueprintInfo[name].enable = false
                    disableBlueprint[name] = true
                end
            end
        end
    end
end)

function FN.SetEnable(name, enable)
    if fileBlueprintInfo[name] then
        fileBlueprintInfo[name].enable = enable
        disableBlueprint[name] = not enable or nil
        SavePersistentString(DATA_FILE, DataDumper(disableBlueprint, nil, true), false)
    end
end

function FN.IsEnable(name)
    return fileBlueprintInfo[name] and fileBlueprintInfo[name].enable or false
end

----------- 这里不考虑兼容客机蓝图信息了，因为实现起来很复杂，而且不同客机蓝图信息不一致，动态修改较麻烦 -----------

---获取本地蓝图基本数据，需要一个调用时机，本地目录的可以一开始就获取，但是基地投影mod里的需要等mod加载完后再获取
function FN.GetBlueprintBaseInfo()
    -- local status, data = pcall(BPU.GetBlueprintBaseInfo)
    -- if status then
    --     return MergeMaps(data, fileBlueprintInfo)
    -- else
    --     print("读取基地投影mod蓝图数据失败，请联系作者更新。" .. data)
    return fileBlueprintInfo
    -- end
end

---通过蓝图名获取格式转换后的本地蓝图数据
function FN.GetBlueprintByName(name)
    -- if BLUEPRINTS[name] then
    return BLUEPRINTS[name]
    -- end

    -- local status, data = pcall(BPU.GetBlueprintByName, name)
    -- if status then
    --     return data
    -- else
    --     return "读取基地投影mod " .. name .. " 蓝图失败，请联系作者更新。" .. data
    -- end
end

---获取本地蓝图（主客机都有）
function FN.GetFileBluprintByName(name)
    return fileBlueprintInfo[name]
end

--- 根据蓝图名获取部落半径平方根
function FN.GetMaxDisSq(name)
    local d = BP.BLUEPRINTS[name]
    if not d then return -1 end

    local w, h = d.width, d.height
    return (w * w + h * h) * 16
end

----------------------------------------------------------------------------------------------------
local PHYSICS_RADIUS = {} --预制体的默认物理半径

--- 注册对象物理半径
function FN.RegisterPhysicsRadius(prefab)
    PHYSICS_RADIUS[prefab.prefab] = prefab:GetPhysicsRadius(0) or 0
end

function FN.GetPhysicsRadius(name)
    return PHYSICS_RADIUS[name]
end

return FN
