--@author: 绯世行
--欢迎其他开发者直接使用，但是强烈谴责搬用代码后对搬用代码加密的行为！
--使用案例及最新版：https://n77a3mjegs.feishu.cn/docx/K9bUdpb5Qo85j2xo8XkcOsU1nuh?from=from_copylink

--初始化一些数据结构

local FN = {}
local _source = debug.getinfo(1, 'S').source
local KEY = "_" .. _source:match(".*scripts[/\\](.*)%.lua"):gsub("[/\\]", "_") .. "_"
local Utils = require(_source:match(".*scripts[/\\](.*[/\\])") .. "utils")

---虽然写了，但是感觉挺麻烦的，不如结合childspawner和entitytracker自定义个组件
---强化EntityTracker记录多对象的功能，后面追加索引的方式添加同类型对象，当前索引位置不存在对象时，从某位查找对象替代该位置
function FN.EnhanceEntitytrackerTrackEntity()
    local self = require("components/entitytracker")

    self[KEY .. "entList"] = {}

    self[KEY .. "TrackEntity"] = function(self, name, ent)
        local count = self[KEY .. "entList"][name]

        count = (count or 0) + 1
        self[KEY .. "entList"][name] = count

        self:TrackEntity(name .. tostring(count), ent)
    end

    self[KEY .. "ForeachEntity"] = function(self, name, fn)
        local count = self[KEY .. "entList"][name]
        if not count then return end
        local endIndex = count
        for i = 1, count do
            local ent = self:GetEntity(name .. tostring(count))
            if not ent then
                --交换位置
                while endIndex > i do
                    ent = self:GetEntity(name .. tostring(endIndex))
                    endIndex = endIndex - 1

                    if ent then
                        self:TrackEntity(name .. tostring(i), ent)
                        self:ForgetEntity(name .. tostring(endIndex + 1))
                        break
                    end
                end
            end

            if ent then
                fn(ent)
            else
                break
            end
        end

        self[KEY .. "entList"][name] = endIndex
    end
end

---允许玩家通过trader给予道具允许每次给予一组，在onaccept中不要忘了可能是一组道具
---@param checkFn function 校验函数，如果为true则表示可以给予一组
function FN.AllowTraderGiveAll(GLOBAL, checkFn)
    assert(checkFn)

    Utils.FnDecorator(GLOBAL.ACTIONS.GIVE, "fn", function(act)
        if act.target ~= nil
            and not (act.target:HasTag("playbill_lecturn") and act.invobject.components.playbill)
            and not (act.target.components.ghostlyelixirable ~= nil and act.invobject.components.ghostlyelixir ~= nil)
            and act.target.components.trader ~= nil then
            local able, reason = act.target.components.trader:AbleToAccept(act.invobject, act.doer)
            if not able then
                return { false, reason }, true
            end

            act.target.components.trader:AcceptGift(act.doer, act.invobject,
                checkFn(act) and GetStackSize(act.invobject) or nil)
            return { true }, true
        end
    end)
end

--- 修复Inventory的GetItemsWithTag方法bug，无法正确获取手上物品
function FN.RepairInventoryGetItemsWithTag()
    local Inventory = require("components/inventory")
    function Inventory:GetItemsWithTag(tag)
        local items = {}
        for k, v in pairs(self.itemslots) do
            if v and v:HasTag(tag) then
                table.insert(items, v)
            end
        end

        if self.activeitem and self.activeitem:HasTag(tag) then
            table.insert(items, self.activeitem) --修复这里
        end

        local overflow = self:GetOverflowContainer()
        if overflow ~= nil then
            local overflow_items = overflow:GetItemsWithTag(tag)
            for _, item in ipairs(overflow_items) do
                table.insert(items, item)
            end
        end

        return items
    end
end

---添加方法AddToHistoryCanRepeat(sender_name, message, colour, icondata, ...)，使其支持图标的同时还能显示重复内容
function FN.ChatHistoryAddToHistoryCanRepeat()
    function ChatHistory:AddToHistoryCanRepeat(sender_name, message, colour, icondata, ...)
        local old = self.NPC_CHATTER_MAX_CHAT_NO_DUPES
        self.NPC_CHATTER_MAX_CHAT_NO_DUPES = 0 --移除对重复内容的判断

        self:AddToHistory(ChatTypes.ChatterMessage, nil, nil, sender_name, message, colour, icondata, ...)

        self.NPC_CHATTER_MAX_CHAT_NO_DUPES = old
    end
end

local tempTagKey = "_tempTags"

---监听标签的添加和移除，并添加AddTempTag和RemoveTempTag两个方法支持临时标签
function FN.AddTempTagMethod()
    -- Utils.FnDecorator(EntityScript, "AddTag", function(self, tag) end)

    Utils.FnDecorator(EntityScript, "RemoveTag", function(self, tag)
        local tags = self[tempTagKey]
        if not tags or not tags[tag] then return end

        if tags[tag].isForbidRemove then return nil, true end

        tags[tag] = nil
        if GetTableSize(tags) <= 0 then
            self[tempTagKey] = nil
        end
    end)

    ---添加临时标签
    ---@param isForbidRemove boolean|nil 是否禁止使用RemoveTag移除该标签，默认为false，为true时只能使用RemoveTempTag来移除标签
    function EntityScript:AddTempTag(tag, isForbidRemove)
        self[tempTagKey] = self[tempTagKey] or {}
        self[tempTagKey][tag] = { isForbidRemove = isForbidRemove }
        self:AddTag(tag)
    end

    function EntityScript:RemoveTempTag(tag)
        local d = self[tempTagKey] and self[tempTagKey][tag]
        if d then
            d.isForbidRemove = nil
            self:RemoveTag(tag)
        end
    end
end

----------------------------------------------------------------------------------------------------

local TAG_VAR = "inst." .. KEY .. "_tags"

local function OnTagdirty(inst)
    local data = inst[TAG_VAR]
    -- print("值为", data.tagDirty:value())
    local tags = json.decode(data.tagDirty:value())
    for tag, enable in pairs(tags) do
        data.tags[tag] = enable or nil --添加或移除标签
    end
end

local MAX_COUNT = 100 --每帧设置的标签数量上限
local function PushTag(inst)
    local data = inst[TAG_VAR]

    -- 均衡
    local tags = {}
    local count = 0
    for k, v in pairs(data.canche) do
        data.canche[k] = nil
        tags[k] = v
        count = count + 1
        if count >= MAX_COUNT then break end
    end

    if count > 0 then
        data.tagDirty:set(json.encode(tags))
    end

    if next(data.canche) then
        data.task = inst:DoTaskInTime(0, PushTag)
    else
        data.task = nil
    end
end

local function AddTag(inst, tag)
    local data = inst[TAG_VAR]
    if not data or data.tags[tag] then return end

    data.tags[tag] = true
    if TheWorld.ismastersim then --这里可以优化一下，对于单人不开洞穴的情况不需要
        data.canche[tag] = true
        if not data.task then
            data.task = inst:DoTaskInTime(0, PushTag)
        end
    end
end

local function RemoveTagBefore(inst, tag)
    local data = inst[TAG_VAR]
    if not data or not data.tags[tag] then return end

    data.tags[tag] = nil
    if TheWorld.ismastersim then
        data.canche[tag] = false
        if not data.task then
            data.task = inst:DoTaskInTime(0, PushTag)
        end
    end
end

local function HasTagBefore(inst, tag)
    local data = inst[TAG_VAR]
    if data and data.tags[tag] then
        return { true }, true
    end
end

local function HasTagsBefore(inst, ...)
    local data = inst[TAG_VAR]
    if not data then return end

    local tags = {}
    for _, t in ipairs({ ... }) do
        if not data.tags[t] then
            table.insert(tags, t)
        end
    end

    if #tags <= 0 then
        return { true }, true
    end
    return nil, false, { inst, unpack(tags) }
end

local function HasOneOfTagsBefore(inst, ...)
    local data = inst[TAG_VAR]
    if not data then return end

    for _, t in ipairs({ ... }) do
        if data.tags[t] then
            return { true }, true
        end
    end
end

function FN.ExtendTag(inst)
    local netKey = inst.prefab .. ".custom_tag"
    inst[TAG_VAR] = {
        tags = {},                                        --标签表
        tagDirty = net_string(inst.GUID, netKey, netKey), --同步标签使用
        canche = {},                                      --缓存标题，每帧同步一次
        task = nil                                        --同步定时任务
    }

    if not TheWorld.ismastersim then
        inst:ListenForEvent(netKey, OnTagdirty) --客机监听
    end

    inst.AddTag = AddTag
    Utils.FnDecorator(inst, "RemoveTag", RemoveTagBefore)
    Utils.FnDecorator(inst, "HasTag", HasTagBefore)
    Utils.FnDecorator(inst, "HasTags", HasTagsBefore)
    inst.HasAllTags = inst.HasTags
    Utils.FnDecorator(inst, "HasOneOfTags", HasOneOfTagsBefore)
    inst.HasAnyTag = inst.HasOneOfTags
end

--- 使含有drawable组件的物品（比如小木牌、画框）支持显示mod物品，要求是inventoryimages目录下的
function FN.RegisterDrawable()
    local MOD_ITEM_PRE = "images/inventoryimages/"
    local Drawable = require("components/drawable")
    Utils.FnDecorator(Drawable, "OnDrawn", nil,
        function(retTab, self, imagename, imagesource, atlasname)
            if atlasname and string.match(atlasname, "^" .. MOD_ITEM_PRE) then --非mod物品一般atlasname为空，而且也不可能有inventoryimages目录
                self.inst.AnimState:OverrideSymbol("SWAP_SIGN",
                    resolvefilepath(MOD_ITEM_PRE .. imagename .. ".xml"), imagename .. ".tex")
            end
        end)
end

return FN
