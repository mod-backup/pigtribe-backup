local Utils = require("ptribe_utils/utils")
local GetPrefab = require("ptribe_utils/getprefab")

local FN = {}
local UNDEFINED_RESOURCE = {} --对于未定义的预制体价值的判断

local function PickOne(table, isSpawnPrefab)
    local item = table[math.random(1, #table)]
    if isSpawnPrefab then
        return SpawnPrefab(item)
    else
        return item
    end
end

local function SpawnItem(inst, manager, prefab, count, processFn)
    local item = SpawnPrefab(prefab)
    local items = { item }

    if count > 1 then
        if item.components.stackable then
            item.components.stackable.stacksize = count
        else
            for _ = 1, count - 1 do
                table.insert(items, SpawnPrefab(prefab))
            end
        end
    end
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-count * FN.GetResource(prefab))
    end

    for _, it in ipairs(items) do
        processFn(inst, it)
    end
end

local function PutContainer(inst, item)
    inst.components.container:GiveItem(item)
end

--预制体生成后的初始化
local PREFABS_INIT = {}

---添加预制体初始化，用来初始化猪人建造的预制体，并且定义猪人的交互，在调用interactFn前会先用isInteractiveFn判断
---@param name string 预制体名
---@param initFn function|nil 第一次创建时调用
---@param onLoadFn function|nil 每次进入游戏都会在组件的OnLoad中调用
---@param isInteractiveFn function|nil 是否可互动，根据对象当前的状态判断是否可以互动，比如锅如果正在做饭就不能互动，锅做完了或者锅空闲就可以，允许返回两个值，一个是是否可以交互，另一个是数据，传递给interactFn
---@param interactFn function|nil 互动函数，当管理者查找收集任务时进行的互动
---@param interactCd number|nil 交互cd，默认45秒
local function AddPrefabInit(name, initFn, onLoadFn, isInteractiveFn, interactFn, interactCd)
    if not interactFn then interactCd = 9999999 end
    PREFABS_INIT[name] = {
        init = initFn,
        load = onLoadFn,
        isInteractive = isInteractiveFn,
        interact = interactFn,
        interactCd = interactCd,
    }
end

local function onhammered(inst)
    SpawnPrefab("collapse_big").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst:Remove()
end

local function AddWorkable(inst)
    if not inst.components.workable then
        inst:AddComponent("workable") --临时添加的，敲击不会引起仇恨，这里不做处理
        inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
        inst.components.workable:SetWorkLeft(4)
        inst.components.workable:SetOnFinishCallback(onhammered)
    end
end

-- 随机武器
local RANDOM_WEAPONS = {
    "perdfan", "hambat", "houndwhistle", "pig_token"
}

-- 往木箱里塞点宝贝
AddPrefabInit("treasurechest", function(inst, doer, manager)
    if math.random() < 0.25 then
        --随机武器
        SpawnItem(inst, manager, PickOne(RANDOM_WEAPONS), 1, PutContainer)
    end
    if math.random() < 0.1 then
        -- 铸币
        SpawnItem(inst, manager, "pig_coin", 1, PutContainer)
    end
    -- 金子
    SpawnItem(inst, manager, "goldnugget", math.random(2, 5), PutContainer)
end)

-- 蘑菇灯里放几个灯泡，交互条件为容器为空，交互为往里面放灯泡
AddPrefabInit("mushroom_light", function(inst, doer, manager)
    if math.random() < 0.05 then
        --启迪碎片
        SpawnItem(inst, manager, "alterguardianhatshard", 1, PutContainer)
    end
    SpawnItem(inst, manager, "winter_ornament_light" .. tostring(math.random(1, 7)), 1, PutContainer)
end, nil, function(inst)
    return inst.components.container and inst.components.container:IsEmpty()
end, function(inst, doer, manager)
    SpawnItem(inst, manager, "winter_ornament_light" .. tostring(math.random(1, 7)), 1, PutContainer)
end)

-- 冰箱里塞点食物
AddPrefabInit("icebox", function(inst, doer, manager)
    -- 浆果
    SpawnItem(inst, manager, "berries", math.random(2, 5), PutContainer)
end)

-- 随机蔬菜
local RANDOM_VEGGIES = {}
require("prefabs/veggies")
for name, _ in pairs(VEGGIES) do
    table.insert(RANDOM_VEGGIES, name)
    UNDEFINED_RESOURCE[name] = 3
end
-- 盐盒建成时里面塞点食物
AddPrefabInit("saltbox", function(inst, doer, manager)
    SpawnItem(inst, manager, PickOne(RANDOM_VEGGIES), math.random(1, 3), PutContainer)
    SpawnItem(inst, manager, PickOne(RANDOM_VEGGIES), math.random(1, 3), PutContainer)
end)

-- 猪火炬，默认生成在屏幕之外，这里要手动生成下
AddPrefabInit("pigtorch", function(inst)
    -- inst.components.fueled.DoUpdate = Utils.EmptyFn --一直燃烧没意思
    inst.components.spawner:ReleaseChild()
end)
AddPrefabInit("ptribe_pigtorch", function(inst)
    inst.components.spawner:ReleaseChild()
end)


local RANDOM_FOODS = {} --随机食物
for k, v in pairs(require("preparedfoods")) do
    table.insert(RANDOM_FOODS, k)
    UNDEFINED_RESOURCE[k] = math.ceil((v.health or 0) / 10 + (v.hunger or 0) / 25 + (v.sanity or 0) / 10)
end

for k, v in pairs(require("preparedfoods_warly")) do
    table.insert(RANDOM_FOODS, k)
    UNDEFINED_RESOURCE[k] = math.ceil((v.health or 0) / 10 + (v.hunger or 0) / 25 + (v.sanity or 0) / 10)
end

-- 撒了调味料的食物就不要了
-- for k, _ in pairs(require("spicedfoods")) do
--     table.insert(RANDOM_FOODS, k)
-- end

local function StartCooking(inst, doer, manager)
    inst.components.container:GiveItem(SpawnPrefab("berries"))
    inst.components.container:GiveItem(SpawnPrefab("berries"))
    inst.components.container:GiveItem(SpawnPrefab("berries"))
    inst.components.container:GiveItem(SpawnPrefab("berries"))
    inst.components.stewer:StartCooking(doer)
    local product = PickOne(RANDOM_FOODS)
    inst.components.stewer.product = product --虽然这里修改了产物，但是烹饪时间还是果酱的
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-math.max(10, FN.GetResource(product)))
    end
end

-- 烹饪锅做点简单饭，交互为烹饪或者收获
AddPrefabInit("cookpot", StartCooking, nil, function(inst)
    return inst.components.container and inst.components.container:IsEmpty()
        and inst.components.stewer and not inst.components.stewer:IsCooking() --没有正在烹饪
end, function(inst, doer, manager)
    if inst.components.stewer:IsDone() then
        if manager and inst.components.stewer.chef_id then --玩家做的料理
            manager.components.ptribe_pigmanager:FavorabilityDelta(15)
        end
        inst.components.stewer:Harvest(doer)
    else
        StartCooking(inst, doer, manager)
    end
end, 0.5 * TUNING.TOTAL_DAY_TIME) --0.5天烹饪一次

PREFABS_INIT["portablecookpot"] = PREFABS_INIT["cookpot"]
PREFABS_INIT["archive_cookpot"] = PREFABS_INIT["cookpot"]


local function PlantItem(inst, doer, manager)
    inst.components.grower:PlantItem(SpawnPrefab("seeds"), doer)
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-1)
    end
end

local function FarmplotInit(inst)
    if inst.components.grower then
        Utils.FnDecorator(inst.components.grower, "onplantfn", function()
            local crop = next(inst.components.grower.crops) --crop一定存在
            crop.components.ptribe_pigmanbuild:Init()
        end)
    end
end

-- 基础农场种菜，交互为施肥、栽种、收获
AddPrefabInit("slow_farmplot", Utils.MergeFn(FarmplotInit, PlantItem), FarmplotInit, function(inst)
    if not inst.components.grower then return false end
    -- 施肥
    if inst.components.grower.cycles_left <= 0 and inst.components.hauntable then
        return true, 1
    end

    --栽种
    if inst.components.grower:IsEmpty() then
        return true, 2
    end

    -- 收获
    local crop = next(inst.components.grower.crops) --crop一定存在
    if crop and crop.components.crop and crop.components.crop:IsReadyForHarvest() then
        return true, 3
    end
    return false
end, function(inst, doer, manager, data)
    if data == 1 then
        local poop = SpawnPrefab("poop")
        inst.components.grower:Fertilize(poop, doer)
    elseif data == 2 then
        PlantItem(inst, doer, manager)
    else
        local crop = next(inst.components.grower.crops)
        crop.components.crop:Harvest(doer)
    end
end, 2 * TUNING.TOTAL_DAY_TIME)

-- 改良农场种菜，交互为施肥、栽种、收获
PREFABS_INIT["fast_farmplot"] = PREFABS_INIT["slow_farmplot"]

-- 随机蘑菇
local RANDOM_MUSHROOMS = { "blue_cap", "red_cap", "green_cap", "moon_cap" }

local function PlantMushroom(inst, doer, manager)
    local grow_time = TUNING.MUSHROOMFARM_FULL_GROW_TIME
    local productname = PickOne(RANDOM_MUSHROOMS)
    local max_produce = math.random(3, 6)
    inst.AnimState:OverrideSymbol("swap_mushroom", "mushroom_farm_" .. (string.split(productname, "_")[1]) .. "_build",
        "swap_mushroom")

    inst.components.harvestable:SetProduct(productname, max_produce)
    inst.components.harvestable:SetGrowTime(grow_time / max_produce)
    inst.components.harvestable:Grow()

    TheWorld:PushEvent("itemplanted", { doer = doer, pos = inst:GetPosition() }) --this event is pushed in other places too
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-1)
    end
end

-- 蘑菇农场随便种点蘑菇，互动为施肥、种蘑菇、收获
AddPrefabInit("mushroom_farm", PlantMushroom, nil, function(inst)
    if not inst.components.harvestable then return false end

    if inst.remainingharvests == 0 then
        -- 需要施肥
        return true, 1
    end

    if not inst.components.harvestable.task then --其实由于刚种下去就能收获一个，判断产量也可以
        -- 种蘑菇
        return true, 2
    end

    if inst.components.harvestable.produce == inst.components.harvestable.maxproduce then
        -- 收获
        return true, 3
    end

    return false
end, function(inst, doer, manager, data)
    if data == 1 then
        inst.components.trader.onaccept(inst, doer) --不用生成活木
    elseif data == 2 then
        PlantMushroom(inst, doer, manager)
    elseif data == 3 then
        inst.components.harvestable:Harvest(doer)
    end
end, 2.5 * TUNING.TOTAL_DAY_TIME)

-- 随机药剂
local RANDOM_EXPERIMENT_RESULTS = {
    "halloween_experiment_bravery",
    "halloween_experiment_health",
    "halloween_experiment_sanity",
    "halloween_experiment_volatile",
    "halloween_experiment_moon",
    "halloween_experiment_root",
}

local function MadeLab(inst, doer, manager)
    inst.components.madsciencelab:StartMakingScience(
        PickOne(RANDOM_EXPERIMENT_RESULTS))
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-10)
    end
end

-- 疯狂科学家实验室随机制作点药水,互动为做实验
AddPrefabInit("madscience_lab", MadeLab, nil, function(inst)
    return inst.components.madsciencelab and not inst.components.madsciencelab:IsMakingScience() or false --猪人会实验药品，不用收获
end, MadeLab, 1 * TUNING.TOTAL_DAY_TIME)

-- 随机鸟
local RANDOM_BIRDS = { "crow", "robin", "robin_winter", "canary", "puffin", "bird_mutant_spitter", "bird_mutant" }

local function PutInBird(inst, doer, manager)
    local bird = PickOne(RANDOM_BIRDS, true)
    inst.components.occupiable:Occupy(bird)
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-10)
    end
end

-- 鸟笼随便装个鸟进去，互动为放入鸟、喂食
AddPrefabInit("birdcage", PutInBird, nil, function(inst)
    if not inst.components.occupiable or not inst.components.trader then return false end

    if not inst.components.occupiable:IsOccupied() then
        -- 空鸟笼
        return true, 1
    end

    -- 喂食
    return true, 2
end, function(inst, doer, manager, data)
    if data == 1 then
        PutInBird(inst, doer, manager)
    else
        local item
        if math.random() < 0.5 then
            item = SpawnPrefab("meat")
        else
            item = PickOne(RANDOM_VEGGIES, true)
        end
        inst.components.trader.onaccept(inst, doer, item)
        item:Remove()
        if manager then
            manager.components.ptribe_pigmanager:ResourceDoDelta(-1)
        end
    end
end, 60)

-- 随机装饰品
local RANDOM_DECORS = { "decor_flowervase", "decor_lamp", "decor_centerpiece", "decor_portraitframe", "phonograph" }

local RANDOM_RECORDS = {
    "record", --一个不存在的皮肤
    "record_creepyforest",
    "record_drstyle",
    "record_efs",
}

-- 木桌上放点东西，互动为给餐桌灯加燃料
AddPrefabInit("wood_table_round", function(inst, doer, manager)
    local item = PickOne(RANDOM_DECORS, true)
    -- if item.prefab == "decor_pictureframe" then
    -- 画框来个肉，需要在屏幕内
    -- local featherpencil = SpawnPrefab("featherpencil")
    -- local meat = SpawnPrefab("meat")
    -- featherpencil.components.drawingtool:Draw(item, featherpencil.components.drawingtool:GetImageToDraw(item))
    -- meat:Remove()
    -- end
    if item.prefab == "phonograph" then
        --留声机里放入一个唱片
        local userid = AllPlayers[1] and AllPlayers[1].userid
        local record = SpawnPrefab("record", PickOne(RANDOM_RECORDS), nil, userid)
        item.components.trader.onaccept(item, nil, record)
    end


    inst.components.furnituredecortaker:AcceptDecor(item, doer)
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-FN.GetResource(item.prefab))
    end
end, nil, function(inst)
    local item = inst.components.furnituredecortaker and inst.components.furnituredecortaker.decor_item
    if not item or not item.components.fueled or not item.components.machine then return false end --其实是餐桌灯
    return item.components.fueled:GetPercent() < 0.5, item
end, function(inst, doer, manager, data)
    local isEmpty = data.components.fueled:IsEmpty()
    data.components.fueled:DoDelta(data.components.fueled.maxfuel * 0.6, doer)
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-1)
    end

    if isEmpty then
        data.components.machine:TurnOn() -- 开灯
    end
end, 0.5 * TUNING.TOTAL_DAY_TIME)

PREFABS_INIT["wood_table_square"] = PREFABS_INIT["wood_table_round"]

--随机冬季盛宴食物
local RANDOM_WINTERS_FEAST_COOKED_FOODS = {}
for name, _ in pairs(require("wintersfeastcookedfoods").foods) do
    table.insert(RANDOM_WINTERS_FEAST_COOKED_FOODS, name)
end

local function FeastCookedFoods(inst, doer, manager)
    inst.components.madsciencelab:StartMakingScience("wintercooking_" .. PickOne(RANDOM_WINTERS_FEAST_COOKED_FOODS))
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-5)
    end
end

-- 砖砌烤炉做饭，互动为采集和烹饪
AddPrefabInit("wintersfeastoven", FeastCookedFoods, nil, function(inst)
    if not inst.components.madsciencelab or not inst.components.pickable then return false end

    if inst.components.pickable:CanBePicked() then
        -- 采集
        return true, 1
    end

    if not inst.components.madsciencelab:IsMakingScience() then
        -- 烹饪
        return true, 2
    end
    return false
end, function(inst, doer, manager, data)
    if data == 1 then
        inst.components.pickable:Pick(doer)
    elseif data == 2 then
        FeastCookedFoods(inst, doer, manager)
    end
end, TUNING.TOTAL_DAY_TIME)

local function PutFoodToTable(inst, doer, manager)
    local item = PickOne(RANDOM_WINTERS_FEAST_COOKED_FOODS, true)
    inst.components.inventory:GiveItem(item)
    inst.components.trader.onaccept(inst, doer, item)
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-5)
    end
end

-- 冬日盛宴餐桌上放点食物，互动为放置食物和享用食物
AddPrefabInit("table_winters_feast", PutFoodToTable, nil, function(inst)
    local item = inst.components.inventory:GetItemInSlot(1)
    return inst.components.wintersfeasttable ~= nil,
        item and item:IsValid() and item or nil
end, function(inst, doer, manager, item)
    if item then
        item.components.finiteuses:Use(19)
        inst.components.wintersfeasttable:DepleteFood()
        doer.components.inventory:GiveItem(SpawnPrefab("bananapop")) --吃个香蕉冻回点血
    else
        PutFoodToTable(inst, doer, manager)
    end
end)

local function RemoveLavaPond(inst)
    inst:RemoveComponent("propagator")
    inst.Physics:SetCollisionCallback(nil)
    AddWorkable(inst)
end

-- 岩浆池不会过热，不会让旁边的东西着火
AddPrefabInit("lava_pond", RemoveLavaPond, RemoveLavaPond)

-- 随机晾晒物
local RANDOM_DRYABLES = {
    "batwing", "batnose", "drumstick", "smallmeat", "froglegs", "fishmeat_small",
    "meat", "fishmeat", "eel",
    "monstermeat",
    "kelp"
}

local function SunCure(inst, doer, manager)
    local item = PickOne(RANDOM_DRYABLES, true)
    inst.components.dryer:StartDrying(item)
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-3)
    end

    AddWorkable(inst) --用于蟹奶奶岛的晾肉架
end

-- 晾肉架晾晒点东西，互动为晾晒
AddPrefabInit("meatrack", SunCure, AddWorkable, function(inst)
    if not inst.components.dryer or inst.components.dryer:IsDrying() then return false end
    return true, not inst.components.dryer.product and 1 or 2 --1为空的，2为晾晒完成
end, function(inst, doer, manager, data)
    if data == 1 then
        SunCure(inst, doer, manager)
    elseif data == 2 then
        inst.components.dryer:Harvest(doer)
    end
end, TUNING.TOTAL_DAY_TIME)

--伪造组件，防止采集时被蜜蜂仇恨
local fakeSkilltreeupdater = { IsActivated = Utils.TrueFn }

-- 晾肉架
PREFABS_INIT["meatrack_hermit"] = PREFABS_INIT["meatrack"]

-- 猪人采集蜂蜜不会被攻击（春天除外），互动为采集蜂蜜
-- 虽然这里写了代码，但是感觉不应该放在蓝图里，有人说春天和猪人一直打架很卡
AddPrefabInit("beebox", nil, nil, function(inst)
    if not inst.components.harvestable then return false end
    return inst.components.harvestable.produce == inst.components.harvestable.maxproduce
end, function(inst, doer)
    doer.components.skilltreeupdater = fakeSkilltreeupdater
    inst.components.harvestable:Harvest(doer)
    doer.components.skilltreeupdater = nil --猪人不可能有这个组件
end, TUNING.TOTAL_DAY_TIME)

-- 雪球发射器填充燃料，互动为添加燃料
AddPrefabInit("firesuppressor", nil, nil, function(inst)
    return inst.components.fueled and inst.components.fueled:GetPercent() < 0.5 or false
end, function(inst, doer, manager)
    inst.components.fueled:DoDelta(inst.components.fueled.maxfuel * 0.6, doer)
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-3)
    end
end, TUNING.TOTAL_DAY_TIME)

-- 随机树种
local RANDOM_TREE_SEEDS = { "acorn", "pinecone", "twiggy_nut", "palmcone_seed" }

-- 随机装饰，不过不含boss装饰
local function GetRandomWinterOrnament()
    local num = math.random(1, 4)
    if num == 1 then
        return GetRandomBasicWinterOrnament()
    elseif num == 2 then
        return GetRandomFancyWinterOrnament()
    elseif num == 3 then
        return GetRandomLightWinterOrnament()
    else
        return GetRandomFestivalEventWinterOrnament()
    end
end

local WINTER_TREE_MUST_TAGS = { "winter_tree" }
-- 盛宴树盆装饰一下，互动为添加装饰品，这个树盆比较特殊，生成圣诞树后就会直接移除树盆，我需要重新初始化生成的树
AddPrefabInit("winter_treestand", function(inst, doer, manager)
    local x, y, z = inst.Transform:GetWorldPosition()
    inst:PushEvent("plantwintertreeseed", { seed = PickOne(RANDOM_TREE_SEEDS, true), doer = doer })
    local ents = TheSim:FindEntities(x, y, z, 0.5, WINTER_TREE_MUST_TAGS)
    if #ents > 0 then
        local tree = ents[1]
        tree.components.ptribe_pigmanbuild:Init()
        FN.PrefabInit(tree, doer, manager)
    end
end)

-- 这里没有写在毁坏的时候修好
-- 石支柱直接建好
AddPrefabInit("support_pillar", function(inst, doer)
    local stone = SpawnPrefab("rocks")
    stone.components.stackable.stacksize = 40
    inst.components.constructionsite:OnConstruct(doer, { stone })
end)
-- 绝望石支柱直接建好
AddPrefabInit("support_pillar_dreadstone", function(inst, doer)
    local stone = SpawnPrefab("dreadstone")
    stone.components.stackable.stacksize = 40
    inst.components.constructionsite:OnConstruct(doer, { stone })
end)

local function OnPillarRemove(inst)
    local pos = inst:GetPosition()
    TheWorld:DoTaskInTime(0, function()
        local hole = GetPrefab.FindClosestEntity(pos, 4, true, nil, nil, nil, function(ent)
            return ent.prefab == "tentacle_pillar_hole"
        end)
        if hole then
            ErodeAway(hole)
        end
    end)
end

local function ForbidSpawnHole(inst)
    inst:ListenForEvent("onremove", OnPillarRemove)
end

-- 把大触手生成的洞删了
AddPrefabInit("tentacle_pillar", ForbidSpawnHole, ForbidSpawnHole)

AddPrefabInit("monkeyisland_portal", AddWorkable, AddWorkable)
AddPrefabInit("atrium_overgrowth", AddWorkable, AddWorkable)
AddPrefabInit("atrium_light", AddWorkable, AddWorkable)
AddPrefabInit("basalt_pillar", AddWorkable, AddWorkable)
AddPrefabInit("pond", AddWorkable, AddWorkable)
AddPrefabInit("pond_mos", AddWorkable, AddWorkable)
AddPrefabInit("pond_cave", AddWorkable, AddWorkable)

-- 暗夜灯添加燃料
AddPrefabInit("nightlight", nil, nil, function(inst)
    return inst.components.fueled
end, function(inst, doer, manager)
    inst.components.fueled:DoDelta(inst.components.fueled.maxfuel * 0.6, doer)
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-3)
    end
end, TUNING.TOTAL_DAY_TIME / 2)

local function IsAddDecorate(inst)
    return inst.components.container and inst.components.container.canbeopened and not inst.components.container:IsFull()
end

local function AddDecorate(inst, doer, manager)
    inst.components.container:GiveItem(SpawnPrefab(GetRandomWinterOrnament()))
    inst:PushEvent("updatelight")
    if manager then
        manager.components.ptribe_pigmanager:ResourceDoDelta(-2)
    end
end

local RANDOM_TREES = { "tree", "twiggytree", "deciduoustree", "palmconetree" }
for _, name in ipairs(RANDOM_TREES) do
    AddPrefabInit("winter_" .. name, nil, nil, IsAddDecorate, AddDecorate, TUNING.TOTAL_DAY_TIME)
end

local function CanBePicked(inst)
    return inst.components.pickable and inst.components.pickable:CanBePicked()
end

local function CanBePickedWithFertilize(inst)
    if not inst.components.pickable then return false end

    if inst.components.pickable:CanBeFertilized() then
        -- 施肥
        return true, 1
    end
    -- 采集
    return inst.components.pickable:CanBePicked(), 2
end

local function Pick(inst, doer)
    inst.components.pickable:Pick(doer)
end

local function PickWithFertilize(inst, doer, manager, data)
    if data == 1 then
        inst.components.pickable:Fertilize(SpawnPrefab("poop"), doer) --便便施肥
    else
        inst.components.pickable:Pick(doer)
    end
end

-- 采集
AddPrefabInit("sapling", nil, nil, CanBePickedWithFertilize, PickWithFertilize, TUNING.TOTAL_DAY_TIME)         --树枝
AddPrefabInit("grass", nil, nil, CanBePickedWithFertilize, PickWithFertilize, TUNING.TOTAL_DAY_TIME)           --草
AddPrefabInit("berrybush", nil, nil, CanBePickedWithFertilize, PickWithFertilize, TUNING.TOTAL_DAY_TIME)       --浆果丛
AddPrefabInit("berrybush2", nil, nil, CanBePickedWithFertilize, PickWithFertilize, TUNING.TOTAL_DAY_TIME)      --浆果丛
AddPrefabInit("berrybush_juicy", nil, nil, CanBePickedWithFertilize, PickWithFertilize, TUNING.TOTAL_DAY_TIME) --多汁浆果丛
AddPrefabInit("reeds", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                                     --芦苇
AddPrefabInit("marsh_bush", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                                --尖刺灌木
AddPrefabInit("cactus", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                                    --仙人掌
AddPrefabInit("oasis_cactus", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                              --仙人掌
AddPrefabInit("blue_mushroom", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                             --蓝蘑菇
AddPrefabInit("green_mushroom", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                            --绿蘑菇
AddPrefabInit("red_mushroom", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                              --红蘑菇
AddPrefabInit("flower_cave", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                               --荧光草
AddPrefabInit("lichen", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                                    --洞穴苔藓
AddPrefabInit("wormlight_plant", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                           --神秘植物
AddPrefabInit("cave_banana_tree", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                          --洞穴香蕉树
AddPrefabInit("weed_firenettle", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                           --火荨麻
AddPrefabInit("mandrake_planted", nil, nil, CanBePicked, Pick, TUNING.TOTAL_DAY_TIME)                          --曼德拉草
-- 空芯树桩，可以偷狸猫的东西，不过思来想去还是决定当个好人
-- catcoonden

local function OnBeDestroyRemove(inst)
    inst.components.health.nofadeout = false
end

-- 墙体被锤毁时直接消失
AddPrefabInit("wall_ruins", OnBeDestroyRemove, OnBeDestroyRemove)
AddPrefabInit("wall_moonrock", OnBeDestroyRemove, OnBeDestroyRemove)
AddPrefabInit("wall_ruins", OnBeDestroyRemove, OnBeDestroyRemove)
AddPrefabInit("wall_hay", OnBeDestroyRemove, OnBeDestroyRemove)
AddPrefabInit("wall_wood", OnBeDestroyRemove, OnBeDestroyRemove)
AddPrefabInit("wall_stone", OnBeDestroyRemove, OnBeDestroyRemove)
AddPrefabInit("wall_scrap", OnBeDestroyRemove, OnBeDestroyRemove)
AddPrefabInit("wall_moonrock", OnBeDestroyRemove, OnBeDestroyRemove)
AddPrefabInit("wall_dreadstone", OnBeDestroyRemove, OnBeDestroyRemove)

local function CanPlantOrHarvest(inst)
    return inst.components.growable ~= nil
end

-- 直接催熟、收获
local function PlantOrHarvest(inst, doer)
    inst.force_oversized = inst.force_oversized or (math.random() < 0.2) --强制巨大化，加点趣味性

    if inst.components.pickable and inst.components.pickable:CanBePicked() then
        local status, loot = inst.components.pickable:Pick(doer)
        -- 这里不直接敲除，把巨大作物记入猪人的回收名单里，让猪人来捡
        -- if status then
        --     for _, v in ipairs(loot) do
        --         if v.components and v.components.workable then --巨大作物
        --             v:DoTaskInTime(1, function()
        --                 if v.components.workable then
        --                     v.components.workable:Destroy(inst)
        --                 end
        --             end)
        --         end
        --     end
        -- end
    else
        inst.components.growable:DoGrowth()
    end
end

local PLANT_DEFS = require("prefabs/farm_plant_defs").PLANT_DEFS
for _, data in pairs(PLANT_DEFS) do
    AddPrefabInit(data.prefab, nil, nil, CanPlantOrHarvest, PlantOrHarvest, TUNING.TOTAL_DAY_TIME) --作物
end

-- 这个是普通种子，在第一次成长后就会移除并生成随机种子，可以在onFinish函数中根据坐标拿到新生成的种子
AddPrefabInit("farm_plant_randomseed", nil, nil, CanPlantOrHarvest, PlantOrHarvest, TUNING.TOTAL_DAY_TIME) --刚种下去的种子

----------------------------------------------------------------------------------------------------
--资源换算表，换算单位为多少个木头，没定义的就返回1
local RESOURCE_CONVERT = {
    spoiled_food = 0,  --腐烂物
    acorn_sapling = 0, --树苗
    poop = 0,          --大便
    ash = 0,           --灰烬

    -- 货币
    oinc = 1,
    oinc10 = 10,
    oinc100 = 100,
    oinc1000 = 1000,

    -- 道具
    ptribe_upgrade = 10, --通用升级道具

    -- 疯狂科学药剂
    halloweenpotion_bravery_small = 15,
    halloweenpotion_bravery_large = 20,
    halloweenpotion_health_small = 15,
    halloweenpotion_health_large = 20,
    halloweenpotion_sanity_small = 15,
    halloweenpotion_sanity_large = 20,
    halloweenpotion_embers = 15,
    halloweenpotion_sparks = 15,
    halloweenpotion_moon = 20,
    livingtree_root = 15,

    waterdrop = 50, --魔力泉水
    -- 冬季盛宴食物
    berrysauce = 8,
    bibingka = 8,
    cabbagerolls = 8,
    festivefish = 8,
    gravy = 8,
    latkes = 8,
    lutefisk = 8,
    mulleddrink = 8,
    panettone = 8,
    pavlova = 8,
    pickledherring = 8,
    polishcookie = 8,
    pumpkinpie = 8,
    roastturkey = 8,
    stuffing = 8,
    sweetpotato = 8,
    tamales = 8,
    tourtiere = 8,

    -- 植物
    berrybush = 4,         --灌木丛
    berrybush2 = 4,        --灌木丛
    berrybush_juicy = 4,   --多汁浆果从
    blue_mushroom = 6,     --蓝蘑菇
    green_mushroom = 6,    --绿蘑菇
    red_mushroom = 6,      --红蘑菇
    cactus = 10,           --球形仙人掌
    oasis_cactus = 10,     --仙人掌
    monkeytail = 10,       --猴尾草
    bananabush = 10,       --香蕉丛
    --  洞穴植物
    flower_cave = 12,      --荧光草
    wormlight_plant = 16,  --神秘植物（小发光浆果）
    lichen = 8,            --洞穴苔藓
    cave_banana_tree = 10, --洞穴香蕉树

    -- 动物
    catcoonden = 8,   --空芯树桩
    pigtorch = 20,    --猪火炬
    walrus_camp = 60, --海象巢穴

    -- 建筑
    treasurechest = 3,                --木箱
    lightning_rod = 5,                --避雷针
    firesuppressor = 10,              --雪球发射器
    saltlick = 6,                     --动物舔盐器
    firepit = 4,                      --火坑
    pighouse = 10,                    --猪人房
    rabbithouse = 18,                 --兔人房
    wobster_den = 20,                 --龙虾窝
    moonglass_wobster_den = 25,       --月光玻璃窝
    dragonflychest = 20,              --龙鳞宝箱
    dragonflyfurnace = 12,            --龙鳞火炉
    eyeturret = 40,                   --眼球炮塔，这个不应该放在蓝图里，因为眼球塔是忠诚玩家的，玩家打猪人反而还会帮玩家
    mushroom_light = 40,              --蘑菇灯
    mushroom_light2 = 40,             --菌伞灯
    tent = 6,                         --帐篷
    siestahut = 8,                    --遮阳棚
    slow_farmplot = 6,                --基础农场
    fast_farmplot = 8,                --改良农场
    mushroom_farm = 7,                --蘑菇农场
    beebox = 10,                      --蜂箱，这玩意会打猪人
    meatrack = 3,                     --晾肉架
    cookpot = 6,                      --烹饪锅
    icebox = 8,                       --冰箱
    saltbox = 14,                     --盐盒
    researchlab = 4,                  --科学机器
    researchlab2 = 8,                 --炼金引擎
    cartographydesk = 6,              --制图者桌
    sculptingtable = 5,               --制陶者轮
    winterometer = 4,                 --温度计
    rainometer = 5,                   --雨量计
    wardrobe = 5,                     --衣柜
    birdcage = 10,                    --鸟笼
    scarecrow = 6,                    --友好的稻草人
    researchlab4 = 14,                --灵子分解器
    researchlab3 = 16,                --暗影操纵器
    moondial = 7,                     --月晷
    trophyscale_oversizedveggies = 5, --农产品秤
    support_pillar_dreadstone = 300,  --绝望石支柱
    oceantree_pillar = 300,           --水中木
    nightlight = 16,                  --暗影灯

    -- 冬季盛宴
    madscience_lab = 5,              --疯狂科学家实验室
    winter_treestand = 3,            --盛宴树盆
    wintersfeastoven = 3,            --砖砌烤炉
    table_winters_feast = 2,         --冬季盛宴餐桌
    -- 鸦年活动
    carnival_prizebooth = 20,        --奖品摊位
    ptribe_carnival_prizebooth = 20, --猪人摊位
    -- 装饰
    wood_table_round = 3,            --木圆桌
    wood_table_square = 3,           --木方桌
    endtable = 6,                    --茶几
    decor_lamp = 5,                  --餐桌灯
    decor_portraitframe = 6,         --愉快画像
    phonograph = 8,                  --留声机
    decor_centerpiece = 2,           --艺术？
    decor_flowervase = 1,            --餐桌花瓶
    decor_pictureframe = 1,          --空画框

    -- 其他
    lava_pond = 20,               --岩浆池
    gingerbreadhouse = 5,         --姜饼猪屋
    basalt = 10,                  --玄武岩
    sanityrock = 10,              --方尖碑
    grotto_pool_small = 15,       --小玻璃绿洲
    grotto_pool_big = 30,         --玻璃绿洲
    grotto_waterfall_small1 = 10, --小玻璃绿洲，没有碰撞
    grotto_waterfall_small2 = 10, --小玻璃绿洲，没有碰撞
    hotspring = 20,               --温泉
    atrium_overgrowth = 50,       --远古方尖碑
    atrium_light = 10,            --远古灯座

    -- 书籍
    book_research_station = 15 --万物百科
}

-- 武器，武器工厂使用
FN.WEAPONS = {
    -- 1
    axe = 2,              --斧头
    goldenaxe = 5,        --黄金斧头
    hammer = 10,          --锤子
    spear_wathgrithr = 6, --战斗长矛
    spear = 4,            --长矛
    halberd = 5,          --战戟

    -- 2
    whip = 14,           --三尾猫鞭
    hambat = 8,          --火腿棒
    boomerang = 4,       --回旋镖
    brush = 16,          -- 刷子
    bugnet = 9,          --捕虫网
    cutless = 6,         --木头短剑
    farm_hoe = 4,        --园艺锄
    golden_farm_hoe = 6, --黄金园艺锄
    fence_rotator = 8,   --栅栏击剑
    fishingrod = 6,      --淡水钓竿

    -- 3
    batbat = 21,        --蝙蝠棒
    tentaclespike = 10, --触手尖刺
    firestaff = 16,     --火魔杖
    icestaff = 10,      --冰魔杖
    bullkelp_root = 16, --公牛海带茎
    moonglassaxe = 16,  --月光玻璃斧

    -- 4
    nightsword = 24,                 --暗夜剑
    wathgrithr_shield = 10,          --战斗圆盾
    glasscutter = 31,                --玻璃刀
    spear_wathgrithr_lightning = 18, --奔雷矛
    pocketwatch_weapon = 30,         --警钟
    waterplant_bomb = 15,            --种壳
    cane = 22,                       --步行手杖
    firepen = 10,                    --火焰笔

    -- 5
    blowdart_pipe = 10,                      --吹箭
    multitool_axe_pickaxe = 20,              --多用斧镐
    nightstick = 16,                         --晨星锤
    ruins_bat = 44,                          --铥矿棒
    sword_lunarplant = 47,                   --亮茄剑
    voidcloth_scythe = 32,                   --暗影收割者
    spear_wathgrithr_lightning_charged = 40, --充能奔雷矛
    houndstooth_blowpipe = 60,               --嚎弹炮
    staff_lunarplant = 54,                   --亮茄魔杖
    gunpowder = 10,                          --火药
    cannonball_rock_item = 12,               --炮弹
    bomb_lunarplant = 25,                    --亮茄炸弹


    shieldofterror = 60, --恐怖盾牌
    trident = 50         --刺耳三叉戟
}
for name, val in pairs(FN.WEAPONS) do
    RESOURCE_CONVERT[name] = val
end

--物品，也是商店卖的
local ITEM_CONVERT = {
    -- 食物，这里只是简单列举几个，根据食物回复三维也不错
    berries = 1,            --浆果
    bird_egg = 2,           --鸟蛋
    cactus_flower = 3,      --仙人掌花
    goatmilk = 6,           --电羊奶
    royal_jelly = 10,       --蜂王浆
    jellybean = 12,         --彩虹糖豆
    butter = 10,            --黄油
    mandrake = 20,          --曼德拉草
    voltgoatjelly = 12,     --伏特羊角冻
    mandrakesoup = 15,      --曼德拉草汤
    perogies = 4,           --饺子
    meat_dried = 2,         --肉干
    ice = 3,                --冰块
    glommerfuel = 5,        --格罗姆粘液
    deerclops_eyeball = 20, --巨鹿眼球
    minotaurhorn = 30,      --守护者之角
    wormlight_lesser = 5,   --小发光浆果
    wormlight = 10,         --发光浆果
    fish = 4,               --鱼
    lightbulb = 3,          --荧光果
    icecream = 6,           --冰淇淋
    glowberrymousse = 15,   --发光浆果慕斯
    tallbirdegg = 4,        --高脚鸟蛋
    dragonpie = 6,          --火龙果派
    blue_cap = 3,           --蓝蘑菇
    red_cap = 3,            --红蘑菇
    green_cap = 3,          --绿蘑菇
    moon_cap = 5,           --月亮蘑菇

    -- 物品
    dug_berrybush = 3,             --浆果丛
    dug_berrybush2 = 3,            --浆果丛
    dug_berrybush_juicy = 4,       --多汁浆果丛
    dug_monkeytail = 6,            --猴尾草
    dug_bananabush = 6,            --香蕉丛
    pig_coin = 6,                  --铸币
    lunarplant_husk = 6,           --亮茄外壳
    alterguardianhatshard = 30,    --启迪碎片
    walrus_tusk = 16,              --海象牙
    lightninggoathorn = 8,         --电羊角
    fossil_piece = 6,              --化石碎片
    townportaltalisman = 5,        --沙之石
    marble = 3,                    --大理石
    saltrock = 3,                  --盐晶
    moonrocknugget = 4,            --月岩
    cutreeds = 2,                  --芦苇
    boneshard = 3,                 --骨头碎片
    houndstooth = 4,               --犬牙
    stinger = 2,                   --蜂刺
    feather_robin_winter = 3,      --蓝色羽毛
    feather_robin = 2,             --红色羽毛
    feather_crow = 2,              --黑色羽毛
    feather_canary = 4,            --金色羽毛
    pigskin = 2,                   --猪皮
    beefalowool = 2,               --牛毛
    horn = 4,                      --牛角
    guano = 2,                     --鸟粪
    coontail = 3,                  --猫尾巴
    lureplantbulb = 6,             --食人花种子
    silk = 2,                      --蜘蛛网
    spidergland = 2,               --蜘蛛腺体
    gears = 6,                     --齿轮
    tentaclespots = 6,             --触手皮
    nightmarefuel = 4,             --噩梦燃料
    moonglass = 5,                 --月亮碎片
    livinglog = 4,                 --活木
    phlegm = 6,                    --浓鼻涕
    steelwool = 5,                 --钢丝绵
    redgem = 3,                    --红宝石
    bluegem = 3,                   --蓝宝石
    purplegem = 4,                 --紫宝石
    orangegem = 5,                 --橙宝石
    greengem = 5,                  --绿宝石
    yellowgem = 5,                 --黄宝石
    manrabbit_tail = 2,            --兔毛
    slurper_pelt = 5,              --啜食者皮
    goose_feather = 4,             --麋鹿鹅羽毛
    dragon_scales = 8,             --鳞片
    bearger_fur = 8,               --熊皮
    furtuft = 4,                   --毛丛
    shroom_skin = 8,               --蘑菇皮
    voidcloth = 8,                 --暗影碎布
    dreadstone = 10,               --绝望石
    shadowheart = 30,              --暗影心房
    boatpatch = 4,                 --船补丁
    purebrilliance = 6,            --纯粹辉煌
    horrorfuel = 6,                --纯粹恐惧
    soil_amender = 4,              --催长剂起子
    turf_shellbeach = 2,           --贝壳海滩地皮
    cookiecuttershell = 3,         --饼干切割机壳
    opalpreciousgem = 25,          --彩虹宝石
    thulecite = 4,                 --铥矿
    rock_avocado_fruit_sprout = 5, --发芽的石果
    waterplant_planter = 10,       --海芽插穗
    dug_trap_starfish = 10,        --海星陷阱
    gnarwail_horn = 10,            --一角鲸的角
    leif_idol = 5,                 --树精守卫雕像
    ptribe_jungletreeseed = 5,     --丛林树种子
    ptribe_telescope = 20,         --望远镜
    ptribe_supertelescope = 30,    --超级望远镜

    -- 装备
    hambat = 5,                 --火腿棒
    perdfan = 5,                --幸运扇
    houndwhistle = 5,           --幸运哨子
    cane = 17,                  --步行手杖
    multitool_axe_pickaxe = 10, --多用斧镐
    minerhat = 6,               --矿工帽
    raincoat = 10,              --雨衣
    eyebrellahat = 18,          --眼球伞
    armor_sanity = 18,          --暗夜甲
    nightsword = 16,            --暗夜剑
    voidcloth_umbrella = 30,    --暗影伞
    voidcloth_scythe = 35,      --暗影收割者
    bootleg = 10,               --出逃腿靴
    telestaff = 22,             --传送魔杖
    monkey_mediumhat = 13,      --船长的三角帽
    blowdart_pipe = 7,          --吹箭
    blowdart_fire = 5,          --火焰吹箭
    blowdart_sleep = 6,         --催眠吹箭
    blowdart_yellow = 7,        --雷电吹箭
    trident = 40,               --刺耳三叉戟
    slurtlehat = 10,            --背壳头盔
    walrushat = 12,             --贝雷帽
    cookiecutterhat = 12,       --饼干切割机帽子
    icehat = 22,                --冰帽
    icestaff = 5,               --冰魔杖
    greenstaff = 24,            --拆解魔杖
    nightstick = 20,            --晨星锤
    tentaclespike = 5,          --触手尖刺
    armormarble = 18,           --大理石甲
    fishingrod = 6,             --淡水钓竿
    tacklecontainer = 8,        --钓具箱
    supertacklecontainer = 50,  --超级钓具箱
    ruins_bat = 28,             --铥矿棒
    ruinshat = 24,              --铥矿皇冠
    hivehat = 30,               --蜂王冠
    armorskeleton = 50,         --骨头盔甲
    yellowstaff = 24,           --唤星者魔杖
    opalstaff = 50,             --唤月者魔杖
    orangeamulet = 15,          --懒人护符
    yellowamulet = 15,          --魔光护符
    greenamulet = 20,           --建造护符
}

for name, val in pairs(ITEM_CONVERT) do
    RESOURCE_CONVERT[name] = val
end

-- setmetatable(RESOURCE_CONVERT, {
--     __index = function() return 1 end
-- })

FN.MAX_NEED = 0
for _, v in pairs(RESOURCE_CONVERT) do
    FN.MAX_NEED = math.max(FN.MAX_NEED, v)
end

local function GR(prefab)
    local val = RESOURCE_CONVERT[prefab]
    if val then
        return val
    end

    val = UNDEFINED_RESOURCE[prefab]
    if val then
        return val
    end

    return 1
end

---获取预制体的价值，如果已经定义则返回定义值，否则返回1
---@param prefabs string|table 预制体名、预制体名列表
function FN.GetResource(prefabs)
    local num = 0

    if type(prefabs) == "table" then
        for _, p in ipairs(prefabs) do
            num = num + GR(p)
        end
    else
        num = GR(prefabs)
    end

    return num
end

----------------------------------------------------------------------------------------------------

--- 捡的
local GARBAGES = {
    poop                   = true, --便便不捡会越来越多
    charcoal               = true, --木炭
    log                    = true, --木头
    twigs                  = true, --树枝
    cutgrass               = true, --草
    goldnugget             = true, --金子
    rocks                  = true, --石头
    flint                  = true, --燧石
    boards                 = true, --木板
    cutstone               = true, --石砖

    -- 实验药剂，不能喝的捡起来
    halloweenpotion_embers = true,
    halloweenpotion_sparks = true,
    halloweenpotion_moon   = true,
    livingtree_root        = true,

    -- 非自然传送门
    dug_bananabush         = true, --香蕉丛
    dug_monkeytail         = true, --猴尾草
    palmcone_seed          = true, --棕榈松果树芽
    cursed_monkey_token    = true, --诅咒饰品，让猪人把这东西也捡走
    rock_avocado_fruit     = true, --石果
}

--冬季盛宴的食物吃不了
for _, v in ipairs(RANDOM_WINTERS_FEAST_COOKED_FOODS) do
    GARBAGES[v] = true
end

--- 猪人是否回收该物品
function FN.IsPickUpGarbage(inst)
    -- 回收巨大作物
    -- if inst:HasTag("weighable_OVERSIZEDVEGGIES") then
    --     return true
    -- end

    return GARBAGES[inst.prefab]
end

function FN.PrefabInit(inst, doer, manager)
    local fns = PREFABS_INIT[inst.prefab]
    if fns and fns.init then
        fns.init(inst, doer, manager)
    end
end

function FN.PrefabLoad(inst)
    local fns = PREFABS_INIT[inst.prefab]
    if fns and fns.load then
        fns.load(inst)
    end
end

function FN.PrefabIsInteractive(inst)
    local fns = PREFABS_INIT[inst.prefab]
    return fns and fns.isInteractive and fns.isInteractive(inst) or false
end

function FN.PrefabInteract(inst, doer, manager)
    local fns = PREFABS_INIT[inst.prefab]
    if fns and fns.interact then
        local status, data = true, nil
        if fns.isInteractive then
            status, data = fns.isInteractive(inst)
        end
        if status then
            fns.interact(inst, doer, manager, data)
        end
    end
end

--- 获取预制体的交互cd，默认45秒
function FN.GetInteractCd(name)
    local fns = PREFABS_INIT[name]
    if fns then
        return fns.interactCd or 45
    end
end

----------------------------------------------------------------------------------------------------
---获取所有商品
local RANDOM_GOODS = {}
-- 蔬菜，但是蔬菜没有贴图，这里就不加了
-- for _, name in ipairs(RANDOM_VEGGIES) do
--     GOODS[name] = 2
-- end

-- 烹饪的食物
for _, name in ipairs(RANDOM_FOODS) do
    RANDOM_GOODS[name] = 4
end

-- 自定义
for name, val in pairs(ITEM_CONVERT) do
    RANDOM_GOODS[name] = val
end
ITEM_CONVERT = nil

-- 随机蓝图
FN.RANDOM_BLUEPRINTS = {
    playerhouse_city_blueprint = 100,
    city_lamp_blueprint = 50,
    hedge_block_item_blueprint = 10,
    hedge_cone_item_blueprint = 10,
    hedge_layered_item_blueprint = 10,
    lawnornament_1_blueprint = 30,
    lawnornament_2_blueprint = 30,
    lawnornament_3_blueprint = 30,
    lawnornament_4_blueprint = 30,
    lawnornament_5_blueprint = 30,
    lawnornament_6_blueprint = 30,
    lawnornament_7_blueprint = 30,
    pugalisk_fountain_blueprint = 100,
    ptribe_pigweaponfactory_blueprint = 30
}

-- 一定包含的商品
local MUST_GOODS = {
    deed = 50,
    ptribe_carnival_prizebooth_kit = 50,
}

----------------------------------------------------------------------------------------------------
--- 猪人交税
local RANDOM_MATERIALS = {
    twigs = 1,                   --树枝
    flint = 1,                   --燧石
    nitre = 0.5,                 --硝石
    rocks = 1,                   --岩石
    cutgrass = 1,                --草
    log = 1,                     --木头
    cutreeds = 0.6,              --芦苇
    silk = 0.5,                  --蛛丝
    halloweencandy_11 = 1,       --巧克力猪
    seeds = 1,                   --种子
    acorn_cooked = 1,            --烤桦树果
    thulecite_pieces = 0.1,      --铥矿碎片
    goatmilk = 0.1,              --带电的羊奶
    trinket_4 = 0.3,             --玩偶
    yotc_seedpacket = 0.1,       --种子包
    yotc_seedpacket_rare = 0.05, --高级种子包
    -- 礼物需要处理
    redpouch_yotp = 1,           --呼噜币
    gift = 0.01,                 --礼物
}

-- 把礼物记入垃圾回收列表中，防止堆积过多
for key, _ in pairs(RANDOM_MATERIALS) do
    GARBAGES[key] = true
end

local RANDOM_GIFTS = {
    "dragon_scales",    --鳞片
    "mandrake",         --曼德拉草
    "lightninggoathorn" --电羊角
}

--- 初始化给予猪猪公主的礼物
function FN.InitPigmanGiveMaterials(pig, note, player)
    local prefab = pig._pig_token_prefab
    if note.components.unwrappable then
        -- 礼物需要包装一下
        if prefab == "redpouch_yotp" then
            note.components.unwrappable:WrapItems({ "oinc" })
        elseif prefab == "gift" then
            note.components.unwrappable:WrapItems({ RANDOM_GIFTS[math.random(1, #RANDOM_GIFTS)] })
        end
    elseif note.components.edible and player.components.hunger.current < 10 then
        note.ptribe_pigForbidEat = true --禁止猪人吃掉，该字段不保存
    end
end

--- 获取给予猪猪公主的随机礼物
function FN.GetRandomGiveMaterials()
    return weighted_random_choice(RANDOM_MATERIALS)
end

--- 猪人给予的随机料理
function FN.GetRandomGiveFood()
    return RANDOM_FOODS[math.random(1, #RANDOM_FOODS)]
end

----------------------------------------------------------------------------------------------------
function FN.GetAllGoods()
    return MergeMaps(RANDOM_GOODS, FN.RANDOM_BLUEPRINTS, MUST_GOODS)
end

function FN.IsGoods(name)
    return name and (RANDOM_GOODS[name] ~= nil or FN.RANDOM_BLUEPRINTS[name] ~= nil or MUST_GOODS[name] ~= nil)
end

function FN.SelectGoods(rate)
    local count    = 7 + math.ceil(rate * 14) -- 每次7~21
    local newGoods = {}

    -- 一定含有的
    for name, _ in pairs(MUST_GOODS) do
        table.insert(newGoods, name)
    end

    -- 随机商品
    for _, name in ipairs(PickSomeWithDups(count, table.getkeys(RANDOM_GOODS))) do
        table.insert(newGoods, name)
    end

    -- 随机蓝图
    for _, name in ipairs(PickSomeWithDups(math.random(2, 3), table.getkeys(FN.RANDOM_BLUEPRINTS))) do
        table.insert(newGoods, name)
    end

    return newGoods
end

return FN
