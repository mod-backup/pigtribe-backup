local PigTasks = require("ptribe_pigtasks")
local GetPrefab = require("ptribe_utils/getprefab")

---初始化雕像
-- local function Init(self, statuemaxwell)
-- end

local function OnInit(self, isLoading)
    return PigTasks.DefaultInit(self, isLoading, nil, math.random(300, 400))
end

local function GetTargetPos(self)
    if self.taskData.isSpawned then
        for i = 1, self.taskData.monsterCount do
            local monster = self.pig.components.entitytracker:GetEntity(
                "ptribe_5TheShadowOfThePigVillage_monster" .. tostring(i))
            if monster then
                return monster:GetPosition()
            end
        end

        local statuemaxwell = self.pig.components.entitytracker:GetEntity(
            "ptribe_5TheShadowOfThePigVillage_statuemaxwell")
        if statuemaxwell then
            return statuemaxwell:GetPosition()
        end

        return self.pig:GetPosition()
    else
        return self.taskData.pos
    end
end

local RANDOM_MONSTERS = { "knight", "rook", "bishop" }

local RANDOM_CHESSPIECE = {
    "chesspiece_rook_marble",
    "chesspiece_knight_marble",
    "chesspiece_bishop_marble",
    "chesspiece_muse_marble",
    "chesspiece_formal_marble",
    "statueharp",
}

local RANDOM_TREASURES = {
    thulecite_pieces = 8,
    ruinshat = 0.8,
    armorruins = 0.8,
    ruins_bat = 0.8,
    gears = 3,
    nightmarefuel = 6,
    thulecite = 5,
}

local RANDOM_TILES = {
    WORLD_TILES.TILES, WORLD_TILES.ARCHIVE, WORLD_TILES.MOSAIC_GREY, WORLD_TILES.MONKEY_DOCK, WORLD_TILES.TRIM,
    WORLD_TILES.COTL_GOLD, WORLD_TILES.BEARD_RUG, WORLD_TILES.TILES_GLOW, WORLD_TILES.MOSAIC_BLUE, WORLD_TILES.CARPET,
    WORLD_TILES.MOSAIC_RED, WORLD_TILES.CARPET2,
}

local SPAWN_DIS_SQ = 46 * 46
local function OnUpdate(self)
    if self.taskData.isSpawned then return end
    local pos

    if self.inst:GetPosition():DistSq(self.taskData.pos) < SPAWN_DIS_SQ then
        if TUNING.TRIBE_ALLOW_TASK_GENERATE_TILE then
            --地皮
            GetPrefab.SpawnRoundTile(self.taskData.pos, 4, RANDOM_TILES[math.random(1, #RANDOM_TILES)], false, 0.5)
        end

        --麦斯威尔雕像
        local statuemaxwell = SpawnAt("statuemaxwell", self.taskData.pos)
        self.pig.components.entitytracker:TrackEntity("ptribe_5TheShadowOfThePigVillage_statuemaxwell", statuemaxwell)

        --宝物
        pos = PigTasks.SpawnPos(self.taskData.pos, math.random(2, 4))
        if pos then
            local box = SpawnAt("terrariumchest", pos)
            for p, c in pairs(RANDOM_TREASURES) do
                local count = math.floor(math.random() * c + 0.5)

                if count > 0 then
                    local item = SpawnPrefab(p)
                    if item.components.stackable then --自己写的表，就不那么严格了
                        item.components.stackable.stacksize = count
                    end
                    box.components.container:GiveItem(item)
                end
            end
        end

        --怪物
        self.taskData.monsterCount = math.random(2, 4) + TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 2
        for i = 1, self.taskData.monsterCount do
            pos = PigTasks.SpawnPos(self.taskData.pos, math.random(4, 16))
            if pos then
                local monster = SpawnAt(RANDOM_MONSTERS[math.random(1, #RANDOM_MONSTERS)], pos)
                self.pig.components.entitytracker:TrackEntity("ptribe_5TheShadowOfThePigVillage_monster" .. tostring(i),
                    monster)
            end
        end

        --雕像
        for i = 1, math.random(4, 6) do
            pos = PigTasks.SpawnPos(self.taskData.pos, math.random(4, 14))
            if pos then
                SpawnAt(RANDOM_CHESSPIECE[math.random(1, #RANDOM_CHESSPIECE)], pos)
            end
        end
        --邪恶花
        for i = 1, math.random(3, 6) do
            pos = PigTasks.SpawnPos(self.taskData.pos, math.random(4, 14))
            if pos then
                SpawnAt("flower_evil", pos)
            end
        end

        self.taskData.isSpawned = true
    end
end

local function FinishTask(self)
    if not self.taskData.isSpawned then
        return false, STRINGS.PTRIBE_PIG_TASK.THE_SHADOW_OF_THE_PIG_VILLAGE.FAIL.VILLAGE_EXIST
    end

    for i = 1, self.taskData.monsterCount do
        local monster = self.pig.components.entitytracker:GetEntity(
            "ptribe_5TheShadowOfThePigVillage_monster" .. tostring(i))
        if monster then
            return false, STRINGS.PTRIBE_PIG_TASK.THE_SHADOW_OF_THE_PIG_VILLAGE.FAIL.TARGETS_EXIST
        end
    end

    local statuemaxwell = self.pig.components.entitytracker:GetEntity("ptribe_5TheShadowOfThePigVillage_statuemaxwell")
    if statuemaxwell then
        return false, STRINGS.PTRIBE_PIG_TASK.THE_SHADOW_OF_THE_PIG_VILLAGE.FAIL.STATUE_EXIST
    end

    return true
end

local function OnEnd(self)
    if self.taskData.isSpawned then
        for i = 1, self.taskData.monsterCount do
            self.pig.components.entitytracker:ForgetEntity("ptribe_5TheShadowOfThePigVillage_monster" .. tostring(i))
        end

        self.pig.components.entitytracker:ForgetEntity("ptribe_5TheShadowOfThePigVillage_statuemaxwell")
    end
end

PigTasks.AddTask(5,
    STRINGS.PTRIBE_PIG_TASK.THE_SHADOW_OF_THE_PIG_VILLAGE.NAME,
    STRINGS.PTRIBE_PIG_TASK.THE_SHADOW_OF_THE_PIG_VILLAGE.DESC,
    STRINGS.PTRIBE_PIG_TASK.THE_SHADOW_OF_THE_PIG_VILLAGE.COND,
    { oinc = 18, leif_idol = 5 },
    {
        time = TUNING.TOTAL_DAY_TIME * 4,
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
