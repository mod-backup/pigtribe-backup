local PigTasks = require("ptribe_pigtasks")

local function FinishTask(self)
    local item = self.inst.components.inventory:FindItem(function(ent)
        return ent.prefab == "decor_pictureframe"
            and ent.components.drawable and ent.components.drawable:GetImage() == "meat"
    end)
    if item then
        item:Remove()
        return true
    else
        return false, STRINGS.PTRIBE_PIG_TASK.PIGMAN_LIKES_TO_PICTURE.FAIL.NO_ITEM
    end
end

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_LIKES_TO_PICTURE.NAME,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_LIKES_TO_PICTURE.DESC,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_LIKES_TO_PICTURE.COND,
    { oinc = 3 },
    {
        FinishTask = FinishTask
    }
)
