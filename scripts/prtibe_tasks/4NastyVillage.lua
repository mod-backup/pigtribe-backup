local PigTasks = require("ptribe_pigtasks")

local function OnInit(self, isLoading)
    if isLoading then return true end

    local home = self.pig.components.homeseeker and self.pig.components.homeseeker:GetHome()
    local manager = home and home.components.ptribe_pigmanager and home.components.ptribe_pigmanager:GetManager()

    if not manager or not manager:IsValid() or not manager.components.ptribe_pigmanager:IsValidManager() then
        return false, STRINGS.PTRIBE_PIG_TASK.NASTY_VILLAGE.FAIL.NOT_TRIBE
    end

    local ents = {}
    for _, ent in pairs(Ents) do
        if ent.components.ptribe_pigmanager and ent.components.ptribe_pigmanager:IsValidManager() and ent ~= manager then
            table.insert(ents, ent)
        end
    end

    if #ents <= 0 then
        return false, STRINGS.PTRIBE_PIG_TASK.NASTY_VILLAGE.FAIL.NOT_TARGET
    end

    self.pig.components.entitytracker:TrackEntity("ptribe_4NastyVillage_target", ents[math.random(1, #ents)])

    return true
end

local function GetTargetPos(self)
    local target = self.pig.components.entitytracker:GetEntity("ptribe_4NastyVillage_target")
    if target then
        return target:GetPosition()
    else
        return self.pig:GetPosition()
    end
end

local function FinishTask(self)
    local target = self.pig.components.entitytracker:GetEntity("ptribe_4NastyVillage_target")
    if target then
        return false, STRINGS.PTRIBE_PIG_TASK.NASTY_VILLAGE.FAIL.TARGET_EXIST
    else
        return true
    end
end

local function OnEnd(self)
    self.pig.components.entitytracker:ForgetEntity("ptribe_4NastyVillage_target")
end

PigTasks.AddTask(4,
    STRINGS.PTRIBE_PIG_TASK.NASTY_VILLAGE.NAME,
    STRINGS.PTRIBE_PIG_TASK.NASTY_VILLAGE.DESC,
    STRINGS.PTRIBE_PIG_TASK.NASTY_VILLAGE.COND,
    { oinc = 10, greenamulet = 1 },
    {
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
