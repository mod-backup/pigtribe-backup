local PigTasks = require("ptribe_pigtasks")

local FENCE_MUST_TAGS = { "fence" }
local function FinishTask(self)
    local home = self.pig.components.homeseeker and self.pig.components.homeseeker:GetHome()
    if home then
        local x, y, z = home.Transform:GetWorldPosition()
        if #TheSim:FindEntities(x, y, z, 20, FENCE_MUST_TAGS) >= 10 then
            return true
        else
            return false, STRINGS.PTRIBE_PIG_TASK.THE_INSECURE_PIGMAN.FAIL.LACK
        end
    else
        self.pig:DoTaskInTime(0, function()
            self.pig.components.ptribe_pigtask.taskId:set(0)
        end)
        return false, STRINGS.PTRIBE_PIG_TASK.THE_INSECURE_PIGMAN.FAIL.HOMELESS
    end
end

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.THE_INSECURE_PIGMAN.NAME,
    STRINGS.PTRIBE_PIG_TASK.THE_INSECURE_PIGMAN.DESC,
    STRINGS.PTRIBE_PIG_TASK.THE_INSECURE_PIGMAN.COND,
    { oinc = 5 },
    {
        FinishTask = FinishTask,
    }
)
