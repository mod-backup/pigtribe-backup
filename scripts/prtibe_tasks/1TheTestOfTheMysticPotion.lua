local PigTasks = require("ptribe_pigtasks")
local Utils = require("ptribe_utils/utils")

local function OnUseAfterFn(retTab, inst, doer, target, success, transformed_inst, container)
    if target.prefab == "pigman" and target.components.werebeast then
        target.components.werebeast:TriggerDelta(4) --变身
        if doer.components.ptribe_playertask.taskData then
            doer.components.ptribe_playertask.taskData.IsUsed = true
        end
    else
        if doer.components.ptribe_playertask.taskData then
            doer.components.ptribe_playertask.taskData.taskFail = true
        end
    end
end

local PIG_MUST_TAGS = { "character", "pig" }
local PIG_CANT_TAGS = { "guard", "moonbeast", "werepig", "hostile" }
local function OnInit(self, isLoading)
    if isLoading then
        local potion = self.pig.components.entitytracker:GetEntity("ptribe_1TheTestOfTheMysticPotion_potion")
        if potion then
            Utils.FnDecorator(potion.components.halloweenpotionmoon, "onusefn", nil, OnUseAfterFn)
        end
    else
        local x, y, z = self.pig.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x, y, z, 300, PIG_MUST_TAGS, PIG_CANT_TAGS)
        if #ents > 1 then           --排除自己
            local pig = ents[#ents] --最远的一头猪
            if pig == self.pig then --应该不可能
                pig = ents[#ents - 1]
            end
            self.pig.components.entitytracker:TrackEntity("ptribe_1TheTestOfTheMysticPotion_subject", pig)

            local potion = SpawnPrefab("halloweenpotion_moon")
            Utils.FnDecorator(potion.components.halloweenpotionmoon, "onusefn", nil, OnUseAfterFn)
            self.pig.components.entitytracker:TrackEntity("ptribe_1TheTestOfTheMysticPotion_potion", potion)
            self.inst.components.inventory:GiveItem(potion)
        else
            return false, STRINGS.PTRIBE_PIG_TASK.THE_TEST_OF_THE_MYSTIC_POTION.FAIL.NO_PIG
        end
    end
    return true
end

local function GetTargetPos(self)
    if self.taskData.IsUsed then
        return self.pig:GetPosition()
    else
        local pig = self.pig.components.entitytracker:GetEntity("ptribe_1TheTestOfTheMysticPotion_subject")
        if pig then
            return pig:GetPosition()
        else
            return nil, STRINGS.PTRIBE_PIG_TASK.THE_TEST_OF_THE_MYSTIC_POTION.FAIL.PIG_DEATH
        end
    end
end

local function FinishTask(self)
    if self.taskData.IsUsed then --只要使用过就行，这里不检查是哪头猪
        return true
    elseif self.taskData.taskFail then
        return false, STRINGS.PTRIBE_PIG_TASK.THE_TEST_OF_THE_MYSTIC_POTION.FAIL.POTION_WASTE
    else
        return false, STRINGS.PTRIBE_PIG_TASK.THE_TEST_OF_THE_MYSTIC_POTION.FAIL.NOT_DRINK
    end
end

local function OnEnd(self, taskFinished, isInValid)
    self.pig.components.entitytracker:ForgetEntity("ptribe_1TheTestOfTheMysticPotion_potion")
    self.pig.components.entitytracker:ForgetEntity("ptribe_1TheTestOfTheMysticPotion_subject")

    if not taskFinished and not isInValid then
        self.pig.components.ptribe_pigtask.taskId:set(0) --不能让玩家一直刷材料
    end
end

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.THE_TEST_OF_THE_MYSTIC_POTION.NAME,
    STRINGS.PTRIBE_PIG_TASK.THE_TEST_OF_THE_MYSTIC_POTION.DESC,
    STRINGS.PTRIBE_PIG_TASK.THE_TEST_OF_THE_MYSTIC_POTION.COND,
    { oinc = 5, halloweenpotion_moon = 2 },
    {
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
