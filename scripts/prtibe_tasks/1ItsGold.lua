local PigTasks = require("ptribe_pigtasks")

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.ITS_GOLD.NAME,
    STRINGS.PTRIBE_PIG_TASK.ITS_GOLD.DESC,
    STRINGS.PTRIBE_PIG_TASK.ITS_GOLD.COND,
    { oinc = 5 }
)
