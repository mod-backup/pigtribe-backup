local PigTasks = require("ptribe_pigtasks")
local GetPrefab = require("ptribe_utils/getprefab")

local function OnDeployItem(inst, data)
    if data.prefab == "acorn" then
        local self = inst.components.ptribe_playertask
        self.taskData.count = self.taskData.count + 1
    end
end

local function OnInit(self, isLoading)
    if not isLoading then
        local acorn = SpawnPrefab("acorn")
        acorn.components.stackable.stacksize = 10
        self.inst.components.inventory:GiveItem(acorn)
        self.taskData.count = 0
    end

    self.inst:ListenForEvent("deployitem", OnDeployItem)
    return true
end

local function FinishTask(self)
    if self.taskData.count >= 10 then
        if self.taskData.count >= 20 then
            self.pig:DoTaskInTime(0, function()
                GetPrefab.ReturnMaterial("oinc", self.pig, {
                    count = math.random(1, 3),
                    target = self.inst,
                })
            end)
            return true, STRINGS.PTRIBE_PIG_TASK.TO_PROTECT_THE_ENVIRONMENT.SUCCESS.MANY_TREES
        end

        return true
    else
        return false, STRINGS.PTRIBE_PIG_TASK.TO_PROTECT_THE_ENVIRONMENT.FAIL.LACK_TREE
    end
end

local function OnEnd(self, taskFinished, isInValid)
    self.inst:RemoveEventCallback("deployitem", OnDeployItem)

    if not taskFinished and not isInValid then
        self.pig.components.ptribe_pigtask.taskId:set(0) --不能让玩家一直刷材料
    end
end

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.TO_PROTECT_THE_ENVIRONMENT.NAME,
    STRINGS.PTRIBE_PIG_TASK.TO_PROTECT_THE_ENVIRONMENT.DESC,
    STRINGS.PTRIBE_PIG_TASK.TO_PROTECT_THE_ENVIRONMENT.COND,
    { oinc = 2, dug_monkeytail = 2 },
    {
        OnInit = OnInit,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
