local PigTasks = require("ptribe_pigtasks")

local function OnInit(self, isLoading)
    if isLoading then return true end
    self.pig.components.follower:SetLeader(self.inst)
    self.pig.components.follower:AddLoyaltyTime(self.pig.components.ptribe_pigtask:GetTaskRemaining())

    if TheWorld.components.ptribe_tribemanager:GetPigking() then
        return true
    end

    return false, STRINGS.PTRIBE_PIG_TASK.LOYAL_SUBJECT_OF_THE_KING.FAIL.NO_PIG_KING
end

local function GetTargetPos(self)
    local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
    return pigking and pigking:GetPosition() or nil
end

local function FinishTask(self)
    local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
    if pigking and self.pig:GetDistanceSqToInst(pigking) <= 256 then  --4地皮
        return true
    else
        return false, STRINGS.PTRIBE_PIG_TASK.LOYAL_SUBJECT_OF_THE_KING.FAIL.TOO_FAR_AWAY
    end
end

local function OnEnd(self, taskFinished, IsInValid)
    local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
    if pigking then
        if taskFinished then
            local trinket_6 = SpawnPrefab("trinket_6") --随便一个玩具
            pigking.components.trader.onaccept(pigking, self.inst, trinket_6)
            trinket_6:Remove()
        end
    end

    if not IsInValid then
        self.pig.components.follower:StopFollowing()
    end
end

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.LOYAL_SUBJECT_OF_THE_KING.NAME,
    STRINGS.PTRIBE_PIG_TASK.LOYAL_SUBJECT_OF_THE_KING.DESC,
    STRINGS.PTRIBE_PIG_TASK.LOYAL_SUBJECT_OF_THE_KING.COND,
    { oinc = 10 },
    {
        time = 3 * TUNING.TOTAL_DAY_TIME,
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        FinishTask = FinishTask,
        OnEnd = OnEnd,
    }
)
