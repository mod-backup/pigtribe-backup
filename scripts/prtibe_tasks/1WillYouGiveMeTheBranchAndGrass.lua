local PigTasks = require("ptribe_pigtasks")

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.WILL_YOU_GIVE_ME_THE_BRANCHANDGRASS.NAME,
    STRINGS.PTRIBE_PIG_TASK.WILL_YOU_GIVE_ME_THE_BRANCHANDGRASS.DESC,
    STRINGS.PTRIBE_PIG_TASK.WILL_YOU_GIVE_ME_THE_BRANCHANDGRASS.COND,
    { oinc = 5 }
)
