local PigTasks = require("ptribe_pigtasks")

local function OnTimerDone(inst, data)
    if data.name == "ptribe_3PigmanWhoWantsToTravel_end" then --猪人一定存活
        local self = inst.components.ptribe_playertask
        if self.pig.components.talker then
            self.pig.components.talker:Say(STRINGS.PTRIBE_PIG_TASK.PIGMAN_WHO_WANTS_TO_TRAVEL.SUCCESS.THANK)
        end
        local pig = self.pig
        self:FinishTask()
        pig.components.ptribe_pigtask.taskId:set(0)
    end
end

local function OnStopFollowing(inst, data)
    if inst.components.combat.target == data.leader then
        inst.components.ptribe_pigtask.taskId:set(0)
    end
end

local function OnInit(self, isLoading)
    if not isLoading then
        -- 如果已经有该任务了就解除那个猪人的雇佣关系
        for k, _ in pairs(self.inst.components.leader.followers) do
            if k ~= self.pig and k.components.ptribe_pigtask and k.components.ptribe_pigtask.taskId:value() == self.taskId then
                self.inst.components.leader:RemoveFollower(k)
            end
        end

        self.pig.components.follower:SetLeader(self.inst)
        self.pig.components.follower:AddLoyaltyTime(TUNING.TOTAL_DAY_TIME * 2)
        self.pig.components.ptribe_pigtask:AddTaskTime(TUNING.TOTAL_DAY_TIME * 2)

        self.inst.components.timer:StartTimer("ptribe_3PigmanWhoWantsToTravel_end", TUNING.TOTAL_DAY_TIME * 2)
    end

    self.inst:ListenForEvent("timerdone", OnTimerDone)
    self.pig:ListenForEvent("stopfollowing", OnStopFollowing) --如果玩家打它，任务就结束了

    return true
end

local function FinishTask()
    return false, STRINGS.PTRIBE_PIG_TASK.PIGMAN_WHO_WANTS_TO_TRAVEL.FAIL.LACK_OF_TIME
end

local function OnEnd(self)
    self.inst.components.timer:StopTimer("ptribe_3PigmanWhoWantsToTravel_end")

    self.inst:RemoveEventCallback("timerdone", OnTimerDone)
    if self.pig:IsValid() then
        self.pig:RemoveEventCallback("stopfollowing", OnStopFollowing)
        self.pig.components.follower:StopFollowing()
    end
end

PigTasks.AddTask(3,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_WHO_WANTS_TO_TRAVEL.NAME,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_WHO_WANTS_TO_TRAVEL.DESC,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_WHO_WANTS_TO_TRAVEL.COND,
    { oinc = 10, butter = 4 },
    {
        OnInit = OnInit,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
