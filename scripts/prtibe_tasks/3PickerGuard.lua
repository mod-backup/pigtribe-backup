local Utils = require("ptribe_utils/utils")
local GetPrefab = require("ptribe_utils/getprefab")
local PigTasks = require("ptribe_pigtasks")

local function OnPickablePicked(inst, picker)
    if picker.prefab == "pigman" and picker.prtibe_taskPlayer then --猪人采集后直接消失
        inst.ptribe_masterPick = true
        local self = picker.prtibe_taskPlayer.components.ptribe_playertask
        self.taskData.level = self.taskData.level + 1

        if self.taskData.level == 2 then
            --开始采集曼德拉草
            local home = picker.components.homeseeker and picker.components.homeseeker:GetHome()
            local mandrake = self.pig.components.entitytracker:GetEntity("ptribe_3PickerGuard_mandrake")
            if home and mandrake then
                home.components.ptribe_pigmanager:CancelTask()
                home:DoTaskInTime(0, function() --不知道为什么不加个延时就设置不起效果
                    home.components.ptribe_pigmanager.task = {
                        id = 0,
                        prefab = mandrake,
                        priority = 1,
                        worker = 1,
                        startTime = GetTime(),
                        type = "interact",
                        isWalk = true,
                    }
                end)
            else
                --任务失败
                picker.components.ptribe_pigtask.taskId:set(0)
            end
        elseif self.taskData.level == 3 then
            if self.spawnMonsterTask then
                self.spawnMonsterTask:Cancel()
                self.spawnMonsterTask = nil
            end
        end

        return nil, true
    elseif inst.ptribe_master and inst.ptribe_master:IsValid() and inst.ptribe_master.components.combat then
        inst.ptribe_master.components.ptribe_pigtask.taskId:set(0)
        inst.ptribe_master.components.combat:SetTarget(picker)
    end
end

local RANDOM_MONSTER = { "icehound", "icehound", "bishop", "hound", "hound", "hound", "firehound", "firehound",
    "spider_warrior", "spider_warrior", "hedgehound" }
if TUNING.TRIBE_TASK_DIFFICULTY_LEVEL > 2 then
    table.insert(RANDOM_MONSTER, "bearger") --熊大
end
local monsterHealthMax = TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 300

---任务开始后猪人附近随机刷出来怪物打猪人，我这里就不取消掉落物了
local function RandomSpawnMonster(inst)
    if math.random() > 0.7 then return end

    local pos = GetPrefab.GetSpawnPoint(inst:GetPosition())
    if not pos then return end

    local monster = SpawnAt(RANDOM_MONSTER[math.random(1, #RANDOM_MONSTER)], pos)
    local cur = monster.components.health.currenthealth
    if cur > monsterHealthMax then
        monster.components.health:DoDelta(monsterHealthMax - cur) --1000血的熊大还是挺危险的
    end
    monster.components.combat:SetTarget(inst)
    inst.components.combat:SetKeepTargetFunction(Utils.TrueFn)
end

local function OnRemove(inst)
    if not inst.ptribe_masterPick
        and inst.ptribe_master and inst.ptribe_master:IsValid() then
        inst.ptribe_master.components.ptribe_pigtask.taskId:set(0)
    end
end

local function Init(self, firenettle, mandrake, home)
    GetPrefab.SetMaxHealth(self.pig, 1000 * (1 + 0.3 * (2 - TUNING.TRIBE_TASK_DIFFICULTY_LEVEL)))
    Utils.FnDecorator(firenettle.components.pickable, "onpickedfn", OnPickablePicked)
    firenettle.components.pickable.remove_when_picked = true
    firenettle.ptribe_master = self.pig
    firenettle:ListenForEvent("onremove", OnRemove)

    Utils.FnDecorator(mandrake.components.pickable, "onpickedfn", OnPickablePicked)
    mandrake.ptribe_master = self.pig
    mandrake:ListenForEvent("onremove", OnRemove)

    home.components.ptribe_pigmanager:CancelTask()
    home.components.timer:StopTimer("ptribe_findTask_cd")
    home.components.timer:StartTimer("ptribe_findTask_cd", self.pig.components.ptribe_pigtask:GetTaskRemaining())
    home.components.ptribe_pigmanager.task = {
        id = 0,
        prefab = self.taskData.level == 1 and firenettle or mandrake,
        priority = 1,
        worker = 1,
        startTime = GetTime(),
        type = "interact",
        isWalk = true,
    }

    self.spawnMonsterTask = self.pig:DoPeriodicTask(4, RandomSpawnMonster, 8)
end

local function OnInit(self, isLoading)
    if isLoading then
        local firenettle = self.pig.components.entitytracker:GetEntity("ptribe_3PickerGuard_firenettle")
        local mandrake = self.pig.components.entitytracker:GetEntity("ptribe_3PickerGuard_mandrake")
        local home = self.pig.components.homeseeker and self.pig.components.homeseeker:GetHome()
        if firenettle and mandrake and home then
            self.pig.prtibe_taskPlayer = self.inst

            Init(self, firenettle, mandrake, home)
        end
    else
        local pos1 = PigTasks.SpawnPos(self.inst:GetPosition(), 150)
        local pos2 = PigTasks.SpawnPos(self.inst:GetPosition(), 200)

        if not pos1 or not pos2 then
            return false, STRINGS.PTRIBE_PIG_TASK.PICKER_GUARD.FAIL.NOT_POS
        end

        local home = self.pig.components.homeseeker and self.pig.components.homeseeker:GetHome()
        if not home or not home.components.ptribe_pigmanager then --第二个应该不可能
            return false, STRINGS.PTRIBE_PIG_TASK.PICKER_GUARD.FAIL.NOT_HOME
        end

        --火荨麻
        local firenettle = SpawnAt("weed_firenettle", pos1)
        firenettle.components.growable:SetStage(3)
        self.pig.components.entitytracker:TrackEntity("ptribe_3PickerGuard_firenettle", firenettle)
        --曼德拉草
        local mandrake = SpawnAt("mandrake_planted", pos2)
        self.pig.components.entitytracker:TrackEntity("ptribe_3PickerGuard_mandrake", mandrake)

        self.taskData.level = 1
        self.pig.prtibe_taskPlayer = self.inst

        Init(self, firenettle, mandrake, home)
    end

    return true
end

local function FinishTask(self)
    if self.taskData.level >= 3 then
        return true
    else
        return false, STRINGS.PTRIBE_PIG_TASK.PICKER_GUARD.FAIL.LACK
    end
end

local function OnEnd(self)
    local firenettle = self.pig.components.entitytracker:GetEntity("ptribe_3PickerGuard_firenettle")
    if firenettle then
        firenettle:Remove()
    end
    local mandrake = self.pig.components.entitytracker:GetEntity("ptribe_3PickerGuard_mandrake")
    if mandrake then
        mandrake:Remove()
    end

    local home = self.pig:IsValid() and self.pig.components.homeseeker and self.pig.components.homeseeker:GetHome()
    if home then
        home.components.timer:StopTimer("ptribe_findTask_cd")
    end

    if self.spawnMonsterTask then
        self.spawnMonsterTask:Cancel()
        self.spawnMonsterTask = nil
    end

    self.pig.prtibe_taskPlayer = nil
end

PigTasks.AddTask(3,
    STRINGS.PTRIBE_PIG_TASK.PICKER_GUARD.NAME,
    STRINGS.PTRIBE_PIG_TASK.PICKER_GUARD.DESC,
    STRINGS.PTRIBE_PIG_TASK.PICKER_GUARD.COND,
    { oinc = 10, honey = 10 },
    {
        OnInit = OnInit,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
