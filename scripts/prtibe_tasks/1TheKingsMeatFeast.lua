local PigTasks = require("ptribe_pigtasks")
local GetPrefab = require("ptribe_utils/getprefab")

local function OnTrade(inst, data)
    local self = data.giver.components.ptribe_playertask
    if not data.item:HasTag("meat") or not self then return end

    self.taskData.meatCount = self.taskData.meatCount + 1
end

local function OnInit(self, isLoading)
    if isLoading then
        local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
        if pigking then
            pigking:ListenForEvent("trade", OnTrade)
            return true
        end
    else
        self.taskData.meatCount = 0
        --找到猪王
        local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
        if pigking then
            pigking:ListenForEvent("trade", OnTrade)
            return true
        end

        return false, STRINGS.PTRIBE_PIG_TASK.THE_KINGS_MEAT_FEAST.FAIL.NO_PIG_KING
    end
end

local function GetTargetPos(self)
    if self.taskData.meatCount > 10 then
        return self.pig:GetPosition()
    else
        local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
        return pigking and pigking:GetPosition() or nil
    end
end

local function FinishTask(self)
    if self.taskData.meatCount > 10 then
        if self.taskData.meatCount > 15 then
            GetPrefab.ReturnMaterial("oinc", self.pig, { target = self.inst })
        end

        return true
    else
        return false, STRINGS.PTRIBE_PIG_TASK.THE_KINGS_MEAT_FEAST.FAIL.LACK_MEAT
    end
end

local function OnEnd(self)
    local pigking = TheWorld.components.ptribe_tribemanager:GetPigking()
    if pigking then
        pigking:RemoveEventCallback("trade", OnTrade)
    end
end

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.THE_KINGS_MEAT_FEAST.NAME,
    STRINGS.PTRIBE_PIG_TASK.THE_KINGS_MEAT_FEAST.DESC,
    STRINGS.PTRIBE_PIG_TASK.THE_KINGS_MEAT_FEAST.COND,
    { oinc = 5, pig_coin = 2 },
    {
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
