local PigTasks = require("ptribe_pigtasks")

local function FinishTask(self)
    local hat = self.pig.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    if hat then
        if hat.prefab == "minerhat" then
            if hat.components.fueled and not hat.components.fueled:IsEmpty() then
                return true
            else
                return false, STRINGS.PTRIBE_PIG_TASK.IM_AFRAID_OF_THE_DARK.FAIL.HAT_NOT_SHINE
            end
        else
            --即便不是矿工帽，只要猪人在发光就可以
            if self.pig.components.bloomer and self.pig.components.bloomer:GetCurrentFX() then
                return true
            else
                return false, STRINGS.PTRIBE_PIG_TASK.IM_AFRAID_OF_THE_DARK.FAIL.NOT_MINER_HAT
            end
        end
    else
        return false, STRINGS.PTRIBE_PIG_TASK.IM_AFRAID_OF_THE_DARK.FAIL.NOT_HAT
    end
end

PigTasks.AddTask(2,
    STRINGS.PTRIBE_PIG_TASK.IM_AFRAID_OF_THE_DARK.NAME,
    STRINGS.PTRIBE_PIG_TASK.IM_AFRAID_OF_THE_DARK.DESC,
    STRINGS.PTRIBE_PIG_TASK.IM_AFRAID_OF_THE_DARK.COND,
    { oinc = 5, berries = 10 },
    {
        FinishTask = FinishTask,
    }
)
