local PigTasks = require("ptribe_pigtasks")

local function GetTargetPos(self)
    if self.taskData.isSpawned then
        local pig = self.pig.components.entitytracker:GetEntity("ptribe_1TheLostPigman_lostpig")
        if pig then
            if pig.components.follower.leader then
                return self.pig:GetPosition()
            else
                return pig:GetPosition()
            end
        else
            return self.pig:GetPosition() --目标死了
        end
    else
        return self.taskData.pos
    end
end

local function SpawnFn(self, pos)
    local pig = SpawnAt("pigman", pos)
    pig.components.sleeper:GoToSleep(15 + math.random() * 4) --睡美人
    pig.sg:GoToState("sleeping")

    self.pig.components.entitytracker:TrackEntity("ptribe_1TheLostPigman_lostpig", pig)
end

local function OnUpdate(self)
    PigTasks.DefaultOnUpdate(self, SpawnFn)
end

local function FinishTask(self)
    if not self.taskData.isSpawned then
        return false, STRINGS.PTRIBE_PIG_TASK.THE_LOST_PIGMAN.FAIL.NO_BRING_BACK
    end

    local pig = self.pig.components.entitytracker:GetEntity("ptribe_1TheLostPigman_lostpig")
    if pig then
        if self.inst:GetDistanceSqToInst(pig) < 400 then
            return true
        else
            return false, STRINGS.PTRIBE_PIG_TASK.THE_LOST_PIGMAN.FAIL.NO_BRING_BACK
        end
    else --猪人死了
        local p = self.pig
        self.inst:DoTaskInTime(0, function()
            if p:IsValid() then
                p.components.combat:SetTarget(self.inst)
                p.components.ptribe_pigtask.taskId:set(0)
            end
        end)
        return false, STRINGS.PTRIBE_PIG_TASK.THE_LOST_PIGMAN.FAIL.PIG_DEATH
    end
end

local function OnEnd(self)
    if self.taskData.isSpawned then
        local pig = self.pig.components.entitytracker:GetEntity("ptribe_1TheLostPigman_lostpig")
        if pig then
            pig.components.follower:StopFollowing()
            self.pig.components.entitytracker:ForgetEntity("ptribe_1TheLostPigman_lostpig")
        end
    end
end

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.THE_LOST_PIGMAN.NAME,
    STRINGS.PTRIBE_PIG_TASK.THE_LOST_PIGMAN.DESC,
    STRINGS.PTRIBE_PIG_TASK.THE_LOST_PIGMAN.COND,
    { oinc = 3, meat_dried = 3 },
    {
        time = TUNING.TOTAL_DAY_TIME * 3,
        OnInit = PigTasks.DefaultInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
