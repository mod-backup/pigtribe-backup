local Shapes = require("ptribe_utils/shapes")
local PigTasks = require("ptribe_pigtasks")

local mermCount = 2 + TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 3
local function OnInit(self, isLoading)
    if isLoading then return true end

    local pos1 = PigTasks.SpawnPos(self.inst:GetPosition(), math.random(200, 250)) --猪人
    local pos2 = pos1 and PigTasks.SpawnPos(pos1, 8) or nil                        --鱼人

    if not pos1 or not pos2 then
        return false, STRINGS.PTRIBE_PIG_TASK.PIGMAN_AND_MERM_FIGHT.FAIL.NOT_POS
    end

    self.taskData.pos = { pos1, pos2 }

    return true
end

local function GetTargetPos(self)
    if self.taskData.isSpawned then
        for i = 1, mermCount do
            local merm = self.pig.components.entitytracker:GetEntity("ptribe_3PigmanAndMermFight_merm" .. tostring(i))
            if merm then
                return merm:GetPosition()
            end
        end
        return self.pig:GetPosition()
    else
        return self.taskData.pos[1]
    end
end

local function OnUpdate(self)
    if self.taskData.isSpawned then return end

    if self.inst:GetPosition():DistSq(self.taskData.pos[1]) < 900 then
        local pos1 = Vector3(TheWorld.Map:GetTileCenterPoint(self.taskData.pos[1]:Get()))
        local pos2 = Vector3(TheWorld.Map:GetTileCenterPoint(self.taskData.pos[2]:Get()))

        local pigs = {}
        for i = 1, 6 do
            local pig = SpawnAt("pigman", Shapes.GetRandomLocation(pos1, 0, 2))
            pig.components.combat:SetTarget(self.inst)
            self.pig.components.entitytracker:TrackEntity("ptribe_3PigmanAndMermFight_pig" .. tostring(i), pig)
            table.insert(pigs, pig)
        end

        for i = 1, mermCount do
            local merm = SpawnAt("merm", Shapes.GetRandomLocation(pos2, 0, 2))
            merm.components.combat:SetTarget(self.inst)
            self.pig.components.entitytracker:TrackEntity("ptribe_3PigmanAndMermFight_merm" .. tostring(i), merm)

            --来点坏心思，给鱼人加个帽子
            if math.random() < 0.3 then
                local hat = SpawnPrefab("footballhat")
                merm.components.inventory:Equip(hat)
                merm.AnimState:Show("hat")
            end

            merm.components.combat:SetTarget(pigs[math.random(1, #pigs)])
        end

        self.taskData.isSpawned = true
    end
end

local function FinishTask(self)
    if not self.taskData.isSpawned then
        return false, STRINGS.PTRIBE_PIG_TASK.PIGMAN_AND_MERM_FIGHT.FAIL.ENEMY_SURVIVAL
    end

    local isSurvival = false
    for i = 1, 6 do
        local pig = self.pig.components.entitytracker:GetEntity("ptribe_3PigmanAndMermFight_pig" .. tostring(i))
        if pig then
            isSurvival = true
            break
        end
    end
    if not isSurvival then
        self.pig:DoTaskInTime(0, function()
            self.pig.components.ptribe_pigtask.taskId:set(0)
        end)
        return false, STRINGS.PTRIBE_PIG_TASK.PIGMAN_AND_MERM_FIGHT.FAIL.PIGS_DEATH
    end

    for i = 1, mermCount do
        local merm = self.pig.components.entitytracker:GetEntity("ptribe_3PigmanAndMermFight_merm" .. tostring(i))
        if merm then
            return false, STRINGS.PTRIBE_PIG_TASK.PIGMAN_AND_MERM_FIGHT.FAIL.ENEMY_SURVIVAL
        end
    end

    return true
end

local function OnEnd(self)
    if self.taskData.isSpawned then
        for i = 1, 6 do
            self.pig.components.entitytracker:ForgetEntity("ptribe_3PigmanAndMermFight_pig" .. tostring(i))
        end
        for i = 1, mermCount do
            self.pig.components.entitytracker:ForgetEntity("ptribe_3PigmanAndMermFight_merm" .. tostring(i))
        end
    end
end

-- 6个打8个
PigTasks.AddTask(3,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_AND_MERM_FIGHT.NAME,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_AND_MERM_FIGHT.DESC,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_AND_MERM_FIGHT.COND,
    { oinc = 8, yellowamulet = 1 },
    {
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
