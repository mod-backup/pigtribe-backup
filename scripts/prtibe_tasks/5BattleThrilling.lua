local PigTasks = require("ptribe_pigtasks")
local GetPrefab = require("ptribe_utils/getprefab")

--如果玩家打它，任务就结束了
local function OnStopFollowing(inst, data)
    if inst.components.combat.target == data.leader then
        inst.components.ptribe_pigtask.taskId:set(0)
    end
end

local function OnAttackOther(inst, data)
    if data.target and data.target.prefab == "dragonfly" then
        local self = inst.prtibe_taskPlayer.components.ptribe_playertask
        self.taskData.attacked = true
        self.pig.components.entitytracker:TrackEntity("ptribe_5BattleThrilling_target", data.target) --猪人必须得打龙蝇一下
    end
end

local function Init(self)
    self.pig.prtibe_taskPlayer = self.inst

    --稍微强化一下
    GetPrefab.SetMaxHealth(self.pig, 1000 + TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 500)
    self.pig.components.combat:SetDefaultDamage(150)
    self.pig.components.locomotor.runspeed = TUNING.WEREPIG_RUN_SPEED
    self.pig.components.locomotor.walkspeed = TUNING.WEREPIG_WALK_SPEED

    self.pig:ListenForEvent("onattackother", OnAttackOther)
    self.pig:ListenForEvent("stopfollowing", OnStopFollowing)
end

local function OnInit(self, isLoading)
    if not isLoading then
        self.pig.components.follower:SetLeader(self.inst)
        self.pig.components.follower:AddLoyaltyTime(TUNING.TOTAL_DAY_TIME * 2)
        self.pig.components.ptribe_pigtask:AddTaskTime(TUNING.TOTAL_DAY_TIME * 2)
    end

    Init(self)
    return true
end

local function FinishTask(self)
    if not self.taskData.attacked then
        return false, STRINGS.PTRIBE_PIG_TASK.BATTLE_THRILLING.FAIL.NOT_DEFEAT_DRAGONFLY
    end

    local dragonfly = self.pig.components.entitytracker:GetEntity("ptribe_5BattleThrilling_target")
    if dragonfly then
        return false, STRINGS.PTRIBE_PIG_TASK.BATTLE_THRILLING.FAIL.NOT_DEFEAT_DRAGONFLY
    else
        return true
    end
end

local function OnEnd(self)
    self.pig.components.entitytracker:ForgetEntity("ptribe_5BattleThrilling_target")
    if self.pig:IsValid() then
        self.pig.prtibe_taskPlayer = nil

        GetPrefab.SetMaxHealth(self.pig, TUNING.PIG_HEALTH)
        self.pig.components.combat:SetDefaultDamage(TUNING.PIG_DAMAGE)
        self.pig.components.locomotor.runspeed = TUNING.PIG_RUN_SPEED
        self.pig.components.locomotor.walkspeed = TUNING.PIG_WALK_SPEED

        self.pig:RemoveEventCallback("onattackother", OnAttackOther)
        self.pig:RemoveEventCallback("stopfollowing", OnStopFollowing)
        self.pig.components.follower:StopFollowing()
    end
end

PigTasks.AddTask(5,
    STRINGS.PTRIBE_PIG_TASK.BATTLE_THRILLING.NAME,
    STRINGS.PTRIBE_PIG_TASK.BATTLE_THRILLING.DESC,
    STRINGS.PTRIBE_PIG_TASK.BATTLE_THRILLING.COND,
    { oinc = 8, dragon_scales = 2 },
    {
        time = TUNING.TOTAL_DAY_TIME * 3,
        OnInit = OnInit,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
