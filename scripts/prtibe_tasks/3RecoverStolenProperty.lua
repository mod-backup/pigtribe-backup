local Utils = require("ptribe_utils/utils")
local Shapes = require("ptribe_utils/shapes")
local PigTasks = require("ptribe_pigtasks")
local GetPrefab = require("ptribe_utils/getprefab")

local function GenerateLootAfter(retTab)
    local loots = unpack(retTab)

    if math.random() < 0.1 then
        table.insert(loots, "krampus_sack")
    end

    return { loots }
end

---强化坎普斯
local function Init(self, krampus)
    GetPrefab.SetMaxHealth(krampus, 600 + TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 200)
    krampus.components.combat:SetDefaultDamage(60 + TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 20)
    krampus.components.combat:SetAttackPeriod(1)                       --调太低就超模了
    krampus:DoTaskInTime(0, function() krampus.brain.greed = 999 end); --防止坎普斯拿完东西就跑

    --也许可以监听攻击事件做一些技能？

    --提高一下小偷背包掉落率
    Utils.FnDecorator(krampus.components.lootdropper, "GenerateLoot", nil, GenerateLootAfter)
end

local function OnInit(self, isLoading)
    if isLoading then
        if self.taskData.isSpawned then
            local krampus = self.pig.components.entitytracker:GetEntity("ptribe_3RecoverStolenProperty_krampus")
            if krampus then
                Init(self, krampus)
            end
        end
    else
        self.taskData.pos = PigTasks.SpawnPos(self.inst:GetPosition(), math.random(150, 250))
        if self.taskData.pos then
            return true
        else
            return false, STRINGS.PTRIBE_PIG_TASK.RECOVER_STOLEN_PROPERTY.FAIL.NOT_POS
        end
    end
    return true
end

local function GetTargetPos(self)
    if self.taskData.isSpawned then
        local krampus = self.pig.components.entitytracker:GetEntity("ptribe_3RecoverStolenProperty_krampus")
        if krampus then
            return krampus:GetPosition()
        end

        return self.pig:GetPosition()
    else
        return self.taskData.pos
    end
end

local RANDOM_BRIBES = {
    "log", "boards", "cutstone", "charcoal", "berries", "monstermeat", "beefalowool", "meat_dried", "goldnugget",
    "oinc"
}

local function OnUpdate(self)
    if self.taskData.isSpawned then return end

    if self.inst:GetPosition():DistSq(self.taskData.pos) < 900 then
        local krampus = SpawnAt("krampus", self.taskData.pos)
        self.pig.components.entitytracker:TrackEntity("ptribe_3RecoverStolenProperty_krampus", krampus)
        Init(self, krampus)

        --赃物
        local count = math.random(3, 8)
        for i = 1, count do
            SpawnAt(RANDOM_BRIBES[math.random(1, #RANDOM_BRIBES)], Shapes.GetRandomLocation(self.taskData.pos, 2, 10))
        end

        self.taskData.isSpawned = true
    end
end

local function FinishTask(self)
    if not self.taskData.isSpawned then
        return false, STRINGS.PTRIBE_PIG_TASK.RECOVER_STOLEN_PROPERTY.FAIL.ENEMY_SURVIVAL
    end

    local krampus = self.pig.components.entitytracker:GetEntity("ptribe_3RecoverStolenProperty_krampus")
    if krampus then
        return false, STRINGS.PTRIBE_PIG_TASK.RECOVER_STOLEN_PROPERTY.FAIL.ENEMY_SURVIVAL
    end

    return true
end

local function OnEnd(self)
    if self.taskData.isSpawned then --赃物我就不移除了
        local krampus = self.pig.components.entitytracker:GetEntity("ptribe_3RecoverStolenProperty_krampus")
        if krampus then
            krampus:Remove()
        end
    end
end

--稍微强化一下坎普斯
PigTasks.AddTask(3,
    STRINGS.PTRIBE_PIG_TASK.RECOVER_STOLEN_PROPERTY.NAME,
    STRINGS.PTRIBE_PIG_TASK.RECOVER_STOLEN_PROPERTY.DESC,
    STRINGS.PTRIBE_PIG_TASK.RECOVER_STOLEN_PROPERTY.COND,
    { oinc = 8, redgem = 1, bluegem = 1, purplegem = 1 },
    {
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
