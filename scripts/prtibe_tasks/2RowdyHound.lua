local Shapes = require("ptribe_utils/shapes")
local PigTasks = require("ptribe_pigtasks")

local houndCount =  2 + TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 2
local function GetTargetPos(self)
    if self.taskData.isSpawned then
        for i = 1, houndCount do
            local hound = self.pig.components.entitytracker:GetEntity("ptribe_2RowdyHound_hound" .. tostring(i))
            if hound then
                return hound:GetPosition()
            end
        end
        --四个猎犬都死了
        return self.pig:GetPosition()
    else
        return self.taskData.pos
    end
end

local DIS_SQ = 38 * 38
local function OnUpdate(self)
    if self.taskData.isSpawned then return end

    if self.inst:GetPosition():DistSq(self.taskData.pos) < DIS_SQ then
        local pos = Vector3(TheWorld.Map:GetTileCenterPoint(self.taskData.pos:Get()))

        for i = 1, houndCount do
            local hound = SpawnAt("hound", Shapes.GetRandomLocation(pos, 0, 8))
            hound.components.combat:SetTarget(self.inst)
            self.pig.components.entitytracker:TrackEntity("ptribe_2RowdyHound_hound" .. tostring(i), hound)
        end
        self.taskData.isSpawned = true
    end
end

local function FinishTask(self)
    if not self.taskData.isSpawned then
        return false, STRINGS.PTRIBE_PIG_TASK.ROWDY_HOUND.FAIL.HOUND_SURVIVAL
    end

    for i = 1, houndCount do
        local hound = self.pig.components.entitytracker:GetEntity("ptribe_2RowdyHound_hound" .. tostring(i))
        if hound then
            return false, STRINGS.PTRIBE_PIG_TASK.ROWDY_HOUND.FAIL.HOUND_SURVIVAL
        end
    end
    return true
end

local function OnEnd(self)
    if self.taskData.isSpawned then
        for i = 1, houndCount do
            self.pig.components.entitytracker:ForgetEntity("ptribe_2RowdyHound_hound" .. tostring(i))
        end
    end
end

PigTasks.AddTask(2,
    STRINGS.PTRIBE_PIG_TASK.ROWDY_HOUND.NAME,
    STRINGS.PTRIBE_PIG_TASK.ROWDY_HOUND.DESC,
    STRINGS.PTRIBE_PIG_TASK.ROWDY_HOUND.COND,
    { oinc = 6, wormlight = 1 },
    {
        OnInit = PigTasks.DefaultInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd,
    }
)
