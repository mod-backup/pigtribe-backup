local PigTasks = require("ptribe_pigtasks")

local function GetTargetPos(self)
    if self.taskData.isSpawned then
        local spiderqueen = self.pig.components.entitytracker:GetEntity("ptribe_4PigmenHateSpider_spiderqueen")
        if spiderqueen then
            return spiderqueen:GetPosition()
        end

        return self.pig:GetPosition()
    else
        return self.taskData.pos
    end
end

local RANDOM_SPIDERDENS = { "spiderden", "spiderden_2", "spiderden_3" }
local function OnUpdate(self)
    if self.taskData.isSpawned then return end

    if self.inst:GetPosition():DistSq(self.taskData.pos) < 900 then
        local spiderqueen = SpawnAt("spiderqueen", self.taskData.pos)
        self.pig.components.entitytracker:TrackEntity("ptribe_4PigmenHateSpider_spiderqueen", spiderqueen)
        
        for i = 1, TUNING.TRIBE_TASK_DIFFICULTY_LEVEL do
            local pos = PigTasks.SpawnPos(self.taskData.pos, math.random(3, 12))
            if pos then
                SpawnAt(RANDOM_SPIDERDENS[math.random(1, #RANDOM_SPIDERDENS)], pos)
            end
        end

        self.taskData.isSpawned = true
    end
end

local function FinishTask(self)
    if not self.taskData.isSpawned then
        return false, STRINGS.PTRIBE_PIG_TASK.PIGMAN_HATE_SPIDER.FAIL.NOT_DEFEAT_QUEEN
    end

    local spiderqueen = self.pig.components.entitytracker:GetEntity("ptribe_4PigmenHateSpider_spiderqueen")
    if spiderqueen then
        return false, STRINGS.PTRIBE_PIG_TASK.PIGMAN_HATE_SPIDER.FAIL.NOT_DEFEAT_QUEEN
    end

    return true
end

local function OnEnd(self)
    self.pig.components.entitytracker:ForgetEntity("ptribe_4PigmenHateSpider_spiderqueen")
end

PigTasks.AddTask(4,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_HATE_SPIDER.NAME,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_HATE_SPIDER.DESC,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_HATE_SPIDER.COND,
    { oinc = 8, pig_coin = 2 },
    {
        time = TUNING.TOTAL_DAY_TIME * 2,
        OnInit = PigTasks.DefaultInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
