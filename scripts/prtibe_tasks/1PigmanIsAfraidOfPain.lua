local PigTasks = require("ptribe_pigtasks")

local function OnEnd(self, taskFinished)
    if taskFinished then
        self.pig.components.follower:SetLeader(self.inst)
        self.pig.components.follower:AddLoyaltyTime(TUNING.TOTAL_DAY_TIME)
    end
end

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_IS_AFRAID_OF_PAIN.NAME,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_IS_AFRAID_OF_PAIN.DESC,
    STRINGS.PTRIBE_PIG_TASK.PIGMAN_IS_AFRAID_OF_PAIN.COND,
    { oinc = 3 },
    {
        OnEnd = OnEnd
    }
)
