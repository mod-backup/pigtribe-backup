local PigTasks = require("ptribe_pigtasks")
local Shapes = require("ptribe_utils/shapes")

local function OnInit(self, isLoading)
    if isLoading then return true end

    self.taskData.pos = PigTasks.SpawnPos(self.inst:GetPosition(), math.random(200, 300))
    self.taskData.pos1 = self.taskData.pos and PigTasks.SpawnPos(self.taskData.pos, math.random(0, 8))
    self.taskData.pos2 = self.taskData.pos and PigTasks.SpawnPos(self.taskData.pos, math.random(0, 8))
    if not self.taskData.pos1 or not self.taskData.pos2 then
        return false, STRINGS.PTRIBE_PIG_TASK.GENERIC.NOT_POS
    end

    return true
end

local function GetTargetPos(self)
    if self.taskData.isSpawned then
        for i = 1, 2 do
            local leif = self.pig.components.entitytracker:GetEntity("ptribe_3TheForestCounterattack_leif" .. tostring(i))
            if leif then
                return leif:GetPosition()
            end
        end
        return self.pig:GetPosition()
    else
        return self.taskData.pos
    end
end

local DIS_SQ = 38 * 38
local RANDOM_LEIFS = { "leif", "leif_sparse", "deciduoustree" }
local TREES = {
    leif = "evergreen_tall",
    leif_sparse = "evergreen_sparse_tall",
    deciduoustree = "deciduoustree_tall"
}

local function SpawnScene(self, pos, leifIndex, from, to)
    local centerPos = Vector3(TheWorld.Map:GetTileCenterPoint(pos:Get()))
    local prefab = RANDOM_LEIFS[math.random(1, #RANDOM_LEIFS)]
    local leif = SpawnAt(prefab, Shapes.GetRandomLocation(centerPos, 0, 2))
    self.pig.components.entitytracker:TrackEntity("ptribe_3TheForestCounterattack_leif" .. tostring(leifIndex), leif)

    local pig
    for i = from, to do
        pig = SpawnAt("pigman", Shapes.GetRandomLocation(centerPos, 0, 2))
        if math.random() < 0.2 then
            pig.components.inventory:Equip(SpawnPrefab("footballhat"))
            pig.AnimState:Show("hat")
        end
        pig.components.combat:SetTarget(leif)

        self.pig.components.entitytracker:TrackEntity("ptribe_3TheForestCounterattack_pig" .. tostring(i), pig)
    end

    SpawnAt(TREES[prefab], Shapes.GetRandomLocation(centerPos, 0, 2)).components.workable:Destroy(pig)
    for i = 1, math.random(3, 5) do
        SpawnAt("log", Shapes.GetRandomLocation(centerPos, 1, 8))
    end
end

local function OnUpdate(self)
    if self.taskData.isSpawned then return end

    if self.inst:GetPosition():DistSq(self.taskData.pos) < DIS_SQ then
        local count, count2 = math.random(4, 6) - TUNING.TRIBE_TASK_DIFFICULTY_LEVEL,
            math.random(4, 6) - TUNING.TRIBE_TASK_DIFFICULTY_LEVEL
        SpawnScene(self, self.taskData.pos1, 1, 1, count)
        SpawnScene(self, self.taskData.pos1, 2, count + 1, count + count2)
        self.taskData.count = count + count2

        SpawnAt("firestaff", self.taskData.pos).components.finiteuses:SetPercent(math.random()) --火魔杖

        self.taskData.isSpawned = true
    end
end

local function FinishTask(self)
    if not self.taskData.isSpawned then
        return false, STRINGS.PTRIBE_PIG_TASK.THE_FOREST_COUNTERATTACK.FAIL.ENEMY_SURVIVAL
    end

    for i = 1, 2 do
        local leif = self.pig.components.entitytracker:GetEntity("ptribe_3TheForestCounterattack_leif" .. tostring(i))
        if leif then
            return false, STRINGS.PTRIBE_PIG_TASK.THE_FOREST_COUNTERATTACK.FAIL.ENEMY_SURVIVAL
        end
    end

    local isSurvival = false
    for i = 1, self.taskData.count do
        local pig = self.pig.components.entitytracker:GetEntity("ptribe_3TheForestCounterattack_pig" .. tostring(i))
        if pig then
            isSurvival = true
            break
        end
    end

    if not isSurvival then
        return false, STRINGS.PTRIBE_PIG_TASK.THE_FOREST_COUNTERATTACK.FAIL.PIG_DEATH
    end

    return true
end

local function OnEnd(self)
    if not self.taskData.isSpawned then return end

    -- 自产自销
    for i = 1, 2 do
        PigTasks.RemoveEntity(self, "ptribe_3TheForestCounterattack_leif" .. tostring(i))
    end
    for i = 1, self.taskData.count do
        PigTasks.RemoveEntity(self, "ptribe_3TheForestCounterattack_pig" .. tostring(i))
    end
end

PigTasks.AddTask(3,
    STRINGS.PTRIBE_PIG_TASK.THE_FOREST_COUNTERATTACK.NAME,
    STRINGS.PTRIBE_PIG_TASK.THE_FOREST_COUNTERATTACK.DESC,
    STRINGS.PTRIBE_PIG_TASK.THE_FOREST_COUNTERATTACK.COND,
    { oinc = 10, leif_idol = 2 }, {
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
