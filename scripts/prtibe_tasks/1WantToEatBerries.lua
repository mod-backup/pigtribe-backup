local PigTasks = require("ptribe_pigtasks")

PigTasks.AddTask(1,
    STRINGS.PTRIBE_PIG_TASK.WANT_TO_EAT_BERRIES.NAME,
    STRINGS.PTRIBE_PIG_TASK.WANT_TO_EAT_BERRIES.DESC,
    STRINGS.PTRIBE_PIG_TASK.WANT_TO_EAT_BERRIES.COND,
    { oinc = 2, dragonfruit = 1 }
)
