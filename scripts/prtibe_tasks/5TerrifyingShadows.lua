local PigTasks = require("ptribe_pigtasks")

local function LoadInitFn(self, target)
    if not target then
        target = self.pig.components.entitytracker:GetEntity("ptribe_5TerrifyingShadows_target")
        if not target then return end
    end

    target:ListenForEvent("healthdelta", function(target)
        local per = target.components.health:GetPercent()

        if (per < 0.66 and #target.levelupsource <= 0)
            or (per < 0.33 and #target.levelupsource <= 1 and TUNING.TRIBE_TASK_DIFFICULTY_LEVEL >= 2) then
            table.insert(target.levelupsource, "shadow_knight")
        end
    end)

    --对玩家友好点，这里就不过多强化了
end

local function OnInit(self, isLoading)
    if isLoading then
        LoadInitFn(self)
    else
        self.taskData.pos = PigTasks.SpawnPos(self.inst:GetPosition(), math.random(100, 200))
        if not self.taskData.pos then
            return false, STRINGS.PTRIBE_PIG_TASK.GENERIC.NOT_POS
        end

        if not self.inst.components.timer:TimerExists("ptribe_5TerrifyingShadows_coin") then
            -- 可能三阶段太难了，给几个铸币
            local pig_coin = SpawnPrefab("pig_coin")
            pig_coin.components.stackable.stacksize = 5
            self.inst.components.inventory:GiveItem(pig_coin)
            self.inst.components.timer:StartTimer("ptribe_5TerrifyingShadows_coin", TUNING.TOTAL_DAY_TIME * 2)
        end

        if self.pig.components.talker then
            self.pig.components.talker:Say(STRINGS.PTRIBE_PIG_TASK.TERRIFYING_SHADERS.GIVE_COIN)
        end
    end

    -- 强化一下
    self.inst.components.damagetypebonus:AddBonus("shadow_aligned", self.pig,
        3 - TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 0.5, "ptribe_5TerrifyingShadowsTask")
    self.inst.components.locomotor:SetExternalSpeedMultiplier(self.pig, "ptribe_5TerrifyingShadowsTask", 1.25)

    return true
end

local RANDOM_MONSTERS = {
    shadow_rook = 1,
    shadow_knight = 0.3, --骑士太难打
    shadow_bishop = 1,
}

local function GetTargetPos(self)
    return PigTasks.DefaultGetTargetPos(self, "ptribe_5TerrifyingShadows_target")
end

local function SpawnFn(self, pos)
    local target = SpawnAt(weighted_random_choice(RANDOM_MONSTERS), pos)
    self.pig.components.entitytracker:TrackEntity("ptribe_5TerrifyingShadows_target", target)
    target.components.combat:SetTarget(self.inst)

    LoadInitFn(self, target)
end

local function OnUpdate(self)
    return PigTasks.DefaultOnUpdate(self, SpawnFn)
end

local function FinishTask(self)
    return PigTasks.DefaultFinishTask(self,
        STRINGS.PTRIBE_PIG_TASK.TERRIFYING_SHADERS.FAIL.NOT_DEFEAT_SHADOW,
        STRINGS.PTRIBE_PIG_TASK.TERRIFYING_SHADERS.FAIL.NOT_DEFEAT_SHADOW,
        "ptribe_5TerrifyingShadows_target")
end

local function OnEnd(self)
    PigTasks.DefaultOnEnd(self)

    self.inst.components.damagetypebonus:RemoveBonus("shadow_aligned", self.pig, "ptribe_5TerrifyingShadowsTask")
    self.inst.components.locomotor:RemoveExternalSpeedMultiplier(self.pig, "ptribe_5TerrifyingShadowsTask")
end

PigTasks.AddTask(5,
    STRINGS.PTRIBE_PIG_TASK.TERRIFYING_SHADERS.NAME,
    STRINGS.PTRIBE_PIG_TASK.TERRIFYING_SHADERS.DESC,
    STRINGS.PTRIBE_PIG_TASK.TERRIFYING_SHADERS.COND,
    { oinc = 8, nightmarefuel = 10 },
    {
        time = TUNING.TOTAL_DAY_TIME * 3,
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
