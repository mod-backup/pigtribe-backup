local GetPrefab = require("ptribe_utils/getprefab")
local Shapes = require("ptribe_utils/shapes")
local PigTasks = require("ptribe_pigtasks")

local function RetargetFunction(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    for _, v in pairs(TheSim:FindEntities(x, y, z, 12)) do
        local tar = v.components.combat and v.components.combat.target
        if v:HasTag("monster") or (tar and (tar == inst or tar == inst.ptribe_cooperate)) then
            return v
        end
    end
end

local function Check(self, pig, merm)
    if not merm:IsValid() then
        if pig.prtibe_checkTask then
            pig.prtibe_checkTask:Cancel()
            pig.prtibe_checkTask = nil
        end
        pig.components.health:Kill()
        return
    end

    if RetargetFunction(pig) then return end

    self.taskData.taskFinished = true

    --留点礼物
    local x1, y1, z1 = pig.Transform:GetWorldPosition()
    local x2, y2, z2 = merm.Transform:GetWorldPosition()
    local x, z = (x1 + x2) / 2, (z1 + z2) / 2
    for i, p in ipairs(Shapes.GenerateHeartPoints(x, z, 0.4, 20)) do --这里也可以根据两者的距离改变爱心的大小
        TheWorld:DoTaskInTime(i * 0.5, function()
            SpawnPrefab("goldnugget").Transform:SetPosition(p[1], 0, p[2])
        end)
    end

    SpawnAt("small_puff", pig:GetPosition())
    pig:Remove()

    SpawnAt("small_puff", merm:GetPosition())
    merm:Remove()
end

local function LoadInitFn(self, pig, merm)
    if not pig then
        pig = self.pig.components.entitytracker:GetEntity("ptribe_4FallInto_pig")
    end

    if not merm then
        merm = self.pig.components.entitytracker:GetEntity("ptribe_4FallInto_merm")
    end

    --强化一下，不然没法完成任务
    if pig then
        pig.ptribe_cooperate = merm
        GetPrefab.SetMaxHealth(pig, 1600 - TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 300)
        pig.components.combat:SetAttackPeriod(0)
        pig.components.combat:SetDefaultDamage(TUNING.MERM_DAMAGE * 1.5)
        pig.components.combat:SetRetargetFunction(3, RetargetFunction)
        pig.prtibe_checkTask = pig:DoPeriodicTask(1, function() Check(self, pig, merm) end)
    end

    if merm then
        merm.ptribe_cooperate = pig
        GetPrefab.SetMaxHealth(pig, 1600 - TUNING.TRIBE_TASK_DIFFICULTY_LEVEL * 300)
        merm.components.combat:SetAttackPeriod(0)
        merm.components.combat:SetDefaultDamage(TUNING.PIG_DAMAGE * 1.5)
        merm.components.combat:SetRetargetFunction(3, RetargetFunction)
    end
end

local function OnInit(self, isLoading)
    return PigTasks.DefaultInit(self, isLoading, function()
        LoadInitFn(self)
    end)
end

local function GetTargetPos(self)
    if self.taskData.isSpawned then
        local pig = self.pig.components.entitytracker:GetEntity("ptribe_4FallInto_pig")
        local merm = self.pig.components.entitytracker:GetEntity("ptribe_4FallInto_merm")
        if pig and merm then
            return pig:GetPosition()
        else
            return self.pig:GetPosition()
        end
    else
        return self.taskData.pos
    end
end

local function SpawnFn(self, pos)
    pos = Vector3(TheWorld.Map:GetTileCenterPoint(pos:Get()))

    local pig = SpawnAt("pigman", Shapes.GetRandomLocation(pos, 0, 2))
    local merm = SpawnAt("merm", Shapes.GetRandomLocation(pos, 0, 2))
    self.pig.components.entitytracker:TrackEntity("ptribe_4FallInto_pig", pig)
    self.pig.components.entitytracker:TrackEntity("ptribe_4FallInto_merm", merm)
    self.taskData.survivalCount = 0

    --蜘蛛
    for i = 1, math.random(15, 20) do
        local spawnPos = PigTasks.SpawnPos(pos, math.random(4, 12))
        if spawnPos then
            local spider = SpawnAt("spider", spawnPos)
            spider.components.combat:SetTarget(math.random() < 0.5 and pig or merm)
        end
    end
    for i = 1, math.random(1, 3) do
        local spawnPos = PigTasks.SpawnPos(pos, math.random(4, 12))
        if spawnPos then
            SpawnAt(math.random() < 0.8 and "spiderden" or "spiderden_2", spawnPos)
        end
    end

    if TUNING.TRIBE_ALLOW_TASK_GENERATE_TILE then
        --沼泽地皮
        GetPrefab.SpawnRoundTile(pos, 3, WORLD_TILES.MARSH, false, 0.5)
    end

    LoadInitFn(self, pig, merm)
end

local DIS_SQ = 42 * 42
local function OnUpdate(self)
    PigTasks.DefaultOnUpdate(self, SpawnFn, DIS_SQ)
end

local function FinishTask(self)
    if not self.taskData.isSpawned then
        return false, STRINGS.PTRIBE_PIG_TASK.FALL_INTO.FAIL.IN_BATTLE
    end

    if self.taskData.taskFinished then return true end

    local pig = self.pig.components.entitytracker:GetEntity("ptribe_4FallInto_pig")
    local merm = self.pig.components.entitytracker:GetEntity("ptribe_4FallInto_merm")
    local count = (pig and 1 or 0) + (merm and 1 or 0)
    if count < 2 then
        self.inst:DoTaskInTime(0, function()
            self.pig.components.ptribe_pigtask.taskId:set(0)
        end)
        return false, STRINGS.PTRIBE_PIG_TASK.FALL_INTO.FAIL.DEATH
    else
        return false, STRINGS.PTRIBE_PIG_TASK.FALL_INTO.FAIL.IN_BATTLE
    end
end

local function OnEnd(self)
    if self.taskData.isSpawned then
        self.pig.components.entitytracker:ForgetEntity("ptribe_4FallInto_pig")
        self.pig.components.entitytracker:ForgetEntity("ptribe_4FallInto_merm")
    end
end

PigTasks.AddTask(4,
    STRINGS.PTRIBE_PIG_TASK.FALL_INTO.NAME,
    STRINGS.PTRIBE_PIG_TASK.FALL_INTO.DESC,
    STRINGS.PTRIBE_PIG_TASK.FALL_INTO.COND,
    { oinc = 10, tentaclespots = 5, poop = 10 },
    {
        time = TUNING.TOTAL_DAY_TIME * 3,
        OnInit = OnInit,
        GetTargetPos = GetTargetPos,
        OnUpdate = OnUpdate,
        FinishTask = FinishTask,
        OnEnd = OnEnd
    }
)
