local PigTasks = require("ptribe_pigtasks")

local function OnEnd(self, taskFinished, IsInValid)
    if taskFinished then
        --只能重新生成一顶蜂王冠戴上了
        local hat = SpawnPrefab("hivehat")
        self.pig.components.trader.onaccept(self.pig, self.inst, hat)
    end
end

PigTasks.AddTask(4,
    STRINGS.PTRIBE_PIG_TASK.BRAVEHEART.NAME,
    STRINGS.PTRIBE_PIG_TASK.BRAVEHEART.DESC,
    STRINGS.PTRIBE_PIG_TASK.BRAVEHEART.COND,
    { oinc = 10, rock_avocado_fruit_sprout = 4 },
    {
        OnEnd = OnEnd
    }
)
