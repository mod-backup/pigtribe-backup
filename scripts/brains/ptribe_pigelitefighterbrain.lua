require("behaviours/chaseandattackandavoid")
require("behaviours/leashandavoid")
require("behaviours/panicandavoid")
require("behaviours/standstill")
require("behaviours/wander")

local MIN_FOLLOW_DIST = 0
local TARGET_FOLLOW_DIST = 4
local MAX_FOLLOW_DIST = 6
local MAX_WANDER_DIST = 20

local PigEliteFighterBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local function GetLeader(inst)
    return inst.components.follower.leader
end

local function HasValidHome(inst)
    local home = inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
    return home ~= nil
        and home:IsValid()
        and not (home.components.burnable ~= nil and home.components.burnable:IsBurning())
        and not home:HasTag("burnt")
end

local function GetHomePos(inst)
    return HasValidHome(inst) and inst.components.homeseeker:GetHomePos()
end

local function GetNoLeaderHomePos(inst)
    if GetLeader(inst) then
        return nil
    end
    return GetHomePos(inst)
end

function PigEliteFighterBrain:OnStart()
    local root = PriorityNode(
        {
            WhileNode(function() return self.inst.sg:HasStateTag("jumping") end, "Standby",
                ActionNode(function() --[[do nothing]] end)),
            WhileNode(function() return self.inst.components.hauntable.panic end, "PanicHaunted",
                ChattyNode(self.inst, "PIG_TALK_PANICHAUNT",
                    Panic(self.inst))),
            WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire",
                ChattyNode(self.inst, "PIG_TALK_PANICFIRE",
                    Panic(self.inst))),
            -- 时间到了就离开
            WhileNode(function() return self.inst.ptribe_should_despawn end, "Standby",
                ParallelNode {
                    StandStill(self.inst),
                    LoopNode({ ActionNode(function() self.inst:PushEvent("despawn") end) }),
                }),

            ChaseAndAttack(self.inst),
            Follow(self.inst, GetLeader, MIN_FOLLOW_DIST, TARGET_FOLLOW_DIST, MAX_FOLLOW_DIST),
            -- 没有leader离开改为回家
            -- WhileNode(function() return GetLeader(self.inst) == nil end, "Standby",
            --     ParallelNode {
            --         StandStill(self.inst),
            --         LoopNode({ ActionNode(function() self.inst:PushEvent("despawn") end) }),
            --     }),
            --### 漫步
            Wander(self.inst, GetNoLeaderHomePos, MAX_WANDER_DIST)
        }, .5)

    self.bt = BT(self.inst, root)
end

return PigEliteFighterBrain
