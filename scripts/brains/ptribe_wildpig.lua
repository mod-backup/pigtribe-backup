require "behaviours/wander"
require "behaviours/follow"
require "behaviours/faceentity"
require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/doaction"
--require "behaviours/choptree"
require "behaviours/findlight"
require "behaviours/panic"
require "behaviours/chattynode"
require "behaviours/leash"

local BrainCommon = require "brains/braincommon"
local GetPrefab = require("ptribe_utils/getprefab")
local ResourceConvert = require("ptribe_resourceconvert")
local Utils = require("ptribe_utils/utils")
local Shapes = require("ptribe_utils/shapes")
local WildPigUtils = require("ptribe_wildpigutils")

local MAX_WANDER_DIST = 20

local LEASH_RETURN_DIST = 10
local LEASH_MAX_DIST = 30

local MIN_FOLLOW_DIST = 2
local TARGET_FOLLOW_DIST = 5
local MAX_FOLLOW_DIST = 9
local START_RUN_DIST = 3
local STOP_RUN_DIST = 5
local MAX_CHASE_TIME = 10
local MAX_CHASE_DIST = 30
local TRADE_DIST = 20
local SEE_FOOD_DIST = 10
local SEE_TREE_DIST = 15
local KEEP_CHOPPING_DIST = 10

local RUN_AWAY_DIST = 5
local STOP_RUN_AWAY_DIST = 8

local function GetTraderFn(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local players = FindPlayersInRange(x, y, z, TRADE_DIST, true)
    for i, v in ipairs(players) do
        if inst.components.trader:IsTryingToTradeWithMe(v) then
            return v
        end
    end
end

local function KeepTraderFn(inst, target)
    return inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GetLeader(inst)
    return inst.components.follower.leader
end

local function IsDeciduousTreeMonster(guy)
    return guy.monster and guy.prefab == "deciduoustree"
end

local CHOP_MUST_TAGS = { "CHOP_workable" }
local function FindDeciduousTreeMonster(inst)
    return FindEntity(inst, SEE_TREE_DIST / 3, IsDeciduousTreeMonster, CHOP_MUST_TAGS)
end

local function KeepChoppingAction(inst)
    return inst.tree_target ~= nil
        or (inst.components.follower.leader ~= nil and
            inst:IsNear(inst.components.follower.leader, KEEP_CHOPPING_DIST))
        or FindDeciduousTreeMonster(inst) ~= nil
end

local function StartChoppingCondition(inst)
    return inst.tree_target ~= nil
        or (inst.components.follower.leader ~= nil and
            inst.components.follower.leader.sg ~= nil and
            inst.components.follower.leader.sg:HasStateTag("chopping"))
        or FindDeciduousTreeMonster(inst) ~= nil
end

local function FindTreeToChopAction(inst)
    local target = FindEntity(inst, SEE_TREE_DIST, nil, CHOP_MUST_TAGS)
    if target ~= nil then
        if inst.tree_target ~= nil then
            target = inst.tree_target
            inst.tree_target = nil
        else
            target = FindDeciduousTreeMonster(inst) or target
        end
        return BufferedAction(inst, target, ACTIONS.CHOP)
    end
end

local FINDFOOD_CANT_TAGS = { "outofreach" }
local function FindFoodAction(inst)
    if inst.sg:HasStateTag("busy") then
        return
    end

    local target = inst.components.inventory:FindItem(function(item) return inst.components.eater:CanEat(item) end)
    if target ~= nil then
        return BufferedAction(inst, target, ACTIONS.EAT)
    end

    target = FindEntity(inst,
        SEE_FOOD_DIST,
        function(item)
            return item:GetTimeAlive() >= 8
                and item.components.edible ~= nil
                and item:IsOnPassablePoint()
                and inst.components.eater:CanEat(item)
        end,
        nil,
        FINDFOOD_CANT_TAGS
    )
    if target ~= nil then
        return BufferedAction(inst, target, ACTIONS.EAT)
    end

    target = FindEntity(inst,
        SEE_FOOD_DIST,
        function(item)
            return item.components.shelf ~= nil
                and item.components.shelf.itemonshelf ~= nil
                and item.components.shelf.cantakeitem
                and item.components.shelf.itemonshelf.components.edible ~= nil
                and item:IsOnPassablePoint()
                and inst.components.eater:CanEat(item.components.shelf.itemonshelf)
        end,
        nil,
        FINDFOOD_CANT_TAGS
    )
    if target ~= nil then
        return BufferedAction(inst, target, ACTIONS.TAKEITEM)
    end
end

local function GetNoLeaderHomePos(inst)
    if inst:HasTag("ptribe_wildpig_scavenger") then
        return nil
    end
    return inst.components.knownlocations:GetLocation("spawnpoint") or nil
end

local function GetFaceTargetNearestPlayerFn(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    return FindClosestPlayerInRange(x, y, z, START_RUN_DIST + 1, true)
end

local function KeepFaceTargetNearestPlayerFn(inst, target)
    return GetFaceTargetNearestPlayerFn(inst) == target
end

----------------------------------------------------------------------------------------------------
local MAX_ACTIVE_DIST = 18 --最大活动范围，小于brain中的wander范围

local function TrackEntity(inst, count, key, entity)
    for i = 1, count do
        local ent = inst.components.entitytracker:GetEntity(key .. i)
        if not ent then
            inst.components.entitytracker:TrackEntity(key .. i, entity)
            return
        end
    end
end

local function IsHasPlace(inst, count, key)
    for i = 1, count do
        local ent = inst.components.entitytracker:GetEntity(key .. i)
        if not ent then
            return i
        end
    end
end

--- 随机锅
local RANDOM_COOKPOTS = {
    "cookpot", "portablecookpot", "archive_cookpot"
}

local function OnNewTarget(inst)
    if not inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
        local weapon = SpawnPrefab("bomb_lunarplant")
        inst.components.inventory:GiveItem(weapon)
        inst.components.inventory:Equip(weapon, EQUIPSLOTS.HANDS)
    end
end

-- 玩家偷吃
local function HarvestBefore(self, harvester)
    if not harvester or not harvester:IsValid() or harvester:HasTag("pig") then return end

    WildPigUtils.PIGS.chef.GiveRandomBuff(harvester)

    local pig = self.inst.components.ptribe_wildpigbuild.pig
    if pig and pig:IsValid() then
        if pig.components.talker then
            pig.components.talker:Say(STRINGS.PTRIBE_PIG_CHEF_WARN_EATER
                [math.random(1, #STRINGS.PTRIBE_PIG_CHEF_WARN_EATER)])
            pig.sg:GoToState("abandon")
        end

        pig.ptribe_ragePoint = pig.ptribe_ragePoint + 1
        if pig.ptribe_ragePoint > 10 then
            -- 杀戮开始！
            pig.components.combat:SetTarget(harvester)

            OnNewTarget(pig)
            pig:ListenForEvent("newcombattarget", OnNewTarget)
            pig:ListenForEvent("unequip", function() pig:DoTaskInTime(0, OnNewTarget) end)
        end

        -- 重置
        if pig.ptribe_ragePointTask then
            pig.ptribe_ragePointTask:Cancel()
        end
        pig.ptribe_ragePointTask = pig:DoPeriodicTask(15, function(pig)
            pig.ptribe_ragePoint = pig.ptribe_ragePoint - 1
            if pig.ptribe_ragePoint <= 0 then
                pig.ptribe_ragePointTask:Cancel()
                pig.ptribe_ragePointTask = nil
            end
        end)
    end
end

local function OnPlacePotFinish(inst, pot)
    -- 锅初始化
    pot:AddComponent("ptribe_wildpigbuild"):Init(inst, 30,
        ResourceConvert.PrefabIsInteractive,
        ResourceConvert.PrefabInteract)

    Utils.FnDecorator(pot.components.stewer, "Harvest", HarvestBefore)
    TrackEntity(inst, TUNING.PTRIBE_WILD_PIG_POT_COUNT_MAX, "ptribe_wildpig_pot", pot)
end

--- 放置锅
local function PlacePot(inst)
    if IsHasPlace(inst, TUNING.PTRIBE_WILD_PIG_POT_COUNT_MAX, "ptribe_wildpig_pot") then
        local spawnPos = GetPrefab.GetSpawnPoint(GetNoLeaderHomePos(inst), math.random(1, 6), 4)
        if spawnPos then
            inst.ptribe_task = {
                id = 0,
                pos = spawnPos,
                prefab = RANDOM_COOKPOTS[math.random(1, #RANDOM_COOKPOTS)],
                type = "spawn",
                onFinish = OnPlacePotFinish
            }

            return BufferedAction(inst, nil, ACTIONS.PTRIBE_DOACTION, nil, spawnPos)
        end

        -- 没有合适的生成位置
    end
end

--- 做料理和收菜
local function DoCook(inst)
    for i = 1, TUNING.PTRIBE_WILD_PIG_POT_COUNT_MAX do
        local pot = inst.components.entitytracker:GetEntity("ptribe_wildpig_pot" .. i)
        if pot and pot.components.ptribe_wildpigbuild:CanInteract() then
            inst.ptribe_task = {
                id = 0,
                prefab = pot,
                type = "wildpig",
            }
            return BufferedAction(inst, pot, ACTIONS.PTRIBE_DOACTION)
        end
    end
end

----------------------------------------------------------------------------------------------------

local function OnSpawnSeedFinish(inst, plant)
    plant:AddComponent("ptribe_wildpigbuild"):Init(inst, 15,
        ResourceConvert.PrefabIsInteractive,
        ResourceConvert.PrefabInteract)

    local x, y, z = plant.Transform:GetWorldPosition()
    TheWorld.Map:CollapseSoilAtPoint(x, 0, z)
    plant.SoundEmitter:PlaySound("dontstarve/wilson/plant_seeds")

    TrackEntity(inst, TUNING.PTRIBE_WILD_PIG_SEED_COUNT_MAX, "ptribe_wildpig_seeds", plant)
end

--- 随机种子
local RANDOM_SEEDS = {}
local PLANT_DEFS = require("prefabs/farm_plant_defs").PLANT_DEFS
for _, data in pairs(PLANT_DEFS) do
    table.insert(RANDOM_SEEDS, data.prefab)
end

local function FarmerTillLand(inst)
    if IsHasPlace(inst, TUNING.PTRIBE_WILD_PIG_SEED_COUNT_MAX, "ptribe_wildpig_seeds") then
        local spawnPos = GetPrefab.GetSpawnPoint(GetNoLeaderHomePos(inst), math.random(1, 5), 4)
        if spawnPos then
            inst.ptribe_task = {
                id = 0,
                pos = spawnPos,
                prefab = RANDOM_SEEDS[math.random(1, #RANDOM_SEEDS)],
                type = "spawn",
                onFinish = OnSpawnSeedFinish
            }

            return BufferedAction(inst, nil, ACTIONS.PTRIBE_DOACTION, nil, spawnPos)
        end

        -- 没有合适的生成位置
    end
end

--- 施肥
local function FarmerFertilize(inst)
    for i = 1, TUNING.PTRIBE_WILD_PIG_SEED_COUNT_MAX do
        local seed = inst.components.entitytracker:GetEntity("ptribe_wildpig_seeds" .. i)
        if seed and seed.components.growable
            and seed.components.ptribe_wildpigbuild:CanInteract() then
            if seed.components.pickable and seed.components.pickable:CanBePicked() then
                return --放弃施肥，直接进入收获
            end

            inst.ptribe_task = {
                id = 0,
                prefab = seed,
                type = "wildpig",
            }
            return BufferedAction(inst, seed, ACTIONS.PTRIBE_DOACTION)
        end
    end
end

--- 收获
local function FarmerHarvest(inst)
    -- 砸开巨大化作物
    local oversized = FindEntity(inst, 20, function(ent)
        return ent:HasTag("weighable_OVERSIZEDVEGGIES") and ent.components.workable and
            ent.components.workable:GetWorkAction() == ACTIONS.HAMMER
    end)
    if oversized then
        return BufferedAction(inst, oversized, ACTIONS.HAMMER)
    end

    --已有的
    for i = 1, TUNING.PTRIBE_WILD_PIG_SEED_COUNT_MAX do
        local seed = inst.components.entitytracker:GetEntity("ptribe_wildpig_seeds" .. i)
        if seed and seed.components.ptribe_wildpigbuild:CanInteract()
            and seed.components.pickable and seed.components.pickable:CanBePicked() then
            inst.ptribe_task = {
                id = 0,
                prefab = seed,
                type = "wildpig",
            }
            return BufferedAction(inst, seed, ACTIONS.PTRIBE_DOACTION)
        end
    end
end

----------------------------------------------------------------------------------------------------
local WORK_ACTIONS = {
    [ACTIONS.CHOP] = true,
    [ACTIONS.HAMMER] = true,
    [ACTIONS.MINE] = true,
    [ACTIONS.DIG] = true,
}
local function GetTargetAction(target)
    if target:HasTag("irreplaceable") then return end
    -- 捡起
    local inventoryitem = target.components.inventoryitem
    if inventoryitem
        and inventoryitem.canbepickedup
        and not inventoryitem:IsHeld()
        and inventoryitem.cangoincontainer then
        return ACTIONS.PICKUP
    end

    if target.components.pickable and target:HasTag("pickable") then
        return ACTIONS.PICK
    end

    if (target.components.crop and target.components.crop:IsReadyForHarvest())
        or (target.components.harvestable and target.components.harvestable:CanBeHarvested())
        or (target.components.stewer and target.components.stewer:IsDone())
        or (target.components.dryer and not target.components.dryer:IsDrying() and target.components.dryer.product)
        or (target.components.occupiable and target.components.occupiable:IsOccupied())
    then
        return ACTIONS.HARVEST
    end

    -- 工作
    local act = target.components.workable and target.components.workable:CanBeWorked() and
        target.components.workable:GetWorkAction()
    if act and WORK_ACTIONS[act] and not target:HasTag("structure") then
        return act
    end
end

local FindWorkableTargetDis = { 8, 16, 24, 30, 60 }
-- 队长选择目标
local function CollectMaterials(inst)
    local leader = GetLeader(inst)
    if leader then
        -- 队员不会离队长太远
        local x, y, z = leader.Transform:GetWorldPosition()
        for _, v in ipairs(TheSim:FindEntities(x, y, z, 12)) do
            local act = GetTargetAction(v)
            if act then
                return BufferedAction(inst, v, act)
            end
        end

        return
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    for _, dis in ipairs(FindWorkableTargetDis) do
        for _, v in ipairs(TheSim:FindEntities(x, y, z, dis)) do
            local act = GetTargetAction(v)
            if act then
                return BufferedAction(inst, v, act)
            end
        end
    end

    for _, ent in pairs(Ents) do
        local act = GetTargetAction(ent)
        if act then
            return BufferedAction(inst, ent, act)
        end
    end
end

----------------------------------------------------------------------------------------------------

local PigBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

function PigBrain:OnStart()
    -- 厨子
    local root = PriorityNode({
        --恐惧
        BrainCommon.PanicWhenScared(self.inst, .25, "PIG_TALK_PANICBOSS"),
        --被作祟后执行Panic
        WhileNode(function() return self.inst.components.hauntable and self.inst.components.hauntable.panic end,
            "PanicHaunted",
            ChattyNode(self.inst, "PIG_TALK_PANICHAUNT",
                Panic(self.inst))),
        -- 受到火焰伤害后执行Panic
        WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire",
            ChattyNode(self.inst, "PIG_TALK_PANICFIRE",
                Panic(self.inst))),

        BrainCommon.IpecacsyrupPanicTrigger(self.inst),
        -- 追逐并攻击
        ChattyNode(self.inst, "PIG_TALK_FIGHT",
            WhileNode(
                function()
                    return self.inst.components.combat.target == nil or
                        not self.inst.components.combat:InCooldown()
                end, "AttackMomentarily",
                ChaseAndAttack(self.inst, MAX_CHASE_TIME, MAX_CHASE_DIST))),
        --猪人攻击后在冷却期间远离目标
        ChattyNode(self.inst, "PIG_TALK_FIGHT",
            WhileNode(
                function() return self.inst.components.combat.target and self.inst.components.combat:InCooldown() end,
                "Dodge",
                RunAway(self.inst, function() return self.inst.components.combat.target end, RUN_AWAY_DIST,
                    STOP_RUN_AWAY_DIST))),
        --有猪人想打自己就跑
        RunAway(self.inst,
            function(guy)
                return guy:HasTag("pig") and guy.components.combat and
                    guy.components.combat.target == self.inst
            end, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST),
        --有人要跟自己交易
        ChattyNode(self.inst, "PIG_TALK_ATTEMPT_TRADE",
            FaceEntity(self.inst, GetTraderFn, KeepTraderFn)),
        --猪找吃的
        ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
            DoAction(self.inst, FindFoodAction)),
        --生物追逐玩家跑出的领地的最大距离后，会先停在原地，然后等到下一次检测时，返回原本位置
        Leash(self.inst, GetNoLeaderHomePos, LEASH_MAX_DIST, LEASH_RETURN_DIST),
        --猪人不会离玩家太近
        ChattyNode(self.inst, "PIG_TALK_RUNAWAY_WILSON",
            RunAway(self.inst, "player", START_RUN_DIST, STOP_RUN_DIST)),
        --面朝玩家，时不时讲两句
        ChattyNode(self.inst, "PIG_TALK_LOOKATWILSON",
            FaceEntity(self.inst, GetFaceTargetNearestPlayerFn, KeepFaceTargetNearestPlayerFn)),
        --跟着领导者
        ChattyNode(self.inst, "PIG_TALK_FOLLOWWILSON",
            Follow(self.inst, GetLeader, 0, TARGET_FOLLOW_DIST, MAX_FOLLOW_DIST)),
        --------------------------------------------------------------------------------------------
        -- 厨子
        WhileNode(function()
                return self.inst:HasTag("ptribe_wildpig_chef")
                    and not self.inst.components.combat.target
            end, "Chef",
            PriorityNode {
                -- 放置锅
                ChattyNode(self.inst, "PTRIBE_PIG_CHEF_PLACE_POT",
                    DoAction(self.inst, PlacePot)),
                -- 做饭和采集
                ChattyNode(self.inst, "PTRIBE_PIG_CHEF_DO_COOK",
                    DoAction(self.inst, DoCook)),
            }),

        -- 农民
        WhileNode(function()
                return self.inst:HasTag("ptribe_wildpig_farmer")
                    and not self.inst.components.combat.target
            end, "Farmer",
            PriorityNode {
                -- 种地
                ChattyNode(self.inst, "PTRIBE_PIG_FARMER_PLACE", DoAction(self.inst, FarmerTillLand)),
                -- 施肥
                ChattyNode(self.inst, "PTRIBE_PIG_FARMER_FERTILIZE", DoAction(self.inst, FarmerFertilize)),
                -- 收获
                ChattyNode(self.inst, "PTRIBE_PIG_FARMER_HARVEST", DoAction(self.inst, FarmerHarvest)),
            }),

        -- 采集物资的猪人小队
        WhileNode(function()
                return self.inst:HasTag("ptribe_wildpig_scavenger")
                    and not self.inst.components.combat.target
            end, "Scavenger",
            PriorityNode {
                WhileNode(function() return not self.inst.components.inventory:IsFull() end, "Collect Materials",
                    ChattyNode(self.inst, "PTRIBE_PIG_SCAVENGER_COLLECT", DoAction(self.inst, CollectMaterials))),
            }),

        --------------------------------------------------------------------------------------------
        --漫步
        Wander(self.inst, GetNoLeaderHomePos, MAX_WANDER_DIST)
    }, .5)
    self.bt = BT(self.inst, root)
end

function PigBrain:OnInitializationComplete()
    self.inst.components.knownlocations:RememberLocation("spawnpoint", self.inst:GetPosition(), true)
end

return PigBrain
