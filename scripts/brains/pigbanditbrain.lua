require "behaviours/wander"
require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/doaction"
require "behaviours/panic"
require "behaviours/chattynode"

local MAX_CHASE_TIME = 10
local MAX_CHASE_DIST = 30
local SEE_FOOD_DIST = 10
local RUN_AWAY_DIST = 5
local STOP_RUN_AWAY_DIST = 8
local SEE_STOLEN_ITEM_DIST = 10
local MAX_WANDER_DIST = 20

local function FindRandomOffscreenPoint(inst)
    local OFFSET = 70

    local pt = inst:GetPosition()
    local theta = math.random() * 2 * PI
    local radius = OFFSET

    local offset = FindWalkableOffset(pt, theta, radius, 12, true) --12

    if offset then
        local newpt = pt + offset
        if TheWorld.Map:IsAboveGroundAtPoint(newpt:Get()) then
            return newpt
        end
    end
    return nil
end

-- 逃跑
local function GoHomeAction(inst)
    if not inst.components.homeseeker then return end

    if not inst.components.homeseeker:HasHome() then
        local position = FindRandomOffscreenPoint(inst)
        if position then
            inst.components.homeseeker:SetHome(SpawnPrefab("pigbanditexit")) --不可见对象
            inst.components.homeseeker.home.Transform:SetPosition(position.x, position.y, position.z)
        end
    end

    if inst.components.homeseeker:HasHome() then
        return BufferedAction(inst, inst.components.homeseeker.home, ACTIONS.GOHOME)
    end
end

-- 附近是否有钱
local function OincNearby(inst)
    return FindEntity(inst, SEE_STOLEN_ITEM_DIST,
        function(item)
            return item:HasTag("oinc") --小偷只偷钱
                and not item.components.inventoryitem:IsHeld()
                and item:IsOnValidGround()
        end)
end

local function PickupAction(inst)
    local target = OincNearby(inst)

    if target then
        return BufferedAction(inst, target, ACTIONS.PICKUP)
    end
end

local PigBanditBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local function GetPlayerPos(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local closestPlayer = FindClosestPlayerInRangeSq(x, y, z, 256, true)
    return closestPlayer and closestPlayer:GetPosition()
end

function PigBanditBrain:OnStart()
    local root =
        PriorityNode(
            {
                -- 被点燃时
                WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire",
                    ChattyNode(self.inst, STRINGS.PIG_TALK_PANICFIRE, Panic(self.inst))),
                -- 被攻击、偷钱后就逃跑
                WhileNode(
                    function()
                        --消失标志
                        if self.inst.isDisappear then return true end

                        self.inst.isDisappear = (self.inst.attacked or (self.inst.components.inventory:NumItems() > 0 and not OincNearby(self.inst)))
                            and not self.inst.sg:HasStateTag("busy")

                        return self.inst.isDisappear
                    end, "run off with prize",
                    DoAction(self.inst, GoHomeAction, "disappear", true)),
                -- 找钱
                DoAction(self.inst, PickupAction, "searching for prize", true),
                ChattyNode(self.inst, STRINGS.BANDIT_TALK_FIGHT,
                    WhileNode(
                        function()
                            return self.inst.components.combat.target == nil or
                                not self.inst.components.combat:InCooldown()
                        end, "AttackMomentarily",
                        ChaseAndAttack(self.inst, MAX_CHASE_TIME, MAX_CHASE_DIST))),
                -- 有猪人打自己就跑
                RunAway(self.inst,
                    function(guy)
                        return guy:HasTag("pig") and guy.components.combat and
                            guy.components.combat.target == self.inst
                    end,
                    RUN_AWAY_DIST, STOP_RUN_AWAY_DIST),
                Wander(self.inst, GetPlayerPos, MAX_WANDER_DIST)
            }, .5)

    self.bt = BT(self.inst, root)
end

return PigBanditBrain
